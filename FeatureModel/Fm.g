grammar Fm;

options {
	language = Java;
	output=AST;
}


tokens {
  RULE;
  FEATURE;
  GROUP;
  ASSIGN;
}

@header {
package ca.uwaterloo.gsd.fm.grammar;
}
@lexer::header {
package ca.uwaterloo.gsd.fm.grammar;
}


@lexer::members {
  @Override
  public void reportError(RecognitionException e) {
    Thrower.sneakyThrow(e);
  }

  /**
   * See "Puzzle 43: Exceptionally Unsafe" from Bloch Gafter, <i>Java Puzzlers</i>. Addison Wesley 2005.
   */
  static class Thrower {
    private static Throwable t;
    private Thrower() throws Throwable {
      throw t;
    }
    public static synchronized void sneakyThrow(Throwable t) {
      Thrower.t = t;
      try {
        Thrower.class.newInstance();
      } catch (InstantiationException e) {
        throw new IllegalArgumentException(e);
      } catch (IllegalAccessException e) {
        throw new IllegalArgumentException(e);
      } finally {
        Thrower.t = null; // Avoid memory leak
      }
    }
  }
}

@members {
  @Override
	protected Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow)
	    throws RecognitionException
	{
	    throw new MismatchedTokenException(ttype, input);
	}

	protected void mismatch(IntStream input, int ttype, BitSet follow)
	   throws RecognitionException
	{
	throw new MismatchedTokenException(ttype, input);
	}
	public Object recoverFromMismatchedSet(IntStream input, RecognitionException e, BitSet follow)
	    throws RecognitionException
	{
	  throw e;
	}
}

// Alter code generation so catch-clauses get replace with
// this action.
@rulecatch {
	catch (RecognitionException e) {
	 throw e;
	}
}




input
    : ( production ';'!
      | expr ';'!
      )+
    ;

production
    : IDENTIFIER ':' feature+
    -> ^(RULE IDENTIFIER feature+)
    ;

feature
    : IDENTIFIER '?'?
    -> ^(FEATURE IDENTIFIER '?'?)
    | '('! IDENTIFIER ('|'! IDENTIFIER)* ')'! (PLUS|OPT)?
    -> ^(GROUP IDENTIFIER+ PLUS? OPT?)
    | (name=IDENTIFIER '=')? '('! ids+=IDENTIFIER ('&'! ids+=IDENTIFIER)+ ')'!
    -> {$name == null}? ^(GROUP $ids+ '&')
    ->                   ^(GROUP $ids+ '&' ASSIGN[$name])
    ;

PLUS: '+';
OPT: '?';

expr
    : or_expr (expr)?
    ;

or_expr
    : and_expr ('|'^ and_expr)*
    ;

and_expr
    : impl_expr ('&'^ impl_expr)*
    ;

impl_expr
    : unary_expr ('->'^ unary_expr)*
    ;

unary_expr
    : '!'^ unary_expr
    | primary
    ;

primary
    : IDENTIFIER
    | '('! expr ')'!
    ;

IDENTIFIER: ('a'..'z'|'A'..'Z'|'0'..'9'|'_')+;

WS  :  (' '|'\r'|'\t'|'\u000C'|'\n')
    {  $channel = HIDDEN; }
    ;