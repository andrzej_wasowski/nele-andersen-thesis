package ca.uwaterloo.gsd.fm;

import java.util.ArrayList;
import java.util.Collection;

public class EdgeContainer<T> {

	private final Collection<FeatureNode<T>> _sources;
	private FeatureNode<T> _target;
	private int _type;
	
	public EdgeContainer(FeatureNode<T> source, FeatureNode<T> target) {
		_sources = new ArrayList<FeatureNode<T>>();
		_sources.add(source);
		_target = target;
		_type = FeatureEdge.OPTIONAL;
	}
	
	public EdgeContainer(Collection<FeatureNode<T>> sources, FeatureNode<T> target,
			int type) {
		_sources = sources;
		_target = target;
		_type = type;
	}
	
	public Collection<FeatureNode<T>> getSources() {
		return _sources;
	}
	public FeatureNode<T> getTarget() {
		return _target;
	}
	public int getType() {
		return _type;
	}
	
}
