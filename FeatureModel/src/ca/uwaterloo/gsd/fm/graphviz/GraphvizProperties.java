package ca.uwaterloo.gsd.fm.graphviz;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * This class is used to store arbitrary key/value attributes for vertices and
 * edges in a GraphvizGraph.
 * 
 * @author Steven She <shshe@uwaterloo.ca>
 * 
 * @param <V>
 * @param <E>
 */
public class GraphvizProperties<V, E> {

	private Map<V, Map<String, String>> mVertexProps = new HashMap<V, Map<String, String>>();
	private Map<E, Map<String, String>> mEdgeProps = new HashMap<E, Map<String, String>>();

	/**
	 * Should only be called by GraphvizGraph.
	 */
	protected String getEdgePropertiesString(E e) {
		Map<String, String> props = edgeProperties(e);
		if (props.size() == 0)
			return "";
		
		StringBuilder builder = new StringBuilder();
		for (Entry<String, String> entry : props.entrySet()) {
			builder.append(entry.getKey());
			builder.append("=");
			builder.append(entry.getValue());
			builder.append(",");
		}
		builder.deleteCharAt(builder.length() - 1);
		return builder.toString();
	}

	
	/**
	 * Should only be called by GraphvizGraph.
	 */
	protected String getVertexPropertiesString(V v) {
		Map<String, String> props = vertexProperties(v);
		if (props.size() == 0)
			return "";

		StringBuilder builder = new StringBuilder();
		for (Entry<String, String> entry : props.entrySet()) {
			builder.append(entry.getKey());
			builder.append("=");
			builder.append(entry.getValue());
			builder.append(",");
		}
		builder.deleteCharAt(builder.length() - 1);
		return builder.toString();
	}

	/**
	 * Can be overridden by a client.
	 */
	protected Map<String, String> vertexProperties(V v) {
		Map<String, String> props = mVertexProps.get(v);
		if (props == null) {
			props = new HashMap<String, String>();
			mVertexProps.put(v, props);
		}
		return props;
	}

	/**
	 * Can be overridden by a client.
	 */
	protected Map<String, String> edgeProperties(E e) {
		Map<String, String> props = mEdgeProps.get(e);
		if (props == null) {
			props = new HashMap<String, String>();
			mEdgeProps.put(e, props);
		}
		return props;		
	}
}
