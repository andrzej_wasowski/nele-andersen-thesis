package ca.uwaterloo.gsd.fm.graphviz;

/**
 * Implement this interface to provide a label for vertices or edges in a
 * GraphvizGraph.
 * 
 * @author Steven She <shshe@uwaterloo.ca>
 * 
 * @param <T> The vertex or edge type that this label provider will describe.
 */
public interface IGraphvizLabelProvider<T> {
	public String getLabel(T vertexOrEdge);
}
