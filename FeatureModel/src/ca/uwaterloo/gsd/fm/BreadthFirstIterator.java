package ca.uwaterloo.gsd.fm;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class BreadthFirstIterator<T> implements Iterator<FeatureEdge>{

	private final FeatureGraph<T> _g;
	private final Queue<FeatureEdge> _next;

	public BreadthFirstIterator(FeatureGraph<T> g) {
		_g = g;
		_next = new LinkedList<FeatureEdge>();
		if (g.vertices().size() > 0)
			for (FeatureEdge e : g.incomingEdges(g.root()))
				_next.add(e);
	}
	
	@Override
	public boolean hasNext() {
		return !_next.isEmpty();
	}

	@Override
	public FeatureEdge next() {
		FeatureEdge polled = _next.poll();
		for (FeatureNode<T> source : _g.getSources(polled)) {
			_next.addAll(_g.incomingEdges(source));
		}
		return polled;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

}
