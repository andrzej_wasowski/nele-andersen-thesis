// $ANTLR 3.1.1 /home/shshe/public/FeatureModel/Fm.g 2009-04-04 01:50:09

package ca.uwaterloo.gsd.fm.grammar;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;


import org.antlr.runtime.tree.*;

public class FmParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE", "FEATURE", "GROUP", "ASSIGN", "IDENTIFIER", "PLUS", "OPT", "WS", "';'", "':'", "'('", "'|'", "')'", "'='", "'&'", "'->'", "'!'"
    };
    public static final int RULE=4;
    public static final int T__20=20;
    public static final int OPT=10;
    public static final int EOF=-1;
    public static final int FEATURE=5;
    public static final int T__19=19;
    public static final int GROUP=6;
    public static final int WS=11;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int IDENTIFIER=8;
    public static final int ASSIGN=7;
    public static final int PLUS=9;

    // delegates
    // delegators


        public FmParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public FmParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        
    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    public String[] getTokenNames() { return FmParser.tokenNames; }
    public String getGrammarFileName() { return "/home/shshe/public/FeatureModel/Fm.g"; }


      @Override
    	protected Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow)
    	    throws RecognitionException
    	{
    	    throw new MismatchedTokenException(ttype, input);
    	}

    	protected void mismatch(IntStream input, int ttype, BitSet follow)
    	   throws RecognitionException
    	{
    	throw new MismatchedTokenException(ttype, input);
    	}
    	public Object recoverFromMismatchedSet(IntStream input, RecognitionException e, BitSet follow)
    	    throws RecognitionException
    	{
    	  throw e;
    	}


    public static class input_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "input"
    // /home/shshe/public/FeatureModel/Fm.g:84:1: input : ( production ';' | expr ';' )+ ;
    public final FmParser.input_return input() throws RecognitionException {
        FmParser.input_return retval = new FmParser.input_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal2=null;
        Token char_literal4=null;
        FmParser.production_return production1 = null;

        FmParser.expr_return expr3 = null;


        Object char_literal2_tree=null;
        Object char_literal4_tree=null;

        try {
            // /home/shshe/public/FeatureModel/Fm.g:85:5: ( ( production ';' | expr ';' )+ )
            // /home/shshe/public/FeatureModel/Fm.g:85:7: ( production ';' | expr ';' )+
            {
            root_0 = (Object)adaptor.nil();

            // /home/shshe/public/FeatureModel/Fm.g:85:7: ( production ';' | expr ';' )+
            int cnt1=0;
            loop1:
            do {
                int alt1=3;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==IDENTIFIER) ) {
                    int LA1_2 = input.LA(2);

                    if ( (LA1_2==13) ) {
                        alt1=1;
                    }
                    else if ( (LA1_2==IDENTIFIER||LA1_2==12||(LA1_2>=14 && LA1_2<=15)||(LA1_2>=18 && LA1_2<=20)) ) {
                        alt1=2;
                    }


                }
                else if ( (LA1_0==14||LA1_0==20) ) {
                    alt1=2;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/shshe/public/FeatureModel/Fm.g:85:9: production ';'
            	    {
            	    pushFollow(FOLLOW_production_in_input102);
            	    production1=production();

            	    state._fsp--;

            	    adaptor.addChild(root_0, production1.getTree());
            	    char_literal2=(Token)match(input,12,FOLLOW_12_in_input104); 

            	    }
            	    break;
            	case 2 :
            	    // /home/shshe/public/FeatureModel/Fm.g:86:9: expr ';'
            	    {
            	    pushFollow(FOLLOW_expr_in_input115);
            	    expr3=expr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, expr3.getTree());
            	    char_literal4=(Token)match(input,12,FOLLOW_12_in_input117); 

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        	 throw e;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end "input"

    public static class production_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "production"
    // /home/shshe/public/FeatureModel/Fm.g:90:1: production : IDENTIFIER ':' ( feature )+ -> ^( RULE IDENTIFIER ( feature )+ ) ;
    public final FmParser.production_return production() throws RecognitionException {
        FmParser.production_return retval = new FmParser.production_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token IDENTIFIER5=null;
        Token char_literal6=null;
        FmParser.feature_return feature7 = null;


        Object IDENTIFIER5_tree=null;
        Object char_literal6_tree=null;
        RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
        RewriteRuleTokenStream stream_13=new RewriteRuleTokenStream(adaptor,"token 13");
        RewriteRuleSubtreeStream stream_feature=new RewriteRuleSubtreeStream(adaptor,"rule feature");
        try {
            // /home/shshe/public/FeatureModel/Fm.g:91:5: ( IDENTIFIER ':' ( feature )+ -> ^( RULE IDENTIFIER ( feature )+ ) )
            // /home/shshe/public/FeatureModel/Fm.g:91:7: IDENTIFIER ':' ( feature )+
            {
            IDENTIFIER5=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_production144);  
            stream_IDENTIFIER.add(IDENTIFIER5);

            char_literal6=(Token)match(input,13,FOLLOW_13_in_production146);  
            stream_13.add(char_literal6);

            // /home/shshe/public/FeatureModel/Fm.g:91:22: ( feature )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==IDENTIFIER||LA2_0==14) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // /home/shshe/public/FeatureModel/Fm.g:91:22: feature
            	    {
            	    pushFollow(FOLLOW_feature_in_production148);
            	    feature7=feature();

            	    state._fsp--;

            	    stream_feature.add(feature7.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);



            // AST REWRITE
            // elements: feature, IDENTIFIER
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 92:5: -> ^( RULE IDENTIFIER ( feature )+ )
            {
                // /home/shshe/public/FeatureModel/Fm.g:92:8: ^( RULE IDENTIFIER ( feature )+ )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE, "RULE"), root_1);

                adaptor.addChild(root_1, stream_IDENTIFIER.nextNode());
                if ( !(stream_feature.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_feature.hasNext() ) {
                    adaptor.addChild(root_1, stream_feature.nextTree());

                }
                stream_feature.reset();

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        	 throw e;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end "production"

    public static class feature_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "feature"
    // /home/shshe/public/FeatureModel/Fm.g:95:1: feature : ( IDENTIFIER ( '?' )? -> ^( FEATURE IDENTIFIER ( '?' )? ) | '(' IDENTIFIER ( '|' IDENTIFIER )* ')' ( PLUS | OPT )? -> ^( GROUP ( IDENTIFIER )+ ( PLUS )? ( OPT )? ) | (name= IDENTIFIER '=' )? '(' ids+= IDENTIFIER ( '&' ids+= IDENTIFIER )+ ')' -> {$name == null}? ^( GROUP ( $ids)+ '&' ) -> ^( GROUP ( $ids)+ '&' ASSIGN[$name] ) );
    public final FmParser.feature_return feature() throws RecognitionException {
        FmParser.feature_return retval = new FmParser.feature_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token name=null;
        Token IDENTIFIER8=null;
        Token char_literal9=null;
        Token char_literal10=null;
        Token IDENTIFIER11=null;
        Token char_literal12=null;
        Token IDENTIFIER13=null;
        Token char_literal14=null;
        Token PLUS15=null;
        Token OPT16=null;
        Token char_literal17=null;
        Token char_literal18=null;
        Token char_literal19=null;
        Token char_literal20=null;
        Token ids=null;
        List list_ids=null;

        Object name_tree=null;
        Object IDENTIFIER8_tree=null;
        Object char_literal9_tree=null;
        Object char_literal10_tree=null;
        Object IDENTIFIER11_tree=null;
        Object char_literal12_tree=null;
        Object IDENTIFIER13_tree=null;
        Object char_literal14_tree=null;
        Object PLUS15_tree=null;
        Object OPT16_tree=null;
        Object char_literal17_tree=null;
        Object char_literal18_tree=null;
        Object char_literal19_tree=null;
        Object char_literal20_tree=null;
        Object ids_tree=null;
        RewriteRuleTokenStream stream_PLUS=new RewriteRuleTokenStream(adaptor,"token PLUS");
        RewriteRuleTokenStream stream_17=new RewriteRuleTokenStream(adaptor,"token 17");
        RewriteRuleTokenStream stream_18=new RewriteRuleTokenStream(adaptor,"token 18");
        RewriteRuleTokenStream stream_15=new RewriteRuleTokenStream(adaptor,"token 15");
        RewriteRuleTokenStream stream_16=new RewriteRuleTokenStream(adaptor,"token 16");
        RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
        RewriteRuleTokenStream stream_14=new RewriteRuleTokenStream(adaptor,"token 14");
        RewriteRuleTokenStream stream_OPT=new RewriteRuleTokenStream(adaptor,"token OPT");

        try {
            // /home/shshe/public/FeatureModel/Fm.g:96:5: ( IDENTIFIER ( '?' )? -> ^( FEATURE IDENTIFIER ( '?' )? ) | '(' IDENTIFIER ( '|' IDENTIFIER )* ')' ( PLUS | OPT )? -> ^( GROUP ( IDENTIFIER )+ ( PLUS )? ( OPT )? ) | (name= IDENTIFIER '=' )? '(' ids+= IDENTIFIER ( '&' ids+= IDENTIFIER )+ ')' -> {$name == null}? ^( GROUP ( $ids)+ '&' ) -> ^( GROUP ( $ids)+ '&' ASSIGN[$name] ) )
            int alt8=3;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==IDENTIFIER) ) {
                int LA8_1 = input.LA(2);

                if ( (LA8_1==17) ) {
                    alt8=3;
                }
                else if ( (LA8_1==IDENTIFIER||LA8_1==OPT||LA8_1==12||LA8_1==14) ) {
                    alt8=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA8_0==14) ) {
                int LA8_2 = input.LA(2);

                if ( (LA8_2==IDENTIFIER) ) {
                    int LA8_5 = input.LA(3);

                    if ( (LA8_5==18) ) {
                        alt8=3;
                    }
                    else if ( ((LA8_5>=15 && LA8_5<=16)) ) {
                        alt8=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 8, 5, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 2, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // /home/shshe/public/FeatureModel/Fm.g:96:7: IDENTIFIER ( '?' )?
                    {
                    IDENTIFIER8=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_feature181);  
                    stream_IDENTIFIER.add(IDENTIFIER8);

                    // /home/shshe/public/FeatureModel/Fm.g:96:18: ( '?' )?
                    int alt3=2;
                    int LA3_0 = input.LA(1);

                    if ( (LA3_0==OPT) ) {
                        alt3=1;
                    }
                    switch (alt3) {
                        case 1 :
                            // /home/shshe/public/FeatureModel/Fm.g:96:18: '?'
                            {
                            char_literal9=(Token)match(input,OPT,FOLLOW_OPT_in_feature183);  
                            stream_OPT.add(char_literal9);


                            }
                            break;

                    }



                    // AST REWRITE
                    // elements: OPT, IDENTIFIER
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 97:5: -> ^( FEATURE IDENTIFIER ( '?' )? )
                    {
                        // /home/shshe/public/FeatureModel/Fm.g:97:8: ^( FEATURE IDENTIFIER ( '?' )? )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(FEATURE, "FEATURE"), root_1);

                        adaptor.addChild(root_1, stream_IDENTIFIER.nextNode());
                        // /home/shshe/public/FeatureModel/Fm.g:97:29: ( '?' )?
                        if ( stream_OPT.hasNext() ) {
                            adaptor.addChild(root_1, stream_OPT.nextNode());

                        }
                        stream_OPT.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 2 :
                    // /home/shshe/public/FeatureModel/Fm.g:98:7: '(' IDENTIFIER ( '|' IDENTIFIER )* ')' ( PLUS | OPT )?
                    {
                    char_literal10=(Token)match(input,14,FOLLOW_14_in_feature207);  
                    stream_14.add(char_literal10);

                    IDENTIFIER11=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_feature210);  
                    stream_IDENTIFIER.add(IDENTIFIER11);

                    // /home/shshe/public/FeatureModel/Fm.g:98:23: ( '|' IDENTIFIER )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==15) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // /home/shshe/public/FeatureModel/Fm.g:98:24: '|' IDENTIFIER
                    	    {
                    	    char_literal12=(Token)match(input,15,FOLLOW_15_in_feature213);  
                    	    stream_15.add(char_literal12);

                    	    IDENTIFIER13=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_feature216);  
                    	    stream_IDENTIFIER.add(IDENTIFIER13);


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);

                    char_literal14=(Token)match(input,16,FOLLOW_16_in_feature220);  
                    stream_16.add(char_literal14);

                    // /home/shshe/public/FeatureModel/Fm.g:98:47: ( PLUS | OPT )?
                    int alt5=3;
                    int LA5_0 = input.LA(1);

                    if ( (LA5_0==PLUS) ) {
                        alt5=1;
                    }
                    else if ( (LA5_0==OPT) ) {
                        alt5=2;
                    }
                    switch (alt5) {
                        case 1 :
                            // /home/shshe/public/FeatureModel/Fm.g:98:48: PLUS
                            {
                            PLUS15=(Token)match(input,PLUS,FOLLOW_PLUS_in_feature224);  
                            stream_PLUS.add(PLUS15);


                            }
                            break;
                        case 2 :
                            // /home/shshe/public/FeatureModel/Fm.g:98:53: OPT
                            {
                            OPT16=(Token)match(input,OPT,FOLLOW_OPT_in_feature226);  
                            stream_OPT.add(OPT16);


                            }
                            break;

                    }



                    // AST REWRITE
                    // elements: PLUS, OPT, IDENTIFIER
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 99:5: -> ^( GROUP ( IDENTIFIER )+ ( PLUS )? ( OPT )? )
                    {
                        // /home/shshe/public/FeatureModel/Fm.g:99:8: ^( GROUP ( IDENTIFIER )+ ( PLUS )? ( OPT )? )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(GROUP, "GROUP"), root_1);

                        if ( !(stream_IDENTIFIER.hasNext()) ) {
                            throw new RewriteEarlyExitException();
                        }
                        while ( stream_IDENTIFIER.hasNext() ) {
                            adaptor.addChild(root_1, stream_IDENTIFIER.nextNode());

                        }
                        stream_IDENTIFIER.reset();
                        // /home/shshe/public/FeatureModel/Fm.g:99:28: ( PLUS )?
                        if ( stream_PLUS.hasNext() ) {
                            adaptor.addChild(root_1, stream_PLUS.nextNode());

                        }
                        stream_PLUS.reset();
                        // /home/shshe/public/FeatureModel/Fm.g:99:34: ( OPT )?
                        if ( stream_OPT.hasNext() ) {
                            adaptor.addChild(root_1, stream_OPT.nextNode());

                        }
                        stream_OPT.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 3 :
                    // /home/shshe/public/FeatureModel/Fm.g:100:7: (name= IDENTIFIER '=' )? '(' ids+= IDENTIFIER ( '&' ids+= IDENTIFIER )+ ')'
                    {
                    // /home/shshe/public/FeatureModel/Fm.g:100:7: (name= IDENTIFIER '=' )?
                    int alt6=2;
                    int LA6_0 = input.LA(1);

                    if ( (LA6_0==IDENTIFIER) ) {
                        alt6=1;
                    }
                    switch (alt6) {
                        case 1 :
                            // /home/shshe/public/FeatureModel/Fm.g:100:8: name= IDENTIFIER '='
                            {
                            name=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_feature258);  
                            stream_IDENTIFIER.add(name);

                            char_literal17=(Token)match(input,17,FOLLOW_17_in_feature260);  
                            stream_17.add(char_literal17);


                            }
                            break;

                    }

                    char_literal18=(Token)match(input,14,FOLLOW_14_in_feature264);  
                    stream_14.add(char_literal18);

                    ids=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_feature269);  
                    stream_IDENTIFIER.add(ids);

                    if (list_ids==null) list_ids=new ArrayList();
                    list_ids.add(ids);

                    // /home/shshe/public/FeatureModel/Fm.g:100:51: ( '&' ids+= IDENTIFIER )+
                    int cnt7=0;
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0==18) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // /home/shshe/public/FeatureModel/Fm.g:100:52: '&' ids+= IDENTIFIER
                    	    {
                    	    char_literal19=(Token)match(input,18,FOLLOW_18_in_feature272);  
                    	    stream_18.add(char_literal19);

                    	    ids=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_feature277);  
                    	    stream_IDENTIFIER.add(ids);

                    	    if (list_ids==null) list_ids=new ArrayList();
                    	    list_ids.add(ids);


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt7 >= 1 ) break loop7;
                                EarlyExitException eee =
                                    new EarlyExitException(7, input);
                                throw eee;
                        }
                        cnt7++;
                    } while (true);

                    char_literal20=(Token)match(input,16,FOLLOW_16_in_feature281);  
                    stream_16.add(char_literal20);



                    // AST REWRITE
                    // elements: ids, ids, 18, 18
                    // token labels: 
                    // rule labels: retval
                    // token list labels: ids
                    // rule list labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_ids=new RewriteRuleTokenStream(adaptor,"token ids", list_ids);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"token retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 101:5: -> {$name == null}? ^( GROUP ( $ids)+ '&' )
                    if (name == null) {
                        // /home/shshe/public/FeatureModel/Fm.g:101:25: ^( GROUP ( $ids)+ '&' )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(GROUP, "GROUP"), root_1);

                        if ( !(stream_ids.hasNext()) ) {
                            throw new RewriteEarlyExitException();
                        }
                        while ( stream_ids.hasNext() ) {
                            adaptor.addChild(root_1, stream_ids.nextNode());

                        }
                        stream_ids.reset();
                        adaptor.addChild(root_1, stream_18.nextNode());

                        adaptor.addChild(root_0, root_1);
                        }

                    }
                    else // 102:5: -> ^( GROUP ( $ids)+ '&' ASSIGN[$name] )
                    {
                        // /home/shshe/public/FeatureModel/Fm.g:102:26: ^( GROUP ( $ids)+ '&' ASSIGN[$name] )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(GROUP, "GROUP"), root_1);

                        if ( !(stream_ids.hasNext()) ) {
                            throw new RewriteEarlyExitException();
                        }
                        while ( stream_ids.hasNext() ) {
                            adaptor.addChild(root_1, stream_ids.nextNode());

                        }
                        stream_ids.reset();
                        adaptor.addChild(root_1, stream_18.nextNode());
                        adaptor.addChild(root_1, (Object)adaptor.create(ASSIGN, name));

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        	 throw e;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end "feature"

    public static class expr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "expr"
    // /home/shshe/public/FeatureModel/Fm.g:108:1: expr : or_expr ( expr )? ;
    public final FmParser.expr_return expr() throws RecognitionException {
        FmParser.expr_return retval = new FmParser.expr_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        FmParser.or_expr_return or_expr21 = null;

        FmParser.expr_return expr22 = null;



        try {
            // /home/shshe/public/FeatureModel/Fm.g:109:5: ( or_expr ( expr )? )
            // /home/shshe/public/FeatureModel/Fm.g:109:7: or_expr ( expr )?
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_or_expr_in_expr367);
            or_expr21=or_expr();

            state._fsp--;

            adaptor.addChild(root_0, or_expr21.getTree());
            // /home/shshe/public/FeatureModel/Fm.g:109:15: ( expr )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==IDENTIFIER||LA9_0==14||LA9_0==20) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // /home/shshe/public/FeatureModel/Fm.g:109:16: expr
                    {
                    pushFollow(FOLLOW_expr_in_expr370);
                    expr22=expr();

                    state._fsp--;

                    adaptor.addChild(root_0, expr22.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        	 throw e;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end "expr"

    public static class or_expr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "or_expr"
    // /home/shshe/public/FeatureModel/Fm.g:112:1: or_expr : and_expr ( '|' and_expr )* ;
    public final FmParser.or_expr_return or_expr() throws RecognitionException {
        FmParser.or_expr_return retval = new FmParser.or_expr_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal24=null;
        FmParser.and_expr_return and_expr23 = null;

        FmParser.and_expr_return and_expr25 = null;


        Object char_literal24_tree=null;

        try {
            // /home/shshe/public/FeatureModel/Fm.g:113:5: ( and_expr ( '|' and_expr )* )
            // /home/shshe/public/FeatureModel/Fm.g:113:7: and_expr ( '|' and_expr )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_and_expr_in_or_expr389);
            and_expr23=and_expr();

            state._fsp--;

            adaptor.addChild(root_0, and_expr23.getTree());
            // /home/shshe/public/FeatureModel/Fm.g:113:16: ( '|' and_expr )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==15) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // /home/shshe/public/FeatureModel/Fm.g:113:17: '|' and_expr
            	    {
            	    char_literal24=(Token)match(input,15,FOLLOW_15_in_or_expr392); 
            	    char_literal24_tree = (Object)adaptor.create(char_literal24);
            	    root_0 = (Object)adaptor.becomeRoot(char_literal24_tree, root_0);

            	    pushFollow(FOLLOW_and_expr_in_or_expr395);
            	    and_expr25=and_expr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, and_expr25.getTree());

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        	 throw e;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end "or_expr"

    public static class and_expr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "and_expr"
    // /home/shshe/public/FeatureModel/Fm.g:116:1: and_expr : impl_expr ( '&' impl_expr )* ;
    public final FmParser.and_expr_return and_expr() throws RecognitionException {
        FmParser.and_expr_return retval = new FmParser.and_expr_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal27=null;
        FmParser.impl_expr_return impl_expr26 = null;

        FmParser.impl_expr_return impl_expr28 = null;


        Object char_literal27_tree=null;

        try {
            // /home/shshe/public/FeatureModel/Fm.g:117:5: ( impl_expr ( '&' impl_expr )* )
            // /home/shshe/public/FeatureModel/Fm.g:117:7: impl_expr ( '&' impl_expr )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_impl_expr_in_and_expr414);
            impl_expr26=impl_expr();

            state._fsp--;

            adaptor.addChild(root_0, impl_expr26.getTree());
            // /home/shshe/public/FeatureModel/Fm.g:117:17: ( '&' impl_expr )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==18) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // /home/shshe/public/FeatureModel/Fm.g:117:18: '&' impl_expr
            	    {
            	    char_literal27=(Token)match(input,18,FOLLOW_18_in_and_expr417); 
            	    char_literal27_tree = (Object)adaptor.create(char_literal27);
            	    root_0 = (Object)adaptor.becomeRoot(char_literal27_tree, root_0);

            	    pushFollow(FOLLOW_impl_expr_in_and_expr420);
            	    impl_expr28=impl_expr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, impl_expr28.getTree());

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        	 throw e;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end "and_expr"

    public static class impl_expr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "impl_expr"
    // /home/shshe/public/FeatureModel/Fm.g:120:1: impl_expr : unary_expr ( '->' unary_expr )* ;
    public final FmParser.impl_expr_return impl_expr() throws RecognitionException {
        FmParser.impl_expr_return retval = new FmParser.impl_expr_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal30=null;
        FmParser.unary_expr_return unary_expr29 = null;

        FmParser.unary_expr_return unary_expr31 = null;


        Object string_literal30_tree=null;

        try {
            // /home/shshe/public/FeatureModel/Fm.g:121:5: ( unary_expr ( '->' unary_expr )* )
            // /home/shshe/public/FeatureModel/Fm.g:121:7: unary_expr ( '->' unary_expr )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_unary_expr_in_impl_expr439);
            unary_expr29=unary_expr();

            state._fsp--;

            adaptor.addChild(root_0, unary_expr29.getTree());
            // /home/shshe/public/FeatureModel/Fm.g:121:18: ( '->' unary_expr )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==19) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // /home/shshe/public/FeatureModel/Fm.g:121:19: '->' unary_expr
            	    {
            	    string_literal30=(Token)match(input,19,FOLLOW_19_in_impl_expr442); 
            	    string_literal30_tree = (Object)adaptor.create(string_literal30);
            	    root_0 = (Object)adaptor.becomeRoot(string_literal30_tree, root_0);

            	    pushFollow(FOLLOW_unary_expr_in_impl_expr445);
            	    unary_expr31=unary_expr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, unary_expr31.getTree());

            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        	 throw e;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end "impl_expr"

    public static class unary_expr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "unary_expr"
    // /home/shshe/public/FeatureModel/Fm.g:124:1: unary_expr : ( '!' unary_expr | primary );
    public final FmParser.unary_expr_return unary_expr() throws RecognitionException {
        FmParser.unary_expr_return retval = new FmParser.unary_expr_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal32=null;
        FmParser.unary_expr_return unary_expr33 = null;

        FmParser.primary_return primary34 = null;


        Object char_literal32_tree=null;

        try {
            // /home/shshe/public/FeatureModel/Fm.g:125:5: ( '!' unary_expr | primary )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==20) ) {
                alt13=1;
            }
            else if ( (LA13_0==IDENTIFIER||LA13_0==14) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // /home/shshe/public/FeatureModel/Fm.g:125:7: '!' unary_expr
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal32=(Token)match(input,20,FOLLOW_20_in_unary_expr464); 
                    char_literal32_tree = (Object)adaptor.create(char_literal32);
                    root_0 = (Object)adaptor.becomeRoot(char_literal32_tree, root_0);

                    pushFollow(FOLLOW_unary_expr_in_unary_expr467);
                    unary_expr33=unary_expr();

                    state._fsp--;

                    adaptor.addChild(root_0, unary_expr33.getTree());

                    }
                    break;
                case 2 :
                    // /home/shshe/public/FeatureModel/Fm.g:126:7: primary
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_primary_in_unary_expr475);
                    primary34=primary();

                    state._fsp--;

                    adaptor.addChild(root_0, primary34.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        	 throw e;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end "unary_expr"

    public static class primary_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "primary"
    // /home/shshe/public/FeatureModel/Fm.g:129:1: primary : ( IDENTIFIER | '(' expr ')' );
    public final FmParser.primary_return primary() throws RecognitionException {
        FmParser.primary_return retval = new FmParser.primary_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token IDENTIFIER35=null;
        Token char_literal36=null;
        Token char_literal38=null;
        FmParser.expr_return expr37 = null;


        Object IDENTIFIER35_tree=null;
        Object char_literal36_tree=null;
        Object char_literal38_tree=null;

        try {
            // /home/shshe/public/FeatureModel/Fm.g:130:5: ( IDENTIFIER | '(' expr ')' )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==IDENTIFIER) ) {
                alt14=1;
            }
            else if ( (LA14_0==14) ) {
                alt14=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // /home/shshe/public/FeatureModel/Fm.g:130:7: IDENTIFIER
                    {
                    root_0 = (Object)adaptor.nil();

                    IDENTIFIER35=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_primary492); 
                    IDENTIFIER35_tree = (Object)adaptor.create(IDENTIFIER35);
                    adaptor.addChild(root_0, IDENTIFIER35_tree);


                    }
                    break;
                case 2 :
                    // /home/shshe/public/FeatureModel/Fm.g:131:7: '(' expr ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal36=(Token)match(input,14,FOLLOW_14_in_primary500); 
                    pushFollow(FOLLOW_expr_in_primary503);
                    expr37=expr();

                    state._fsp--;

                    adaptor.addChild(root_0, expr37.getTree());
                    char_literal38=(Token)match(input,16,FOLLOW_16_in_primary505); 

                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) {
        	 throw e;
        	}
        finally {
        }
        return retval;
    }
    // $ANTLR end "primary"

    // Delegated rules


 

    public static final BitSet FOLLOW_production_in_input102 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_input104 = new BitSet(new long[]{0x0000000000104102L});
    public static final BitSet FOLLOW_expr_in_input115 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_input117 = new BitSet(new long[]{0x0000000000105102L});
    public static final BitSet FOLLOW_IDENTIFIER_in_production144 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_production146 = new BitSet(new long[]{0x0000000000004100L});
    public static final BitSet FOLLOW_feature_in_production148 = new BitSet(new long[]{0x0000000000004102L});
    public static final BitSet FOLLOW_IDENTIFIER_in_feature181 = new BitSet(new long[]{0x0000000000000402L});
    public static final BitSet FOLLOW_OPT_in_feature183 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_feature207 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_IDENTIFIER_in_feature210 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_15_in_feature213 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_IDENTIFIER_in_feature216 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_16_in_feature220 = new BitSet(new long[]{0x0000000000000602L});
    public static final BitSet FOLLOW_PLUS_in_feature224 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OPT_in_feature226 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_feature258 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_feature260 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_feature264 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_IDENTIFIER_in_feature269 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_feature272 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_IDENTIFIER_in_feature277 = new BitSet(new long[]{0x0000000000050000L});
    public static final BitSet FOLLOW_16_in_feature281 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_or_expr_in_expr367 = new BitSet(new long[]{0x0000000000104100L});
    public static final BitSet FOLLOW_expr_in_expr370 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_and_expr_in_or_expr389 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_15_in_or_expr392 = new BitSet(new long[]{0x000000000010C100L});
    public static final BitSet FOLLOW_and_expr_in_or_expr395 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_impl_expr_in_and_expr414 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_18_in_and_expr417 = new BitSet(new long[]{0x0000000000144100L});
    public static final BitSet FOLLOW_impl_expr_in_and_expr420 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_unary_expr_in_impl_expr439 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_19_in_impl_expr442 = new BitSet(new long[]{0x0000000000184100L});
    public static final BitSet FOLLOW_unary_expr_in_impl_expr445 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_20_in_unary_expr464 = new BitSet(new long[]{0x0000000000104100L});
    public static final BitSet FOLLOW_unary_expr_in_unary_expr467 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_primary_in_unary_expr475 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_primary492 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_primary500 = new BitSet(new long[]{0x0000000000114100L});
    public static final BitSet FOLLOW_expr_in_primary503 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_primary505 = new BitSet(new long[]{0x0000000000000002L});

}