// $ANTLR 3.1.1 /home/shshe/public/FeatureModel/Fm.g 2009-04-04 19:06:53

package ca.uwaterloo.gsd.fm.grammar;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class FmLexer extends Lexer {
    public static final int RULE=4;
    public static final int OPT=10;
    public static final int T__20=20;
    public static final int EOF=-1;
    public static final int FEATURE=5;
    public static final int T__19=19;
    public static final int GROUP=6;
    public static final int WS=11;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int IDENTIFIER=8;
    public static final int ASSIGN=7;
    public static final int PLUS=9;

      @Override
      public void reportError(RecognitionException e) {
        Thrower.sneakyThrow(e);
      }

      /**
       * See "Puzzle 43: Exceptionally Unsafe" from Bloch Gafter, <i>Java Puzzlers</i>. Addison Wesley 2005.
       */
      static class Thrower {
        private static Throwable t;
        private Thrower() throws Throwable {
          throw t;
        }
        public static synchronized void sneakyThrow(Throwable t) {
          Thrower.t = t;
          try {
            Thrower.class.newInstance();
          } catch (InstantiationException e) {
            throw new IllegalArgumentException(e);
          } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
          } finally {
            Thrower.t = null; // Avoid memory leak
          }
        }
      }


    // delegates
    // delegators

    public FmLexer() {;} 
    public FmLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public FmLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "/home/shshe/public/FeatureModel/Fm.g"; }

    // $ANTLR start "T__12"
    public final void mT__12() throws RecognitionException {
        try {
            int _type = T__12;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/shshe/public/FeatureModel/Fm.g:39:7: ( ';' )
            // /home/shshe/public/FeatureModel/Fm.g:39:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__12"

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/shshe/public/FeatureModel/Fm.g:40:7: ( ':' )
            // /home/shshe/public/FeatureModel/Fm.g:40:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/shshe/public/FeatureModel/Fm.g:41:7: ( '(' )
            // /home/shshe/public/FeatureModel/Fm.g:41:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/shshe/public/FeatureModel/Fm.g:42:7: ( '|' )
            // /home/shshe/public/FeatureModel/Fm.g:42:9: '|'
            {
            match('|'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/shshe/public/FeatureModel/Fm.g:43:7: ( ')' )
            // /home/shshe/public/FeatureModel/Fm.g:43:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/shshe/public/FeatureModel/Fm.g:44:7: ( '=' )
            // /home/shshe/public/FeatureModel/Fm.g:44:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/shshe/public/FeatureModel/Fm.g:45:7: ( '&' )
            // /home/shshe/public/FeatureModel/Fm.g:45:9: '&'
            {
            match('&'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/shshe/public/FeatureModel/Fm.g:46:7: ( '->' )
            // /home/shshe/public/FeatureModel/Fm.g:46:9: '->'
            {
            match("->"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/shshe/public/FeatureModel/Fm.g:47:7: ( '!' )
            // /home/shshe/public/FeatureModel/Fm.g:47:9: '!'
            {
            match('!'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "PLUS"
    public final void mPLUS() throws RecognitionException {
        try {
            int _type = PLUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/shshe/public/FeatureModel/Fm.g:105:5: ( '+' )
            // /home/shshe/public/FeatureModel/Fm.g:105:7: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "PLUS"

    // $ANTLR start "OPT"
    public final void mOPT() throws RecognitionException {
        try {
            int _type = OPT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/shshe/public/FeatureModel/Fm.g:106:4: ( '?' )
            // /home/shshe/public/FeatureModel/Fm.g:106:6: '?'
            {
            match('?'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "OPT"

    // $ANTLR start "IDENTIFIER"
    public final void mIDENTIFIER() throws RecognitionException {
        try {
            int _type = IDENTIFIER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/shshe/public/FeatureModel/Fm.g:134:11: ( ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )+ )
            // /home/shshe/public/FeatureModel/Fm.g:134:13: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )+
            {
            // /home/shshe/public/FeatureModel/Fm.g:134:13: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>='0' && LA1_0<='9')||(LA1_0>='A' && LA1_0<='Z')||LA1_0=='_'||(LA1_0>='a' && LA1_0<='z')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/shshe/public/FeatureModel/Fm.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "IDENTIFIER"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/shshe/public/FeatureModel/Fm.g:136:5: ( ( ' ' | '\\r' | '\\t' | '\\u000C' | '\\n' ) )
            // /home/shshe/public/FeatureModel/Fm.g:136:8: ( ' ' | '\\r' | '\\t' | '\\u000C' | '\\n' )
            {
            if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||(input.LA(1)>='\f' && input.LA(1)<='\r')||input.LA(1)==' ' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

              _channel = HIDDEN; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "WS"

    public void mTokens() throws RecognitionException {
        // /home/shshe/public/FeatureModel/Fm.g:1:8: ( T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | PLUS | OPT | IDENTIFIER | WS )
        int alt2=13;
        switch ( input.LA(1) ) {
        case ';':
            {
            alt2=1;
            }
            break;
        case ':':
            {
            alt2=2;
            }
            break;
        case '(':
            {
            alt2=3;
            }
            break;
        case '|':
            {
            alt2=4;
            }
            break;
        case ')':
            {
            alt2=5;
            }
            break;
        case '=':
            {
            alt2=6;
            }
            break;
        case '&':
            {
            alt2=7;
            }
            break;
        case '-':
            {
            alt2=8;
            }
            break;
        case '!':
            {
            alt2=9;
            }
            break;
        case '+':
            {
            alt2=10;
            }
            break;
        case '?':
            {
            alt2=11;
            }
            break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case 'A':
        case 'B':
        case 'C':
        case 'D':
        case 'E':
        case 'F':
        case 'G':
        case 'H':
        case 'I':
        case 'J':
        case 'K':
        case 'L':
        case 'M':
        case 'N':
        case 'O':
        case 'P':
        case 'Q':
        case 'R':
        case 'S':
        case 'T':
        case 'U':
        case 'V':
        case 'W':
        case 'X':
        case 'Y':
        case 'Z':
        case '_':
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
        case 'g':
        case 'h':
        case 'i':
        case 'j':
        case 'k':
        case 'l':
        case 'm':
        case 'n':
        case 'o':
        case 'p':
        case 'q':
        case 'r':
        case 's':
        case 't':
        case 'u':
        case 'v':
        case 'w':
        case 'x':
        case 'y':
        case 'z':
            {
            alt2=12;
            }
            break;
        case '\t':
        case '\n':
        case '\f':
        case '\r':
        case ' ':
            {
            alt2=13;
            }
            break;
        default:
            NoViableAltException nvae =
                new NoViableAltException("", 2, 0, input);

            throw nvae;
        }

        switch (alt2) {
            case 1 :
                // /home/shshe/public/FeatureModel/Fm.g:1:10: T__12
                {
                mT__12(); 

                }
                break;
            case 2 :
                // /home/shshe/public/FeatureModel/Fm.g:1:16: T__13
                {
                mT__13(); 

                }
                break;
            case 3 :
                // /home/shshe/public/FeatureModel/Fm.g:1:22: T__14
                {
                mT__14(); 

                }
                break;
            case 4 :
                // /home/shshe/public/FeatureModel/Fm.g:1:28: T__15
                {
                mT__15(); 

                }
                break;
            case 5 :
                // /home/shshe/public/FeatureModel/Fm.g:1:34: T__16
                {
                mT__16(); 

                }
                break;
            case 6 :
                // /home/shshe/public/FeatureModel/Fm.g:1:40: T__17
                {
                mT__17(); 

                }
                break;
            case 7 :
                // /home/shshe/public/FeatureModel/Fm.g:1:46: T__18
                {
                mT__18(); 

                }
                break;
            case 8 :
                // /home/shshe/public/FeatureModel/Fm.g:1:52: T__19
                {
                mT__19(); 

                }
                break;
            case 9 :
                // /home/shshe/public/FeatureModel/Fm.g:1:58: T__20
                {
                mT__20(); 

                }
                break;
            case 10 :
                // /home/shshe/public/FeatureModel/Fm.g:1:64: PLUS
                {
                mPLUS(); 

                }
                break;
            case 11 :
                // /home/shshe/public/FeatureModel/Fm.g:1:69: OPT
                {
                mOPT(); 

                }
                break;
            case 12 :
                // /home/shshe/public/FeatureModel/Fm.g:1:73: IDENTIFIER
                {
                mIDENTIFIER(); 

                }
                break;
            case 13 :
                // /home/shshe/public/FeatureModel/Fm.g:1:84: WS
                {
                mWS(); 

                }
                break;

        }

    }


 

}