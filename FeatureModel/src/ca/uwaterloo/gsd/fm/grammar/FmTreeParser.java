// $ANTLR 3.1.1 /home/shshe/public/FeatureModel/FmTreeParser.g 2009-04-04 19:06:58

  package ca.uwaterloo.gsd.fm.grammar;
  import ca.uwaterloo.gsd.fm.*;
  import java.util.List;
  import java.util.ArrayList;
  import java.util.Collection;
  import java.util.Map;
  import java.util.HashMap;
  import java.util.Set;
  import java.util.HashSet;


import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class FmTreeParser extends TreeParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE", "FEATURE", "GROUP", "ASSIGN", "IDENTIFIER", "PLUS", "OPT", "WS", "';'", "':'", "'('", "'|'", "')'", "'='", "'&'", "'->'", "'!'"
    };
    public static final int RULE=4;
    public static final int OPT=10;
    public static final int T__20=20;
    public static final int EOF=-1;
    public static final int FEATURE=5;
    public static final int T__19=19;
    public static final int GROUP=6;
    public static final int WS=11;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int IDENTIFIER=8;
    public static final int ASSIGN=7;
    public static final int PLUS=9;

    // delegates
    // delegators


        public FmTreeParser(TreeNodeStream input) {
            this(input, new RecognizerSharedState());
        }
        public FmTreeParser(TreeNodeStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return FmTreeParser.tokenNames; }
    public String getGrammarFileName() { return "/home/shshe/public/FeatureModel/FmTreeParser.g"; }


      Map<String, FeatureNode<String>> _nameToGroup = new HashMap<String, FeatureNode<String>>();

      private List<String> asList(List<CommonTree> tree) {
          List<String> groupIds = new ArrayList<String>(tree.size());
          for (CommonTree node : tree) {
            groupIds.add(node.getToken().getText());
          }
          return groupIds;
      }

      private boolean isGroupValid(FeatureNode<String> root, List<String> groupIds) {
          for (String s : groupIds) {
            if (root.features().equals(root)) {
              return false;
            }
          }
          return true;
      }



    // $ANTLR start "input"
    // /home/shshe/public/FeatureModel/FmTreeParser.g:50:1: input returns [FeatureModel<String> result] : ( production[$result.getDiagram()] | e= expr )+ ;
    public final FeatureModel<String> input() throws RecognitionException {
        FeatureModel<String> result = null;

        Expression e = null;



          result = new FeatureModel<String>();

        try {
            // /home/shshe/public/FeatureModel/FmTreeParser.g:54:5: ( ( production[$result.getDiagram()] | e= expr )+ )
            // /home/shshe/public/FeatureModel/FmTreeParser.g:55:5: ( production[$result.getDiagram()] | e= expr )+
            {
            // /home/shshe/public/FeatureModel/FmTreeParser.g:55:5: ( production[$result.getDiagram()] | e= expr )+
            int cnt1=0;
            loop1:
            do {
                int alt1=3;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE) ) {
                    alt1=1;
                }
                else if ( (LA1_0==IDENTIFIER||LA1_0==15||(LA1_0>=18 && LA1_0<=20)) ) {
                    alt1=2;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/shshe/public/FeatureModel/FmTreeParser.g:56:7: production[$result.getDiagram()]
            	    {
            	    pushFollow(FOLLOW_production_in_input89);
            	    production(result.getDiagram());

            	    state._fsp--;


            	    }
            	    break;
            	case 2 :
            	    // /home/shshe/public/FeatureModel/FmTreeParser.g:57:7: e= expr
            	    {
            	    pushFollow(FOLLOW_expr_in_input100);
            	    e=expr();

            	    state._fsp--;

            	     result.getConstraints().add(e); 

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }

        }

          catch (RecognitionException except) {
           throw except;
          }
        finally {
        }
        return result;
    }
    // $ANTLR end "input"


    // $ANTLR start "production"
    // /home/shshe/public/FeatureModel/FmTreeParser.g:62:1: production[FeatureGraph<String> g] : ^( RULE id= IDENTIFIER {...}? ( feature[parent, $g] )+ ) ;
    public final void production(FeatureGraph<String> g) throws RecognitionException {
        CommonTree id=null;

        try {
            // /home/shshe/public/FeatureModel/FmTreeParser.g:63:5: ( ^( RULE id= IDENTIFIER {...}? ( feature[parent, $g] )+ ) )
            // /home/shshe/public/FeatureModel/FmTreeParser.g:63:7: ^( RULE id= IDENTIFIER {...}? ( feature[parent, $g] )+ )
            {
            match(input,RULE,FOLLOW_RULE_in_production134); 

            match(input, Token.DOWN, null); 
            id=(CommonTree)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_production138); 

            	        if (g.vertices().size() == 0)
            	          g.addVertex(new FeatureNode<String>((id!=null?id.getText():null)));

            	        //Check if there is a named AND-group
            	        FeatureNode<String> parent = _nameToGroup.containsKey((id!=null?id.getText():null)) ?
                            _nameToGroup.get((id!=null?id.getText():null)) : g.findNode((id!=null?id.getText():null));
            	      
            if ( !((_nameToGroup.containsKey((id!=null?id.getText():null)) || g.findNode((id!=null?id.getText():null)) != null)) ) {
                throw new FailedPredicateException(input, "production", "_nameToGroup.containsKey($id.text) || $g.findNode($id.text) != null");
            }
            // /home/shshe/public/FeatureModel/FmTreeParser.g:74:8: ( feature[parent, $g] )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>=FEATURE && LA2_0<=GROUP)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // /home/shshe/public/FeatureModel/FmTreeParser.g:74:8: feature[parent, $g]
            	    {
            	    pushFollow(FOLLOW_feature_in_production172);
            	    feature(parent, g);

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);


            match(input, Token.UP, null); 

            }

        }

          catch (RecognitionException except) {
           throw except;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "production"


    // $ANTLR start "feature"
    // /home/shshe/public/FeatureModel/FmTreeParser.g:78:1: feature[FeatureNode<String> parent, FeatureGraph<String> g] : ( ^( FEATURE id= IDENTIFIER (isOpt= '?' )? ) | ^( GROUP (groupNodes+= IDENTIFIER )+ (isOr= PLUS )? (isMutex= OPT )? ) {...}? | ^( GROUP (groupNodes+= IDENTIFIER )+ '&' (name= ASSIGN )? ) {...}?);
    public final void feature(FeatureNode<String> parent, FeatureGraph<String> g) throws RecognitionException {
        CommonTree id=null;
        CommonTree isOpt=null;
        CommonTree isOr=null;
        CommonTree isMutex=null;
        CommonTree name=null;
        CommonTree groupNodes=null;
        List list_groupNodes=null;

        try {
            // /home/shshe/public/FeatureModel/FmTreeParser.g:79:5: ( ^( FEATURE id= IDENTIFIER (isOpt= '?' )? ) | ^( GROUP (groupNodes+= IDENTIFIER )+ (isOr= PLUS )? (isMutex= OPT )? ) {...}? | ^( GROUP (groupNodes+= IDENTIFIER )+ '&' (name= ASSIGN )? ) {...}?)
            int alt9=3;
            alt9 = dfa9.predict(input);
            switch (alt9) {
                case 1 :
                    // /home/shshe/public/FeatureModel/FmTreeParser.g:79:7: ^( FEATURE id= IDENTIFIER (isOpt= '?' )? )
                    {
                    match(input,FEATURE,FOLLOW_FEATURE_in_feature201); 

                    match(input, Token.DOWN, null); 
                    id=(CommonTree)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_feature205); 
                    // /home/shshe/public/FeatureModel/FmTreeParser.g:79:36: (isOpt= '?' )?
                    int alt3=2;
                    int LA3_0 = input.LA(1);

                    if ( (LA3_0==OPT) ) {
                        alt3=1;
                    }
                    switch (alt3) {
                        case 1 :
                            // /home/shshe/public/FeatureModel/FmTreeParser.g:79:36: isOpt= '?'
                            {
                            isOpt=(CommonTree)match(input,OPT,FOLLOW_OPT_in_feature209); 

                            }
                            break;

                    }


                    match(input, Token.UP, null); 

                          FeatureNode<String> f = new FeatureNode<String>((id!=null?id.getText():null));

                          g.addVertex(f);
                          g.addEdge(f, parent, FeatureEdge.OPTIONAL);

                          if (isOpt == null)
                            g.addEdge(f, parent, FeatureEdge.MANDATORY);
                        

                    }
                    break;
                case 2 :
                    // /home/shshe/public/FeatureModel/FmTreeParser.g:91:7: ^( GROUP (groupNodes+= IDENTIFIER )+ (isOr= PLUS )? (isMutex= OPT )? ) {...}?
                    {
                    match(input,GROUP,FOLLOW_GROUP_in_feature228); 

                    match(input, Token.DOWN, null); 
                    // /home/shshe/public/FeatureModel/FmTreeParser.g:91:25: (groupNodes+= IDENTIFIER )+
                    int cnt4=0;
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==IDENTIFIER) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // /home/shshe/public/FeatureModel/FmTreeParser.g:91:25: groupNodes+= IDENTIFIER
                    	    {
                    	    groupNodes=(CommonTree)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_feature232); 
                    	    if (list_groupNodes==null) list_groupNodes=new ArrayList();
                    	    list_groupNodes.add(groupNodes);


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt4 >= 1 ) break loop4;
                                EarlyExitException eee =
                                    new EarlyExitException(4, input);
                                throw eee;
                        }
                        cnt4++;
                    } while (true);

                    // /home/shshe/public/FeatureModel/FmTreeParser.g:91:43: (isOr= PLUS )?
                    int alt5=2;
                    int LA5_0 = input.LA(1);

                    if ( (LA5_0==PLUS) ) {
                        alt5=1;
                    }
                    switch (alt5) {
                        case 1 :
                            // /home/shshe/public/FeatureModel/FmTreeParser.g:91:43: isOr= PLUS
                            {
                            isOr=(CommonTree)match(input,PLUS,FOLLOW_PLUS_in_feature237); 

                            }
                            break;

                    }

                    // /home/shshe/public/FeatureModel/FmTreeParser.g:91:57: (isMutex= OPT )?
                    int alt6=2;
                    int LA6_0 = input.LA(1);

                    if ( (LA6_0==OPT) ) {
                        alt6=1;
                    }
                    switch (alt6) {
                        case 1 :
                            // /home/shshe/public/FeatureModel/FmTreeParser.g:91:57: isMutex= OPT
                            {
                            isMutex=(CommonTree)match(input,OPT,FOLLOW_OPT_in_feature242); 

                            }
                            break;

                    }


                    match(input, Token.UP, null); 

                          Set<FeatureNode<String>> members = new HashSet<FeatureNode<String>>();
                          for (String s : asList(list_groupNodes)) {
                            FeatureNode<String> v = _nameToGroup.containsKey(s)
                                  ? _nameToGroup.get(s) : new FeatureNode<String>(s);

                             g.addVertex(v);
                             g.addEdge(v, parent, FeatureEdge.OPTIONAL);
                             members.add(v);
                          }

                          g.addEdge(members, parent,
                              isOr != null
                                  ? FeatureEdge.OR : isMutex != null
                                  ? FeatureEdge.MUTEX : FeatureEdge.XOR);
                        
                    if ( !((isGroupValid(parent, asList(list_groupNodes)) )) ) {
                        throw new FailedPredicateException(input, "feature", "isGroupValid($parent, asList($groupNodes)) ");
                    }

                    }
                    break;
                case 3 :
                    // /home/shshe/public/FeatureModel/FmTreeParser.g:111:7: ^( GROUP (groupNodes+= IDENTIFIER )+ '&' (name= ASSIGN )? ) {...}?
                    {
                    match(input,GROUP,FOLLOW_GROUP_in_feature267); 

                    match(input, Token.DOWN, null); 
                    // /home/shshe/public/FeatureModel/FmTreeParser.g:111:25: (groupNodes+= IDENTIFIER )+
                    int cnt7=0;
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0==IDENTIFIER) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // /home/shshe/public/FeatureModel/FmTreeParser.g:111:25: groupNodes+= IDENTIFIER
                    	    {
                    	    groupNodes=(CommonTree)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_feature271); 
                    	    if (list_groupNodes==null) list_groupNodes=new ArrayList();
                    	    list_groupNodes.add(groupNodes);


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt7 >= 1 ) break loop7;
                                EarlyExitException eee =
                                    new EarlyExitException(7, input);
                                throw eee;
                        }
                        cnt7++;
                    } while (true);

                    match(input,18,FOLLOW_18_in_feature274); 
                    // /home/shshe/public/FeatureModel/FmTreeParser.g:111:47: (name= ASSIGN )?
                    int alt8=2;
                    int LA8_0 = input.LA(1);

                    if ( (LA8_0==ASSIGN) ) {
                        alt8=1;
                    }
                    switch (alt8) {
                        case 1 :
                            // /home/shshe/public/FeatureModel/FmTreeParser.g:111:47: name= ASSIGN
                            {
                            name=(CommonTree)match(input,ASSIGN,FOLLOW_ASSIGN_in_feature278); 

                            }
                            break;

                    }


                    match(input, Token.UP, null); 

                          Set<String> idSet = new HashSet<String>(asList(list_groupNodes));
                          FeatureNode<String> and = new FeatureNode<String>(idSet);
                          g.addVertex(and);
                          g.addEdge(and, parent, FeatureEdge.OPTIONAL);
                          if (name != null) {
                            _nameToGroup.put((name!=null?name.getText():null), and);
                          }
                        
                    if ( !((idSet.size() == list_groupNodes.size())) ) {
                        throw new FailedPredicateException(input, "feature", "idSet.size() == $groupNodes.size()");
                    }

                    }
                    break;

            }
        }

          catch (RecognitionException except) {
           throw except;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "feature"


    // $ANTLR start "expr"
    // /home/shshe/public/FeatureModel/FmTreeParser.g:126:1: expr returns [Expression value] : ( ^( '|' a= expr b= expr ) | ^( '&' a= expr b= expr ) | ^( '->' a= expr b= expr ) | ^( '!' a= expr ) | id= IDENTIFIER );
    public final Expression expr() throws RecognitionException {
        Expression value = null;

        CommonTree id=null;
        Expression a = null;

        Expression b = null;


        try {
            // /home/shshe/public/FeatureModel/FmTreeParser.g:127:5: ( ^( '|' a= expr b= expr ) | ^( '&' a= expr b= expr ) | ^( '->' a= expr b= expr ) | ^( '!' a= expr ) | id= IDENTIFIER )
            int alt10=5;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt10=1;
                }
                break;
            case 18:
                {
                alt10=2;
                }
                break;
            case 19:
                {
                alt10=3;
                }
                break;
            case 20:
                {
                alt10=4;
                }
                break;
            case IDENTIFIER:
                {
                alt10=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // /home/shshe/public/FeatureModel/FmTreeParser.g:127:7: ^( '|' a= expr b= expr )
                    {
                    match(input,15,FOLLOW_15_in_expr316); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr320);
                    a=expr();

                    state._fsp--;

                    pushFollow(FOLLOW_expr_in_expr324);
                    b=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 

                          value = new Expression<String>(ExpressionType.OR, a, b);
                        

                    }
                    break;
                case 2 :
                    // /home/shshe/public/FeatureModel/FmTreeParser.g:131:7: ^( '&' a= expr b= expr )
                    {
                    match(input,18,FOLLOW_18_in_expr340); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr344);
                    a=expr();

                    state._fsp--;

                    pushFollow(FOLLOW_expr_in_expr348);
                    b=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 

                          value = new Expression<String>(ExpressionType.AND, a, b);
                        

                    }
                    break;
                case 3 :
                    // /home/shshe/public/FeatureModel/FmTreeParser.g:135:7: ^( '->' a= expr b= expr )
                    {
                    match(input,19,FOLLOW_19_in_expr364); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr368);
                    a=expr();

                    state._fsp--;

                    pushFollow(FOLLOW_expr_in_expr372);
                    b=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 

                          value = new Expression<String>(ExpressionType.IMPLIES, a, b);
                        

                    }
                    break;
                case 4 :
                    // /home/shshe/public/FeatureModel/FmTreeParser.g:139:7: ^( '!' a= expr )
                    {
                    match(input,20,FOLLOW_20_in_expr392); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr396);
                    a=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 

                          value = new Expression<String>(ExpressionType.NOT, a, null);
                        

                    }
                    break;
                case 5 :
                    // /home/shshe/public/FeatureModel/FmTreeParser.g:143:7: id= IDENTIFIER
                    {
                    id=(CommonTree)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_expr417); 

                          if ((id!=null?id.getText():null).equals("1")) {
                            value = new Expression<String>(ExpressionType.TRUE);
                          }
                          else if ((id!=null?id.getText():null).equals("0")) {
                            value = new Expression<String>(ExpressionType.FALSE);
                          }
                          else {
                            value = new Expression<String>((id!=null?id.getText():null));
                          }
                        

                    }
                    break;

            }
        }

          catch (RecognitionException except) {
           throw except;
          }
        finally {
        }
        return value;
    }
    // $ANTLR end "expr"

    // Delegated rules


    protected DFA9 dfa9 = new DFA9(this);
    static final String DFA9_eotS =
        "\7\uffff";
    static final String DFA9_eofS =
        "\7\uffff";
    static final String DFA9_minS =
        "\1\5\1\uffff\1\2\1\10\1\3\2\uffff";
    static final String DFA9_maxS =
        "\1\6\1\uffff\1\2\1\10\1\22\2\uffff";
    static final String DFA9_acceptS =
        "\1\uffff\1\1\3\uffff\1\3\1\2";
    static final String DFA9_specialS =
        "\7\uffff}>";
    static final String[] DFA9_transitionS = {
            "\1\1\1\2",
            "",
            "\1\3",
            "\1\4",
            "\1\6\4\uffff\1\4\2\6\7\uffff\1\5",
            "",
            ""
    };

    static final short[] DFA9_eot = DFA.unpackEncodedString(DFA9_eotS);
    static final short[] DFA9_eof = DFA.unpackEncodedString(DFA9_eofS);
    static final char[] DFA9_min = DFA.unpackEncodedStringToUnsignedChars(DFA9_minS);
    static final char[] DFA9_max = DFA.unpackEncodedStringToUnsignedChars(DFA9_maxS);
    static final short[] DFA9_accept = DFA.unpackEncodedString(DFA9_acceptS);
    static final short[] DFA9_special = DFA.unpackEncodedString(DFA9_specialS);
    static final short[][] DFA9_transition;

    static {
        int numStates = DFA9_transitionS.length;
        DFA9_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA9_transition[i] = DFA.unpackEncodedString(DFA9_transitionS[i]);
        }
    }

    class DFA9 extends DFA {

        public DFA9(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 9;
            this.eot = DFA9_eot;
            this.eof = DFA9_eof;
            this.min = DFA9_min;
            this.max = DFA9_max;
            this.accept = DFA9_accept;
            this.special = DFA9_special;
            this.transition = DFA9_transition;
        }
        public String getDescription() {
            return "78:1: feature[FeatureNode<String> parent, FeatureGraph<String> g] : ( ^( FEATURE id= IDENTIFIER (isOpt= '?' )? ) | ^( GROUP (groupNodes+= IDENTIFIER )+ (isOr= PLUS )? (isMutex= OPT )? ) {...}? | ^( GROUP (groupNodes+= IDENTIFIER )+ '&' (name= ASSIGN )? ) {...}?);";
        }
    }
 

    public static final BitSet FOLLOW_production_in_input89 = new BitSet(new long[]{0x00000000001C8112L});
    public static final BitSet FOLLOW_expr_in_input100 = new BitSet(new long[]{0x00000000001C8112L});
    public static final BitSet FOLLOW_RULE_in_production134 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_IDENTIFIER_in_production138 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_feature_in_production172 = new BitSet(new long[]{0x0000000000000068L});
    public static final BitSet FOLLOW_FEATURE_in_feature201 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_IDENTIFIER_in_feature205 = new BitSet(new long[]{0x0000000000000408L});
    public static final BitSet FOLLOW_OPT_in_feature209 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_GROUP_in_feature228 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_IDENTIFIER_in_feature232 = new BitSet(new long[]{0x0000000000000708L});
    public static final BitSet FOLLOW_PLUS_in_feature237 = new BitSet(new long[]{0x0000000000000408L});
    public static final BitSet FOLLOW_OPT_in_feature242 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_GROUP_in_feature267 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_IDENTIFIER_in_feature271 = new BitSet(new long[]{0x0000000000040100L});
    public static final BitSet FOLLOW_18_in_feature274 = new BitSet(new long[]{0x0000000000000088L});
    public static final BitSet FOLLOW_ASSIGN_in_feature278 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_15_in_expr316 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr320 = new BitSet(new long[]{0x00000000001C8118L});
    public static final BitSet FOLLOW_expr_in_expr324 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_18_in_expr340 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr344 = new BitSet(new long[]{0x00000000001C8118L});
    public static final BitSet FOLLOW_expr_in_expr348 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_19_in_expr364 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr368 = new BitSet(new long[]{0x00000000001C8118L});
    public static final BitSet FOLLOW_expr_in_expr372 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_20_in_expr392 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr396 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_IDENTIFIER_in_expr417 = new BitSet(new long[]{0x0000000000000002L});

}