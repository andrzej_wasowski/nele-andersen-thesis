package ca.uwaterloo.gsd.fm;

import java.util.Collection;
import java.util.Set;

/**
 * This interface is incomplete and should be extended as needed.
 */
public interface BasicGraph<V,E> {
	public Set<V> children(V v);
	public Set<V> parents(V v);
	public E findEdge(V v1, V v2);
	public Collection<E> edges();
	public Collection<V> vertices();
	public Collection<E> outgoingEdges(V v);
	public Collection<E> incomingEdges(V v);
	public V getSource(E e);
	public V getTarget(E e);
	public boolean removeEdge(E e);
}
