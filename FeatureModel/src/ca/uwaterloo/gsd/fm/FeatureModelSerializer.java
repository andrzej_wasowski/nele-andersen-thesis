/**
* Copyright (c) 2009 Steven She
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
*
* Contributors:
*   Steven She - initial API and implementation
*/
package ca.uwaterloo.gsd.fm;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.collections15.Closure;
import org.apache.commons.collections15.Factory;
import org.apache.commons.collections15.map.LazyMap;

public class FeatureModelSerializer {

	Map<FeatureNode<?>, String> groupToId
		= LazyMap.decorate(new HashMap<FeatureNode<?>, String>(),
			new Factory<String>() {

		private int groupId = 1;

		public String create() {
			return "G" + groupId++;
		}

	});
	private final boolean _newLine;

	public FeatureModelSerializer(boolean newLine) {
		_newLine = newLine;
	}

	/**
	 * FIXME allow user to specify a Transformer to transform feature object to
	 * string. Currently uses {@link #toString()} method.
	 *
	 * @param fm
	 * @return
	 */
	public <T extends Object> String toString(FeatureModel<T> fm) {
		final StringBuilder sb = new StringBuilder();

		if (fm.features().size() == 0)
			return "";

		//process FeatureGraph
		final FeatureGraph<T> g = fm.getDiagram();
		final Queue<FeatureNode<T>> rest = new LinkedList<FeatureNode<T>>();


		Closure<FeatureNode<T>> processNode = new Closure<FeatureNode<T>>() {

			public void execute(FeatureNode<T> v) {

				Collection<FeatureNode<T>> children = g.children(v);
				if (children.size() == 0) return;

				rest.addAll(children);

				sb.append((v.getType() == FeatureType.AND_GROUP
						? groupToId.get(v) : v.getFeature()) + ": ");

				//First, process AND-Groups
				Iterator<FeatureNode<T>> iter = children.iterator();
				while (iter.hasNext()) {
					FeatureNode<T> child = iter.next();
					if (child.getType() == FeatureType.AND_GROUP) {
						String groupId = groupToId.get(child);
						groupToId.put(child, groupId);

						sb.append(groupId).append("=(");
						for (T f : child.features()) {
							sb.append(f.toString()).append("&");
						}
						sb.deleteCharAt(sb.length()-1);
						sb.append(")");

						iter.remove();
					}
				}

				//Process Groups
				for (FeatureEdge e : g.incomingEdges(v)) {
					Set<FeatureNode<T>> sources = g.getSources(e);

					if (sources.size() == 1)
						continue;

					sb.append("(");
					for (FeatureNode<T> m : g.getSources(e)) {
						sb.append(m.getType() == FeatureType.AND_GROUP
								? groupToId.get(m)
								: m.getFeature()).append("|");
					}
					sb.deleteCharAt(sb.length()-1);
					sb.append(")");
					switch (e.getType()) {
					case FeatureEdge.MUTEX:
						sb.append("?");
						break;
					case FeatureEdge.OR:
						sb.append("+");
						break;
					}
					sb.append(" ");
					children.removeAll(sources);
				}

				//Process remaining children
				for (FeatureNode<T> child : children) {
					sb.append(child.getFeature());
					if (g.findMandatoryEdge(child, v) == null) {
							sb.append("?");
					}
					sb.append(" ");
				}
				sb.append(";");
				if (_newLine)
					sb.append("\n");
			}
		};

		if (g.vertices().size() > 0) {
			rest.add(g.root());
			while (!rest.isEmpty()) {
				processNode.execute(rest.poll());
			}
		}

		//process Constraints
		for (Expression<T> e : fm.getConstraints()) {
			sb.append(e.toString()).append(";");
			if (_newLine)
				sb.append("\n");
		}

		return sb.toString();
	}

}
