package ca.uwaterloo.gsd.fm;



public class FeatureEdge {

	public static final int OPTIONAL = 0;
	public static final int MANDATORY = 1;
	public static final int MUTEX = 2;
	public static final int OR = 3;
	public static final int XOR = 4;

	private final int _type;

	protected FeatureEdge(int type) {
		_type = type;

	}

	public int getType() {
		return _type;
	}
}
