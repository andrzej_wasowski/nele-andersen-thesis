package ca.uwaterloo.gsd.fm;

import java.io.IOException;
import java.util.logging.Logger;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.Token;
import org.antlr.runtime.TokenRewriteStream;
import org.antlr.runtime.TokenStream;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeAdaptor;
import org.antlr.runtime.tree.CommonTreeNodeStream;
import org.antlr.runtime.tree.TreeAdaptor;

import ca.uwaterloo.gsd.fm.grammar.FmLexer;
import ca.uwaterloo.gsd.fm.grammar.FmParser;
import ca.uwaterloo.gsd.fm.grammar.FmTreeParser;

/**
 *
 * Uses the ANTLR grammar to return a Feature Model consisting of String objects.
 *
 * @author Steven She <shshe@uwaterloo.ca>
 *
 */
public class FeatureModelParser {

	static Logger logger = Logger.getLogger("fm.FeatureModelParser");

	/**
	 * A simple adaptor that stores the tokens of the tree
	 */
	static final TreeAdaptor adaptor = new CommonTreeAdaptor() {
		@Override
		public Object create(Token token) {
			return new CommonTree(token);
		}


		/**
		 * Disable error node creation.
		 */
		public Object errorNode(TokenStream input, Token start, Token stop,
				RecognitionException e)
		{
			return null;
		}

	};


	public static FeatureModel<String> parseString(String text) {
		ANTLRStringStream input = new ANTLRStringStream(text);
		try {
			return parse(input);
		}
		catch (IOException e) {
			//This should never happen since we're using string stream
			return null;
		}
	}
	public static FeatureModel<String> parseFile(String filename) throws IOException {
		ANTLRFileStream fs = new ANTLRFileStream(filename);
		return parse(fs);
	}

	protected static FeatureModel<String> parse(CharStream stream) throws IOException {
		FmLexer lex = new FmLexer(stream);
		TokenRewriteStream tokens = new TokenRewriteStream(lex);
		FmParser grammar = new FmParser(tokens);
		grammar.setTreeAdaptor(adaptor);

		try {
			FmParser.input_return input = grammar.input();
			CommonTree ast = (CommonTree) input.getTree();

			CommonTreeNodeStream nodes = new CommonTreeNodeStream(ast);
			FmTreeParser walker = new FmTreeParser(nodes);
			return walker.input();
		}
		catch (RecognitionException exception) {
			logger.warning("Recognition Exception: " + exception);
			return null;
		}
	}

}
