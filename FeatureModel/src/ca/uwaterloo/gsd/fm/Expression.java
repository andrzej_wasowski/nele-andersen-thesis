package ca.uwaterloo.gsd.fm;

public class Expression<T> {
	private Expression<T> mLeft, mRight;
	private T mFeature;

	private ExpressionType mType;

	public Expression(ExpressionType type) {
		mType = type;
		mLeft = null;
		mRight = null;
	}

	public Expression(ExpressionType type, Expression<T> expression, Expression<T> expression2) {
		mType = type;
		mLeft = expression;
		mRight = expression2;
	}

	public Expression(ExpressionType type, T leftFeat, T rightFeat) {
		assert type != ExpressionType.FEATURE;
		mType = type;
		mLeft = new Expression<T>(leftFeat);
		mRight = new Expression<T>(rightFeat);
	}

	public Expression(T feature) {
		assert feature != null;
		mType = ExpressionType.FEATURE;
		mFeature = feature;
	}

	public T getFeature() {
		return mFeature;
	}

	public void setFeature(T feature) {
		mFeature = feature;
	}

	public Expression<T> getLeft() {
		return mLeft;
	}
	public void setLeft(Expression<T> left) {
		mLeft = left;
	}


	public Expression<T> getRight() {
		return mRight;
	}
	public void setRight(Expression<T> right) {
		mRight = right;
	}

	public ExpressionType getType() {
		return mType;
	}


	public Expression<T> implies(Expression<T> other) {
		return new Expression<T>(ExpressionType.IMPLIES, this, other);
	}

	public Expression<T> or(Expression<T> other) {
		return new Expression<T>(ExpressionType.OR, this, other);
	}

	public Expression<T> and(Expression<T> other) {
		return new Expression<T>(ExpressionType.AND, this, other);
	}

	public Expression<T> not() {
		return new Expression<T>(ExpressionType.NOT, this, null);
	}


	@Override
	public int hashCode() {
		return getType() == ExpressionType.FEATURE ? mFeature.hashCode() + 3
			: getLeft().hashCode() + (getRight() == null ? 0 : getRight().hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Expression) {
			Expression<?> other = (Expression<?>) obj;
			return getType().equals(other.getType())
				&& (getLeft() == null ? true : getLeft().equals(other.getLeft()))
				&& (getRight() == null ? true : getRight().equals(other.getRight()));
		}
		return false;
	}

	@Override
	public String toString() {
		switch (mType) {
		case FEATURE:
			return mFeature.toString();
		case TRUE:
			return "1";
		case FALSE:
			return "0";
		case NOT:
			return "!" + mLeft;
		default:
			StringBuffer sb = new StringBuffer();
			sb.append('(');
			sb.append(mLeft);
			sb.append(" " + mType + " ");
			sb.append(mRight);
			sb.append(')');
			return sb.toString();
		}
	}
}
