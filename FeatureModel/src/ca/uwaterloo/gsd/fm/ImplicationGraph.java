package ca.uwaterloo.gsd.fm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.collections15.CollectionUtils;

import ca.uwaterloo.gsd.fm.graphviz.GraphvizGraph;
import edu.uci.ics.jung.graph.DirectedSparseGraph;

public class ImplicationGraph<T> extends DirectedSparseGraph<T, SimpleEdge> 
	implements Cloneable, BasicGraph<T, SimpleEdge>{

	private static final long serialVersionUID = 1L;

	@Override
	public ImplicationGraph<T> clone() {
		ImplicationGraph<T> result = new ImplicationGraph<T>();
		for (T v : getVertices()) {
			result.addVertex(v);
		}
		for (SimpleEdge e : getEdges()) {
			T source = getSource(e);
			T target = getDest(e);
			result.addEdge(e, source, target);
		}
		return result;
	}

	public boolean addEdge(T v1, T v2) {
		return addEdge(new SimpleEdge(), v1, v2);
	}

	@Override
	public String toString() {
		return new GraphvizGraph<T, SimpleEdge>(this).toString();
	}

	public List<SimpleEdge> topologicalEdges() {
		ArrayList<SimpleEdge> result = new ArrayList<SimpleEdge>();
		for (T root : roots()) {
			topologicalEdges_Internal(root, new LinkedList<T>(), result);
		}

		assert new HashSet<SimpleEdge>(result).size() == result.size();
		return result;
	}

	private void topologicalEdges_Internal(T v, Queue<T> q,
			List<SimpleEdge> result) {
		assert CollectionUtils.intersection(result, getOutEdges(v)).size() == 0;
		
		q.addAll(getPredecessors(v));
		result.addAll(getOutEdges(v));

		if (!q.isEmpty())
			topologicalEdges_Internal(q.poll(), q, result);
	}

	private Set<T> roots() {
		Set<T> result = new HashSet<T>();
		for (T v : getVertices()) {
			if (getSuccessorCount(v) == 0)
				result.add(v);
		}
		return result;
	}
	
	public void printEdges() {
		for (SimpleEdge e : getEdges()) {
			System.out.println(edgeString(e));
		}
	}

	public String edgeString(SimpleEdge e) {
		return getSource(e) + "->" + getDest(e);
	}
	
	
	/*-----
	 * Following is needed to implement BasicGraph
	 *----*/

	@Override
	public Set<T> children(T v) {
		return new HashSet<T>(getPredecessors(v));
	}

	@Override
	public Collection<SimpleEdge> edges() {
		return getEdges();
	}

	@Override
	public Set<T> parents(T v) {
		return new HashSet<T>(getSuccessors(v));
	}

	@Override
	public Collection<T> vertices() {
		return getVertices();
	}

	@Override
	public T getTarget(SimpleEdge e) {
		return getDest(e);
	}

	public Collection<SimpleEdge> incomingEdges(T v) {
		return getInEdges(v);
	}

	@Override
	public Collection<SimpleEdge> outgoingEdges(T v) {
		return getOutEdges(v);
	}

}
