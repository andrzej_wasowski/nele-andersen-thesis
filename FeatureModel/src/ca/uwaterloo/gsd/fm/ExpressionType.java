/**
 *
 */
package ca.uwaterloo.gsd.fm;

public enum ExpressionType {
	AND("&"), OR("|"), NOT("!"), IMPLIES("->"), IFF("<->"), FEATURE, TRUE("1"), FALSE("0");

	private ExpressionType() {
		this.symbol = null;
	}

	private ExpressionType(String symbol) {
		this.symbol = symbol;
	}

	private String symbol = null;

	public String toString() {
		return symbol == null ? super.toString() : symbol;
	}
}