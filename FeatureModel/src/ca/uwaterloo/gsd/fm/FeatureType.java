package ca.uwaterloo.gsd.fm;

public enum FeatureType {
	AND_GROUP, SOLITARY;
}
