package ca.uwaterloo.gsd.fm;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * A class containing a FeatureGraph describing the feature diagram and a set of
 * extra constraints
 *
 * @author Steven She <shshe@uwaterloo.ca>
 */
public class FeatureModel<T> implements Cloneable {

	private FeatureGraph<T> mDiagram;
	List<Expression<T>> mConstraints = new ArrayList<Expression<T>>();


	public FeatureModel() {
		mDiagram = new FeatureGraph<T>();
	}

	public FeatureModel(FeatureGraph<T> model) {
		mDiagram = model;
	}

	public FeatureModel<T> clone() {
		FeatureModel<T> result = new FeatureModel<T>(mDiagram.clone());
		result.mConstraints = new ArrayList<Expression<T>>(mConstraints);
		return result;
	}

	public Set<T> features() {
		Set<T> result = mDiagram.features();
		for (Expression<T> c : mConstraints) {
			features(c, result);
		}
		return result;
	}

	private void features(Expression<T> expr, Set<T> features) {
		if (expr == null)
			return;
		features(expr.getLeft(), features);
		features(expr.getRight(), features);
		if (expr.getType() == ExpressionType.FEATURE) {
			features.add(expr.getFeature());
		}
	}

	/**
	 * Structurally (syntactically) compare two FeatureModels. To compare
	 * semantically, use the BDD representation.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof FeatureModel) {
			FeatureModel<?> other = (FeatureModel<?>) obj;
			return getDiagram().equals(other.getDiagram())
				&& getConstraints().equals(other.getConstraints());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return getDiagram().hashCode() + getConstraints().hashCode();
	}

	/**
	 * FIXME currently manually appending constraints to the Graphviz output
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(mDiagram.toString());
		builder.delete(builder.lastIndexOf("}"), builder.length());
		builder.append("CONSTRAINTS[shape=plaintext,label=<");
		builder.append("<TABLE BORDER=\"0\" CELLBORDER=\"1\" CELLSPACING=\"0\" CELLPADDING=\"4\">");
		builder.append("<TR><TD>Constraints</TD></TR>\n");
		for (Expression<T> e : getConstraints()) {
			builder.append("<TR><TD>").append(e).append("</TD></TR>\n");
		}
		builder.append("</TABLE>>]\n");
		builder.append("}");

		return builder.toString();
	}


	public FeatureGraph<T> getDiagram() {
		return mDiagram;
	}

	public List<Expression<T>> getConstraints() {
		return mConstraints;
	}

}
