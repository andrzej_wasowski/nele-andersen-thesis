package ca.uwaterloo.gsd.fm;


public class ExpressionParser {
	public static Expression<String> parseString(String expr)  {
		FeatureModel<String> fm = FeatureModelParser.parseString(expr);
		return fm.getConstraints().get(0);
	}
}
