package ca.uwaterloo.gsd.fm;


import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import junit.framework.TestCase;

public class FeatureGraphTests extends TestCase {
	FeatureGraph<String> _g;
	@Override
	protected void setUp() throws Exception {
		_g = new FeatureGraph<String>();
	}

	public void testAbsentVertex() {
		FeatureNode<String> a = new FeatureNode<String>("a");
		FeatureNode<String> b = new FeatureNode<String>("b");
		try {
			_g.addEdge(a, b, FeatureEdge.MANDATORY);
			fail();
		}
		catch (IllegalArgumentException e) {
		}
	}

	public void testBasicAdd() {
		FeatureNode<String> a = new FeatureNode<String>("a");
		FeatureNode<String> b = new FeatureNode<String>("b");
		FeatureNode<String> c = new FeatureNode<String>("c");
		FeatureNode<String> d = new FeatureNode<String>("d");

		_g.addVertex(a);
		_g.addVertex(b);
		_g.addVertex(c);
		_g.addVertex(d);

		_g.addEdge(a, b, FeatureEdge.OPTIONAL);
		assertEquals(1, _g.outgoingEdges(a).size());
		FeatureEdge e = _g.outgoingEdges(a).iterator().next();
		Collection<FeatureNode<String>> sources = _g.getSources(e);
		assertEquals(1, sources.size());
		assertTrue(sources.contains(a));

		_g.addEdge(c,b, FeatureEdge.MANDATORY);
		assertEquals(1, _g.outgoingEdges(c).size());
		assertEquals(2, _g.incomingEdges(b).size());

		_g.addEdge(b, d, FeatureEdge.OPTIONAL);
		assertEquals(1, _g.outgoingEdges(b).size());
		assertEquals(1, _g.incomingEdges(d).size());
	}

	@SuppressWarnings("unchecked")
	public void testBasicRemove() {
		FeatureNode<String> a = new FeatureNode<String>("a");
		FeatureNode<String> b = new FeatureNode<String>("b");
		FeatureNode<String> c = new FeatureNode<String>("c");
		FeatureNode<String> d = new FeatureNode<String>("d");

		_g.addVertex(a);
		_g.addVertex(b);
		_g.addVertex(c);
		_g.addVertex(d);

		_g.addEdge(b, a, FeatureEdge.OPTIONAL);
		_g.addEdge(c, a, FeatureEdge.OPTIONAL);
		_g.addEdge(d, a, FeatureEdge.OPTIONAL);

		assertEquals(3, _g.incomingEdges(a).size());
		_g.removeVertex(b);

		_g.printEdges();

		assertEquals(2, _g.edges().size());
		assertEquals(new HashSet<FeatureNode<String>>(Arrays.asList(a, c, d)),
				_g.vertices());

		_g.addVertex(b);
		_g.addEdge(b, c, FeatureEdge.OPTIONAL);
		_g.addEdge(b, c, FeatureEdge.MANDATORY);
		_g.printEdges();
		assertEquals(4, _g.edges().size());
	}

	@SuppressWarnings("unchecked")
	public void testLeaves() {
		FeatureNode<String> a = new FeatureNode<String>("a");
		FeatureNode<String> b = new FeatureNode<String>("b");
		FeatureNode<String> c = new FeatureNode<String>("c");
		FeatureNode<String> d = new FeatureNode<String>("d");
		FeatureNode<String> e = new FeatureNode<String>("e");
		FeatureNode<String> x = new FeatureNode<String>("x");
		FeatureNode<String> y = new FeatureNode<String>("y");

		_g.addVertex(a);
		_g.addVertex(b);
		_g.addVertex(c);
		_g.addVertex(d);
		_g.addVertex(e);
		_g.addVertex(x);
		_g.addVertex(y);

		_g.addEdge(b, a, FeatureEdge.OPTIONAL);
		_g.addEdge(c, a, FeatureEdge.OPTIONAL);
		_g.addEdge(d, a, FeatureEdge.OPTIONAL);
		_g.addEdge(e, d, FeatureEdge.OPTIONAL);
		_g.addEdge(Arrays.asList(x, y), e, FeatureEdge.OR);

		assertEquals(new HashSet<FeatureNode<String>>(Arrays.asList(x,y,b,c)), _g.leaves());
	}


	@SuppressWarnings("unchecked")
	public void testParser() throws Exception {
		FeatureModel<String> fm1 = FeatureModelParser.parseString("a: b? c? d?;");
		FeatureGraph<String> fg1 = fm1.getDiagram();

		FeatureModel<String> fm2 = FeatureModelParser.parseString("a: b? c? d?;");
		FeatureGraph<String> fg2 = fm2.getDiagram();

		assertEquals(fg1, fg2);

		FeatureModel<String> fm3 = FeatureModelParser.parseString("a: b? c? d;");
		FeatureGraph<String> fg3 = fm3.getDiagram();


		assertFalse(fg1.equals(fg3));
		assertFalse(fg2.equals(fg3));

		FeatureModel<String> fm4 = FeatureModelParser.parseString("a: (b|c)+ d;");
		FeatureGraph<String> fg4 = fm4.getDiagram();

		FeatureNode<String> b = fg4.findNode("b");
		FeatureNode<String> c = fg4.findNode("c");
		FeatureNode<String> a = fg4.findNode("a");

		assertNotNull(fg4.findEdge(Arrays.asList(b, c), a, FeatureEdge.OR));
	}

	public void testClone() throws Exception {
		FeatureModel<String> fm1
			= FeatureModelParser.parseString("a: b? c?; b: (d|e)+; e: f;");
		FeatureModel<String> clone1 = fm1.clone();
		assertEquals(fm1, clone1);

		FeatureGraph<String> fd1 = fm1.getDiagram();
		FeatureNode<String> d1 = fd1.findNode("d");
		fd1.replaceVertex(d1, new FeatureNode<String>("x"));
		assertFalse(fm1.equals(clone1));
	}

	public void testReplaceVertex() throws Exception {
		{
			FeatureModel<String> fm1
			= FeatureModelParser.parseString("a: G=(b&c&d); G: x y?; a: (z|q)+;");
			FeatureGraph<String> fd1 = fm1.getDiagram();
			FeatureNode<String> c = fd1.findNode("c");
			fd1.replaceVertex(c, c.remove("c"));

			FeatureModel<String> ex1
			= FeatureModelParser.parseString("a: G=(b&d); G: x y?; a: (z|q)+;");
			assertEquals(ex1, fm1);
		}

		{

			FeatureModel<String> fm2
			= FeatureModelParser.parseString("a: (x|y|z)+;");
			FeatureGraph<String> fd2 = fm2.getDiagram();
			FeatureNode<String> x = fd2.findNode("x");
			fd2.replaceVertex(x, new FeatureNode<String>("w"));

			FeatureModel<String> ex2 = FeatureModelParser.parseString("a: (w|y|z)+;");
			assertEquals(ex2, fm2);

		}
	}
}
