package ca.uwaterloo.gsd.fm.graphstream;

import java.util.Set;

import org.apache.commons.collections15.Factory;
import org.miv.graphstream.graph.Edge;
import org.miv.graphstream.graph.Graph;
import org.miv.graphstream.graph.Node;
import org.miv.graphstream.graph.implementations.MultiGraph;
import org.miv.graphstream.ui.GraphViewerRemote;

import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureModelParser;
import ca.uwaterloo.gsd.fm.FeatureNode;

public class Graphstream {

	public static Graphstream INSTANCE = new Graphstream();

	private Graphstream() {}

	private Factory<String> _groupFactory = new Factory<String>() {
		int i=1;
		public String create() {
			return "G" + i++;
		}
	};

	private Factory<String> _edgeFactory = new Factory<String>() {
		int i=1;
		public String create() {
			return String.valueOf(i++);
		}

	};

	public Graph createGraphstreamGraph(FeatureGraph<?> g) {
		Graph result = new MultiGraph();

		for (FeatureNode<?> v : g.vertices()) {
			Node gs = result.addNode(getFeaturesString(v));
			gs.addAttribute("label", getFeaturesString(v));
		}

		for (FeatureEdge e : g.edges()) {
			Set<? extends FeatureNode<?>> sources = g.getSources(e);
			FeatureNode<?> target = g.getTarget(e);
			if (sources.size() > 1) {
				String group = _groupFactory.create();
				Node gsV = result.addNode(group);

				switch (e.getType()) {
				case FeatureEdge.MUTEX:
					gsV.setAttribute("label", "MTX");
					break;
				case FeatureEdge.OR:
					gsV.setAttribute("label", "OR");
					break;
				case FeatureEdge.XOR:
					gsV.setAttribute("label", "XOR");
					break;
				}

				result.addEdge(_edgeFactory.create(), group, getFeaturesString(target));
				for (FeatureNode<?> v : sources) {
					Edge gs = result.addEdge(_edgeFactory.create(), getFeaturesString(v), group);
					gs.setDirected(true);
				}
			}
			else {
				Edge gs = result.addEdge(_edgeFactory.create(),
						getFeaturesString(sources.iterator().next()),
						getFeaturesString(target));
				gs.setDirected(true);
			}
			//TODO Mandatory edges
		}

		result.addAttribute("ui.stylesheet" , "node {color: #eeeeee; text-color: black; text-style: bold; text-size: 16;}");

		return result;
	}

	private String getFeaturesString(FeatureNode<?> v) {
		StringBuilder sb = new StringBuilder();

		for (Object o : v.features()) {
			sb.append(o.toString()).append("&");
		}
		sb.deleteCharAt(sb.length()-1);

		return sb.toString();
	}


	public static void main(String[] args) throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: b? (c|d)+; d: (e|f) (f|g);");
		Graph gsg = Graphstream.INSTANCE.createGraphstreamGraph(fm.getDiagram());
		System.out.println("GSG:" + gsg.getNodeCount() + " edges: " + gsg.getEdgeCount());

		GraphViewerRemote remote = gsg.display();
	}

}
