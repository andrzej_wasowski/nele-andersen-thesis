package ca.uwaterloo.gsd.fm.jung;

import java.awt.Color;
import java.awt.Paint;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.collections15.BidiMap;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;

import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureNode;
import ca.uwaterloo.gsd.fm.FeatureType;
import ca.uwaterloo.gsd.fm.graphviz.GraphvizGraph;
import ca.uwaterloo.gsd.fm.graphviz.GraphvizGraph.RankDir;
import edu.uci.ics.jung.graph.DirectedSparseGraph;

public class VisualGraph extends DirectedSparseGraph<VisualNode, VisualEdge>{

	private static final long serialVersionUID = -8001754967506416422L;
	private final BidiMap<FeatureNode<?>, VisualNode> _nodeMap = new DualHashBidiMap<FeatureNode<?>, VisualNode>();
	private final BidiMap<Object, VisualNode> _featureMap = new DualHashBidiMap<Object, VisualNode>();
	public static Logger logger = Logger.getLogger("fm.VisualGraph");

	public <T> VisualGraph(FeatureGraph<T> g) {

		for (FeatureNode<T> v : g.vertices()) {
			VisualType type = v.getType() == FeatureType.AND_GROUP
				? VisualType.AND_GROUP : v == g.root()
				? VisualType.ROOT : VisualType.OPTIONAL;

			VisualNode visual = new VisualNode(v.toString(), type);
			_nodeMap.put(v, visual);

			for (Object feat : v.features())
				_featureMap.put(feat, visual);

			addVertex(visual);
		}

		for (FeatureEdge e : g.edges()) {
			Set<FeatureNode<T>> sources = g.getSources(e);
			VisualNode vDest = _nodeMap.get(g.getTarget(e));

			if (sources.size() == 1) {
				if (g.findMandatoryEdge(g.getSource(e), g.getTarget(e)) != null) {
					addEdge(new VisualEdge(FeatureEdge.MANDATORY),
							vDest, _nodeMap.get(g.getSource(e)));
				}
				else {
					addEdge(new VisualEdge(e.getType()),
							vDest, _nodeMap.get(g.getSource(e)));
				}
			}
			else {
				VisualType vType = VisualType.ROOT;
				String typeName = "";
				switch (e.getType()) {
				case FeatureEdge.MUTEX:
					vType = VisualType.MUTEX_GROUP;
					typeName = "MTX";
					break;
				case FeatureEdge.XOR:
					vType = VisualType.XOR_GROUP;
					typeName = "XOR";
					break;
				case FeatureEdge.OR:
					vType = VisualType.OR_GROUP;
					typeName = "OR";
					break;
				}
				VisualNode group = new VisualNode(typeName, vType);
				addEdge(new VisualEdge(e.getType()), vDest, group);

				for (FeatureNode<T> m : sources) {
					addEdge(new VisualEdge(e.getType()), group, _nodeMap.get(m));
				}
			}
		}
	}

	public String getTypeString(int type) {
		switch (type) {
		case FeatureEdge.MUTEX:
			return "MTX";
		case FeatureEdge.OR:
			return "OR";
		case FeatureEdge.XOR:
			return "XOR";
		}
		return "";
	}

	public Transformer<VisualNode, Paint> vertexFillTransformer() {
		return new Transformer<VisualNode, Paint>() {
			@Override
			public Paint transform(VisualNode v) {
				switch (v.getType()) {
				case MUTEX_GROUP:
					return Color.decode("#dddddd");
				case OR_GROUP:
					return Color.decode("#6699aa");
				case XOR_GROUP:
					return Color.decode("#333333");
				case OPTIONAL:
					return Color.WHITE;
				case MANDATORY:
					return Color.BLACK;
				case ROOT:
					return Color.BLUE;
				default:
					return Color.RED;
				}
			}
		};
	}


	public Transformer<VisualNode, String> stringLabeller() {
		return new Transformer<VisualNode, String>() {

			public String transform(VisualNode v) {
				switch (v.getType()) {
				case MUTEX_GROUP:
					return "MTX";
				case OR_GROUP:
					return "OR";
				case XOR_GROUP:
					return "XOR";
				default:
					return v.toString();
				}
			}

		};
	}


	@Override
	public String toString() {
		GraphvizGraph<VisualNode, VisualEdge> gv
			= new GraphvizGraph<VisualNode, VisualEdge>(this, new VisualGraphProperties());
		gv.setDirection(RankDir.TOP_BOTTOM);
		return gv.toString();

	}
}
