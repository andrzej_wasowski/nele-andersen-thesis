package ca.uwaterloo.gsd.fm.jung;


public class VisualNode implements Comparable<VisualNode> {
	private final VisualType _type;
	private final String _name;
	public VisualNode(String name, VisualType type) {
		_name = name;
		_type = type;
	}
	public VisualType getType() {
		return _type;
	}
	public String getName() {
		return _name;
	}

	@Override
	public String toString() {
		switch (_type) {
		default:
			return _name;
		}
	}
	@Override
	public int compareTo(VisualNode o) {
		return _name.compareTo(o._name);
	}
}
