package ca.uwaterloo.gsd.fm.jung;

public class VisualEdge implements Comparable<VisualEdge> {

	private final int _type;

	public VisualEdge(int type) {
		_type = type;
	}

	@Override
	public int compareTo(VisualEdge o) {
		return 0;
	}

	public int getType() {
		return _type;
	}

}
