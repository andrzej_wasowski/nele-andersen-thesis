package ca.uwaterloo.gsd.fm.jung;

import java.util.Map;

import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.graphviz.GraphvizProperties;



/**
 *
 * @author Steven She <shshe@uwaterloo.ca>
 */
public class VisualGraphProperties extends GraphvizProperties<VisualNode, VisualEdge>{

	@Override
	protected Map<String, String> edgeProperties(VisualEdge e) {
		Map<String, String> props = super.edgeProperties(e);
		switch (e.getType()) {
		case FeatureEdge.MANDATORY:
			props.put("penwidth", "2");
			props.put("color", "red");
			break;
		}
		return props;
	}

	@Override
	protected Map<String, String> vertexProperties(VisualNode v) {
		Map<String, String> props = super.vertexProperties(v);
		switch (v.getType()) {
		case MANDATORY:
			props.put("color", "none");
			props.put("style", "filled");
			props.put("fillcolor", "gray");
			break;
		case XOR_GROUP:
			props.put("color", "blue");
			//Fall-through
		case OR_GROUP:
		case MUTEX_GROUP:
		case AND_GROUP:
			props.put("shape", "ellipse");
			break;

		}
		return props;
	}
}
