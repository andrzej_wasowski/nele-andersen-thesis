package ca.uwaterloo.gsd.fm.jung;

public enum VisualType {
	ROOT, OPTIONAL, MANDATORY, AND_GROUP, AND_GROUPED, OR_GROUP, XOR_GROUP, MUTEX_GROUP
}
