package ca.uwaterloo.gsd.fm;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.MultiMap;
import org.apache.commons.collections15.Predicate;
import org.apache.commons.collections15.multimap.MultiHashMap;

import ca.uwaterloo.gsd.fm.jung.VisualGraph;

public class FeatureGraph<T> implements Cloneable, BasicGraph<FeatureNode<T>, FeatureEdge> {

	Map<FeatureNode<T>, Collection<FeatureEdge>> _vertices
		= new HashMap<FeatureNode<T>, Collection<FeatureEdge>>();

	MultiMap<FeatureEdge, FeatureNode<T>> _sources
		= new MultiHashMap<FeatureEdge, FeatureNode<T>>();

	Map<FeatureEdge, FeatureNode<T>> _target
		= new HashMap<FeatureEdge, FeatureNode<T>>();

	Map<T, FeatureNode<T>> _features
		= new HashMap<T, FeatureNode<T>>();
	
	public static final boolean CHECKS_ENABLED = false;


	public synchronized boolean addVertex(FeatureNode<T> v) {
		if (v == null)
			throw new IllegalArgumentException("vertex cannot be null!");

		if (_vertices.containsKey(v))
			return false;
		
		for (T f : v.features())
			if (_features.containsKey(f))
				throw new IllegalArgumentException("feature " + f + " already exists in graph as a different node!");
		
		
		_vertices.put(v, new ArrayList<FeatureEdge>());

		for (T feature : v.features())
			_features.put(feature, v);
		
		return true;
	}
	
	public synchronized void removeAllVertices(Set<FeatureNode<T>> vertices) {
		for (FeatureNode<T> v : vertices) {
			removeVertex(v);
		}
	}
	
	public synchronized boolean removeVertex(FeatureNode<T> v) {
		if (!_vertices.containsKey(v))
			return false;

		if (!leaves().contains(v))
			throw new IllegalArgumentException(v + " must be a leaf!");

		for (FeatureEdge e : outgoingEdges(v)) {
			removeEdge(e);
		}

		for (T f : v.features())
			_features.remove(f);
		
		checkEdgeConsistency();
		_vertices.remove(v);
		return true;
	}

	public synchronized FeatureNode<T> replaceVertex(FeatureNode<T> v, FeatureNode<T> w) {
		checkVertexExists(v);

		Collection<EdgeContainer<T>> edges = new ArrayList<EdgeContainer<T>>();
		
		for (FeatureEdge e : incomingEdges(v)) {
			edges.add(new EdgeContainer<T>(getSources(e), w, e.getType()));
			removeEdge_Internal(e);
		}

		for (FeatureEdge e : outgoingEdges(v)) {
			Set<FeatureNode<T>> sources = getSources(e);
			sources.remove(v);
			sources.add(w);
			edges.add(new EdgeContainer<T>(sources, getTarget(e), e.getType()));
			removeEdge_Internal(e);
		}

		removeVertex(v);
		addVertex(w);
		addAllEdges(edges);
		
		return w;
	}

	public synchronized boolean removeSubtree(FeatureNode<T> v) {
		checkVertexExists(v);

		for (FeatureNode<T> c : children(v)) {
			removeSubtree(c);
		}

		removeVertex(v);
		return true;
	}

	public synchronized void addAllEdges(Collection<EdgeContainer<T>> edges) {
		for (EdgeContainer<T> e : edges)
			addEdge(e.getSources(), e.getTarget(), e.getType());
	}
	

	@SuppressWarnings("unchecked")
	public synchronized boolean addEdge(FeatureNode<T> source, FeatureNode<T> target,
			int type) {
		return addEdge(Arrays.asList(source), target, type);
	}



	public synchronized boolean addEdge(Collection<FeatureNode<T>> sources,
			FeatureNode<T> target, int type) {
		if (sources.size() == 0)
			throw new IllegalArgumentException("Sources cannot be empty collection!");
		if (sources.contains(target))
			throw new IllegalArgumentException("Source cannot contain the target!");

		checkEdgeConsistency();
		checkVertexExists(target);

		FeatureEdge e = new FeatureEdge(type);

		if (findEdge(sources, target, type) != null)
			return false;

		_target.put(e, target);

		for (FeatureNode<T> m : sources) {
			checkVertexExists(m);

			_vertices.get(m).add(e);
			_sources.put(e, m);
		}

		_vertices.get(target).add(e);

		checkEdgeConsistency();
		checkVertexConsistency();
		return true;
	}

	public synchronized void removeAllEdges(Collection<FeatureEdge> edges) {
		for (FeatureEdge e : edges) {
			removeEdge(e);
		}
	}
	

	public synchronized boolean removeEdge(FeatureEdge e) {
		checkEdgeExists(e);

		//Check groups that are invalidated by the removal of a hierarchy edge
//		if (e.getType() == FeatureEdge.OPTIONAL) {
//			for (FeatureEdge over : overlappingEdges(e))
//				removeEdge(over);
//		}

		removeEdge_Internal(e);

		checkEdgeConsistency();
		checkVertexConsistency();
		return true;
	}


	/**
	 * Removes the edge without checking for overlapping cardinality edges.
	 */
	private void removeEdge_Internal(FeatureEdge e) {
		for (FeatureNode<T> v : getSources(e)) {
			_vertices.get(v).remove(e);
		}

		_vertices.get(getTarget(e)).remove(e);
		_sources.remove(e);
		_target.remove(e);
	}
	
	public Set<FeatureNode<T>> ancestors(FeatureNode<T> v) {
		final Set<FeatureNode<T>> result = new HashSet<FeatureNode<T>>();
		ancestors_rec(v, result);
		return result;
	}

	private void ancestors_rec(FeatureNode<T> v, Set<FeatureNode<T>> result) {
		result.addAll(parents(v));
		for (FeatureNode<T> c : parents(v)) {
			ancestors_rec(c, result);
		}
	}
	
	public Set<FeatureNode<T>> descendants(FeatureNode<T> v) {
		final Set<FeatureNode<T>> result = new HashSet<FeatureNode<T>>();
		descendants_rec(v, result);
		return result;
	}

	private void descendants_rec(FeatureNode<T> v, Set<FeatureNode<T>> result) {
		result.addAll(children(v));
		for (FeatureNode<T> c : children(v)) {
			descendants_rec(c, result);
		}
	}

	public Collection<FeatureEdge> hierarchyEdges() {
		return CollectionUtils.select(edges(), new Predicate<FeatureEdge>() {

			public boolean evaluate(FeatureEdge e) {
				return getSources(e).size() == 1
					&& e.getType() == FeatureEdge.OPTIONAL;
			}

		});
	}

	public Set<FeatureNode<T>> parents(FeatureNode<T> v) {
		Set<FeatureNode<T>> result = new HashSet<FeatureNode<T>>();
		for (FeatureEdge e : outgoingEdges(v)) {
			result.add(getTarget(e));
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public FeatureEdge findHierarchyEdge(FeatureNode<T> source, FeatureNode<T> target) {
		return findEdge(Arrays.asList(source), target, FeatureEdge.OPTIONAL);
	}

	@SuppressWarnings("unchecked")
	public FeatureEdge findMandatoryEdge(FeatureNode<T> source, FeatureNode<T> target) {
		return findEdge(Arrays.asList(source), target, FeatureEdge.MANDATORY);
	}

	/**
	 * Finds a hierarchy edge from source to target. Needed to implement
	 * the {@link BasicGraph} interface.
	 */
	public FeatureEdge findEdge(FeatureNode<T> source, FeatureNode<T> target) {
		return findHierarchyEdge(source, target);
	}
	
	/**
	 * Finds an edge equivalent to FeatureEdge e in FeatureGraph g, in this graph.
	 */
	public FeatureEdge findEdge(FeatureEdge e, FeatureGraph<T> g) {
		return findEdge(g.getSources(e), g.getTarget(e), e.getType());
	}

	public FeatureEdge findEdge(final Collection<FeatureNode<T>> sources,
			final FeatureNode<T> target, final int type) {
		checkVertexConsistency();
		
		final HashSet<FeatureNode<T>> sourcesSet
					= new HashSet<FeatureNode<T>>(sources);
		return CollectionUtils.find(_vertices.get(target), new Predicate<FeatureEdge>() {
			public boolean evaluate(FeatureEdge e) {
				return getSources(e).equals(sourcesSet) && type == e.getType();
			}
		});
	}
	
	public Collection<FeatureEdge> findEdges(final Collection<FeatureNode<T>> sources, 
			final FeatureNode<T> target) {
		return CollectionUtils.select(
				_vertices.get(target), new Predicate<FeatureEdge>() {
					
			public boolean evaluate(FeatureEdge e) {
				return getSources(e).equals(sources);
			}
			
		});
	}


	public Collection<FeatureEdge> incomingEdges(FeatureNode<T> v) {
		checkVertexExists(v);

		Collection<FeatureEdge> result = new ArrayList<FeatureEdge>();
		for (FeatureEdge e : _vertices.get(v)) {
			if (_target.get(e).equals(v))
				result.add(e);
		}
		return result;
	}

	public Set<FeatureNode<T>> children(FeatureNode<T> v) {
		Set<FeatureNode<T>> result = new HashSet<FeatureNode<T>>();
		for (FeatureEdge e : incomingEdges(v)) {
			result.addAll(getSources(e));
		}
		return result;
	}

	public Collection<FeatureEdge> outgoingEdges(FeatureNode<T> v) {
		checkVertexExists(v);

		Collection<FeatureEdge> result = new ArrayList<FeatureEdge>();
		for (FeatureEdge e : _vertices.get(v)) {
			if (_sources.get(e).contains(v))
				result.add(e);
		}
		return result;
	}


	public Set<FeatureEdge> overlappingEdges(FeatureEdge e) {
		checkEdgeExists(e);

		Set<FeatureEdge> result = new HashSet<FeatureEdge>();

		Set<FeatureNode<T>> sources = getSources(e);
		FeatureNode<T> target = getTarget(e);

		for (FeatureNode<T> v : sources) {
			for (FeatureEdge out : outgoingEdges(v)) {
				if (out != e && getTarget(out) == target)
					result.add(out);
			}
		}

		return result;
	}

	public Collection<FeatureEdge> incidentEdges(FeatureNode<T> v) {
		return new ArrayList<FeatureEdge>(_vertices.get(v));
	}



	public FeatureNode<T> getSource(FeatureEdge e) {
		if (e.getType() != FeatureEdge.OPTIONAL
				&& e.getType() != FeatureEdge.MANDATORY) {
			throw new IllegalArgumentException(
					"Edge must be a hierarchy edge, use getSources(e) instead!");
		}
		assert _sources.get(e).size() == 1;
		return _sources.get(e).iterator().next();
	}

	public Set<FeatureNode<T>> getSources(FeatureEdge e) {
		checkEdgeExists(e);
		return new HashSet<FeatureNode<T>>(_sources.get(e));
	}

	public FeatureNode<T> getTarget(FeatureEdge e) {
		checkEdgeExists(e);
		return _target.get(e);
	}

	public FeatureNode<T> root() {
		Set<FeatureNode<T>> cand = new HashSet<FeatureNode<T>>(vertices());
		for (FeatureEdge e : edges()) {
			cand.removeAll(getSources(e));
		}
		assert cand.size() == 1 : cand;
		return cand.iterator().next();
	}

	public FeatureNode<T> findNode(T feature) {
		if (!_features.containsKey(feature)) {
			throw new IllegalArgumentException(feature + " does not exist in graph!");
		}
		return _features.get(feature);
	}

	public Set<FeatureNode<T>> leaves() {
		Set<FeatureNode<T>> cand = new HashSet<FeatureNode<T>>(vertices());
		for (FeatureEdge e : edges()) {
			cand.remove(getTarget(e));
		}
		return cand;
	}


	public Set<FeatureEdge> edges() {
		return Collections.unmodifiableSet(_target.keySet());
	}

	public Set<FeatureNode<T>> vertices() {
		return Collections.unmodifiableSet(_vertices.keySet());
	}


	public Set<FeatureEdge> selectGroupEdges() {
		return new HashSet<FeatureEdge>(
				CollectionUtils.select(edges(), new Predicate<FeatureEdge>(){

			public boolean evaluate(FeatureEdge e) {
				return e.getType() != FeatureEdge.OPTIONAL &&
					e.getType() != FeatureEdge.MANDATORY;
			}

		}));
	}


	public Set<FeatureEdge> selectCardinalityEdges() {
		return new HashSet<FeatureEdge>(
				CollectionUtils.select(edges(), new Predicate<FeatureEdge>(){

			public boolean evaluate(FeatureEdge e) {
				return e.getType() != FeatureEdge.OPTIONAL;
			}

		}));
	}

	public Set<FeatureEdge> selectHierarchyEdges() {
		return selectEdges(FeatureEdge.OPTIONAL);
	}


	public Set<FeatureEdge> selectEdges(final int type) {
		return new HashSet<FeatureEdge>(
				CollectionUtils.select(edges(), new Predicate<FeatureEdge>(){

			public boolean evaluate(FeatureEdge e) {
				return e.getType() == type;
			}

		}));
	}

	public Set<FeatureNode<T>> selectMandatoryNodes() {
		Set<FeatureNode<T>> result = new HashSet<FeatureNode<T>>();
		for (FeatureEdge e : edges()) {
			if (e.getType() == FeatureEdge.MANDATORY) {
				result.add(getSource(e));
			}
		}
		return result;
	}

	public Set<FeatureNode<T>> selectAndNodes() {
		return new HashSet<FeatureNode<T>>(
				CollectionUtils.select(vertices(), new Predicate<FeatureNode<T>>() {
					public boolean evaluate(FeatureNode<T> v) {
						return v.features().size() > 1;
					}
				})
			);
	}

	public Set<T> features() {
		Set<T> result = new HashSet<T>();
		for (FeatureNode<T> v : vertices()) {
			result.addAll(v.features());
		}
		return result;
	}

	@Override
	public synchronized FeatureGraph<T> clone() {
		FeatureGraph<T> result = new FeatureGraph<T>();

		for (FeatureNode<T> v : vertices()) {
			result.addVertex(v);
		}

		for (FeatureEdge e : edges()) {
			result.addEdge(getSources(e), getTarget(e), e.getType());
		}
		return result;
	}


	@Override
	public int hashCode() {
		return _sources.hashCode() * 13
			+ _target.hashCode() * 67
		    - _vertices.hashCode() * 3;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object o) {
		if (o instanceof FeatureGraph) {
			final FeatureGraph<T> other = (FeatureGraph) o;

			if (edges().size() != other.edges().size())
				return false;

			int matches = CollectionUtils.countMatches(edges(),
				new Predicate<FeatureEdge>() {

				public boolean evaluate(FeatureEdge e) {
					return other.findEdge(getSources(e), getTarget(e), e
							.getType()) != null;
				}

			});

			return matches == edges().size()
				&& vertices().equals(other.vertices());
		}
		return false;
	}


	@Override
	public String toString() {
		return asVisualGraph().toString();
	}

	public VisualGraph asVisualGraph() {
		return new VisualGraph(this);
	}


	public void printEdges() {
		for (FeatureEdge e : edges()) {
			System.out.println(edgeString(e));
		}
	}

	public String edgeString(FeatureEdge e) {
		return getSources(e) + "->" + getTarget(e) + ":" + e.getType();
	}


	private void checkVertexConsistency() {
		if (!CHECKS_ENABLED)
			return;
		
		for (Entry<FeatureNode<T>,Collection<FeatureEdge>> en : _vertices.entrySet()) {
			FeatureNode<T> v = en.getKey();
			for (FeatureEdge e : en.getValue()) {
				if (!getSources(e).contains(v) && !getTarget(e).equals(v))
					throw new AssertionError("Error");
			}
		}
	}

	private void checkVertexExists(FeatureNode<T> v) {
		if (v == null)
			throw new IllegalArgumentException("vertex cannot be null!");

		if (!_vertices.containsKey(v))
			throw new IllegalArgumentException(v + " doesn't exist!");
	}

	private void checkEdgeConsistency() {
		if (!CHECKS_ENABLED)
			return;
		
		//TODO remove this
		Set<FeatureEdge> verticeEdges = new HashSet<FeatureEdge>();
		for (Collection<FeatureEdge> edges : _vertices.values()) {
			verticeEdges.addAll(edges);
		}

		if (!_sources.keySet().equals(_target.keySet()) || !_sources.keySet().equals(verticeEdges))
			throw new AssertionError("FeatureEdges in sources and target not in sync!");
	}

	private void checkEdgeExists(FeatureEdge e) {
		if (e == null)
			throw new IllegalArgumentException("edge cannot be null!");

		if (!_sources.containsKey(e))
			throw new IllegalArgumentException(e + " doesn't exist!");
	}


}
