package ca.uwaterloo.gsd.algo;

import java.util.HashSet;
import java.util.Set;

import ca.uwaterloo.gsd.fm.BasicGraph;

public class TransitiveReduction {
	private TransitiveReduction() {}
	
	public static TransitiveReduction INSTANCE = new TransitiveReduction();
	
	public <V,E> void reduce(BasicGraph<V,E> g) {
		for (V v : g.vertices()) {
			_transitiveReduction(v, g);
		}
	}
	
	/**
	 * Copied from {@link dk.itu.fds.Graph}
	 */
	private <V,E> void _transitiveReduction(V v, BasicGraph<V,E> g) {
		Set<V> needless = descendants(v, g);

		Set<V> desc = new HashSet<V>();
		for (V u : g.children(v))
			desc.addAll(descendants(u, g));
		
		needless.retainAll (desc);

		for (V u : needless){
			E edge = g.findEdge(u, v);
			g.removeEdge(edge);
		}
	}
	

	public static <V,E> Set<V> descendants(V v, BasicGraph<V,E> g) {
		HashSet<V> result = new HashSet<V> ();
		for (V u : g.children(v)) {
			result.add(u);
			result.addAll (descendants(u, g));
		}
		return result;
	}

}
