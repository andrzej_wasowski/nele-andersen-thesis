tree grammar FmTreeParser;

options {
  tokenVocab = Fm;
  language = Java;
  ASTLabelType = CommonTree;
}

@header {
  package ca.uwaterloo.gsd.fm.grammar;
  import ca.uwaterloo.gsd.fm.*;
  import java.util.List;
  import java.util.ArrayList;
  import java.util.Collection;
  import java.util.Map;
  import java.util.HashMap;
  import java.util.Set;
  import java.util.HashSet;
}

@members {
  Map<String, FeatureNode<String>> _nameToGroup = new HashMap<String, FeatureNode<String>>();

  private List<String> asList(List<CommonTree> tree) {
      List<String> groupIds = new ArrayList<String>(tree.size());
      for (CommonTree node : tree) {
        groupIds.add(node.getToken().getText());
      }
      return groupIds;
  }

  private boolean isGroupValid(FeatureNode<String> root, List<String> groupIds) {
      for (String s : groupIds) {
        if (root.features().equals(root)) {
          return false;
        }
      }
      return true;
  }
}

// Alter code generation so catch-clauses get replace with
// this action.
@rulecatch {
  catch (RecognitionException except) {
   throw except;
  }
}

input returns [FeatureModel<String> result]
@init {
  $result = new FeatureModel<String>();
}
    :
    (
      production[$result.getDiagram()]
    | e=expr
      { $result.getConstraints().add($e.value); }
    )+
    ;

production[FeatureGraph<String> g]
    : ^(RULE id=IDENTIFIER
	      {
	        if ($g.vertices().size() == 0)
	          $g.addVertex(new FeatureNode<String>($id.text));

	        //Check if there is a named AND-group
	        FeatureNode<String> parent = _nameToGroup.containsKey($id.text) ?
                _nameToGroup.get($id.text) : $g.findNode($id.text);
	      }
	     //Check if the parent node has been previously specified
       {_nameToGroup.containsKey($id.text) || $g.findNode($id.text) != null}?
       feature[parent, $g]+
      )
    ;

feature[FeatureNode<String> parent, FeatureGraph<String> g]
    : ^(FEATURE id=IDENTIFIER isOpt='?'?)
    {
      FeatureNode<String> f = new FeatureNode<String>($id.text);

      $g.addVertex(f);
      $g.addEdge(f, $parent, FeatureEdge.OPTIONAL);

      if ($isOpt == null)
        g.addEdge(f, $parent, FeatureEdge.MANDATORY);
    }


    | ^(GROUP groupNodes+=IDENTIFIER+ isOr=PLUS? isMutex=OPT?)
    {
      Set<FeatureNode<String>> members = new HashSet<FeatureNode<String>>();
      for (String s : asList($groupNodes)) {
        FeatureNode<String> v = _nameToGroup.containsKey(s)
              ? _nameToGroup.get(s) : new FeatureNode<String>(s);

         $g.addVertex(v);
         $g.addEdge(v, $parent, FeatureEdge.OPTIONAL);
         members.add(v);
      }

      $g.addEdge(members, $parent,
          $isOr != null
              ? FeatureEdge.OR : $isMutex != null
              ? FeatureEdge.MUTEX : FeatureEdge.XOR);
    }
    {isGroupValid($parent, asList($groupNodes)) }?


    | ^(GROUP groupNodes+=IDENTIFIER+ '&' name=ASSIGN?)
    {
      Set<String> idSet = new HashSet<String>(asList($groupNodes));
      FeatureNode<String> and = new FeatureNode<String>(idSet);
      $g.addVertex(and);
      $g.addEdge(and, $parent, FeatureEdge.OPTIONAL);
      if ($name != null) {
        _nameToGroup.put($name.text, and);
      }
    }
    {idSet.size() == $groupNodes.size()}?
    ;



expr returns [Expression value]
    : ^('|' a=expr b=expr)
    {
      $value = new Expression<String>(ExpressionType.OR, $a.value, $b.value);
    }
    | ^('&' a=expr b=expr)
    {
      $value = new Expression<String>(ExpressionType.AND, $a.value, $b.value);
    }
    | ^('->' a=expr b=expr)
        {
      $value = new Expression<String>(ExpressionType.IMPLIES, $a.value, $b.value);
    }
    | ^('!' a=expr)
        {
      $value = new Expression<String>(ExpressionType.NOT, $a.value, null);
    }
    | id=IDENTIFIER
    {
      if ($id.text.equals("1")) {
        $value = new Expression<String>(ExpressionType.TRUE);
      }
      else if ($id.text.equals("0")) {
        $value = new Expression<String>(ExpressionType.FALSE);
      }
      else {
        $value = new Expression<String>($id.text);
      }
    }
    ;
