package formula.dnf;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.apache.commons.collections15.BidiMap;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import ca.uwaterloo.gsd.fm.Expression;
import ca.uwaterloo.gsd.fm.ExpressionType;
import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureNode;
import ca.uwaterloo.gsd.fm.ImplicationGraph;
import fds.GraphBuilder;
import formula.Clause;
import formula.Formula;
import formula.FormulaBuilder;

/**
 *  This class makes it possible to create a DNF from a feature model (FeatureModel). 
 *  Each feature of the feature model must be present in the feature diagram. The DNFBuilder
 *  contains a two-way mapping of the actual features to the variables used in the
 *  DNF representation. This class is inspired by ca.uwaterloo.gsd.fm.sat.SATBuilder
 */
public class DNFBuilder<T> extends FormulaBuilder<T>{

	public DNFBuilder(T synth) {
		_featToVar = new DualHashBidiMap<T, Integer>();
		_synth = synth;
	}
	
	public DNFBuilder(Collection<T> vars, T synth) {
		this(synth);
		for (T v : vars)
			add(v);
	}
	
	public DNFBuilder(Collection<T> vars){
		this(vars, null);
	}

	private Set<Set<Integer>> clauses;
	// maps variables to clauses in which it appears, used when traversing the feature diagram.
	private Map<Integer,Set<Set<Integer>>> table = new Hashtable<Integer,Set<Set<Integer>>>();
	// A list containing the extra constraints.
	List<DNFClause> constraints = new LinkedList<DNFClause>();
	private void addToTable(int i, Set<Integer> clause){
		if(table.get(i) == null){
			Set<Set<Integer>> set = new HashSet<Set<Integer>>();
			set.add(clause);
			table.put(i, set);
		}
		else{
			table.get(i).add(clause);
		}
			
	}
	
	private void mkRoot(FeatureGraph<T> g){
		Set<Integer> clause = new HashSet<Integer>();
		for(T f : g.root().features())
			clause.add(variable(f));
		clauses.add(clause);
		for(int i : clause){
			addToTable(i, clause);
		}
	}
	
	public DNF mkModel(FeatureGraph<T> g) {
		clauses = new HashSet<Set<Integer>>();
		mkRoot(g);
		// traverse the graph to enumerate all legal configurations
		travers(g, g.root());
		Set<DNFClause> dnfClauses = new HashSet<DNFClause>();
		clauses.add(new HashSet<Integer>());
		for(Set<Integer> set : clauses){	
			for(int i : _featToVar.values()){
				if(!set.contains(i))
					set.add(-i);
			}
			dnfClauses.add(new DNFClause(set));
		}
		return new DNF(dnfClauses);
	}
	
	public DNF mkModel(FeatureModel<T> fm){
		DNF dnf = mkModel(fm.getDiagram());
		//convert constraints to conjunctions and negate them
		for (Expression<T> expr : fm.getConstraints()) {
			constraints.addAll(mkExpression(expr));
		}
		Set<DNFClause> clauses = new HashSet<DNFClause>();
		for(Clause c : dnf.getClauses())
			clauses.add((DNFClause) c);
		Set<Clause> obsolete = new HashSet<Clause>();
		for(DNFClause c : constraints){
			for(Clause clause : clauses){
				if(clause.contains(c)){
					obsolete.add(clause);
				}
			}
			clauses.removeAll(obsolete);
			obsolete.clear();
		}
		return new DNF(clauses);		
	}

	
	
	private Stack<DNFClause> mkExpression(Expression<T> expr) {
		Stack<DNFClause> vec = new Stack<DNFClause>();		
		for (Expression<T> split : splitConjunction(expr)) {
		
			Set<Integer> result = new HashSet<Integer>();
			mkClause_Rec(split, result);
					
			vec.push(new DNFClause(result));
		}
		return vec;
	}
	
	private void mkClause_Rec(Expression<T> expr, Set<Integer> result) {
		if (expr == null) return;
		assert expr.getType() != ExpressionType.AND;
		
		switch (expr.getType()) {
		case OR:
			mkClause_Rec(expr.getLeft(), result);
			mkClause_Rec(expr.getRight(), result);
			break;
		case NOT:
			assert expr.getLeft().getType() == ExpressionType.FEATURE;
			// not negated feature
			result.add(variable(expr.getLeft().getFeature()));
			break;
		case FEATURE:
			//negated feature...
			result.add(-variable(expr.getFeature()));
			break;
		case IMPLIES:
			mkClause_Rec(new Expression<T>(ExpressionType.OR, expr.getLeft().not(), expr.getRight()), result);
			break;
		default:
			throw new UnsupportedOperationException("Only operators alowed are AND, OR, NOT and IMPLICATION!");
		}
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Expression<T>> splitConjunction(Expression<T> e) {
		if (e == null) return Collections.emptyList();
		else if (e.getType() == ExpressionType.AND) {
			return CollectionUtils.union(
					splitConjunction(e.getLeft()),
					splitConjunction(e.getRight()));
		}
		else {
			return Arrays.asList( e );
		}
	}
	
	private void travers(FeatureGraph<T> g, FeatureNode<T> v){
		Collection<FeatureEdge> edges = g.incomingEdges(v);
		Collection<FeatureEdge> cardEdges = g.selectCardinalityEdges();
		cardEdges.retainAll(edges);
		for(FeatureEdge edge : cardEdges){
			for(FeatureNode<T> node : g.getSources(edge)){
				if(g.findEdge(node, v) != null)
					edges.remove(g.findEdge(node, v));
			}
		}
			
		for(FeatureEdge e : edges){
			switch (e.getType()) {
			case FeatureEdge.MANDATORY:
				mkMandatory(g.getSource(e), g.getTarget(e));
				break;
			case FeatureEdge.OPTIONAL:
				mkOptional(g.getSource(e), g.getTarget(e));
				break;
			case FeatureEdge.OR:
				mkOrGroup(g.getSources(e), g.getTarget(e));
				break;
			case FeatureEdge.MUTEX:
				mkMutex(g.getSources(e), g.getTarget(e));
				break;
			case FeatureEdge.XOR:
				mkXorGroup(g.getSources(e), g.getTarget(e));
				break;
			}
		}
		for(FeatureNode<T> child : g.children(v))
			travers(g, child);
	}
	
	/**
	 * Adds the variables corresponding to each feature in source to the clauses
	 * containing the variable corresponding to target. 
	 * @param source: mandatory subfeature node of target
	 * @param target
	 */
	private void mkMandatory(FeatureNode<T> source, FeatureNode<T> target) {
		for(Set<Integer> clause : table.get(variable(target.features().iterator().next()))) {
			for(T f : source.features()){
				clause.add(variable(f));
				addToTable(variable(f), clause);
			}
		}
	}
	
	/**
	 * Adds a new clause for each clause containing a variable corresponding to target.
	 * This clause will contain all variables corresponding to the features contained in
	 * the feature node source.
	 * @param source optional subfeature node of target.
	 * @param target
	 */
	private void mkOptional(FeatureNode<T> source, FeatureNode<T> target) {
		Set<Set<Integer>> newClauses = new HashSet<Set<Integer>>();
		for(Set<Integer> clause : table.get(variable(target.features().iterator().next()))){
			Set<Integer> copy = new HashSet<Integer>(clause);
			for(T f : source.features())
				copy.add(variable(f));
			newClauses.add(copy);
		}
		for(Set<Integer> clause : newClauses){
			for(int i : clause)
				addToTable(i, clause);
		}
		clauses.addAll(newClauses);
	}
	
	/**
	 * Computes the power set of the nodes contained in sources. For each clause containing
	 * a variable corresponding to target and for each set in the power set a new clause is
	 * constructed.
	 * a new clause is constructed
	 * @param sources a set of nodes forming an or-group of the target node.
	 * @param target
	 */
	private void mkOrGroup(Set<FeatureNode<T>> sources, FeatureNode<T> target){
		Set<Set<Integer>> obsolete = new HashSet<Set<Integer>>();
		for(Set<Integer> clause : table.get(variable(target.features().iterator().next())))
			obsolete.add(clause);
		Set<Set<FeatureNode<T>>> powerSet = new HashSet<Set<FeatureNode<T>>>();
		
		powerSet.add(new HashSet<FeatureNode<T>>());
		for(FeatureNode<T> node : sources){
			Set<Set<FeatureNode<T>>> tmp = new HashSet<Set<FeatureNode<T>>>();
			for(Set<FeatureNode<T>> s1 : powerSet){
				Set<FeatureNode<T>> s2 = new HashSet<FeatureNode<T>>(s1);
				s2.add(node);
				tmp.add(s2);
			}
			powerSet.addAll(tmp);
		}
		
		// to avoid removing the obsolete clauses.
		Iterator<Set<FeatureNode<T>>> itr = powerSet.iterator();
		Set<FeatureNode<T>> s1 = itr.next();
		itr.remove();
		if(s1.isEmpty()){
			s1 = itr.next();
			itr.remove();
		}
		
		for(Set<FeatureNode<T>> s2 : powerSet){
			if(!s2.isEmpty()){
				for(Set<Integer> obs : obsolete){
					Set<Integer> clause = new HashSet<Integer>(obs);
					for(FeatureNode<T> node : s2){
						for(T f : node.features())
							clause.add(variable(f));
					}
					for(int i : clause)
						addToTable(i, clause);
					clauses.add(clause);
				}
			}
		}
		
		for(Set<Integer> clause : obsolete){
			for(FeatureNode<T> node : s1){
				for(T f : node.features()){
					clause.add(variable(f));
					addToTable(variable(f), clause);
				}
			}
			//clauses.add(clause);
		}
	}
	
	/**
	 * hmm
	 * @param sources
	 * @param target
	 */
	private void mkXorGroup(Set<FeatureNode<T>> sources, FeatureNode<T> target){
		// all clauses containing target but no element in sources.
		Set<Set<Integer>> obsolete = new HashSet<Set<Integer>>();
		// all clauses containing more than one element from sources.
		Set<Set<Integer>> contradictions = new HashSet<Set<Integer>>();
		// all nodes which all ready occur in a clause containing target
		Set<FeatureNode<T>> nodes = new HashSet<FeatureNode<T>>();
		Set<Integer> variables = new HashSet<Integer>();
		for(FeatureNode<T> node : sources){
			variables.add(variable(node.features().iterator().next()));
		}
		//check for contradictions.
		for(Set<Integer> clause : table.get(variable(target.features().iterator().next()))){
			Set<Integer> intersection = new HashSet<Integer>(clause);
			intersection.retainAll(variables);
			if(intersection.size() > 1){
				contradictions.add(clause);
			}	
		}
		// and remove them
		clauses.removeAll(contradictions);
		for(Set<Integer> clause : contradictions){
			for(int i : clause){
				table.get(i).remove(clause);
			}
		}
		
		for(Set<Integer> clause : table.get(variable(target.features().iterator().next()))){
			boolean obs = true;
			for(FeatureNode<T> node : sources){
				if(clause.contains(variable(node.features().iterator().next()))){
					nodes.add(node);
					obs = false;
					break;
				}
			}
			if(obs)
				obsolete.add(clause);
		}

		sources.removeAll(nodes);
		Iterator<FeatureNode<T>> itr = sources.iterator();
		FeatureNode<T> n1 = itr.next();
		itr.remove();
		for(FeatureNode<T> n2 : sources){
			for(Set<Integer> obs : obsolete){
				Set<Integer> clause = new HashSet<Integer>(obs);
				for(T f : n2.features()){
					clause.add(variable(f));
				}
				for(int i : clause)
					addToTable(i, clause);
				clauses.add(clause);
			}	
		}
		for(Set<Integer> clause : obsolete){
			for(T f : n1.features()){
				clause.add(variable(f));
				addToTable(variable(f), clause);
			}
			//clauses.add(clause);
		}
	}	
	
	/**
	 * Like an optional xor-group
	 * @param sources
	 * @param target
	 */
	private void mkMutex(Set<FeatureNode<T>> sources, FeatureNode<T> target){
		Set<Set<Integer>> obsolete = new HashSet<Set<Integer>>();
		Set<FeatureNode<T>> nodes = new HashSet<FeatureNode<T>>();
		for(Set<Integer> clause : table.get(variable(target.features().iterator().next()))){
			boolean obs = true;
			for(FeatureNode<T> node : sources){
				if(clause.contains(variable(node.features().iterator().next()))){
					nodes.add(node);
					obs = false;
					break;
				}
			}
			if(obs)
				obsolete.add(clause);

		}
		
		
		for(FeatureNode<T> n2 : sources){
			if(nodes.contains(n2))
				continue;
			for(Set<Integer> obs : obsolete){
				Set<Integer> clause = new HashSet<Integer>(obs);
				for(T f : n2.features()){
					clause.add(variable(f));
				}
				for(int i : clause)
					addToTable(i, clause);
				clauses.add(clause);
			}	
		}
	}

	public ImplicationGraph<T> buildImplicationGraph(Formula formula){
		return GraphBuilder.buildImplicationGraph((DNF) formula, this);
	}
	
	public UndirectedGraph<FeatureNode<T>, DefaultEdge> buildMutexGraph(FeatureGraph<T> g, Formula formula){
		return GraphBuilder.buildMutexGraph(g, (DNF) formula, this);
	}
}
