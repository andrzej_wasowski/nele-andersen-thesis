package formula;

import org.apache.commons.collections15.BidiMap;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureNode;
import ca.uwaterloo.gsd.fm.ImplicationGraph;

/**
 *  This is an abstract class implementing common methods of the CNFBuilder and DNFBuilder,
 *  which makes it possible to create a CNF/DNF from a feature model (FeatureModel).
 *  It contains a two-way mapping of the actual features to the variables used in the
 *  CNF/DNF representation. This class is inspired by ca.uwaterloo.gsd.fm.sat.SATBuilder
 */
public abstract class FormulaBuilder<T> {

	// Two-way map, mapping features to variables
	protected BidiMap<T, Integer> _featToVar;
	// the synthetic root.
	protected T _synth;
	//current variable number.
	protected int i=1;

	
	public BidiMap<Integer, T> getFeatureMap(){
		return _featToVar.inverseBidiMap();
	}
	
	/**
	 * adds a feature to the builder and assigns a variable number to the feature
	 * @param o : the feature to be added
	 * @return the variable corresponding to the feature
	 */
	public int add(T o) {
		assert o != null;
		assert !_featToVar.containsKey(o);
		int var = i++;
		_featToVar.put(o, var);
		return var;
	}
	
	public boolean contains(T t){
		return _featToVar.containsKey(t);
	}
	
	public int variable(T feat) {
		if (!_featToVar.containsKey(feat)) 
			throw new IllegalArgumentException(feat + " does not exist in map!");
		return _featToVar.get(feat);
	}

	public T feature(int i) {
		return _featToVar.inverseBidiMap().get(i);
	}
	
	/**
	 * @return null if the synthetic root has not been defined.
	 */
	public T syntheticRoot(){
		return _synth;
	}
	
	public abstract Formula mkModel(FeatureModel<T> fm);
	
	public abstract Formula mkModel(FeatureGraph<T> g);
	
	public abstract ImplicationGraph<T> buildImplicationGraph(Formula formula);
	
	public abstract UndirectedGraph<FeatureNode<T>, DefaultEdge> buildMutexGraph(FeatureGraph<T> g, Formula formula);
	

	
	

}
