package formula.cnf;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;
import org.apache.commons.collections15.iterators.LoopingIterator;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import ca.uwaterloo.gsd.fm.Expression;
import ca.uwaterloo.gsd.fm.ExpressionType;
import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureNode;
import ca.uwaterloo.gsd.fm.ImplicationGraph;
import fds.GraphBuilder;
import formula.Formula;
import formula.FormulaBuilder;

/**
 *  This class makes it possible to create a CNF from a feature model (FeatureModel)
 *  It contains a two-way mapping of the actual features to the variables used in the
 *  CNF representation. This class is inspired by ca.uwaterloo.gsd.fm.sat.SATBuilder
 */
public class CNFBuilder<T> extends FormulaBuilder<T>{

	public CNFBuilder(T synth) {
		_featToVar = new DualHashBidiMap<T, Integer>();
		_synth = synth;
	}
	
	public CNFBuilder(Collection<T> vars, T synth) {
		this(synth);
		for (T v : vars)
			add(v);
	}
	
	public CNFBuilder(T[] vars, T synth) {
		this(synth);
		for (T v : vars)
			add(v);
	}
	
	public CNFBuilder(Collection<T> vars){
		this(vars, null);
	}

	/**
	 * Creates a CNF from the feature model
	 * @param fm : the feature model
	 * @return a CNF corresponding to the feature model fm 
	 */
	public CNF mkModel(FeatureModel<T> fm){
		CNF _cnf = mkModel(fm.getDiagram());
		
		for (Expression<T> expr : fm.getConstraints()) {
			List<CNFClause> clauses = mkExpression(expr);
			_cnf.addAllClauses(clauses);
		}
		return _cnf;
	}
	
	/**
	 * Creates a CNF from a feature graph
	 * @param g : the feature graph
	 * @return a CNF corresponding to the feature graph g
	 */
	public CNF mkModel(FeatureGraph<T> g) {
		CNF _cnf = new CNF();
		_cnf.addAllClauses( mkHierarchy(g) );
		_cnf.addAllClauses( mkAndGroups(g) );
		_cnf.addAllClauses( mkCardinality(g) );
		return _cnf;
	}
	
	@SuppressWarnings("unchecked")
	private Collection<Expression<T>> splitConjunction(Expression<T> e) {
		if (e == null) return Collections.emptyList();
		else if (e.getType() == ExpressionType.AND) {
			return CollectionUtils.union(
					splitConjunction(e.getLeft()),
					splitConjunction(e.getRight()));
		}
		else
			return Arrays.asList( e );
	}
	
	private Stack<CNFClause> mkExpression(Expression<T> expr) {
		Stack<CNFClause> vec = new Stack<CNFClause>();		
		for (Expression<T> split : splitConjunction(expr)) {
		
			LinkedList<Integer> result = new LinkedList<Integer>();
			mkClause_Rec(split, result);
			
			int[] arr = new int[result.size()];
			int i=0;
			for (int next : result) {
				arr[i++] = next;
			}
			
			vec.push(new CNFClause(arr));
		}
		return vec;
	}
	
	private void mkClause_Rec(Expression<T> expr, List<Integer> result) {
		if (expr == null) return;
		assert expr.getType() != ExpressionType.AND;
		
		switch (expr.getType()) {
		case OR:
			mkClause_Rec(expr.getLeft(), result);
			mkClause_Rec(expr.getRight(), result);
			break;
		case NOT:
			assert expr.getLeft().getType() == ExpressionType.FEATURE;
			result.add(variable(expr.getLeft().getFeature()) * -1);
			break;
		case FEATURE:
			result.add(variable(expr.getFeature()));
			break;
		case IMPLIES:
			mkClause_Rec(new Expression<T>(ExpressionType.OR, expr.getLeft().not(), expr.getRight()), result);
			break;
		default:
			throw new UnsupportedOperationException("Only operators alowed are AND, OR, NOT and IMPLICATION!");
		}
	}
	
	public Stack<CNFClause> mkAndGroups(FeatureGraph<T> g) {
		Stack<CNFClause> result = new Stack<CNFClause>();
		for (FeatureNode<T> v : g.selectAndNodes()) {
			LoopingIterator<T> iter = new LoopingIterator<T>(v.features());
			T start = iter.next();
			T prev = start;
			do {
				T next = iter.next();
				int ante = variable(prev) * -1;
				int cons = variable(next);
				result.push(new CNFClause(new int[] { ante, cons} ));
				prev = next;
			} while (prev != start);
		}
		return result;
	}
	
	public CNFClause mkRoot(FeatureGraph<T> g) {
		T f = g.root().features().iterator().next();
		return new CNFClause(new int[] { variable(f) } );
	}

	public List<CNFClause> mkHierarchy(FeatureGraph<T> g) {
		Stack<CNFClause> result = new Stack<CNFClause>();
		for (FeatureEdge e : g.selectHierarchyEdges()) {
			FeatureNode<T> source = g.getSource(e);
			FeatureNode<T> target = g.getTarget(e);

			for (T sf : source.features()) {
				for (T tf : target.features()) {
					int ante = variable(sf) * -1;
					int cons = variable(tf);
					result.push(new CNFClause(new int[] {ante, cons} ));
				}
			}
		}
		return result;
	}

	public List<CNFClause> mkCardinality(FeatureGraph<T> g) {
		Stack<CNFClause> result = new Stack<CNFClause>();
		for (FeatureEdge e : g.selectCardinalityEdges()) {
			switch (e.getType()) {
			case FeatureEdge.OR:
				combine( result, mkOrGroup(g.getSources(e), g.getTarget(e)) );
				break;
			case FeatureEdge.MUTEX:
				combine ( result, mkMutex(g.getSources(e)) );
				break;
			case FeatureEdge.XOR:
				combine ( result, mkOrGroup(g.getSources(e), g.getTarget(e)) );
				combine ( result, mkMutex(g.getSources(e)) );
				break;
			case FeatureEdge.MANDATORY:
				combine ( result, mkMandatory(g.getSource(e), g.getTarget(e)) );
				break;
			}
		}
		return result;
	}
	
	private CNFClause mkMandatory(FeatureNode<T> source, FeatureNode<T> target) {
		int ante = variable(target.features().iterator().next()) * -1;
		int cons = variable(source.features().iterator().next());
		return new CNFClause(new int[] { ante, cons} );
	}

	private CNFClause mkOrGroup(Set<FeatureNode<T>> sources, FeatureNode<T> target) {
		int[] clause = new int[sources.size() + 1];
		clause[0] = variable(target.features().iterator().next()) * -1;
		Iterator<FeatureNode<T>> iter = sources.iterator();
		for (int i=1; i<clause.length; i++)
			clause[i] = variable(iter.next().features().iterator().next());
		
		return new CNFClause(clause);
	}
	
	private Stack<CNFClause> mkMutex(Set<FeatureNode<T>> sources) {
		Stack<CNFClause> result = new Stack<CNFClause>();
		for(FeatureNode<T> node1 : sources){
			for(FeatureNode<T> node2 : sources){
				if(node1 != node2){
					int ante = variable(node1.features().iterator().next()) * -1;
					int cons = variable(node2.features().iterator().next()) * -1;
					result.push(new CNFClause(new int[] { ante, cons} ));
				}
			}
		}		
		return result;
	}
	
	private List<CNFClause> combine(Stack<CNFClause> base, Stack<CNFClause> merge) {
		Iterator<CNFClause> iter = merge.iterator();
		while (iter.hasNext())
			base.push(iter.next());
		return base;
	}
	
	private List<CNFClause> combine(Stack<CNFClause> base, CNFClause merge) {
		base.push(merge);
		return base;
	}

	public ImplicationGraph<T> buildImplicationGraph(Formula formula) {
		return GraphBuilder.buildImplicationGraph((CNF) formula, this);
	}
	
	public UndirectedGraph<FeatureNode<T>, DefaultEdge> buildMutexGraph(FeatureGraph<T> g, Formula formula){
		return GraphBuilder.buildMutexGraph(g, (CNF) formula, this);
	}	
	
}
