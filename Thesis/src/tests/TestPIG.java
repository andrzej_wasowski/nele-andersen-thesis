package tests;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;
import net.sf.javabdd.BDD;

import org.junit.Test;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.TimeoutException;

import converter.CNFConverter;
import converter.CNFParser;

import pig.PIG;
import ca.uwaterloo.gsd.fds.BDDBuilder;
import ca.uwaterloo.gsd.fds.FDSFactory;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureModelParser;
import ca.uwaterloo.gsd.fm.FeatureNode;
import ca.uwaterloo.gsd.merge.SemanticOperations;
import dk.itu.fds.Implicant;
import dk.itu.fds.PrimeImplicants;
import dk.itu.fds.PrimeImplicantsOptimized;
import formula.Clause;
import formula.cnf.CNF;
import formula.cnf.CNFBuilder;
import formula.cnf.CNFClause;

public class TestPIG {
	
	@Test
	public void testPrimeImplicates() throws IOException, ContradictionException, TimeoutException, InterruptedException{
		
		File dir = new File("tests/pig_test/fm/");
		for(String file : dir.list()){
			System.out.println(file);
			Assert.assertTrue(comparePrimes(FeatureModelParser.parseFile(dir + "/" + file)));
		}
	
		dir = new File("tests/pig_test/dimacs/aim/50/"); 
		for(String file : dir.list())
			Assert.assertTrue(comparePrimes(CNFParser.parseInstance(dir + "/" + file)));
	}

	private boolean comparePrimes(FeatureModel<String> fm){
		
		//construct cnf
		CNFBuilder<String> cnfBuilder = new CNFBuilder<String>(fm.features(), "Synth");
		CNF cnf = cnfBuilder.mkModel(fm);
		cnf.removeDeadFeatures();
	
		//construct bdd
		BDDBuilder<String> bddBuilder = FDSFactory.makeStringBDDBuilder();
		BDD bdd = bddBuilder.mkStructure(fm);
		bdd = SemanticOperations.removeDeadFeatures(bdd);
		
		//compute prime implicates/implicants assuming the root is true.
		FeatureNode<String> root = fm.getDiagram().root();
		
		cnf.addClause(new CNFClause(new int[]{cnfBuilder.variable(root.getFeature())}));
		CNF primesCNF = new PIG(cnf).getPrimeImplicates();
		
		BDD b = bdd.getFactory().nithVar(bddBuilder.variable(root.getFeature()));
		PrimeImplicants primesBDD = new PrimeImplicantsOptimized (b,bdd);
	
		Set<Set<Integer>> cnfSet = new HashSet<Set<Integer>>();
		for(Clause c : primesCNF){
			if(c.isPositive()){
				Set<Integer> tmp = new HashSet<Integer>();
				for(int j : c)
					tmp.add(j);
				cnfSet.add(tmp);
			}
		}
		
		Set<Set<Integer>> bddSet = new HashSet<Set<Integer>>();
		for(Implicant p : primesBDD){
			Set<Integer> tmp = new HashSet<Integer>();
			for(int j : p)
				if(j > 0)
					tmp.add(-1 * cnfBuilder.variable(bddBuilder.feature(j)));
				else
					tmp.add(cnfBuilder.variable(bddBuilder.feature(-j)));
			bddSet.add(tmp);
		}
		
		for(Set<Integer> set : cnfSet){
			if(!bddSet.contains(set))
				return false;
		}
		return true;
	}
	
	private boolean comparePrimes(CNF cnf) throws ContradictionException, TimeoutException{
		cnf.removeDeadFeatures();
		for(int i = 1 ; i <= 20; i++){
			CNF clone = cnf.clone();
			BDD bdd = CNFConverter.toBDD(clone);
			
			cnf.addClause(new CNFClause(new int[]{i}));
			CNF primesCNF = new PIG(clone).getPrimeImplicates();
			
			BDD b = bdd.getFactory().nithVar(i);
			PrimeImplicants primesBDD = new PrimeImplicantsOptimized (b,bdd);

			for(Clause c : primesCNF){
				boolean contains = false;
				for(Implicant imp : primesBDD){
					if (compare(c, imp)){
						contains = true;
						break;
					}
				}
				if(contains == false)
					return false;
			}
		}
		return true;
	}
		
	private boolean compare(Clause c, Implicant imp){
		if(imp.size() != c.size())
			return false;
		for(int i : c){
			if(!imp.contains(-i))
				return false;
		}
		return true;
	}

}
