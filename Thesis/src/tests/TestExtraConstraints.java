package tests;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.sat4j.specs.TimeoutException;

import pig.PIG;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureModelParser;
import extensions.ExtraConstraints;
import fds.FGBuilder;
import formula.Clause;
import formula.cnf.CNF;
import formula.cnf.CNFBuilder;
import formula.cnf.CNFClause;

public class TestExtraConstraints {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testExtraConstraints() throws IOException, TimeoutException{
		File dir = new File("tests/extraconstraints_test/");
		for(String file : dir.list()){
			System.out.println("File " + file);
			FeatureModel<String> fm = FeatureModelParser.parseFile(dir + "/" + file);
			//construct CNF
			CNFBuilder<String> cnfBuilder = new CNFBuilder<String>(fm.features(), null);
			CNF original = cnfBuilder.mkModel(fm);
			original.removeDeadFeatures();
			//build feature graph
			FGBuilder<String> fgBuilder = new FGBuilder<String>(cnfBuilder);
			FeatureGraph<String> g = fgBuilder.build(original);
			//compute extra constraints
			Set<Clause> constraints = ExtraConstraints.compute(original.clone(), cnfBuilder, g);
			System.out.println(constraints);
			// construct CNF representing the feature graph
			CNF synth = cnfBuilder.mkModel(g);
			//add constraints to synth
			for(Clause c : constraints){
				synth.addClause((CNFClause) c);
			}
			//compute prime implicates of both formulas and compare them
			CNF orgPrimes = new PIG(original).getPrimeImplicates();
			System.out.println(orgPrimes);
			CNF synthPrimes = new PIG(synth).getPrimeImplicates();
			System.out.println(synthPrimes);
			for(Clause c : orgPrimes){
				Assert.assertTrue(synthPrimes.containsClause((CNFClause) c));
			}
			for(Clause c : synthPrimes){
				Assert.assertTrue(orgPrimes.containsClause((CNFClause) c));
			}
			
		}
	}
	
	
}
