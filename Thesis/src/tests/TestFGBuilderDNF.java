package tests;

import java.io.File;

import junit.framework.Assert;

import org.junit.Test;

import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureModelParser;
import fds.FGBuilder;
import fds.FGBuilderOptimized;
import formula.cnf.CNF;
import formula.cnf.CNFBuilder;
import formula.dnf.DNF;
import formula.dnf.DNFBuilder;

public class TestFGBuilderDNF {

	@Test
	public void compareFeatureGraphs() throws Exception{
		File dir = new File("tests/fgbuilder_test/dnf/fm");
		for(String file : dir.list()){
			System.out.println(file);
			Assert.assertTrue(compareFeatureGraphsOptimized(dir + "/" + file));
			Assert.assertTrue(compareFeatureGraphs(dir + "/" + file));
		}
	}
	
	private boolean compareFeatureGraphs(String file) throws Exception {
		
		FeatureModel<String> fm = FeatureModelParser.parseFile(file);
		
		//construct cnf
		CNFBuilder<String> cnfBuilder = new CNFBuilder<String>(fm.features(), "Synth");
		CNF cnf = cnfBuilder.mkModel(fm);
		
		//construct dnf
		DNFBuilder<String> dnfBuilder = new DNFBuilder<String>(fm.features(), "Synth");
		DNF dnf = dnfBuilder.mkModel(fm);
		
		//init builders
		FGBuilder<String> fgBuilderCNF = new FGBuilder<String>(cnfBuilder);
		FGBuilder<String> fgBuilderDNF = new FGBuilder<String>(dnfBuilder);	
	
		// build graphs
		FeatureGraph<String> g = fgBuilderDNF.build(dnf);
		FeatureGraph<String> h = fgBuilderCNF.build(cnf);

		return g.equals(h);
	
	}
	
	private boolean compareFeatureGraphsOptimized(String file) throws Exception {
		
		FeatureModel<String> fm = FeatureModelParser.parseFile(file);
		
		//construct cnf
		CNFBuilder<String> cnfBuilder = new CNFBuilder<String>(fm.features(), "Synth");
		CNF cnf = cnfBuilder.mkModel(fm);
		
		//construct dnf
		DNFBuilder<String> dnfBuilder = new DNFBuilder<String>(fm.features(), "Synth");
		DNF dnf = dnfBuilder.mkModel(fm);
		
		//init builders
		FGBuilderOptimized<String> fgBuilderCNF = new FGBuilderOptimized<String>(cnfBuilder);
		FGBuilderOptimized<String> fgBuilderDNF = new FGBuilderOptimized<String>(dnfBuilder);	
		
		//build graphs
		FeatureGraph<String> g = fgBuilderCNF.build(cnf);
		FeatureGraph<String> h = fgBuilderDNF.build(dnf);
		
		return g.equals(h);
		
	}
	
	
}
