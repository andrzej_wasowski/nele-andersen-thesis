package tests;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import formula.cnf.CNFClause;
import formula.dnf.DNFClause;

public class TestDNFClause {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testHashCode() {
		Assert.assertEquals(new DNFClause(new int[]{1,2,3}).hashCode(), new DNFClause(new int[]{1,2,3}).hashCode());
		Assert.assertEquals(new DNFClause(new int[]{1,2,-3}).hashCode(), new DNFClause(new int[]{1,2,-3}).hashCode());
		Assert.assertEquals(new DNFClause(new int[]{3,2,1}).hashCode(), new DNFClause(new int[]{1,2,3}).hashCode());
		Assert.assertEquals(new DNFClause(new int[]{1,3,2}).hashCode(), new DNFClause(new int[]{1,2,3}).hashCode());
	}

	@Test
	public void testDifference() {
		Assert.assertEquals(new DNFClause(), new DNFClause(new int[]{1,2,3}).difference(new DNFClause(new int[]{1,2,3})));
		Assert.assertEquals(new DNFClause(new int[]{2,3}), new DNFClause(new int[]{1,2,3}).difference(new DNFClause(new int[]{1})));
		Assert.assertEquals(new DNFClause(new int[]{1,2,3}), new DNFClause(new int[]{1,2,3}).difference(new DNFClause(new int[]{-1})));
		Assert.assertEquals(new DNFClause(new int[]{1,2}), new DNFClause(new int[]{1,2,3}).difference(new DNFClause(new int[]{3,4})));
		Assert.assertEquals(new DNFClause(new int[]{1,2,3}), new DNFClause(new int[]{1,2,3}).difference(new DNFClause()));
	}

	@Test
	public void testEqualsObject() {
		Assert.assertTrue(new DNFClause(new int[]{1,2,3}).equals(new DNFClause(new int[]{1,2,3})));
		Assert.assertTrue(new DNFClause(new int[]{3,2,1}).equals(new DNFClause(new int[]{1,2,3})));
		Assert.assertTrue(new DNFClause(new int[]{1,3,2}).equals(new DNFClause(new int[]{1,2,3})));
		Assert.assertFalse(new DNFClause(new int[]{1,2,3}).equals(new DNFClause(new int[]{1,2})));
		Assert.assertFalse(new DNFClause(new int[]{1,2,3}).equals(new DNFClause(new int[]{1,2,-3})));
		Assert.assertFalse(new DNFClause(new int[]{1,2,3}).equals(new DNFClause(new int[]{-1,-2,6})));
		Assert.assertFalse(new DNFClause(new int[]{1,2,3}).equals(new CNFClause(new int[]{1,2,3})));
		Assert.assertFalse(new DNFClause(new int[]{1,2,3}).equals(new DNFClause(new int[]{1,2})));
		Assert.assertEquals(new DNFClause(new int[]{-270,1}).hashCode(), new DNFClause(new int[]{1}).hashCode());
		Assert.assertFalse(new DNFClause(new int[]{-270,1}).equals(new DNFClause(new int[]{1})));
	}

}
