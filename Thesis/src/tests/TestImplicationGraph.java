package tests;

import java.io.File;
import java.io.IOException;

import junit.framework.Assert;
import net.sf.javabdd.BDD;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.TimeoutException;

import ca.uwaterloo.gsd.fds.BDDBuilder;
import ca.uwaterloo.gsd.fds.FDSFactory;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureModelParser;
import ca.uwaterloo.gsd.fm.ImplicationGraph;
import ca.uwaterloo.gsd.merge.SemanticOperations;
import fds.GraphBuilder;
import formula.cnf.CNF;
import formula.cnf.CNFBuilder;
import formula.dnf.DNF;
import formula.dnf.DNFBuilder;



public class TestImplicationGraph {
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testBuildImplicationGraph() throws ContradictionException, TimeoutException, IOException {
		File dir = new File("tests/implication_test/fm/");
		for(String file : dir.list()){
			System.out.println(file);
			Assert.assertTrue(compareImplicationGraphsCNF(FeatureModelParser.parseFile(dir + "/" + file)));
			Assert.assertTrue(compareImplicationGraphsDNF(FeatureModelParser.parseFile(dir + "/" + file)));
		}
	}

	private boolean compareImplicationGraphsCNF(FeatureModel<String> fm) throws ContradictionException, TimeoutException{
		//construct cnf
		CNFBuilder<String> cnfBuilder = new CNFBuilder<String>(fm.features(), "Synth");
		CNF cnf = cnfBuilder.mkModel(fm);
		cnf.removeDeadFeatures();
		
		//construct bdd
		BDDBuilder<String> bddBuilder = FDSFactory.makeStringBDDBuilder();
		BDD bdd = bddBuilder.mkStructure(fm);
		bdd = SemanticOperations.removeDeadFeatures(bdd);
	
		ImplicationGraph<String> g = GraphBuilder.buildImplicationGraph(cnf, cnfBuilder);
		ImplicationGraph<String> h = ca.uwaterloo.gsd.fds.IGBuilder.build(bdd, bddBuilder);	
		
		if(g.vertices().size() != h.vertices().size())
				return false;
		if(!g.vertices().containsAll(h.vertices()))
			return false;
		if(g.getEdgeCount() != h.getEdgeCount())
			return false;
			
		boolean result = true;
		for(String v : g.vertices()){
			result = false;
			for(String u : h.vertices()){
				if(g.getIncidentEdges(v).containsAll(h.getInEdges(u))){
					result = true;
					continue;
				}
			}
				
		}
		return result;
	}

	private boolean compareImplicationGraphsDNF(FeatureModel<String> fm) throws ContradictionException, TimeoutException{
		//construct dnf
		DNFBuilder<String> dnfBuilder = new DNFBuilder<String>(fm.features(), "Synth");
		DNF dnf = dnfBuilder.mkModel(fm);
		dnf.removeDeadFeatures();
		
		//construct bdd
		BDDBuilder<String> bddBuilder = FDSFactory.makeStringBDDBuilder();
		BDD bdd = bddBuilder.mkStructure(fm);
		bdd = SemanticOperations.removeDeadFeatures(bdd);
	
		ImplicationGraph<String> g = GraphBuilder.buildImplicationGraph(dnf, dnfBuilder);
		ImplicationGraph<String> h = ca.uwaterloo.gsd.fds.IGBuilder.build(bdd, bddBuilder);	
		
		if(g.vertices().size() != h.vertices().size())
				return false;
		if(!g.vertices().containsAll(h.vertices()))
			return false;
		if(g.getEdgeCount() != h.getEdgeCount())
			return false;
			
		boolean result = true;
		for(String v : g.vertices()){
			result = false;
			for(String u : h.vertices()){
				if(g.getIncidentEdges(v).containsAll(h.getInEdges(u))){
					result = true;
					continue;
				}
			}
				
		}
		return result;
	}

}

