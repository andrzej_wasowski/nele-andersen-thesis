package tests;


import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Test;

import pig.PIG;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureModelParser;
import formula.Clause;
import formula.cnf.CNF;
import formula.cnf.CNFBuilder;
import formula.cnf.VariableElimination;

public class TestVariableElimination {

	@Test
	public void variableEliminationTest() throws IOException{
	
		File dir = new File("tests/elimination_test/fm/");
		for(String file : dir.list()){
			System.out.println(file);
			//make feature model
			FeatureModel<String> fm = FeatureModelParser.parseFile(dir + "/" + file);
			
			//make cnf
			CNFBuilder<String> cnfBuilder = new CNFBuilder<String>(fm.features(), "Synth");
			CNF cnf = cnfBuilder.mkModel(fm);
			
			//make set of variables to eliminate
			Set<Integer> vars = new HashSet<Integer>();
			vars.add(1); vars.add(2); vars.add(3); vars.add(4);
			vars.add(5); vars.add(6); vars.add(7); vars.add(8);
			
			// eliminate variables 
			CNF reduced = VariableElimination.variableElimination(cnf, vars);
			
			Assert.assertFalse(containsVariable(reduced, 1));
			Assert.assertFalse(containsVariable(reduced, 2));
			Assert.assertFalse(containsVariable(reduced, 3));
			Assert.assertFalse(containsVariable(reduced, 4));
			Assert.assertFalse(containsVariable(reduced, 5));
			Assert.assertFalse(containsVariable(reduced, 6));
			Assert.assertFalse(containsVariable(reduced, 7));
			Assert.assertFalse(containsVariable(reduced, 8));
			
			CNF orgPrimes = new PIG(cnf).getPrimeImplicates();
			CNF redPrimes = new PIG(reduced).getPrimeImplicates();
			
			Assert.assertTrue(isSubset(redPrimes, orgPrimes));
			Assert.assertTrue(containsAllReducedPrimes(redPrimes, orgPrimes, vars));
		}
	}
	
	private boolean containsVariable(CNF reduced, int var){
		for(Clause c : reduced){
			if(c.containsLiteral(var) || c.containsLiteral(-var))
				return true;
		}
		return false;
			
	}
	private boolean isSubset(CNF reduced, CNF cnf){
		for(Clause c : reduced){
			if(!cnf.containsClause(c))
				return false;
		}
		return true;
	}
	
	private boolean containsAllReducedPrimes(CNF reduced, CNF cnf, Set<Integer> vars){
		for(Clause c : cnf){
			if(!c.getNegativeLiterals().removeAll(vars) && !c.getPositiveLiterals().removeAll(vars)){
				if(!reduced.containsClause(c))
					return false;
			}
		}
		return true;
	}
}
