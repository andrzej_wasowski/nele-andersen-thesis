package tests;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import formula.cnf.CNFClause;
import formula.dnf.DNFClause;

public class TestCNFClause {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testHashCode() {
		Assert.assertEquals(new CNFClause(new int[]{1,2,3}).hashCode(), new CNFClause(new int[]{1,2,3}).hashCode());
		Assert.assertEquals(new CNFClause(new int[]{1,2,-3}).hashCode(), new CNFClause(new int[]{1,2,-3}).hashCode());
		Assert.assertEquals(new CNFClause(new int[]{3,2,1}).hashCode(), new CNFClause(new int[]{1,2,3}).hashCode());
		Assert.assertEquals(new CNFClause(new int[]{1,3,2}).hashCode(), new CNFClause(new int[]{1,2,3}).hashCode());
	}

	@Test
	public void testDifference() {
		Assert.assertEquals(new CNFClause(), new CNFClause(new int[]{1,2,3}).difference(new CNFClause(new int[]{1,2,3})));
		Assert.assertEquals(new CNFClause(new int[]{2,3}), new CNFClause(new int[]{1,2,3}).difference(new CNFClause(new int[]{1})));
		Assert.assertEquals(new CNFClause(new int[]{1,2,3}), new CNFClause(new int[]{1,2,3}).difference(new CNFClause(new int[]{-1})));
		Assert.assertEquals(new CNFClause(new int[]{1,2}), new CNFClause(new int[]{1,2,3}).difference(new CNFClause(new int[]{3,4})));
		Assert.assertEquals(new CNFClause(new int[]{1,2,3}), new CNFClause(new int[]{1,2,3}).difference(new CNFClause()));
	}

	@Test
	public void testEqualsObject() {
		Assert.assertTrue(new CNFClause(new int[]{1,2,3}).equals(new CNFClause(new int[]{1,2,3})));
		Assert.assertTrue(new CNFClause(new int[]{3,2,1}).equals(new CNFClause(new int[]{1,2,3})));
		Assert.assertTrue(new CNFClause(new int[]{1,3,2}).equals(new CNFClause(new int[]{1,2,3})));
		Assert.assertFalse(new CNFClause(new int[]{1,2,3}).equals(new CNFClause(new int[]{1,2})));
		Assert.assertFalse(new CNFClause(new int[]{1,2,3}).equals(new CNFClause(new int[]{1,2,-3})));
		Assert.assertFalse(new CNFClause(new int[]{1,2,3}).equals(new CNFClause(new int[]{-1,-2,6})));
		Assert.assertFalse(new CNFClause(new int[]{1,2,3}).equals(new DNFClause(new int[]{1,2,3})));
		Assert.assertFalse(new CNFClause(new int[]{1,2,3}).equals(new CNFClause(new int[]{1,2})));
		Assert.assertEquals(new CNFClause(new int[]{-270,1}).hashCode(), new CNFClause(new int[]{1}).hashCode());
		Assert.assertFalse(new CNFClause(new int[]{-270,1}).equals(new CNFClause(new int[]{1})));
	}

	@Test
	public void testGetRandomLiteral() {
		CNFClause clause = new CNFClause(new int[]{1,2,3,-1,-2,-3});
		for(int i = 0; i < 10; i++){
			int literal = clause.getRandomLiteral();
			Assert.assertTrue(clause.containsLiteral(literal));
		}
	}

	@Test
	public void testComputeResolvent() {
		Assert.assertEquals(new CNFClause(new int[]{2,3}), new CNFClause(new int[]{1,2,3}).computeResolvent(new CNFClause(new int[]{-1,2,3})));
		Assert.assertEquals(new CNFClause(), new CNFClause(new int[]{1}).computeResolvent(new CNFClause(new int[]{-1})));
		Assert.assertEquals(new CNFClause(new int[]{2,3,4}), new CNFClause(new int[]{1,2,3}).computeResolvent(new CNFClause(new int[]{-1,3,4})));
		Assert.assertEquals(new CNFClause(new int[]{2,3}), new CNFClause(new int[]{1,2,3}).computeResolvent(new CNFClause(new int[]{-1})));
	}

	@Test
	public void testResolvesToTautology() {
		Assert.assertTrue(new CNFClause(new int[]{1,-2,3}).resolvesToTautology(new CNFClause(new int[]{-1,2,3})));
		Assert.assertTrue(new CNFClause(new int[]{1,2,3}).resolvesToTautology(new CNFClause(new int[]{-1,-2,-3})));
		Assert.assertFalse(new CNFClause(new int[]{1,2,3}).resolvesToTautology(new CNFClause(new int[]{-1,2,3})));
	}

}
