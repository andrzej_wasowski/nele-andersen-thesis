package tests;

import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import formula.Clause;
import formula.dnf.DNF;
import formula.dnf.DNFClause;

public class TestDNF {

	DNF dnf;
	
	@Before
	public void setUp() throws Exception {
		Set<DNFClause> clauses = new HashSet<DNFClause>();
	
		clauses.add(new DNFClause(new int[]{-1,-2,-3,-4,-5}));
		clauses.add(new DNFClause(new int[]{-1,-2,-3,4,-5}));
		clauses.add(new DNFClause(new int[]{-1,-2,3,-4,-5}));
		clauses.add(new DNFClause(new int[]{-1,-2,3,4,-5}));
		clauses.add(new DNFClause(new int[]{-1,2,-3,-4,-5}));
		clauses.add(new DNFClause(new int[]{-1,2,-3,4,-5}));
		clauses.add(new DNFClause(new int[]{-1,2,3,-4,-5}));
		clauses.add(new DNFClause(new int[]{-1,2,3,4,-5}));
		clauses.add(new DNFClause(new int[]{1,2,3,4,-5}));
		clauses.add(new DNFClause(new int[]{1,2,3,-4,-5}));
		clauses.add(new DNFClause(new int[]{1,2,-3,4,-5}));
		clauses.add(new DNFClause(new int[]{-1,-2,3,4,-5}));
		clauses.add(new DNFClause(new int[]{1,-2,3,4,-5}));
		clauses.add(new DNFClause(new int[]{1,-2,3,-4,-5,6,-7}));
		clauses.add(new DNFClause(new int[]{1,-2,-3,4,-5,6,-7}));
		clauses.add(new DNFClause(new int[]{1,-2,-3,-4,-5,6,7}));
		
		dnf = new DNF(clauses);
	}
	
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRemoveDeadFeatures() {
		System.out.println(dnf);
		dnf.removeDeadFeatures();
		System.out.println(dnf);
		for(Clause clause : dnf){
			System.out.println(clause);
			Assert.assertFalse(clause.containsLiteral(5));
			Assert.assertFalse(clause.containsLiteral(-5));
			for(int i = 1; i < 5; i++){
				Assert.assertTrue(clause.containsLiteral(i) | clause.containsLiteral(-i));
			}
		}
	}

	@Test
	public void testImplication() {
		Assert.assertFalse(dnf.implication(1, 2));
		Assert.assertFalse(dnf.implication(1, 3));
		Assert.assertFalse(dnf.implication(1, 4));
		Assert.assertFalse(dnf.implication(1, 5));
		Assert.assertFalse(dnf.implication(1, 6));
		Assert.assertTrue(dnf.implication(6, 1));
		
	}
	
	@Test
	public void testMutex() {
		Assert.assertFalse(dnf.isMutex(1, 6));
		Assert.assertFalse(dnf.isMutex(1, 7));
		Assert.assertTrue(dnf.isMutex(2, 6));
		Assert.assertTrue(dnf.isMutex(4, 7));
	}


	@Test
	public void testClone() {
		DNF clone = dnf.clone();
		for(Clause clause : clone)
			Assert.assertTrue(dnf.containsClause(clause));
		for(Clause clause : dnf)
			Assert.assertTrue(clone.containsClause(clause));
	}

}
