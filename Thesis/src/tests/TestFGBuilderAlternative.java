package tests;

import java.io.File;

import junit.framework.Assert;

import org.junit.Test;

import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureModelParser;
import extensions.AlternativeFGBuilder;
import fds.FGBuilder;
import formula.cnf.CNF;
import formula.cnf.CNFBuilder;

public class TestFGBuilderAlternative {

	@Test 
	public void compareFeatureGraphsOptimized() throws Exception{
	
		File dir = new File("tests/fgbuilder_test/cnf/fm/");
		for(String file : dir.list()){
			System.out.println("File " + file);
			Assert.assertTrue(compareFeatureGraphs(dir + "/" + file));
		}
		
	}
	
	private boolean compareFeatureGraphs(String file) throws Exception {
		
		FeatureModel<String> fm = FeatureModelParser.parseFile(file);
		
		//construct cnf
		CNFBuilder<String> cnfBuilder = new CNFBuilder<String>(fm.features(), "Synth");
		CNF cnf = cnfBuilder.mkModel(fm);
	
		// init builders
		AlternativeFGBuilder<String> fgBuilderAlt = new AlternativeFGBuilder<String>(cnfBuilder);
		FGBuilder<String> fgBuilder = new FGBuilder<String>(cnfBuilder);
				
		FeatureGraph<String> h = fgBuilderAlt.build(cnf.clone());
		FeatureGraph<String> g = fgBuilder.build(cnf);
		return h.equals(g);
	}
	
}
