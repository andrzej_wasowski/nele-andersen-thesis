package tests;

import java.io.File;

import junit.framework.Assert;
import net.sf.javabdd.BDD;

import org.junit.Test;

import ca.uwaterloo.gsd.fds.BDDBuilder;
import ca.uwaterloo.gsd.fds.FDSFactory;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureModelParser;
import ca.uwaterloo.gsd.merge.SemanticOperations;
import fds.FGBuilder;
import fds.FGBuilderBDDBasedExtention;
import fds.FGBuilderOptimized;
import formula.cnf.CNF;
import formula.cnf.CNFBuilder;

public class TestFGBuilderCNF {

	@Test 
	public void compareFeatureGraphsOptimized() throws Exception{
	
		File dir = new File("tests/fgbuilder_test/cnf/fm/");
		for(String file : dir.list()){
			System.out.println("File " + file);
			Assert.assertTrue(compareFeatureGraphs(dir + "/" + file));
			Assert.assertTrue(compareFeatureGraphsOptimized(dir + "/" + file));
		}
		
	}
	
	private boolean compareFeatureGraphs(String file) throws Exception {
		
		FeatureModel<String> fm = FeatureModelParser.parseFile(file);
		
		//construct cnf
		CNFBuilder<String> cnfBuilder = new CNFBuilder<String>(fm.features(), "Synth");
		CNF cnf = cnfBuilder.mkModel(fm);
	
		//construct bdd
		BDDBuilder<String> bddBuilder = FDSFactory.makeStringBDDBuilder();
		BDD bdd = bddBuilder.mkStructure(fm);
		bdd = SemanticOperations.removeDeadFeatures(bdd);
		
		// init builders
		FGBuilderBDDBasedExtention<String> fgBuilderBDD = new FGBuilderBDDBasedExtention<String>(bddBuilder);
		FGBuilder<String> fgBuilderSAT = new FGBuilder<String>(cnfBuilder);
				
		//build bdd based
		FeatureGraph<String> h = fgBuilderBDD.build(bdd);
		// build sat based
		FeatureGraph<String> g = fgBuilderSAT.build(cnf);
		return h.equals(g);
	}
	
	private boolean compareFeatureGraphsOptimized(String file) throws Exception{
		
		FeatureModel<String> fm = FeatureModelParser.parseFile(file);
		
		//construct cnf
		CNFBuilder<String> cnfBuilder = new CNFBuilder<String>(fm.features(), "Synth");
		CNF cnf = cnfBuilder.mkModel(fm);
	
		//construct bdd
		BDDBuilder<String> bddBuilder = FDSFactory.makeStringBDDBuilder();
		BDD bdd = bddBuilder.mkStructure(fm);
		bdd = SemanticOperations.removeDeadFeatures(bdd);
		
		// init builders
		ca.uwaterloo.gsd.fds.FGBuilder<String> fgBuilderBDD = new ca.uwaterloo.gsd.fds.FGBuilder<String>(bddBuilder);
		FGBuilderOptimized<String> fgBuilderSAT = new FGBuilderOptimized<String>(cnfBuilder);
				
		//build bdd based
		FeatureGraph<String> h = fgBuilderBDD.build(bdd);
	
		// build sat based
		FeatureGraph<String> g = fgBuilderSAT.build(cnf);

		return h.equals(g);
	}
	
}
