package tests;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import pig.PIG;
import prime.Prime;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureModelParser;
import formula.Clause;
import formula.cnf.CNF;
import formula.cnf.CNFBuilder;
import formula.cnf.CNFClause;
import formula.dnf.DNF;
import formula.dnf.DNFBuilder;
import formula.dnf.DefaultDNFSolver;

public class TestPrime {

	@Test
	public void primeTest() throws Exception{
		
		File dir = new File("tests/prime_test/fm/");
		for(String file : dir.list()){
			System.out.println(file);
			Assert.assertTrue(comparePrimes(FeatureModelParser.parseFile(dir + "/" + file)));
		}
	}

	private boolean comparePrimes(FeatureModel<String> fm){
		
		DNFBuilder<String> dnfBuilder = new DNFBuilder<String>(fm.features());
		CNFBuilder<String> cnfBuilder = new CNFBuilder<String>(fm.features());
		
		DNF dnf = dnfBuilder.mkModel(fm);
		CNF cnf = cnfBuilder.mkModel(fm);
	
		int root = cnfBuilder.variable(fm.getDiagram().root().features().iterator().next());
		cnf.addClause(new CNFClause(new int[]{root}));
		
		Set<Set<Integer>> clauses = new HashSet<Set<Integer>>();		
		for(Clause c : new PIG(cnf).getPositivePrimeImplicates()){
			clauses.add(c.getLiterals());
		}
		
		root = dnfBuilder.variable(fm.getDiagram().root().features().iterator().next());
		return clauses.equals(new Prime(new DefaultDNFSolver(dnf, root)).positivePrimes());
	}
}
