package tests;

import java.io.File;
import java.io.IOException;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureModelParser;
import formula.Clause;
import formula.cnf.CNF;
import formula.cnf.CNFBuilder;
import formula.cnf.CNFClause;

public class TestCNF {
	
	CNF cnf;
	
	@Before
	public void setUp() throws Exception {
		cnf = new CNF();
		cnf.addClause(new CNFClause(new int[]{-1,-2,-3,-4}));
		cnf.addClause(new CNFClause(new int[]{-1,-2,-3,4}));
		cnf.addClause(new CNFClause(new int[]{-1,-2,3,-4}));
		cnf.addClause(new CNFClause(new int[]{-1,-2,3,4}));
		cnf.addClause(new CNFClause(new int[]{-1,2,-3,-4}));
		cnf.addClause(new CNFClause(new int[]{-1,2,-3,4}));
		cnf.addClause(new CNFClause(new int[]{-1,2,3,-4}));
		cnf.addClause(new CNFClause(new int[]{-1,2,3,4}));
		cnf.addClause(new CNFClause(new int[]{1,2,3,4}));
		cnf.addClause(new CNFClause(new int[]{1,2,3,-4}));
		cnf.addClause(new CNFClause(new int[]{1,2,-3,4}));
		cnf.addClause(new CNFClause(new int[]{-1,-2,3,4}));
		cnf.addClause(new CNFClause(new int[]{1,-2,3,4}));
		cnf.addClause(new CNFClause(new int[]{1,-2,3,-4}));
		cnf.addClause(new CNFClause(new int[]{1,-2,-3,4}));
		cnf.addClause(new CNFClause(new int[]{1,-2,-3,-4}));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testContainsClause() {
		Assert.assertTrue(cnf.containsClause(new CNFClause(new int[]{-1,-2,-3,-4})));
		Assert.assertTrue(cnf.containsClause(new CNFClause(new int[]{1,-2,-3,-4})));
		Assert.assertFalse(cnf.containsClause(new CNFClause(new int[]{-2,-3,-4})));
		Assert.assertFalse(cnf.containsClause(new CNFClause()));
		Assert.assertFalse(cnf.containsClause(new CNFClause(new int[]{-1,-2,-3,-4,5})));
		
		
	}
	
	@Test
	public void testForwardAndBackwardSubsumption(){
		cnf.addClause(new CNFClause(new int[]{-1,-2,-3,-4,5}));
		Assert.assertFalse(cnf.containsClause(new CNFClause(new int[]{-1,-2,-3,-4,5})));
		cnf.addClause(new CNFClause(new int[]{-1,-2,-3,-4}));
		Assert.assertTrue(cnf.containsClause(new CNFClause(new int[]{-1,-2,-3,-4})));
		cnf.addClause(new CNFClause(new int[]{-1,-2,-3}));
		Assert.assertTrue(cnf.containsClause(new CNFClause(new int[]{-1,-2,-3})));
		cnf.addClause(new CNFClause(new int[]{-1,-2,-3,-4}));
		Assert.assertFalse(cnf.containsClause(new CNFClause(new int[]{-1,-2,-3,-4})));
	}
	
	@Test
	public void testDeadFeatures() throws IOException{
		File dir = new File("tests/cnf_test/splot-3cnf-fm-500-50-sat");
		for(String file : dir.list()){
			FeatureModel<String> fm = FeatureModelParser.parseFile(dir + "/" + file);
			CNFBuilder<String> cnfBuilder = new CNFBuilder<String>(fm.features(), "Synth");
			CNF cnf = cnfBuilder.mkModel(fm);
			cnf.removeDeadFeatures();
			for(int i : cnf.getDeadFeatures()){
				for(Clause c : cnf){
					Assert.assertFalse(c.containsLiteral(i));
					Assert.assertFalse(c.containsLiteral(-i));
				}
			}
		}
		
	}
	
	@Test
	public void testImplication() {
		CNF cnf = new CNF();
		cnf.addClause(new CNFClause(new int[]{-1,2}));
		Assert.assertTrue(cnf.implication(1, 2));
		Assert.assertFalse(cnf.implication(2, 1));
		cnf.addClause(new CNFClause(new int[]{1,-2}));
		Assert.assertTrue(cnf.implication(1, 2));
		Assert.assertTrue(cnf.implication(2, 1));
		cnf.addClause(new CNFClause(new int[]{-1,3,4}));
		Assert.assertFalse(cnf.implication(1, 3));
		Assert.assertFalse(cnf.implication(1, 4));
		cnf.addClause(new CNFClause(new int[]{-3,4}));
		Assert.assertFalse(cnf.implication(1, 3));
		Assert.assertTrue(cnf.implication(1, 4));
		
	}
	
	@Test
	public void testClone(){
		CNF clone = cnf.clone();
		for(Clause clause : clone)
			Assert.assertTrue(cnf.containsClause(clause));
		for(Clause clause : cnf)
			Assert.assertTrue(clone.containsClause(clause));
	
	}

}
