package fds;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureNode;
import ca.uwaterloo.gsd.fm.ImplicationGraph;
import formula.Clause;
import formula.Formula;
import formula.FormulaBuilder;

/**
 * This class implements an optimised version of the FGE algorithm which sacrifices the 
 * completeness of the original FGE algorithm. This implementation restricts the search 
 * for or-groups and mutex-groups to features which are siblings in the transitive 
 * reduction of the implication graph. 
 */
public class FGBuilderOptimized<T> extends FGBuilder<T>{

	
	public FGBuilderOptimized(FormulaBuilder<T> builder) {
		super(builder);
	}

	@Override
	public FeatureGraph<T> build(Formula formula) {

		_formula = formula;
		_formula.removeDeadFeatures();
		long start = System.currentTimeMillis();

		FeatureGraph<T> g = new FeatureGraph<T>();

		logger.info("Implication graph");
		long impStart = System.currentTimeMillis();

		ImplicationGraph<T> impl = _builder.buildImplicationGraph(_formula);

		long impEnd = System.currentTimeMillis();
		bmEntry.put("implication", impEnd - impStart);

		logger.info("And-groups");
		long andStart = System.currentTimeMillis();

		mkHierarchyAndGroups(g, impl);

		long andEnd = System.currentTimeMillis();
		bmEntry.put("hierarchy", andEnd - andStart);

		logger.info("Transitive reduction");
		ca.uwaterloo.gsd.algo.TransitiveReduction.INSTANCE.reduce(g);
	
		logger.info("Mutex graph and groups");

		mkMutexGroups(g);

		logger.info("Prime implicants and Or groups");
		long orStart = System.currentTimeMillis();

		mkOrGroups(g);

		long orEnd = System.currentTimeMillis();
		bmEntry.put("or", (orEnd - orStart) );

		mkSyntheticRoot(g);

		long end = System.currentTimeMillis();
		bmEntry.put("total", end - start);

		_benchmark.add(bmEntry);

		return g;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void mkMutexGroups(FeatureGraph<T> g) {
		long mutexStart = System.currentTimeMillis();
		//Make Mutex Graph
		UndirectedGraph<FeatureNode<T>, DefaultEdge> mutex 
		= new SimpleGraph<FeatureNode<T>, DefaultEdge>(DefaultEdge.class);

		for (FeatureNode<T> v : g.vertices())
			mutex.addVertex(v);

		for (FeatureNode<T> v : g.vertices()) {
			FeatureNode<T>[] siblings = g.children(v).toArray(new FeatureNode[0]);

			for (int i=0; i < siblings.length; i++) {
				FeatureNode<T> s1 = siblings[i];

				for (int j=i+1;j<siblings.length; j++) {
					FeatureNode<T> s2 = siblings[j];

					if (_formula.implication(variable(s1), -variable(s2))) {
						mutex.addEdge(s1, s2);
					}
				}
			}

		}

		long mutexEnd = System.currentTimeMillis();
		bmEntry.put("mutex-graph", mutexEnd - mutexStart);
		//Create Mutex Groups
		mutexStart = System.currentTimeMillis();

		_mutexCliques = findMutexCliques(mutex);
		for (Set<FeatureNode<T>> clique : _mutexCliques) {
			FeatureNode<T> parent = commonParent(g, clique);

			//Mutex group doesn't have a common parent ?
			if (parent == null) {
				logger.warning("Mutex group without a common parent: " + clique);
				continue;
			}

			g.addEdge(clique, parent, FeatureEdge.MUTEX);
		}
		mutexEnd = System.currentTimeMillis();
		bmEntry.put("mutex-cliques", mutexEnd - mutexStart);

	}

	@Override
	public void mkOrGroups(FeatureGraph<T> g) {
		for (FeatureNode<T> v : g.vertices()) {

			Set<T> children = new LinkedHashSet<T>();
			for(FeatureNode<T> child : g.children(v))
				children.addAll(child.features());

			if(g.children(v).size() < 2) 
				continue;

			Set<Integer> vars = variablesToEliminate(g, v);

			Formula reduced = _formula.eliminateVariables(vars);

			Set<Clause> orGroups = reduced.getOrGroups(variable(v));

			for(Clause prime : orGroups){
	
				Set<FeatureNode<T>> grouped = new HashSet<FeatureNode<T>>();
				for (int i : prime)
					grouped.add(g.findNode(feature(i)));

				if (grouped.size() == 0 || grouped.contains(v))
					continue;
				assert g.children(v).containsAll(grouped);

				FeatureNode<T> parent = leastCommonAncestor(g, grouped);
				if (g.findEdge(grouped, parent, FeatureEdge.XOR) != null || g.findEdge(grouped, parent, FeatureEdge.OR) != null)
					continue;
				else if (isMutex(grouped)) {
					//Need to remove mutex since xor edge subsumes
					if(g.findEdge(grouped, v, FeatureEdge.MUTEX) != null)
						g.removeEdge(g.findEdge(grouped, v, FeatureEdge.MUTEX));
					g.addEdge(grouped, parent, FeatureEdge.XOR);
				}
				else {
					g.addEdge(grouped, parent, FeatureEdge.OR);
				}
			}
		}
	}

}
