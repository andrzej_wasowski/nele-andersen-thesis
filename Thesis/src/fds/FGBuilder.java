package fds;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.collections15.CollectionUtils;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.BronKerboschCliqueFinder;
import org.jgrapht.graph.DefaultEdge;

import ca.uwaterloo.gsd.algo.DirectedCliqueFinder;
import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureNode;
import ca.uwaterloo.gsd.fm.ImplicationGraph;
import ca.uwaterloo.gsd.fm.SimpleEdge;
import ca.uwaterloo.gsd.merge.experiments.Benchmark;
import ca.uwaterloo.gsd.merge.experiments.BenchmarkEntry;
import formula.Clause;
import formula.Formula;
import formula.FormulaBuilder;

/**
 * This class provides an implementation of the complete FGE algorithm 
 * inspired by ca.uwaterloo.gsd.fds.FGBuilder.
 *  
 */
public class FGBuilder<T> {
	
	protected Benchmark _benchmark = new Benchmark("implication", "hierarchy", "mutex-graph", "mutex-cliques", "or", "total");
	protected BenchmarkEntry bmEntry = _benchmark.mkEntry();

	protected Logger logger = Logger.getLogger("sat.FGBuilder");

	protected Formula _formula;
	protected final FormulaBuilder<T> _builder;

	public Collection<Set<FeatureNode<T>>> _mutexCliques;


	/**
	 * @param builder: the FormulaBuilder used to construct the formula. The builder is need as it contains a mapping from
	 * the variables in the formula to the actual features.
 	 */
	public FGBuilder(FormulaBuilder<T> builder) {
		_builder = builder;
	}

	/**
	 * 
	 * @param formula: the formula from which a feature graph is to be extracted
	 * @return a feature graph containing all implications, and-, or-, xor- and mutex-groups. 
	 */
	public FeatureGraph<T> build(Formula formula) {
		if(!formula.isSatisfiable())
			return null;
		_formula = formula;
		_formula.removeDeadFeatures();
		System.out.println(_formula.getDeadFeatures().size());
		long start = System.currentTimeMillis();
	
		FeatureGraph<T> g = new FeatureGraph<T>();
		
		logger.info("Implication graph");
		long impStart = System.currentTimeMillis();
	
		ImplicationGraph<T> impl = _builder.buildImplicationGraph(_formula);
		
		long impEnd = System.currentTimeMillis();
		bmEntry.put("implication", impEnd - impStart);
		
		logger.info("And-groups");
		long andStart = System.currentTimeMillis();
		
		mkHierarchyAndGroups(g, impl);
		
		long andEnd = System.currentTimeMillis();
		bmEntry.put("hierarchy", andEnd - andStart);
		
		logger.info("Mutex graph and groups");
		
		mkMutexGroups(g);
	
		logger.info("Prime implicants and Or groups");
		long orStart = System.currentTimeMillis();
		
		mkOrGroups(g);
		
		long orEnd = System.currentTimeMillis();
		
		bmEntry.put("or", orEnd - orStart);
		logger.info("Transitive reduction");
		ca.uwaterloo.gsd.algo.TransitiveReduction.INSTANCE.reduce(g);
	
		mkSyntheticRoot(g);
		
		long end = System.currentTimeMillis();
		bmEntry.put("total", end - start);

		_benchmark.add(bmEntry);
		
		return g;
	}

	/**
	 * This method is copied from ca.uwaterloo.gsd.fds.FGBuilder. 
	 * Finds all and-groups (maximal cliques) in the implication graph. The contracted and-groups and
	 * solitary feature nodes  + edges are added to the feature graph g. 
	 * @param g : An empty graph. After this method is called g is corresponds to the implication graph but 
	 * with contracted and-groups.
	 * @param impl : The implication graph of the formula. After this method has been called the implication graph
	 *  is no longer consistent with the formula, but this is ok, since it not used afterwards.
	 */
	protected void mkHierarchyAndGroups(FeatureGraph<T> g, ImplicationGraph<T> impl) {
		List<Set<T>> cliques = DirectedCliqueFinder.INSTANCE.findAll(impl);
		//Make the hierarchy
		for (Set<T> clique : cliques)
			g.addVertex(new FeatureNode<T>(clique));

		for (T v : impl.vertices())
			if (!g.features().contains(v))
				g.addVertex(new FeatureNode<T>(v));

		for (Set<T> clique : cliques) {

			FeatureNode<T> group = g.findNode(clique.iterator().next());

			for (T u : clique) {
				for (SimpleEdge e : impl.incomingEdges(u).toArray(new SimpleEdge[0])) {
					T source = impl.getSource(e);
					if (!clique.contains(source))
						g.addEdge(g.findNode(source), group, FeatureEdge.OPTIONAL);
					impl.removeEdge(e);
				}

				for (SimpleEdge e : impl.outgoingEdges(u).toArray(new SimpleEdge[0])) {
					T target = impl.getTarget(e);
					if (!clique.contains(target))
						g.addEdge(group, g.findNode(target), FeatureEdge.OPTIONAL);
					impl.removeEdge(e);
				}
			}
		}

		//Add the remaining hierarchy edges
		for (SimpleEdge e : impl.edges()) 
			g.addEdge(g.findNode(impl.getSource(e)), 
					g.findNode(impl.getTarget(e)), 
					FeatureEdge.OPTIONAL);

	}

	/**
	 * This method computes the mutex graph and subsequently finds all mutex-groups
	 * (maximal cliques). For each mutex-group an mutex edge is added to the 
	 * Feature graph g.
	 * @param g
	 */
	@SuppressWarnings("unchecked")
	protected void mkMutexGroups(FeatureGraph<T> g) {
		long mutexStart = System.currentTimeMillis();
		//Make Mutex Graph
		UndirectedGraph<FeatureNode<T>, DefaultEdge> mutex 
			= _builder.buildMutexGraph(g, _formula);

		long mutexEnd = System.currentTimeMillis();
		bmEntry.put("mutex-graph", mutexEnd - mutexStart);
		mutexStart = System.currentTimeMillis();
		
		_mutexCliques = findMutexCliques(mutex);
		
		for (Set<FeatureNode<T>> clique : _mutexCliques) {
			FeatureNode<T> parent = leastCommonAncestor(g, clique);
			
			//Mutex group doesn't have a common parent ?
			if (parent == null) {
				logger.warning("Mutex group without a common parent: " + clique);
				continue;
			}
			
			g.addEdge(clique, parent, FeatureEdge.MUTEX);
		}

		mutexEnd = System.currentTimeMillis();
		bmEntry.put("mutex-cliques", mutexEnd - mutexStart);
	}
	
	protected boolean isMutex(Set<FeatureNode<T>> grouped){
		for(Set<FeatureNode<T>> mutex : _mutexCliques){
			if(mutex.containsAll(grouped))
				return true;
		}
		return false;
	}

	/**
	 * Finds all or-groups, for each group it checks if the group is a mutex-group. If this
	 * is the case an xor-edge is added to the feature graph g and the existing mutex-edge 
	 * is removed. If the group is not a mutex-group an or-edge is added to the feature graph g.
	 * @param g
	 */
	public void mkOrGroups(FeatureGraph<T> g) {

		
		for (FeatureNode<T> v : g.vertices()) {
		
			if(g.children(v).size() < 2) 
					continue;
			Set<Integer> vars = variablesToEliminate(g, v);

			Formula reduced = _formula.eliminateVariables(vars);

			Set<Clause> orGroups = reduced.getOrGroups(variable(v));
	
			for(Clause prime : orGroups){

				Set<FeatureNode<T>> grouped = new HashSet<FeatureNode<T>>();
				for (int i : prime)
					grouped.add(g.findNode(feature(i)));

				if (grouped.size() == 0 || grouped.contains(v))
					continue;
				assert g.children(v).containsAll(grouped);

				if (g.findEdge(grouped, v, FeatureEdge.XOR) != null)
					continue;
				else if (isMutex(grouped)) {
					//Need to remove mutex since xor edge subsumes
					if(g.findEdge(grouped, v, FeatureEdge.MUTEX) != null)
						g.removeEdge(g.findEdge(grouped, v, FeatureEdge.MUTEX));
					g.addEdge(grouped, v, FeatureEdge.XOR);
				}
				else
					g.addEdge(grouped, v, FeatureEdge.OR);
			}
		}
	}

	/**
	 * Constructs a set of variables corresponding to the children of the node v in g
	 * plus the node v itself. These variables are the only which are relevant when
	 * computing or-groups of v.  
	 */
	protected Set<Integer> variablesToEliminate(FeatureGraph<T> g, FeatureNode<T> v) {
		Set<Integer> vars = new HashSet<Integer>();
		for(int i : _formula.getVariables())
			vars.add(i);
		Set<Integer> keep = new HashSet<Integer>();
		keep.add(variable(v));
		for(FeatureNode<T> u : g.children(v))
			keep.add(variable(u)); 
		vars.removeAll(keep);
		return vars;
	}

	/**
	 * Adds a synthetic root to the graph, having all roots in g as its children. 
	 * Dead features are also added as children of the synthetic root (this is done
	 * because the original implementation does it). 
	 */
	public void mkSyntheticRoot(FeatureGraph<T> g) {

		if(_builder.syntheticRoot() != null){
			Set<FeatureNode<T>> roots = findRoots(g);

			FeatureNode<T> synth = new FeatureNode<T>(_builder.syntheticRoot());
			synth.setProperties(FeatureNode.SYNTHETIC);
			g.addVertex(synth);
			for (FeatureNode<T> v : roots) {
				g.addEdge(v, synth, FeatureEdge.OPTIONAL);
				g.addEdge(v, synth, FeatureEdge.MANDATORY);
			}

			for (int df : _formula.getDeadFeatures() ) {
				FeatureNode<T> v = new FeatureNode<T>(feature(df));
				v.setProperties(FeatureNode.FREE);
				g.addVertex(v);
				g.addEdge(v, synth, FeatureEdge.OPTIONAL);
			}
		}
	}

	/**
	 *  This method is copied from ca.uwaterloo.gsd.fds.FGBuilder.
	 */
	private Set<FeatureNode<T>> findRoots(FeatureGraph<T> g) {
		Set<FeatureNode<T>> cand = new HashSet<FeatureNode<T>>(g.vertices());
		for (FeatureEdge e : g.edges()) {
			cand.removeAll(g.getSources(e));
		}
		return cand;
	}

	/**
	 * This method is copied from ca.uwaterloo.gsd.fds.FGBuilder.
	 * @return null if the group doesn't have a common parent
	 */
	protected FeatureNode<T> commonParent(FeatureGraph<T> g, Set<FeatureNode<T>> group) {
		Set<FeatureNode<T>> parents = new HashSet<FeatureNode<T>>(g.vertices());
		for (FeatureNode<T> v : group) {
			parents.retainAll(g.parents(v));
			if (parents.size() == 0) return null;
		}

		if (parents.size () > 1)
			logger.warning("Multiple parents for mutex: " + group + "=" + parents);

		return parents.iterator().next ();
	}

	/**
	 * This method is copied from ca.uwaterloo.gsd.fds.FGBuilder.
	 */
	protected FeatureNode<T> leastCommonAncestor(FeatureGraph<T> g, Set<FeatureNode<T>> group) {
		Set<FeatureNode<T>> common = new HashSet<FeatureNode<T>>(g.vertices());

		for (FeatureNode<T> v : group) {
			Set<FeatureNode<T>> ancestors = g.ancestors(v);
			common.retainAll (ancestors);
		}

		//Remove transitive parents //FIXME ?
		Iterator<FeatureNode<T>> iter = common.iterator();
		while (iter.hasNext()) {
			FeatureNode<T> v = iter.next();
			if (CollectionUtils.intersection(common, g.descendants(v)).size() > 0)
				iter.remove();
		}

		if (common.size () > 1)
			logger.warning("Multiple parents for mutex: " + group + "=" + common);

		return common.iterator().next ();
	}

	/**
	 *  This method is copied from ca.uwaterloo.gsd.fds.FGBuilder.
	 */
	protected Collection<Set<FeatureNode<T>>> findMutexCliques(
			UndirectedGraph<FeatureNode<T>, DefaultEdge> g) {

		BronKerboschCliqueFinder<FeatureNode<T>, DefaultEdge> finder
		= new BronKerboschCliqueFinder<FeatureNode<T>, DefaultEdge>(g);

		Collection<Set<FeatureNode<T>>> cliques = finder.getAllMaximalCliques();
		Iterator<Set<FeatureNode<T>>> iter = cliques.iterator();
		while (iter.hasNext()) {
			if (iter.next().size() < 2)
				iter.remove();
		}
		return cliques;
	}

	protected int variable(FeatureNode<T> node){
		return _builder.variable(node.features().iterator().next());
	}

	protected T feature(int i){
		return _builder.feature(i);
	}
	
	public Benchmark getBenchmark() {
		return _benchmark;
	}

}