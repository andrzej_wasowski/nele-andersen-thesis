package fds;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.sf.javabdd.BDD;

import org.apache.commons.collections15.CollectionUtils;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import ca.uwaterloo.gsd.algo.TransitiveReduction;
import ca.uwaterloo.gsd.fds.BDDBuilder;
import ca.uwaterloo.gsd.fds.FDSFactory;
import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureNode;
import ca.uwaterloo.gsd.fm.ImplicationGraph;
import ca.uwaterloo.gsd.merge.experiments.BenchmarkEntry;
import dk.itu.fds.Implicant;
import dk.itu.fds.PrimeImplicants;
import dk.itu.fds.PrimeImplicantsOptimized;
import dk.itu.fds.ValidDomains;

/**
 * This class is an extension of the original BDD based FGBuilder, as opposed to the original 
 * implementation, this class implements the complete FGE algorithm.
 */
public class FGBuilderBDDBasedExtention<T> extends ca.uwaterloo.gsd.fds.FGBuilder<T> {

	protected BenchmarkEntry bmEntry = _benchmark.mkEntry();
	
	public FGBuilderBDDBasedExtention(BDDBuilder<T> builder){
		super(builder);
	}
	
	public FeatureGraph<T> build(BDD formula) {
		long start = System.currentTimeMillis();
		
		_formula = formula;
		_factory = formula.getFactory();
		
		FeatureGraph<T> g = new FeatureGraph<T>();
		
		logger.info("Implication graph");
		long impStart = System.currentTimeMillis();
		ImplicationGraph<T> impl = FDSFactory.makeImplicationGraph(_formula, _builder);
		long impEnd = System.currentTimeMillis();
		bmEntry.put("implication", impEnd - impStart);
		
		logger.info("And-groups");
		long andStart = System.currentTimeMillis();
		mkHierarchyAndGroups(g, impl);
		long andEnd = System.currentTimeMillis();
		bmEntry.put("hierarchy", andEnd - andStart);
		
		logger.info("Mutex graph and groups");
		mkMutexGroups(g);
		logger.info("Prime implicants and Or groups");
		long orStart = System.currentTimeMillis();
		mkOrGroups(g);
		long orEnd = System.currentTimeMillis();
		bmEntry.put("or", orEnd - orStart);
	
		logger.info("Transitive reduction");
		TransitiveReduction.INSTANCE.reduce(g);
		
		mkSyntheticRoot(g);
		long end = System.currentTimeMillis();
		bmEntry.put("total", end - start);
		
		_benchmark.add(bmEntry);

		return g;
	}
	
	@SuppressWarnings("unchecked")
	public void mkMutexGroups(FeatureGraph<T> g) {
		long mutexStart = System.currentTimeMillis();
		//Make Mutex Graph
		UndirectedGraph<FeatureNode<T>, DefaultEdge> mutex 
			= new SimpleGraph<FeatureNode<T>, DefaultEdge>(DefaultEdge.class);
	
		for (FeatureNode<T> v : g.vertices())
			mutex.addVertex(v);
		
		List<Integer> support = support();
		
		FeatureNode<T>[] vertices = g.vertices().toArray(new FeatureNode[0]);
		for (int i=0; i < vertices.length; i++) {
			FeatureNode<T> s1 = vertices[i];
			BDD mx = _formula.id ().andWith (_builder.mkFeatureNodeAnd(s1));

			if (mx.isZero ())
				throw new UnsupportedOperationException ("Dead features should have been removed!");

			ValidDomains vd = new ValidDomains(mx, Collections.min(support), Collections.max(support));
			for (int j=i+1;j<vertices.length; j++) {
				FeatureNode<T> s2 = vertices[j];

				//Only need to check one of the features (if it's an and-group)
				int v2 = _builder.variable(s2.features().iterator().next());
				if (!vd.canBeOne(v2) && vd.canBeZero(v2))
					mutex.addEdge(s1, s2);
			}
			mx.free();
		}
		long mutexEnd = System.currentTimeMillis();
		bmEntry.put("mutex-graph", mutexEnd - mutexStart);
		
		//Create Mutex Groups
		mutexStart = System.currentTimeMillis();
		
		_mutexCliques = findMutexCliques(mutex);
		for (Set<FeatureNode<T>> clique : _mutexCliques) {
			FeatureNode<T> parent = leastCommonAncestor(g, clique);
			
			//Mutex group doesn't have a common parent ?
			if (parent == null) {
				logger.warning("Mutex group without a common parent: " + clique);
				continue;
			}
			
			g.addEdge(clique, parent, FeatureEdge.MUTEX);
		}
		mutexEnd = System.currentTimeMillis();
		bmEntry.put("mutex-cliques", mutexEnd - mutexStart);
	
	}
	
	public void mkOrGroups(FeatureGraph<T> g) {
		for (FeatureNode<T> v : g.vertices()) {
			BDD b = _factory.one();
			for (T f: v.features())
				b.andWith(_factory.nithVar(_builder.variable(f)));
			
			// project formula on v and its children
			Set<T> keep = new HashSet<T>();
			keep.addAll(v.features());
			
			for (FeatureNode<T> child : g.children(v))
				keep.addAll(child.features());
			
			Collection<T> remove = CollectionUtils.subtract(_builder.domain(), keep);
			BDD exist = _builder.mkSet(remove);
			BDD frag = _formula.id().exist(exist);
			
			PrimeImplicants pi = new PrimeImplicantsOptimized (b, frag);
			
			frag.free();
			exist.free();
			b.free();
			
			for (Implicant imp : pi) {
				Implicant imp1 = imp.removeNegations();
				
				Set<FeatureNode<T>> grouped = new HashSet<FeatureNode<T>>();
				for (int i : imp1)
					grouped.add(g.findNode(_builder.feature(i)));
				
				if (grouped.size() == 0 || grouped.contains(v))
					continue;
				assert g.children(v).containsAll(grouped);
				
				if (g.findEdge(grouped, v, FeatureEdge.XOR) != null)
					continue;
	
				else if (isMutex(grouped)) {
					//Need to remove mutex since xor edge subsumes
					if(g.findEdge(grouped, v, FeatureEdge.MUTEX) != null)
						g.removeEdge(g.findEdge(grouped, v, FeatureEdge.MUTEX));
					g.addEdge(grouped, v, FeatureEdge.XOR);
				}
				else
					g.addEdge(grouped, v, FeatureEdge.OR);
			}
		}
	}


}
