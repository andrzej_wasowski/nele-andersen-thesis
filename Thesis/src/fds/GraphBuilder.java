package fds;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.BidiMap;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureNode;
import ca.uwaterloo.gsd.fm.ImplicationGraph;
import formula.cnf.CNF;
import formula.cnf.CNFBuilder;
import formula.dnf.DNF;
import formula.dnf.DNFBuilder;

/**
 *
 * This class contains static methods for computing the implication and mutex graphs from formulas in CNF and DNF. 
 */
public class GraphBuilder<T> {

	
	public static <T> ImplicationGraph<T> buildImplicationGraph(DNF formula, DNFBuilder<T> builder){
		
		BidiMap<Integer, T> features = builder.getFeatureMap();	
		ImplicationGraph<T> impl = new ImplicationGraph<T>();
		int[] variables = formula.getVariables();
		for(int i = 0; i < variables.length; i++){
			for(int j = i+1; j < variables.length; j++){
				if(formula.implication(variables[i], variables[j]))
					impl.addEdge(features.get(variables[i]), features.get(variables[j]));
				if(formula.implication(variables[j], variables[i]))
					impl.addEdge(features.get(variables[j]), features.get(variables[i]));
			}
		}
		return impl;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> UndirectedGraph<FeatureNode<T>, DefaultEdge> buildMutexGraph(FeatureGraph<T> g, DNF formula, DNFBuilder builder) {
		
		UndirectedGraph<FeatureNode<T>, DefaultEdge> mutex 
			= new SimpleGraph<FeatureNode<T>, DefaultEdge>(DefaultEdge.class);
		
		for (FeatureNode<T> v : g.vertices())
			mutex.addVertex(v);

		
		FeatureNode<T>[] vertices = g.vertices().toArray(new FeatureNode[0]);
		
		for(int i = 0; i < vertices.length; i++){
			for(int j = i+1; j < vertices.length; j++){
				if(formula.isMutex(builder.variable(vertices[i].features().iterator().next()), 
						builder.variable(vertices[j].features().iterator().next())))
					mutex.addEdge(vertices[i], vertices[j]);
			}
		}
		return mutex;

	}
	
	public static <T> ImplicationGraph<T> buildImplicationGraph(CNF formula, CNFBuilder<T> builder) {
	
		BidiMap<Integer, T> features = builder.getFeatureMap();
	
		ImplicationGraph<T> impl = new ImplicationGraph<T>();
		
		int[] variables = formula.getVariables();
		boolean[][] tested = new boolean[variables.length][variables.length];
		for(int i = 0; i < variables.length; i++)
			Arrays.fill(tested[i], false);
		// index used to keep track of which variables has been tested, maps a variable to its index in the tested array.
		Map<Integer, Integer> index = new HashMap<Integer, Integer>(variables.length);
		for(int i = 0; i < variables.length; i++){
			impl.addVertex(features.get(variables[i]));
			index.put(variables[i], i);
		}
		for(int i = 0; i < variables.length; i++){
			for(int j = i+1; j < variables.length; j++){
				if(!tested[i][j]){
					if(implication(formula, variables[i], variables[j], index, tested))
						impl.addEdge(features.get(variables[i]), features.get(variables[j]));
				}
				if(!tested[j][i]){
					if(implication(formula, variables[j], variables[i], index, tested))
						impl.addEdge(features.get(variables[j]), features.get(variables[i]));
				}
			}
		}
		return impl;
	}
	
	
	@SuppressWarnings("unchecked")
	public static <T> UndirectedGraph<FeatureNode<T>, DefaultEdge> buildMutexGraph(FeatureGraph<T> g, CNF formula, CNFBuilder builder) {
		
		UndirectedGraph<FeatureNode<T>, DefaultEdge> mutex 
			= new SimpleGraph<FeatureNode<T>, DefaultEdge>(DefaultEdge.class);
		
		for (FeatureNode<T> v : g.vertices())
			mutex.addVertex(v);

		
		FeatureNode<T>[] vertices = g.vertices().toArray(new FeatureNode[0]);
		
		boolean[][] tested = new boolean[vertices.length][vertices.length];
		for(int i = 0; i < vertices.length; i++)
			Arrays.fill(tested[i], false);
		// index used to keep track of which variables has been tested, maps a variable to its index in the tested array.
		Map<Integer, Integer> index = new HashMap<Integer, Integer>(vertices.length);
		for(int i = 0; i < vertices.length; i++){
			index.put(builder.variable(vertices[i].features().iterator().next()), i);
		}
		
		for(int i = 0; i < vertices.length; i++){
			for(int j = i+1; j < vertices.length; j++){
				if(!tested[i][j]){
					if(mutex(formula, builder.variable(vertices[i].features().iterator().next()), 
							builder.variable(vertices[j].features().iterator().next()), index, tested))
						mutex.addEdge(vertices[i], vertices[j]);
				}
			}
		}
		return mutex;
	}
	
	private static boolean implication(CNF formula, int i, int j, Map<Integer,Integer> index, boolean[][] tested) {
		if(formula.implication(i, j)){
			tested[index.get(i)][index.get(j)] = true;
			return true;
		}
		else{
			for(int k : index.keySet()){
				if(formula.assignment(k)){
					for(int l : index.keySet()){
						if(!formula.assignment(l))
							tested[index.get(k)][index.get(l)] = true;
					}
				}
			}
			return false;
		}
	}
	
	private static boolean mutex(CNF formula, int i, int j, Map<Integer,Integer> index, boolean[][] tested) {
		if(formula.implication(i, -j)){
			tested[index.get(i)][index.get(j)] = true;
			return true;
		}
		else{
			for(int k : index.keySet()){
				if(formula.assignment(k)){
					for(int l : index.keySet()){
						if(formula.assignment(l))
							tested[index.get(k)][index.get(l)] = true;
					}
				}
			}
			return false;
		}
	}
	
}
