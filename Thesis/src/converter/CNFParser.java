package converter;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import formula.cnf.CNF;
import formula.cnf.CNFClause;

/**
 * 
 * A parser for reading a DIMACS file.
 */
public class CNFParser {

	static private void skipCommentsAndProblemLine(LineNumberReader in) throws IOException {
		// comments start with 'c'.
		String line = in.readLine();
	    
		while(line.startsWith("c")) {
	        line = in.readLine();
		} 
	    //The first line to follow the comments is the problem line and we don't care about the 
		// problem line.
	}

	static private void readConstrsaints(LineNumberReader in, CNF cnf) throws IOException {
		String line;
			
		Set<Integer> literals = new HashSet<Integer>();
		
		while (true) {
			line = in.readLine();
		
			if (line == null) {
			// end of file
				if (literals.size() > 0) {
				// no 0 end the last clause
					cnf.addClause(new CNFClause(literals));
				}
				break;
			}
			if (line.startsWith("c ")) {
			// ignore comment line
				continue;
			}
			if (line.startsWith("%")) {
		   		System.out.println("Ignoring the rest of the file (SATLIB format");
		   		break;
			}
			handleConstr(line, literals, cnf);
		}	
	}

	static private boolean handleConstr(String line, Set<Integer> literals, CNF cnf) {
		int lit;
	    boolean added = false;
	    Scanner scan = new Scanner(line);
	    while (scan.hasNext()) {
	    	lit = scan.nextInt();
	        if (lit == 0) {
	        	if (literals.size() > 0) {
	        		cnf.addClause(new CNFClause(literals));
	                literals.clear();
	                added = true;
	        	}
	        } else {
	        	literals.add(lit);
	        }
	    }
	    return added;
	}
	
	static public CNF parseInstance(String fileName) throws IOException {
			LineNumberReader in = new LineNumberReader(new java.io.FileReader(fileName));
			
			CNF cnf = new CNF();
			
			skipCommentsAndProblemLine(in);
			readConstrsaints(in, cnf);
			
			return cnf;
			
			
	}

}