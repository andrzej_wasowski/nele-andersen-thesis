package converter;

import java.util.Arrays;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import dk.itu.fds.BDDBuilder;
import formula.Clause;
import formula.cnf.CNF;

/**
 * This class contains static methods for converting a CNF into a BDD.
 * IT ONLY WORKS on CNFs with NO DEAD FEATURES
 */
public class CNFConverter {

	static private BDDBuilder builder = new BDDBuilder ();
	static private BDDFactory factory = builder.factory ();
	
	/**
	 * Only works for CNFs without dead features.
	 * @param cnf: the CNF to be converted
	 * @return a BDD representation of the CNF
	 */
	public static BDD toBDD(CNF cnf){
		
		BDD temp = factory.one ();
		
		for(Clause c : cnf){
			temp.andWith(convertClause(c));
		}
		for(int i : cnf.getVariables()){
			if(Arrays.binarySearch (temp.support().scanSet(), i) < 0){
				temp.support().orWith(factory.ithVar(i));
			}
		}
			
		return temp;
	}
	
	/**
	 * 
	 * @param cnf: the CNF to be converted
	 * @param vars: the number of variables in the CNF
	 * @return a BDD representation of the CNF
	 */
	public static BDD toBDD(CNF cnf, int vars){
		factory.setVarNum(vars);
		return toBDD(cnf);
	}
	
	private static BDD convertClause(Clause c){
		
		BDD temp = factory.zero();
		for(int i : c){
			if(i > 0)
				temp.orWith (factory.ithVar (i));
			else{
				temp.orWith(factory.nithVar(-i));
			}
		}
		return temp;
	}
	
	public static BDDFactory getFactory(){
		return factory;
	}
	
}
