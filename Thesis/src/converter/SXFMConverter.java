package converter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import constraints.CNFClause;
import constraints.CNFLiteral;
import constraints.PropositionalFormula;
import fm.FeatureGroup;
import fm.FeatureModel;
import fm.FeatureModelException;
import fm.FeatureTreeNode;
import fm.SolitaireFeature;
import fm.XMLFeatureModel;

/**
 *  
 * This class provides static methods for converting a feature model in SXFM format to the FM format.
 * The class depend on sxfm.jar, this jar file should only be used when converting models from the 
 * www.splot-research.org, when converting models from fm.gdslab.org use the Fmapi.jar instead. As 
 * the two homepages uses slightly different SXFM formats.
 */
public class SXFMConverter {

	public static void convert(String sxfmFile, String fmFile) throws IOException, FeatureModelException {
		FileWriter writer = new FileWriter(fmFile);
		FeatureModel featureModel = new XMLFeatureModel(sxfmFile, XMLFeatureModel.USE_VARIABLE_NAME_AS_ID);
		
		featureModel.loadModel();
		
		//replace all characters that are illegal in fm format with _
		for(FeatureTreeNode node : featureModel.getNodes())
			node.setID(node.getID().replaceAll("\\W", "_"));
		
		traverseBFS(featureModel.getRoot(), writer);
		
		traverseConstraints(featureModel, writer);
		writer.close();

	}

	private static void traverseBFS(FeatureTreeNode node, FileWriter writer) throws IOException {
		StringBuilder sb = new StringBuilder();
		Queue<FeatureTreeNode> queue = new LinkedList<FeatureTreeNode>();
		queue.add(node);
		
		while(!queue.isEmpty()){
			node = queue.poll();
				
			if(!node.isLeaf())
				sb.append( node.getID() + ":");
			for( int i = 0 ; i < node.getChildCount() ; i++ ) {
				FeatureTreeNode child = (FeatureTreeNode) node.getChildAt(i);
				if ( child instanceof SolitaireFeature ) {
					// Optional Feature
					if ( ((SolitaireFeature) child).isOptional())
					sb.append(" " + child.getID() + "?" );
					// Mandatory Feature
					else
						sb.append(" " + child.getID());
					
					queue.add((FeatureTreeNode) node.getChildAt(i));
				}
				// Feature Group
				else if ( child instanceof FeatureGroup ) {
					int maxCardinality = ((FeatureGroup) child).getMax();
					sb.append("(");
					for( int j = 0 ; j < child.getChildCount() ; j++ ) {
						FeatureTreeNode grandChild = (FeatureTreeNode) child.getChildAt(j); 
						sb.append(grandChild.getID());
						if(j < child.getChildCount() -1)
							sb.append("|");
						queue.add(grandChild);
					}
					sb.append(")");
					//OR-Group
					if(maxCardinality == -1)
						sb.append("+");
				}
			}
			if(!node.isLeaf())
				sb.append(";\n");
		}
		writer.write(sb.toString());
	}
	
	private static void traverseConstraints(FeatureModel featureModel, FileWriter writer) throws IOException {
		StringBuilder sb = new StringBuilder();
		Iterator<PropositionalFormula> formulaItr = featureModel.getConstraints().iterator();
		while(formulaItr.hasNext()) {
			Iterator<CNFClause> clauseItr = formulaItr.next().toCNFClauses().iterator();
			//convert Clause
			sb.append("(");
			while(clauseItr.hasNext()){
				CNFClause clause = clauseItr.next();
				Iterator<CNFLiteral> literalItr = clause.getLiterals().iterator(); 
				while(literalItr.hasNext()){
					CNFLiteral literal = literalItr.next();
					if(!literal.isPositive())
						sb.append("!");
					sb.append(literal.getVariable().getID());
					if(literalItr.hasNext())
						sb.append("|");
				}
				sb.append(")");
				if(clauseItr.hasNext())
					sb.append("&");
			}
			sb.append(";\n");
		}
		writer.write(sb.toString());
	}
	
}
