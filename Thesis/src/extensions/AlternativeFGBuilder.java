package extensions;

import java.util.HashSet;
import java.util.Set;

import pig.PIG;
import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureNode;
import fds.FGBuilder;
import formula.Clause;
import formula.FormulaBuilder;
import formula.cnf.CNF;
import formula.cnf.CNFClause;
/**
 * This class implements the FGBuilder with the alternative optimisation of or-group
 * computation described in Chapter 10 of the Thesis. It only works on formulas 
 * describing well-formed feature models, i.e. the implication graph has exactly
 * one root after contraction of and-groups.  
 *
 */
public class AlternativeFGBuilder<T> extends FGBuilder<T> {

	public AlternativeFGBuilder(FormulaBuilder<T> builder) {
		super(builder);
	}

	public void mkOrGroups(FeatureGraph<T> g) {
		Set<Integer> vars = variablesToEliminate(g, g.root());
		CNF reduced = (CNF) _formula.eliminateVariables(vars);
	
		reduced.addClause(new CNFClause(new int[]{variable(g.root())}));
		PIG pig = new PIG(reduced);
		pig.getPrimeImplicates();
		for (FeatureNode<T> v : g.vertices()) {
			if(g.children(v).size() < 2) 
				continue;
			
			CNF primes = pig.getPrimeImplicates(variable(v));
			Set<Clause> orGroups = new HashSet<Clause>();
			for(Clause prime : primes){
				if(prime.size() > 1 && prime.isPositive())
					orGroups.add(prime);
			}
				
			for(Clause prime : orGroups){
				boolean cont = false;
				//check if the prime implicate consists of children of v
				for(int j : prime.getLiterals()){
					Set<FeatureNode<T>> children = g.children(v);
					if(!children.contains(g.findNode(feature(j)))){
						cont = true;
						break;
					}
				}
				if(cont)
					continue;
				Set<FeatureNode<T>> grouped = new HashSet<FeatureNode<T>>();
				for (int i : prime)
					grouped.add(g.findNode(feature(i)));

				if (grouped.size() == 0 || grouped.contains(v))
					continue;
				assert g.children(v).containsAll(grouped);

				if (g.findEdge(grouped, v, FeatureEdge.XOR) != null)
					continue;
				else if (isMutex(grouped)) {
					//Need to remove mutex since xor edge subsumes
					if(g.findEdge(grouped, v, FeatureEdge.MUTEX) != null)
						g.removeEdge(g.findEdge(grouped, v, FeatureEdge.MUTEX));
					g.addEdge(grouped, v, FeatureEdge.XOR);
				}
				else
					g.addEdge(grouped, v, FeatureEdge.OR);
			}
		}
	}
	
}
