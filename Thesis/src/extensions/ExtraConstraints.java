package extensions;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Set;

import org.apache.commons.collections15.BidiMap;
import org.sat4j.specs.TimeoutException;

import pig.PIG;
import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureNode;
import formula.Clause;
import formula.cnf.CNF;
import formula.cnf.CNFBuilder;
import formula.cnf.CNFClause;

/**
 * This class provides a static method for computing extra constraints as
 * described in chapter 10 of the Thesis. 
 */
public class ExtraConstraints<T> {

	public static <T> Set<Clause> compute(CNF original, CNFBuilder<T> builder, FeatureGraph<T> graph) throws TimeoutException{

		original.removeDeadFeatures();
		CNF synth = builder.mkModel(graph);
		
		// find roots in graph and force them to be true.
		Set<FeatureNode<T>> roots = findRoots(graph);
		for(FeatureNode<T> root : roots){
			for(T feature : root.features()){
				synth.addClause(new CNFClause(new int[]{builder.variable(feature)}));
				original.addClause(new CNFClause(new int[]{builder.variable(feature)}));
			}
		}

		// eliminate and-groups
		Set<Integer> varsToEliminate = new HashSet<Integer>();
		for(FeatureNode<T> node : graph.selectAndNodes()){
			Iterator<T> itr = node.features().iterator();
			itr.next();
			while(itr.hasNext()){
				varsToEliminate.add(builder.variable(itr.next()));
			}
		}
		CNF reducedSynth = (CNF) synth.eliminateVariables(varsToEliminate);
		CNF reducedOriginal = (CNF) original.eliminateVariables(varsToEliminate);
		
		// compute prime implicates of both formulas
		CNF originalPrimes = new PIG(reducedOriginal).getPrimeImplicates();
		Set<Clause> constraints = new HashSet<Clause>();

		for(Clause prime : originalPrimes){
			int[] negated = new int[prime.size()];
			int i = 0;
			for(int p : prime)
				negated[i++] = -p;
			if(reducedSynth.isSatisfiable(negated))
				constraints.add(prime);
		}
		if(constraints.size() == 0){
			return new HashSet<Clause>();
		}
		
		return constraints;
		
	}
	
	private static <T> Set<FeatureNode<T>> findRoots(FeatureGraph<T> g) {
		Set<FeatureNode<T>> cand = new HashSet<FeatureNode<T>>(g.vertices());
		for (FeatureEdge e : g.edges()) {
			cand.removeAll(g.getSources(e));
		}
		return cand;
	}
	
	static class ClauseSizeComparator implements Comparator<CNFClause> {

		@Override
		public int compare(CNFClause o1, CNFClause o2) {
			return o1.size() - o2.size();
		}
		
	}

}