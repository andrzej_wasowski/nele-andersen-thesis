package dk.itu.fds;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;

public class GraphUtil {

	public static <T> String getGraphString(Graph g, int[] support, ca.uwaterloo.gsd.fds.BDDBuilder<T> builder) {
		//Replace the existing label
		for (Map.Entry<Integer, NodeStyle> e : g.vstyles().entrySet ()) {
			Integer i = e.getKey();
			if (Arrays.binarySearch(support, i) >= 0)
				g.vstyles().put (i, new NodeStyle (i + ":" + builder.feature(i).toString()));
		}


		StringBuffer buf = new StringBuffer ();

		buf.append ("digraph {\n");

		for (int v : g.vertices())
			buf.append (v + g.vstyles().get(v).toString () + ";\n");

		for (Map.Entry<Integer, Set<Integer>> me : g.edges().entrySet ()) {
			for (int i : me.getValue ()) {
				buf.append (me.getKey ());
				buf.append ("->");
				buf.append (i);
				buf.append ("; ");
			}
			buf.append ("\n");
		}
		buf.append ("}\n");
		return buf.toString ();
	}
}
