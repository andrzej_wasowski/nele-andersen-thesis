package ca.uwaterloo.gsd.merge;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Set;

import org.apache.commons.collections15.Closure;

import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureNode;
import ca.uwaterloo.gsd.merge.ExpressionGenerator.CannotSatisfyException;

/**
 * Generates pseudo-random basic feature models.
 *
 * @author shshe
 *
 */
public class FeatureModelGenerator {

	
	int features;
	int branch_min = 2, branch_max = 5;

	double perc_mand = 0.2;
	double perc_or = 0.2;
	double perc_xor = 0.2;
	double perc_mutex = 0.2;

	double ecr = 0.2;
	double chance_negate = 0.5;

	String prefix = "f";

	Random rand = new Random(); // Add seed for reproducible testing

	public FeatureModelGenerator(int features) {
		this.features = features;
	}

	public void setSeed(long seed) {
		rand.setSeed(seed);
	}



	public Set<String> featureSet(FeatureModel<String> fm, double percent) {
		int numFeatures = (int) Math.round(fm.features().size() * percent);
		Set<String> result = new HashSet<String>(numFeatures);
		RandomQueue<String> queue = new RandomQueue<String>(fm.features());
		FeatureNode<String> root = fm.getDiagram().root();
		for (int i=0; i<numFeatures; i++) {
			String poll = queue.poll();
			if (root.features().contains(poll))
				continue;
			result.add(poll);
		}

		return result;
	}


	public FeatureModel<String> generate(int offset) {
		while (true) {
			try {
				return generate_internal(offset);
			}
			catch (CannotSatisfyException e) {}
		}
	}
	
	public FeatureModel<String> generate_internal(int offset) throws CannotSatisfyException {
		assert features > 0 && offset >=0;
		FeatureModel<String> fm = new FeatureModel<String>();

		final FeatureGraph<String> g = fm.getDiagram();
		final PriorityQueue<Subtree> subtrees = new RandomQueue<Subtree>(features);

		//Initialize subtrees / features
		for (int i=0; i<features; i++) {
			FeatureNode<String> f = new FeatureNode<String>(prefix + (i + offset));
			subtrees.add(new Subtree(f));
			g.addVertex(f);
		}


		//Generate Feature Groups
		Set<FeatureGroup<String>> groups = new HashSet<FeatureGroup<String>>();
		makeGroups((int) (features * perc_or), FeatureEdge.OR, subtrees, groups);
		makeGroups((int) (features * perc_xor), FeatureEdge.XOR, subtrees, groups);
		makeGroups((int) (features * perc_mutex), FeatureEdge.MUTEX, subtrees, groups);


		//Generate Mandatory Features
		int numMandatory = (int) Math.round(features * perc_mand);
		new Closure<Integer>() {
			public void execute(Integer i) {
				if (i == 0)	return;
				Subtree poll = subtrees.poll();
				poll.setMandatory(true);
				execute(i-1);
				subtrees.add(poll);
			}
		}.execute(numMandatory);


		//Piece together subtrees
		int maxRootChildren = rand.nextInt(branch_max-branch_min)+1+branch_min;
		while (subtrees.size() > maxRootChildren) {
			Subtree t1 = subtrees.poll();
			int numChildren = rand.nextInt(branch_max-branch_min)+1+branch_min;
			for (int i=0; i<numChildren && subtrees.size() > branch_min; i++) {
				t1.children().add(subtrees.poll());
			}
			subtrees.add(t1);
		}

		//Add root feature
		FeatureNode<String> rootNode = new FeatureNode<String>("Root");
		g.addVertex(rootNode);
		Subtree rootTree = new Subtree(rootNode);
		while (!subtrees.isEmpty()) {
			rootTree.children().add(subtrees.poll());
		}

		//Create feature model by connecting subtrees
		new Closure<Subtree>() {
			public void execute(Subtree t) {
				for (FeatureNode<String> child : t.childrenAsFeatureNodes())
					g.addEdge(child, t.featureNode(),
							FeatureEdge.OPTIONAL);

				for (Subtree childTree : t.children()) {
					if (childTree.isMandatory())
						g.addEdge(childTree.featureNode(), t.featureNode(),
							FeatureEdge.MANDATORY);

					execute(childTree);
				}
			}
		}.execute(rootTree);

		for (FeatureGroup<String> p : groups) {
			g.addEdge(p.getSources(), p.getTarget(), p.getType());
		}



		//Add extra constraints
		int numExtraVars = (int) Math.round(ecr * features);
		PriorityQueue<String> featureQueue = new RandomQueue<String>(g.features());
		featureQueue.removeAll(rootNode.features());

		String[] vars = new String[numExtraVars];
		for (int i=0; i<numExtraVars; i++) {
			vars[i] = featureQueue.poll();
		}

		//Generate constraints over these variables
		ExpressionGenerator gen = new ExpressionGenerator(fm);
		for (int i=0; i<numExtraVars; i++) {
			fm.getConstraints().add(gen.makeClause(vars));
		}

		return fm;
	}

	private void makeGroups(int numEdges, int type,
			PriorityQueue<Subtree> subtrees, Set<FeatureGroup<String>> groups) {
		//Partition edges into groups of size 2..branch_max
		RandomIterator groupIt = new RandomIterator(2, branch_max, numEdges);
		while(groupIt.hasNext()) {
			int numMembers = groupIt.next();
			Set<Subtree> members = new HashSet<Subtree>();

			for (int i=0; i<numMembers; i++) {
				members.add(subtrees.poll());
			}

			Subtree groupRoot = subtrees.poll();
			groupRoot.children().addAll(members);
			subtrees.add(groupRoot);

			Set<FeatureNode<String>> sources = new HashSet<FeatureNode<String>>();
			for (Subtree m : members) {
				sources.add(m.featureNode());
			}
			groups.add(new FeatureGroup<String>(sources, groupRoot.featureNode(), type));
		}
	}


	public class FeatureGroup<T> {
		private final Set<FeatureNode<T>> _sources;
		private final FeatureNode<T> _target;
		private final int _type;

		public FeatureGroup(Set<FeatureNode<T>> sources, FeatureNode<T> target, int type) {
			_sources = sources;
			_target = target;
			_type = type;
		}

		public Set<FeatureNode<T>> getSources() {
			return _sources;
		}

		public FeatureNode<T> getTarget() {
			return _target;
		}

		public int getType() {
			return _type;
		}
	}

	public class Subtree {
		Set<Subtree> _children = new HashSet<Subtree>();
		private final FeatureNode<String> _root;
		private boolean _isMandatory = false;

		public Subtree(FeatureNode<String> root) {
			assert root != null;
			_root = root;
		}

		public boolean isMandatory() {
			return _isMandatory;
		}

		public Set<Subtree> children() {
			return _children;
		}

		public Set<FeatureNode<String>> childrenAsFeatureNodes() {
			Set<FeatureNode<String>> result = new HashSet<FeatureNode<String>>();
			for (Subtree tree : _children) {
				result.add(tree.featureNode());
			}
			return result;
		}

		public void setMandatory(boolean isMandatory) {
			_isMandatory = isMandatory;
		}

		public FeatureNode<String> featureNode() {
			return _root;
		}
	}

	public class RandomQueue<T> extends PriorityQueue<T> {
		private static final long serialVersionUID = 165894387004262889L;

		public RandomQueue(int num) {
			super(num, new Comparator<T>() {

				public int compare(Object o1, Object o2) {
					return (int)(rand.nextInt(3) - 1);
				}

			});
		}

		public RandomQueue(Collection<T> items) {
			super (items.size(), new Comparator<T>() {

				public int compare(Object o1, Object o2) {
					return (int)(rand.nextInt(3) - 1);
				}

			});
			addAll(items);
		}
	}


	public class RandomIterator {
		private final int _min;
		private final int _max;
		private final int _total;
		private int _curr;

		public RandomIterator(int min, int max, int total) {
			_min = min;
			_max = max;
			_total = total;
		}

		public boolean hasNext() {

			return _curr < _total;
		}

		public int next() {
			int next = 0;
			int check = 0;

			do {
				next = rand.nextInt(_max - _min + 1) + _min;
				check = _total - _curr - next;
			} while (check > 0 && check < _min || check < 0);

			_curr += next;
			assert _curr <= _total;
			return next;
		}

		public int curr() {
			return _curr;
		}
	}
}
