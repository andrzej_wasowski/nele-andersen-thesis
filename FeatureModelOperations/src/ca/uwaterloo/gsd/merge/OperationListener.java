package ca.uwaterloo.gsd.merge;

import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureNode;

public interface OperationListener {

	public void notifyAmbiguous(FeatureEdge e, FeatureGraph<?> g);
	public void notifyAmbiguous(FeatureNode<?> v, FeatureGraph<?> g);

}
