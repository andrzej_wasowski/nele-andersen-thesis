package ca.uwaterloo.gsd.merge;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.collections15.CollectionUtils;

import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;

public class MergeUtil {

	protected static Logger logger = Logger.getLogger("fmm.MergeUtil");



	public static <T> Set<Set<FeatureEdge>> findGroupEdgeSets(FeatureGraph<T> g) {
		Set<Set<FeatureEdge>> result = new HashSet<Set<FeatureEdge>>();
		makeGroupEdgeSets(g, Collections.<FeatureEdge>emptySet(), g.selectGroupEdges(), result);
		return result;
	}



	/**
	 * FIXME This algorithm is exponential and duplicate sets are being created.
	 */
	protected static <T> void makeGroupEdgeSets(FeatureGraph<T> g, Set<FeatureEdge> chosen,
				Set<FeatureEdge> rest, Set<Set<FeatureEdge>> result) {

		if (chosen.size() == 0 && rest.size() == 0)
			return;
		else if (rest.size() == 0) {
			result.add(chosen);
			return;
		}

		for (FeatureEdge choose : rest) {
			Set<FeatureEdge> take = new HashSet<FeatureEdge>(chosen);
			take.add(choose);
			Set<FeatureEdge> left = new HashSet<FeatureEdge>(rest);
			left.remove(choose);

			//Find groups invalidated by choosing this group
			Iterator<FeatureEdge> iter = left.iterator();
			while (iter.hasNext()) {
				FeatureEdge next = iter.next();
				if (CollectionUtils.containsAny(g.getSources(next), g.getSources(choose)))
					iter.remove();
			}

			makeGroupEdgeSets(g, take, left, result);
		}
	}





}