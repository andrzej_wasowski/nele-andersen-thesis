package ca.uwaterloo.gsd.merge;

import java.util.logging.Logger;

import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureNode;

public class ConsoleOperationListener implements OperationListener{

	Logger logger;

	public ConsoleOperationListener(Class<?> caller) {
		logger = Logger.getLogger(caller.getCanonicalName());
	}

	@Override
	public void notifyAmbiguous(FeatureEdge e, FeatureGraph<?> g) {
		logger.info("Ambiguous edge: " + g.edgeString(e));
	}

	@Override
	public void notifyAmbiguous(FeatureNode<?> v, FeatureGraph<?> g) {
		logger.info("Ambiguous node: " + v);
	}

}
