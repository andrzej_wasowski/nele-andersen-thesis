package ca.uwaterloo.gsd.merge;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Combinations<T> {
	int[] indices;
	T[] items;
	int k;
	boolean hasMore = true;

	@SuppressWarnings("unchecked")
	public Combinations(Collection<T> itemset, int k) {
		this.items = (T[]) new Object[itemset.size()];
		itemset.toArray(items);
		this.k = k;

		//Initialise indices to be the first k elements in itemset
		indices = new int[k];
		for (int i=0; i<k; i++) {
			indices[i] = i;
		}
	}

	public List<T> next() {
		List<T> combo = new ArrayList<T>(k);
		for (int i=0; i<k; i++) {
			combo.add(items[indices[i]]);
		}
		hasMore = moveRightIndices();
		return combo;
	}

	public boolean hasNext() {
		return hasMore;
	}

	/*
    * Move the index forward a notch. The algorithm finds the rightmost
    * index element that can be incremented, increments it, and then
    * changes the elements to the right to each be 1 plus the element on their left.
    * <p>
    * For example, if an index of 5 things taken 3 at a time is at {0 3 4}, only the 0 can
    * be incremented without running out of room. The next index is {1, 1+1, 1+2) or
    * {1, 2, 3}. This will be followed by {1, 2, 4}, {1, 3, 4}, and {2, 3, 4}.
    * <p>
    * The algorithm is from Applied Combinatorics, by Alan Tucker.
    * Source: http://www.koders.com/java/fidB0EDBE3F9620D8D9987501FB538FA9C54864B8D2.aspx?s=mdef%3Ainsert
    */

	protected boolean moveRightIndices() {
		int index = getRightMostIndex();
		if (index >= 0) {
			//move the right most index by 1
			indices[index]++;
			for (int i=index+1; i<k; i++) {
				indices[i] = indices[i-1]+1;
			}
			return true;
		}
		return false;
	}

	/**
	 * Returns the right most index that is not at itemset.size()-1-i
	 * @return index if exists, -1 otherwise
	 */
	protected int getRightMostIndex() {
		//Iterate starting from the right most index
		for (int i=k-1; i>=0; i--) {
			if (indices[i] < items.length - k + i) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Returns (n choose 1) + (n choose 2) + (n choose 3) + ... + (n choose n-1)
	 * TODO Optimise such that anything about (n choose ceiling(n/2)) is
	 * calculated as (n choose n-k)
	 * @param setsize
	 * @return
	 */
	public static long findNumberOfCombinations(int setsize) {
		long result = 0;
		for (int k=1; k<setsize; k++) {
			result += choose(setsize, k);
		}
		return result;
	}

	public static long choose(int n, int k) {
		long topResult = 1, bottomResult = 1;
		for (int numerator=n; numerator>=n-k+1; numerator--) {
			topResult = topResult * numerator;
		}
		for (int denominator=k;denominator>=1; denominator--) {
			bottomResult = bottomResult * denominator;
		}
//		System.out.println("top: " + topResult + " bottom:" + bottomResult
//				+ " result:" + topResult / bottomResult);
		return topResult / bottomResult;
	}

}
