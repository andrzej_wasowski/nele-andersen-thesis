package ca.uwaterloo.gsd.merge.tests;

import junit.framework.TestCase;
import net.sf.javabdd.BDD;
import ca.uwaterloo.gsd.fds.BDDBuilder;
import ca.uwaterloo.gsd.fds.BDDBuilderSerializer;
import ca.uwaterloo.gsd.fds.FDSFactory;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureModelParser;

public class BDDBuilderTests extends TestCase {

	BDDBuilder<String> _builder;

	@Override
	protected void setUp() throws Exception {
		_builder = FDSFactory.makeStringBDDBuilder();
	}

	public void testBDDBuilder1() throws Exception {
		FeatureModel<String> fm1 = FeatureModelParser.parseString("a: b? c?;");
		BDD f1 = _builder.mkStructure(fm1);
		FeatureModel<String> fm1_expected = FeatureModelParser.parseString("b->a; c->a;");
		BDD f1_expected = _builder.mkStructure(fm1_expected);
		assertEquals(f1_expected, f1);
	}
	public void testBDDBuilder2() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: b? c;");
		BDD f = _builder.mkStructure(fm);
		FeatureModel<String> fm1_expected = FeatureModelParser.parseString("b->a; c->a; a->c;");
		BDD f_expected = _builder.mkStructure(fm1_expected);
		assertEquals(f_expected, f);
	}
	public void testBDDBuilder3() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: b (c|d);");
		BDD f = _builder.mkStructure(fm);
		FeatureModel<String> fm1_expected = FeatureModelParser.parseString(
				"b->a; c->a; d->a; a->b; a->(c|d); c->!d;");
		BDD f_expected = _builder.mkStructure(fm1_expected);
		assertEquals(f_expected, f);
	}


	public void testBDDBuilder4() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: (b|c)+ (c|d)+; d: e?;");
		BDD f = _builder.mkStructure(fm);
		FeatureModel<String> fm1_expected = FeatureModelParser.parseString(
				"b->a; c->a; d->a; e->d; a->(b|c); a->(c|d);");
		BDD f_expected = _builder.mkStructure(fm1_expected);
		assertEquals(f_expected, f);
	}

	
	public void testMutex() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: (b|c|d|e);");
		BDD f = _builder.mkStructure(fm);
		FeatureModel<String> expected = FeatureModelParser.parseString(
				"a->(b|c|d|e); b->(!c); b->(!d); b->(!e); c->(!d); c->(!e); d->(!e); b->a; c->a; d->a; e->a;");
		BDD f_ex = _builder.mkStructure(expected);
		assertEquals(f_ex, f);
		f.free();
		f_ex.free();
	}

	public void testSerializer1()  {
		_builder.add("123"); _builder.add("456"); _builder.add("789");
		String s = BDDBuilderSerializer.asString(_builder);
		BDDBuilder<String> loaded = BDDBuilderSerializer.fromString(s);
		assertEquals(_builder.getFeatureMap(), loaded.getFeatureMap());
	}

	public void testSerializer2() {
		for (int i='a'; i<='z';i++) {
			_builder.add(((char)i)+"");
		}
		String s = BDDBuilderSerializer.asString(_builder);
		BDDBuilder<String> loaded = BDDBuilderSerializer.fromString(s);
		assertEquals(_builder.getFeatureMap(), loaded.getFeatureMap());
	}

	public void testSerializer3() {
		for (int i='a'; i<='z';i++) {
			_builder.add((char)i + " " + (char)i);
		}
		String s = BDDBuilderSerializer.asString(_builder);
		BDDBuilder<String> loaded = BDDBuilderSerializer.fromString(s);
		assertEquals(_builder.getFeatureMap(), loaded.getFeatureMap());
	}
	
	public void testSerializer4() {
		for (int i='a'; i<='z';i++) {
			_builder.add(" " + (char)i + " " + (char)i + " ");
		}
		String s = BDDBuilderSerializer.asString(_builder);
		BDDBuilder<String> loaded = BDDBuilderSerializer.fromString(s);
		assertEquals(_builder.getFeatureMap(), loaded.getFeatureMap());
	}

}
