package ca.uwaterloo.gsd.merge.tests;

import junit.framework.TestCase;
import net.sf.javabdd.BDD;
import ca.uwaterloo.gsd.fds.BDDBuilder;
import ca.uwaterloo.gsd.fds.FDSFactory;
import ca.uwaterloo.gsd.fds.RemoveSyntheticRoot;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureModelParser;

public class FDSTests extends TestCase{

	BDDBuilder<String> builder = null;
	
	@Override
	protected void setUp() throws Exception {
		builder = FDSFactory.makeStringBDDBuilder();
	}
	
	public void testAndGroups1() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: b?; b: (c&d);");
		BDD F = builder.mkStructure(fm);
		BDD exF = builder.get("b").impWith(builder.get("a"))
			.andWith(builder.get("c").biimpWith(builder.get("d")))
			.andWith(builder.get("c").impWith(builder.get("b")));
		
		assertEquals(exF, F);
		
		FeatureGraph<String> result = FDSFactory.makeFeatureGraph(F, builder);
		RemoveSyntheticRoot.remove(result);
		
		assertEquals(fm.getDiagram(), result);
		F.free();
	}
	
	public void testAndGroups2() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: (b&c&d);");
		BDD F = builder.mkStructure(fm);
		FeatureGraph<String> result = FDSFactory.makeFeatureGraph(F, builder);
		RemoveSyntheticRoot.remove(result);
		assertEquals(fm.getDiagram(), result);
		F.free();
	}
	
	public void testAndGroups3() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: b? c?; b: (d&e); c:(f&g);");
		BDD F = builder.mkStructure(fm);
		FeatureGraph<String> result = FDSFactory.makeFeatureGraph(F, builder);
		RemoveSyntheticRoot.remove(result);
		assertEquals(fm.getDiagram(), result);
		F.free();
	}
	
	public void testAndGroups4() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: G=(b&c); G: e? (f&g);");
		BDD F = builder.mkStructure(fm);
		FeatureGraph<String> result = FDSFactory.makeFeatureGraph(F, builder);
		RemoveSyntheticRoot.remove(result);
		assertEquals(fm.getDiagram(), result);
		F.free();
	}
	
	public void testMutexGroups1() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: (b|c)?;");
		BDD F = builder.mkStructure(fm);
		FeatureGraph<String> result = FDSFactory.makeFeatureGraph(F, builder);
		RemoveSyntheticRoot.remove(result);
		assertEquals(fm.getDiagram(), result);
		F.free();
	}

	public void testMutexGroups2() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: (b|c|d)?;");
		BDD F = builder.mkStructure(fm);
		FeatureGraph<String> result = FDSFactory.makeFeatureGraph(F, builder);
		RemoveSyntheticRoot.remove(result);
		assertEquals(fm.getDiagram(), result);
		F.free();
	}
	
	public void testMutexGroups3() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: b? c?; b: (d|e)?; c: (f|g)?;");
		BDD F = builder.mkStructure(fm);
		FeatureGraph<String> result = FDSFactory.makeFeatureGraph(F, builder);
		RemoveSyntheticRoot.remove(result);
		assertEquals(fm.getDiagram(), result);
		F.free();
	}
	
	public void testMutexAndGroups1() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: G=(b&c); G: (x|y|z)?;");
		BDD F = builder.mkStructure(fm);
		FeatureGraph<String> result = FDSFactory.makeFeatureGraph(F, builder);
		RemoveSyntheticRoot.remove(result);
		assertEquals(fm.getDiagram(), result);
		F.free();
	}
	
	public void testOrGroups1() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: (b|c)+;");
		BDD F = builder.mkStructure(fm);
		FeatureGraph<String> result = FDSFactory.makeFeatureGraph(F, builder);
		RemoveSyntheticRoot.remove(result);
		assertEquals(fm.getDiagram(), result);
		F.free();
	}
	
	public void testOrGroups2() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: (b|c);");
		BDD F = builder.mkStructure(fm);
		FeatureGraph<String> result = FDSFactory.makeFeatureGraph(F, builder);
		RemoveSyntheticRoot.remove(result);
		assertEquals(fm.getDiagram(), result);
		F.free();
	}

	public void testOrGroups3() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: (b|c); b: (d|e)+;");
		BDD F = builder.mkStructure(fm);
		FeatureGraph<String> result = FDSFactory.makeFeatureGraph(F, builder);
		RemoveSyntheticRoot.remove(result);
		assertEquals(fm.getDiagram(), result);
		F.free();
	}

	public void testOrGroups4() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: (b|c|d)+; c: (e|f); d:(g|h)+;");
		BDD F = builder.mkStructure(fm);
		FeatureGraph<String> result = FDSFactory.makeFeatureGraph(F, builder);
		RemoveSyntheticRoot.remove(result);
		assertEquals(fm.getDiagram(), result);
		F.free();
	}

	public void test20min() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseFile("tests/20min.fm");
		BDD F = builder.mkStructure(fm);
		FeatureGraph<String> result = FDSFactory.makeFeatureGraph(F, builder);
	}

	public void testInfinite() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseFile("tests/infinite.fm");
		BDD F = builder.mkStructure(fm);
		FeatureGraph<String> result = FDSFactory.makeFeatureGraph(F, builder);
	}

	
}
