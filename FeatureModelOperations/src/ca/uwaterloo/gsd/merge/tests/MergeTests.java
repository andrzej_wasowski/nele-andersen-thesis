package ca.uwaterloo.gsd.merge.tests;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.logging.Logger;

import junit.framework.TestCase;
import net.sf.javabdd.BDD;

import org.apache.commons.collections15.Factory;

import ca.uwaterloo.gsd.fds.BDDBuilder;
import ca.uwaterloo.gsd.fds.FDSFactory;
import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureModelParser;
import ca.uwaterloo.gsd.fm.FeatureModelSerializer;
import ca.uwaterloo.gsd.fm.FeatureNode;
import ca.uwaterloo.gsd.merge.BasicFeatureModelIterator;
import ca.uwaterloo.gsd.merge.Disambiguator;
import ca.uwaterloo.gsd.merge.FeatureModelGenerator;
import ca.uwaterloo.gsd.merge.FeatureModelMutator;
import ca.uwaterloo.gsd.merge.SemanticOperations;
/**
 *
 * @author Steven She <shshe@uwaterloo.ca>
 *
 */
public class MergeTests extends TestCase {

	BDDBuilder<String> builder;
	SemanticOperations<String> ops;
	Logger logger = Logger.getLogger("fmm.MergeTests");

	public static final File OUTPUT_DIR = new File("output/");

	@Override
	protected void setUp() throws Exception {
		builder = FDSFactory.makeStringBDDBuilder();
		ops = new SemanticOperations<String>(builder);
	}

//	public void testAbsentLeftCombine() throws Exception {
//		FeatureModel<String> m1 = FeatureModelParser.parseInline("a: b?;b: d?;");
//		FeatureModel<String> m2 = FeatureModelParser.parseInline("a: b? c;b: d?;");
//
//		SemanticOperations<String> merger = new SemanticOperations<String>(builder);
//		FeatureGraph<String> intersection = merger.intersectionUsingLeftFeatures(m1, m2, "Root");
//
//		System.out.println(intersection);
//
//		FeatureModel<String> expected = FeatureModelParser.parseInline("Root: a?; a: b?;b: d?;");
//
//		assertEquals(expected.getDiagram(), intersection);
//
//	}

	/**
	 * TODO Assertions
	 * @throws Exception
	 */
	public void testDisjointIntersection() throws Exception {
		FeatureModel<String> m1 = FeatureModelParser.parseString("a: b?;");
		FeatureModel<String> m2 = FeatureModelParser.parseString("c: d?;");

		BDD c1 = builder.mkStructure(m1);
		BDD c2 = builder.mkStructure(m2);

		BDD merged = ops.intersection(c1, c2);
		FeatureModel<String> result = ops.render(merged, builder.one(), builder.one(), m1, m2);
		
		//FIXME the intersection of the two models should result in an empty model (ie. free variables shouldn't even be added)
		FeatureModel<String> expected = FeatureModelParser.parseString("Synth: a? b? c? d?;");
		assertEquals(expected, result);
		c1.free();
		c2.free();
	}

	public void testExtraConstraintMerge() throws Exception {
		FeatureModel<String> f1 = FeatureModelParser.parseString("a: b? c?; b: d?;");
		FeatureModel<String> f2 = FeatureModelParser.parseString("a: b? c?; b: d?; d->c;");

		BDD c1 = builder.mkStructure(f1);
		BDD c2 = builder.mkStructure(f2);
		BDD union = ops.union(c1,c2);

		FeatureModel<String> expected = FeatureModelParser.parseString("Synth: a; a: b? c?; b: d?;");
		FeatureModel<String> result = ops.render(union, builder.one(), builder.one(), f1, f2);
		System.out.println(expected.toString());
		assertEquals(expected.getDiagram(), result.getDiagram());
		
		c1.free();
		c2.free();
		union.free();
	}

	public void testExtraConstraintCompare() throws Exception {
		FeatureModel<String> f1 = FeatureModelParser.parseString("a: b? c?; b: d?;");
		FeatureModel<String> f2 = FeatureModelParser.parseString("a: b? c?; b: d?; d->c;");

		BDD c1 = builder.mkStructure(f1);
		BDD c2 = builder.mkStructure(f2);
		BDD inter = ops.intersection(c1, c2);

		FeatureModel<String> expected = FeatureModelParser.parseString("Synth: a; a: b? c?; b: d?; c: d?;");
		FeatureGraph<String> result = ops.synthesis(inter);

		assertEquals(expected.getDiagram(), result);

		c1.free();
		c2.free();
		inter.free();
	}

	public void testGroupFreeCompare() throws Exception {
		FeatureModel<String> f1 = FeatureModelParser.parseString("a: (b|c)+;");
		FeatureModel<String> f2 = FeatureModelParser.parseString("a: (c|d)+;");

		BDD d = builder.get("d");
		BDD c = builder.get("c");
		BDD b = builder.get("b");
		BDD a = builder.get("a");

		BDD c1 = builder.mkStructure(f1);

		BDD e1 = a.imp(b.or(c)).andWith(b.imp(a)).andWith(c.imp(a));
		assertEquals(e1, c1);

		BDD c2 = builder.mkStructure(f2);

		BDD e2 = a.imp(c.or(d)).andWith(c.imp(a)).andWith(d.imp(a));
		assertEquals(e2, c2);

		BDD inter = ops.intersection(c1, c2);
		BDD e_inter = e1.and(d.not()).andWith(e2.and(b.not()));
		assertEquals(e_inter, inter);

		logger.info("c1: " + c1  + " c2: " + c2 + " Intersection: " + inter);

		FeatureModel<String> expected = FeatureModelParser.parseString("Synth: (a&c);");
		FeatureModel<String> actual = ops.render(inter, builder.one(), builder.one(), f1, f2);
		System.out.println(actual);
		assertEquals(expected, actual);

		c1.free();
		c2.free();
		inter.free();
	}

	public void testAmbigousXorMerge() throws Exception {
		FeatureModel<String> f1 = FeatureModelParser.parseString("a:(b|c);");
		FeatureModel<String> f2 = FeatureModelParser.parseString("a:(c|d);");

		BDD c1 = builder.mkStructure(f1);
		BDD c2 = builder.mkStructure(f2);

		BDD inter = ops.intersection(c1, c2);
		FeatureModel<String> actual = ops.render(inter, builder.one(), builder.one(), f1, f2);
		FeatureModel<String> expected = FeatureModelParser.parseString("Synth: (a&c);");
		assertEquals(expected, actual);
	}


	public void testExpressionPrecedence() throws Exception  {
		FeatureModel<String> fm = FeatureModelParser.parseString("a->b->c->d;");
		BDD bdd = builder.mkExpression(fm.getConstraints().get(0));
		System.out.println(bdd);
		BDD expected = builder.get("a").impWith(builder.get("b")).impWith(builder.get("c")).impWith(builder.get("d"));
		assertEquals(expected, bdd);
	}


	public void testCombinations() throws Exception {
		FeatureModel<String> ambig = FeatureModelParser.parseString("a:b? c?; b: d?; c: d?;");
		BasicFeatureModelIterator<String> iter = new BasicFeatureModelIterator<String>(ambig.getDiagram());

		int combos = 0;
		while (iter.hasNext()) {
			FeatureModel<String> next = iter.next();
			combos ++;
		}
		assertEquals(2, combos);
	}



	public void testDisambiguateHierarchy() throws Exception {
		FeatureModel<String> gfd = FeatureModelParser.parseString("a: b? c? d?; b: e?; c: e?; d: e?;");
		FeatureModel<String> fm1 = FeatureModelParser.parseString("a: b? c? d?; c: e?;");

		Disambiguator<String> magic = new Disambiguator<String>(fm1.getDiagram(), fm1.getDiagram(), builder);
		FeatureGraph<String> result = magic.doHierarchy(gfd.getDiagram());

		assertEquals(result, fm1.getDiagram());
	}

	public void testDisambiguateGroups() throws Exception {
		FeatureModel<String> gfd = FeatureModelParser.parseString("a: (c|e)+; a: (c|d)+; c: e?;");
		FeatureModel<String> fm1 = FeatureModelParser.parseString("a: b? c? d?; c: e?;");

		Disambiguator<String> magic = new Disambiguator<String>(fm1.getDiagram(), fm1.getDiagram(), builder);
		FeatureGraph<String> result = magic.doHierarchy(gfd.getDiagram());
		result = magic.doGroupsHeuristic(result);

		FeatureModel<String> expected = FeatureModelParser.parseString("a: (c|d)+; c: e?;");

		System.out.println(result);

		System.out.println("Expected\n" + expected.getDiagram());

		assertEquals(expected.getDiagram(), result);
	}

	public void testAndGroup() throws Exception {
		FeatureModel<String> f1 = FeatureModelParser.parseString("a: (b&c);");
		BDD f1_bdd = builder.mkStructure(f1);

		System.out.println(f1);
		FeatureModel<String> e1 = FeatureModelParser.parseString("b->a; c->a; b->c; c->b;");
		BDD e1_bdd = builder.mkStructure(e1);

		assertEquals(e1_bdd, f1_bdd);

		FeatureModel<String> f2 = FeatureModelParser.parseString("a: G=(b&c); G: (d&e) f?;");

		BDD f2_bdd = builder.mkStructure(f2);

		FeatureModel<String> expected =
			FeatureModelParser.parseString("b->a; c->a; b->c; c->b; d->b; e->b; f->b; d->e; e->d;");

		BDD e2_bdd = builder.mkStructure(expected);

		assertEquals(e2_bdd, f2_bdd);
	}

	public void testReplaceVertex() throws Exception {
		FeatureModel<String> f1 = FeatureModelParser.parseString("a: (b|c)+ (c|d)?; c: e?; e: f?;");

		System.out.println(f1);

		FeatureGraph<String> g = f1.getDiagram();

		FeatureNode<String> c = g.findNode("c");
		g.replaceVertex(c, c.add("x"));

		System.out.println(g);

		BDD actual = builder.mkStructure(new FeatureModel<String>(g));

		FeatureModel<String> e1 = FeatureModelParser.parseString("a: (b|c)+ (c|d)?; c: e? f?; e: f?;");
		BDD expected = builder.mkStructure(new FeatureModel<String>(e1.getDiagram()))
			.andWith(builder.get("c").biimpWith(builder.get("x")));

		assertEquals(expected, actual);
	}

	public void testProjectXor1() throws Exception {
		FeatureModel<String> f1 = FeatureModelParser.parseString("a: b?; b: (x|y|z);");
		BDD struct = builder.mkStructure(f1);
		BDD project = ops.project(struct, new HashSet<String>(Arrays.asList("a", "x", "y", "z")));
		struct.free();
		FeatureGraph<String> actual = ops.synthesis(project);
		FeatureModel<String> expected = FeatureModelParser.parseString("Synth: a b?; a: (x|y|z)?;");

		assertEquals(actual, expected.getDiagram());
	}

	public void testProjectXor2() throws Exception {
		FeatureModel<String> f1 = FeatureModelParser.parseString("a: b?; b: (x|y|z);");
		BDD struct = builder.mkStructure(f1);
		BDD project = ops.project(struct, new HashSet<String>(Arrays.asList("b", "x", "y", "z")));
		struct.free();
		FeatureGraph<String> actual = ops.synthesis(project);
		FeatureModel<String> expected = FeatureModelParser.parseString("Synth: a? b; b: (x|y|z);");

		assertEquals(actual, expected.getDiagram());
	}

	/**
	 * FIXME this belongs in FeatureModelTests
	 * @throws Exception
	 */
	public void testSerializer() throws Exception {
		FeatureModelSerializer serial = new FeatureModelSerializer(true);
		{
			FeatureModel<String> f1 = FeatureModelParser.parseString("a: b?; b: (x|y|z);");
			FeatureModel<String> g1 = FeatureModelParser.parseString(serial.toString(f1));
			assertEquals(g1, f1);
		}
		{
			FeatureModel<String> f2 = FeatureModelParser.parseString("a: b? c d; d: (x|y|z)? (q|w)+ (e|r);");
			FeatureModel<String> g2 = FeatureModelParser.parseString(serial.toString(f2));
			assertEquals(g2, f2);
		}
		{
			FeatureModel<String> f3 = FeatureModelParser.parseString("a: G=(x&y&z); G: q? w (e|r)?;");
			FeatureModel<String> g3 = FeatureModelParser.parseString(serial.toString(f3));
			System.out.println(serial.toString(f3));
			assertEquals(g3, f3);
		}
		{
			FeatureModel<String> f4 = FeatureModelParser.parseString("a: b? c?; c: d? (e|f|g)+; b->c; f->g; f&e;");
			FeatureModel<String> g4 = FeatureModelParser.parseString(serial.toString(f4));
			assertEquals(g4, f4);
		}
	}


	public void testSatCount() throws Exception {
		FeatureGraph<String> g = FeatureModelParser.parseString("a: b?;").getDiagram();
		BDD confBDD = builder.mkConfiguration(new FeatureModel<String>(g));
		BDD support = builder.mkSet(g.features());
		assertEquals(2.0, confBDD.satCount(support));
		support.free();
		confBDD.free();

		g = FeatureModelParser.parseString("a: b?; b: c?;").getDiagram();
		confBDD = builder.mkConfiguration(new FeatureModel<String>(g));
		support = builder.mkSet(g.features());
		assertEquals(3.0, confBDD.satCount(support));
		confBDD.free();
		support.free();

		g = FeatureModelParser.parseString("a: b?;").getDiagram();
		confBDD = builder.mkConfiguration(new FeatureModel<String>(g));
		support = builder.mkSet(g.features());
		assertEquals(2.0, confBDD.satCount(support));
		confBDD.free();
		support.free();

		g = FeatureModelParser.parseString("a: b? c?;").getDiagram();
		confBDD = builder.mkConfiguration(new FeatureModel<String>(g));
		support = builder.mkSet(g.features());
		assertEquals(4.0, confBDD.satCount(support));
		confBDD.free();
		support.free();

		g = FeatureModelParser.parseString("a: b? c? d?;").getDiagram();
		confBDD = builder.mkConfiguration(new FeatureModel<String>(g));
		support = builder.mkSet(g.features());
		assertEquals(8.0, confBDD.satCount(support));
		confBDD.free();
		support.free();
	}

	public void testFeatureModelGenerator() throws Exception {
		int num=50;
		FeatureModelGenerator gen = new FeatureModelGenerator(num);
		FeatureModel<String> fm = gen.generate(0);
		assertEquals(num+1, fm.features().size());
		FeatureGraph<String> g = fm.getDiagram();
		assertEquals((int)(num*0.2), g.selectMandatoryNodes().size());

		Set<FeatureNode<String>> grouped = new HashSet<FeatureNode<String>>();
		for(FeatureEdge e : g.selectGroupEdges()) {
			grouped.addAll(g.getSources(e));
		}
		assertEquals((int)(num*0.6), grouped.size());
	}


	//TODO
	public void testDisambiguateAndGroups() throws Exception {
		FeatureGraph<String> gfd = FeatureModelParser.parseString(
				"a: H=(b&c&d);").getDiagram();

		// All siblings --> NO CHANGE!
		{
			FeatureModel<String> fm1 = FeatureModelParser.parseString("a: b? c? d?;");
			FeatureGraph<String> fd1 = fm1.getDiagram();
			Disambiguator<String> disambig1 = new Disambiguator<String>(fd1, builder);
			assertEquals(gfd, disambig1.doAndGroups(gfd));
		}

		{
			// b is a parent of c and d
			FeatureModel<String> fm2 = FeatureModelParser
					.parseString("a: b?; b: c? d?;");
			FeatureGraph<String> fd2 = fm2.getDiagram();
			Disambiguator<String> disambig2 = new Disambiguator<String>(fd2, builder);
			FeatureModel<String> e2 = FeatureModelParser
					.parseString("a: b?; b: c d;");
			assertEquals(e2.getDiagram(), disambig2.doAndGroups(gfd));
		}

		{
			// b is a parent of c, c is a parent of d
			FeatureModel<String> fm3 = FeatureModelParser
					.parseString("a: b?; b: c?; c: d?;");
			FeatureGraph<String> fd3 = fm3.getDiagram();
			Disambiguator<String> disambig3 = new Disambiguator<String>(fd3, builder);
			FeatureModel<String> e3 = FeatureModelParser
					.parseString("a: b?; b: c; c: d;");
			System.out.println(e3.getDiagram());
			assertEquals(e3.getDiagram(), disambig3.doAndGroups(gfd));
		}

		{
			FeatureGraph<String> gfd2 = FeatureModelParser.parseString(
					"a: H=(b&c&d); H: (e&f);").getDiagram();
			FeatureModel<String> fm4 = FeatureModelParser
					.parseString("a: b?; b: c d e;e: f;");
			Disambiguator<String> disambig4 = new Disambiguator<String>(fm4.getDiagram(), builder);
			disambig4.doAndGroups(gfd2);
		}
	}

	public void testFeatureModelMutation() throws Exception {
		for (int i=0; i<50; i++) {
		FeatureModel<String> fm = FeatureModelParser.parseString(
				"a: b? c? (x|y|z)?; z: (q|w|e)+; b: (r|t); r: (u|i)+ o p;");

		FeatureModelMutator mut = new FeatureModelMutator(new Factory<String>() {
			int i=0;
			public String create() {
				return "f" + i++;
			}

		});
		mut.setSeed(1251);
		FeatureModel<String> mutant = mut.mutate(fm, 500);
		System.out.println(mutant);
		}
	}
	
	/**
	 * Note: This doesn't test GFD serialization
	 * @throws Exception
	 */
	public void testFeatureModelSerializer() throws Exception {
		Random rand = new Random();
		for (int i=0; i<100;i++) {
			int num=rand.nextInt(50) + 20;
			FeatureModelGenerator gen = new FeatureModelGenerator(num);
			FeatureModel<String> expected = gen.generate(0);
			FeatureModelSerializer serial = new FeatureModelSerializer(true);
			String s = serial.toString(expected);
			FeatureModel<String> actual = FeatureModelParser.parseString(s);
			assertEquals(expected, actual);
		}
	}
}
