package ca.uwaterloo.gsd.merge.tests;

import junit.framework.TestCase;
import ca.uwaterloo.gsd.fm.Expression;
import ca.uwaterloo.gsd.fm.ExpressionParser;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureModelParser;
import ca.uwaterloo.gsd.fm.sat.SATBuilder;

public class SATBuilderTests extends TestCase {

	SATBuilder<String> builder;
	
	@Override
	protected void setUp() throws Exception {
		builder = new SATBuilder<String>();
		builder.add("a");
		builder.add("b");
		builder.add("c");
	}
	
	public void testHierarchy() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: b?;");
		assertEquals(true, builder.isSatisfiable(fm));
		Expression<String> a1 = ExpressionParser.parseString("!a;");
		fm.getConstraints().add(a1);
		assertEquals(false, builder.isSatisfiable(fm));
	}
	
	public void testMandatory() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: b;");
		assertEquals(true, builder.isSatisfiable(fm));
		Expression<String> a1 = ExpressionParser.parseString("!b;");
		fm.getConstraints().add(a1);
		assertEquals(false, builder.isSatisfiable(fm));
	}

	
	public void testOrGroup() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: (b|c)+;");
		assertEquals(true, builder.isSatisfiable(fm));
		Expression<String> a1 = ExpressionParser.parseString("!b&!c;");
		fm.getConstraints().add(a1);
		assertEquals(false, builder.isSatisfiable(fm));
		Expression<String> a2 = ExpressionParser.parseString("b&c;");
		fm.getConstraints().remove(0);
		fm.getConstraints().add(a2);
		assertEquals(true, builder.isSatisfiable(fm));
	}
	
	public void testXorGroup() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("a: (b|c);");
		assertEquals(true, builder.isSatisfiable(fm));
		Expression<String> a1 = ExpressionParser.parseString("!b&!c;");
		fm.getConstraints().add(a1);
		assertEquals(false, builder.isSatisfiable(fm));
		Expression<String> a2 = ExpressionParser.parseString("b&c;");
		fm.getConstraints().remove(0);
		fm.getConstraints().add(a2);
		assertEquals(false, builder.isSatisfiable(fm));
		Expression<String> a3 = ExpressionParser.parseString("b;");
		fm.getConstraints().remove(0);
		fm.getConstraints().add(a3);
		assertEquals(true, builder.isSatisfiable(fm));
		Expression<String> a4 = ExpressionParser.parseString("c;");
		fm.getConstraints().remove(0);
		fm.getConstraints().add(a4);
		assertEquals(true, builder.isSatisfiable(fm));
	}
	
	public void testSplitConjunction() throws Exception {
		FeatureModel<String> fm = FeatureModelParser.parseString("(a|b|c)&(!d|e)&(f|!g);");
		assertEquals(3,builder.splitConjunction(fm.getConstraints().get(0)).size());
	}
	
}
