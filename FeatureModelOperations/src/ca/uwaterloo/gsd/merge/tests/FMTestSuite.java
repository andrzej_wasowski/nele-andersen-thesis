package ca.uwaterloo.gsd.merge.tests;

import junit.framework.Test;
import junit.framework.TestSuite;
import ca.uwaterloo.gsd.fm.FeatureGraphTests;

public class FMTestSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite("Feature Model Tests");

//		suite.addTestSuite(dk.itu.fds.FeatureGraphTest.class);
		suite.addTestSuite(SATBuilderTests.class);
		suite.addTestSuite(BDDBuilderTests.class);
		suite.addTestSuite(FeatureGraphTests.class);
		suite.addTestSuite(FDSTests.class);		
		suite.addTestSuite(MergeTests.class);

		return suite;
	}

}
