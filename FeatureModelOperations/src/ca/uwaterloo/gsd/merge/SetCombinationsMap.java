package ca.uwaterloo.gsd.merge;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SetCombinationsMap<T> extends SetCombinations<T> {

	private T[] origin;

	@SuppressWarnings("unchecked")
	public SetCombinationsMap(List<Set<T>> sets, List<T> origin) {
		super(sets);
		this.origin = (T[]) origin.toArray();
	}

	/**
	 * Returns a ?bipartite? map from the origin to its choice.
	 * @return
	 */
    public Map<T, T> nextElementMap() {
    	Map<T, T> result = new HashMap<T, T>();
    	T[] next = next();
    	assert next.length == origin.length;
    	for (int i=0; i<origin.length;i++) {
    		result.put(origin[i], next[i]);
    	}
    	return result;
    }


}
