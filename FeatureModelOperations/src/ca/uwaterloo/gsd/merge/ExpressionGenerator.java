package ca.uwaterloo.gsd.merge;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import ca.uwaterloo.gsd.fm.Expression;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureNode;

public class ExpressionGenerator {

	public static class CannotSatisfyException extends Exception {
	}
	
	private final FeatureGraph<String> _g;
	private final FeatureModel<String> _fm;
	private final Set<String> _mandatory;
	public static final int NUM_TRIES = 1000;
	
	public ExpressionGenerator(FeatureModel<String> fm) {
		_fm = fm;
		_g = _fm.getDiagram();
		
		_mandatory = new HashSet<String>(_g.root().features());
		for (FeatureNode<String> v : _g.selectMandatoryNodes()) {
			_mandatory.addAll(v.features());
		}
	}

	Random rand = new Random();

	public Expression<String> makeClause(String[] vars) throws CannotSatisfyException {
		boolean isThreeVar = rand.nextBoolean();
		if (isThreeVar && vars.length > 2)
			return	makeThreeVarClause(vars);
		return	makeTwoVarClause(vars);
	}

	public void setSeed(long seed){
		rand.setSeed(seed);
	}

	public Expression<String> makeThreeVarClause(String[] vars) throws CannotSatisfyException {
		if (vars.length < 3)
			throw new IllegalArgumentException("length of vars cannot be less than 3!");
		
		int v1,v2,v3, count=0;
		do {
			v1 = rand.nextInt(vars.length);
			
			do {
				v2 = rand.nextInt(vars.length);
			} while (v2 == v1);
			
			do {
				v3 = rand.nextInt(vars.length);
			} while (v3 == v2 || v3 == v1 );
			
			if (count++ > NUM_TRIES)
				throw new CannotSatisfyException();
		} while (_mandatory.contains(vars[v1]) 
				|| _mandatory.contains(vars[v2]) 
				|| _mandatory.contains(vars[v3]));

		boolean v3neg = rand.nextBoolean();
		Expression<String> e3 = new Expression<String>(vars[v3]);
		if (v3neg)
			e3 = e3.not();

		return makeTwoVarClause(v1, v2, vars).or(e3);
	}

	public Expression<String> makeTwoVarClause(String[] vars) throws CannotSatisfyException {
		if (vars.length < 2)
			throw new IllegalArgumentException("length of vars cannot be less than 2!");
		
		int v1,v2, count=0;
		do {
			v1 = rand.nextInt(vars.length);
			
			do {
				v2 = rand.nextInt(vars.length);
			} while (v2 == v1);
			
			if (count++ > NUM_TRIES)
				throw new CannotSatisfyException();
		} while (_mandatory.contains(vars[v1]) 
				|| _mandatory.contains(vars[v2]));
		
		return makeTwoVarClause(v1, v2, vars);
	}

	private Expression<String> makeTwoVarClause(int v1, int v2, String[] vars) {

		Expression<String> e1 = new Expression<String>(vars[v1]);
		Expression<String> e2 = new Expression<String>(vars[v2]);

		boolean v1neg = rand.nextBoolean();
		boolean v2neg = rand.nextBoolean();

		if (v1neg)
			e1 = e1.not();
		if (v2neg)
			e2 = e2.not();

		return e1.or(e2);
	}


}
