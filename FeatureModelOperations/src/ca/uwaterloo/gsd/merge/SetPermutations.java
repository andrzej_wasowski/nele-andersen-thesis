package ca.uwaterloo.gsd.merge;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class SetPermutations<T> {
	private final T[][] itemsets;
	private int[] indices;
	private int boundary;

	@SuppressWarnings("unchecked")
	public SetPermutations(List<Collection<? extends T>> sets) {
		this.itemsets = (T[][]) new Object[sets.size()][];

		ListIterator<Collection<? extends T>> iter = sets.listIterator();
		while (iter.hasNext()) {
			int index = iter.nextIndex();
			Collection<? extends T> s = iter.next();
			this.itemsets[index] = 	(T[]) new Object[s.size()];
			Iterator<? extends T> setIter = s.iterator();
			int i=0;
			while (setIter.hasNext()) {
				this.itemsets[index][i++] = setIter.next();
			}
		}

        indices = new int[itemsets.length];
        for (int i = 0; i < itemsets.length; i++)
            indices[i] = 0;

        boundary = 0;
	}

    public boolean hasNext() {
        return boundary<indices.length && indices[boundary] < itemsets[boundary].length;
    }

	public List<T> next() {
    	List<T> result = new ArrayList<T>(itemsets.length);

        for (int i=0; i<itemsets.length; i++) {
        	result.add(itemsets[i][indices[i]]);
        }

        moveIndicies();
        return result;
    }

    /**
     * Resets the right-most iterator.
     */
    private void moveIndicies() {
		for (int i=0;i<=boundary;i++) {
			if (indices[i]<itemsets[i].length-1) {
				indices[i]++;
				//Reset previous indices for permutations
				for (int j=0;j<i;j++) {
					indices[j]=0;
				}
				return;
			}
		}

		boundary++;
		//Set all indices left of boundary to 0
		for (int i=0;i<boundary;i++) {
			indices[i] = 0;
		}

		if (boundary < itemsets.length) {
			indices[boundary]++;
		}
    }


    public int total() {
    	int combos = 1;
    	for (T[] set : itemsets) {
    		combos *= set.length;
    	}
    	return combos;
    }
}
