package ca.uwaterloo.gsd.merge;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import net.sf.javabdd.BDD;
import ca.uwaterloo.gsd.fds.BDDBuilder;
import ca.uwaterloo.gsd.fds.FDSFactory;
import ca.uwaterloo.gsd.fds.FrozenCoreBuilder;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;

/**
 * Merges two feature models based on their propositional logic semantics. The
 * feature-diagram-synthesis algorithm is used to reverse-engineer a feature
 * diagram given the merged semantic formula.
 *
 * @author Steven She <shshe@uwaterloo.ca>
 */
public class SemanticOperations<T> {

	BDDBuilder<T> _builder;
	Logger logger = Logger.getLogger("fmm.SemanticMerge");

	public SemanticOperations(BDDBuilder<T> builder) {
		_builder = builder;
	}


	public BDDBuilder<T> getBuilder() {
		return _builder;
	}

	/**
	 * Merges the left and right feature models and returns a generalized
	 * feature diagram. The resulting diagram is a union of the configurations
	 * in the left and right models.
	 *
	 * @return Generalized Feature Diagram
	 */
	public BDD union(BDD f1, BDD f2) {
		deselectUnique(f1, f2);

		// Create disjunction between f1 and f2
		BDD result = f1.or(f2);
		return result;
	}


	public void deselectUnique(BDD f1, BDD f2) {
		//Maintain set of features
		BDD f1supp = f1.support();
		BDD f2supp = f2.support();

		int[] f1vars = f1supp.scanSet();
		int[] f2vars = f2supp.scanSet();

		f1supp.free();
		f2supp.free();


		Set<Integer> f1set = new HashSet<Integer>();
		if (f1vars != null) {
		for (int i : f1vars) {
			f1set.add(i);
		}
		}

		Set<Integer> f2set = new HashSet<Integer>();
		if (f2vars != null) {
		for (int i : f2vars) {
			f2set.add(i);
		}
		}

		Set<Integer> f1uniq = new HashSet<Integer>(f1set);
		Set<Integer> f2uniq = new HashSet<Integer>(f2set);

		f1uniq.removeAll(f2set);
		f2uniq.removeAll(f1set);

		if (f1uniq.size() > 0) {
			BDD negateF1 = _builder.one();
			for (int i : f1uniq) {
				negateF1.andWith(_builder.getFactory().nithVar(i));
			}
			f2.andWith(negateF1);
		}

		if (f2uniq.size() > 0) {
			BDD negateF2 = _builder.one();
			for (int i : f2uniq) {
				negateF2.andWith(_builder.getFactory().nithVar(i));
			}
			f1.andWith(negateF2);
		}
	}


	public BDD expandPositive(BDD formula, Collection<T> universe) {
		BDD supp = formula.support();
		BDD uni = _builder.mkSet(universe);

		BDD check = uni.imp(supp);
		assert check.isOne();
		check.free();

		BDD unique = uni.exist(supp);
		supp.free();
		uni.free();

		BDD result = formula.and(unique);

		unique.free();

		return result;
	}

	public BDD project(BDD c1, Collection<T> universe) {
		BDD keep = _builder.mkSet(universe);
		BDD result = project(c1, keep);
		keep.free();
		return result;
	}

	public BDD project(BDD c1, BDD keep) {
		BDD supp = c1.support();
		BDD remove = supp.exist(keep);
		supp.free();

		BDD c1exist = c1.exist(remove);

		return c1exist;
	}


	public BDD projectPositive(BDD formula, Collection<T> universe) {
		BDD supp = formula.support();
		BDD uni = _builder.mkSet(universe);

		BDD unique = supp.exist(uni);
		BDD result = formula.restrict(unique);

		unique.free();
		supp.free();
		uni.free();

		return result;

	}

	/**
	 * FIXME not sure if this is correct
	 * @return
	 */
	public BDD difference(BDD f1, BDD f2) {
		deselectUnique(f1, f2);
		BDD f2_not = f2.not();
//		BDD f2_supp = f2.support();
//		BDD f1_supp = f1.support();
//		BDD f1_uniq = f1_supp.exist(f2_supp);
//
//
//		int[] neg_set = f1_uniq.scanSet();
//
//		if (neg_set != null) {
//			for (int i : neg_set) {
//				f2_not.andWith(_builder.nithVar(i));
//			}
//		}
//
//		f2_supp.free(); f1_supp.free(); f1_uniq.free();
//
//		System.out.println(f2 + "\t\t" + f2_not);

		BDD result = f1.and(f2_not);
		f2_not.free();
		return result;
	}

	public BDD difference_constraint(BDD f1, BDD f2, Collection<T> features) {
		BDD f2_not = f2.not();
		BDD result = f1.or(f2_not);
		BDD proj = project(result, features);
		result.free();
		f2_not.free();
		return proj;
	}


	public FeatureGraph<T> freeze(BDD formula, BDD frozen) {
		FrozenCoreBuilder<T> frozenCore = new FrozenCoreBuilder<T>(_builder);
		FeatureGraph<T> result = FDSFactory.makeFeatureGraph(formula, _builder);
		return frozenCore.build(result, frozen);
	}

	public FeatureGraph<T> synthesis(BDD formula) {
		return FDSFactory.makeFeatureGraph(formula, _builder);
	}


	/**
	 * FIXME features parameter is unused
	 */
	public FeatureModel<T> render(BDD formula, BDD features, BDD frozen, FeatureModel<T> f1, FeatureModel<T> f2) {
		BDD alive = removeDeadFeatures(formula);
		FeatureGraph<T> result = FDSFactory.makeFeatureGraph(alive, _builder);
		alive.free();

		FrozenCoreBuilder<T> frozenCore = new FrozenCoreBuilder<T>(_builder);
		result = frozenCore.build(result, frozen);

		Disambiguator<T> disambig = new Disambiguator<T>(f1.getDiagram(), f2.getDiagram(), _builder);
		result = disambig.doHierarchyAlternate(result);
		result = disambig.removeHierarchyViolatingGroups(result);
		result = disambig.doAndGroups(result);
		result = disambig.doGroupsHeuristic(result);
		int count = 0;

		//FIXME temporary fix to ensure that this will terminate
		while (count++<10 && Disambiguator.isAmbiguous(result)) {
			result = disambig.doGroupsHeuristic(result);
		}


		//FIXME add extra constraints
		return new FeatureModel<T>(result);
	}


	public BDD union_constraint(BDD f1, BDD f2) {
		return f1.and(f2);
	}

	//FIXME
	public BDD intersection_constraint(BDD f1, BDD f2, Collection<T> features) {
		BDD or = f1.or(f2);
		BDD result = project(or, features);
		or.free();
		return result;


	}

	public BDD intersection(BDD f1Bdd, BDD f2Bdd) {
		deselectUnique(f1Bdd, f2Bdd);

		// Create disjunction between f1 and f2
		BDD result = f1Bdd.and(f2Bdd);
		return result;
	}
	
	/**
	 * Collapses equivalent (coincident) variables into one in the BDD.
	 *
	 * @param problem
	 * @param sup
	 */
	public static BDD removeDeadFeatures(BDD problem) {
		if (problem.isOne() || problem.isZero())
			return problem.id();
		
		BDD sup = problem.support ();
		int[] support = sup.scanSet();
		sup.free();

		ArrayList<Integer> dead = new ArrayList<Integer>();

		for (int v : support) {
			BDD check =	problem.id().restrictWith(problem.getFactory().ithVar(v));
			if (check.isZero()) {
				dead.add(v);
			}
			check.free();
		}

		int[] deadArr = new int[dead.size()];
		for (int i=0; i<dead.size(); i++) {
			deadArr[i] = dead.get(i);
		}

//		int[] deadArr = Util.asIntArray (dead.toArray(new Integer[0]));

		Logger.getAnonymousLogger().info("Dead Features:" + dead.size());
		BDD exist = problem.getFactory().makeSet(deadArr);
		BDD result = problem.exist(exist);
		exist.free();

		return result;
	}

}
