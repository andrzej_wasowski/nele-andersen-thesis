package ca.uwaterloo.gsd.merge;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

/**
 * Iterates through the m-Combinations in m-sets. We assume that the input sets
 * are given in order, such that the first item in the resulting tuples belongs
 * to the first input set, etc.
 *
 * @author Steven She <shshe@uwaterloo.ca>
 * @param <T>
 */
@SuppressWarnings("unchecked")
public class SetCombinations<T> {

	private final T[][] itemsets;
	private int[] indices;
	private int boundary;

	public SetCombinations(List<Set<T>> sets) {
		this.itemsets = (T[][]) new Object[sets.size()][];

		ListIterator<Set<T>> iter = sets.listIterator();
		while (iter.hasNext()) {
			int index = iter.nextIndex();
			Set<T> s = iter.next();
			this.itemsets[index] = 	(T[]) new Object[s.size()];
			Iterator<T> setIter = s.iterator();
			int i=0;
			while (setIter.hasNext()) {
				this.itemsets[index][i++] = setIter.next();
			}
		}

        indices = new int[itemsets.length];
        for (int i = 0; i < itemsets.length; i++)
            indices[i] = 0;

        boundary = 0;
	}


    public boolean hasNext() {
        return boundary<indices.length && indices[boundary] < itemsets[boundary].length;
    }

    public T[] next() {
    	T[] result = (T[]) new Object[itemsets.length];

        for (int i=0; i<itemsets.length; i++) {
        	result[i] = itemsets[i][indices[i]];
        }

        moveIndicies();
        return result;
    }


    /**
     * Resets the right-most iterator.
     */
    private void moveIndicies() {
//		System.out.println("boundary: " + boundary + " indices:" + Arrays.toString(indices));
		for (int i=0;i<=boundary;i++) {
			if (indices[i]<itemsets[i].length-1) {
				indices[i]++;
				return;
			}
		}

		boundary++;
		//Set all indices left of boundary to 0
		for (int i=0;i<boundary;i++) {
			indices[i] = 0;
		}

		if (boundary < itemsets.length) {
			indices[boundary]++;
		}
    }

}