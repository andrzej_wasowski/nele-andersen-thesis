package ca.uwaterloo.gsd.merge.distance;

import ca.uwaterloo.gsd.fm.FeatureGraph;

public interface DistanceMeasure<T> {
	public long distance(FeatureGraph<T> fd1, FeatureGraph<T> fd2);
}
