package ca.uwaterloo.gsd.merge.distance;

import java.util.Set;

import net.sf.javabdd.BDD;
import ca.uwaterloo.gsd.fds.BDDBuilder;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureNode;

public class DistancePlus<T> implements DistanceMeasure<T> {

	BDDBuilder<T> _builder;

	public DistancePlus(BDDBuilder<T> builder) {
		_builder = builder;

	}

	/**
	 * This one isn't tested either ...
	 *
	 * @param g1
	 * @param g2
	 * @return
	 */
	@Override
	public long distance(FeatureGraph<T> g1, FeatureGraph<T> g2) {

		BDD hf1 = this.mkHierarchyBDD (g1);
		BDD hf2 = this.mkHierarchyBDD (g2);

		// inefficient because we recompute the two BDDs again, but more readable
		// let us postpone optimizing to another stage
		BDD cf1 = _builder.mkConfiguration (new FeatureModel<T>(g1));
		BDD cf2 = _builder.mkConfiguration (new FeatureModel<T>(g2));

		BDD features = _builder.mkSet(g1.features())
				.andWith(_builder.mkSet(g2.features()));

		int hdiff = cardinalityXor (hf1, hf2, features);
		int cdiff = cardinalityXor (cf1, cf2, features);

		hf1.free();
		hf2.free();
		cf1.free();
		cf2.free();
		features.free();

		return hdiff + cdiff;
	}


	/**
	 * Computes | a XOR b | and frees a and b
	 * @param a
	 * @param b
	 * @return
	 */
	private int cardinalityXor(BDD a, BDD b, BDD support) {

		BDD hdiff = a.xor (b);
		int hDiffCount = (int) hdiff.satCount(support);
		hdiff.free ();
		return hDiffCount;
	}


	/**
	 * TODO: test
	 * @param g
	 * @return
	 */
	private BDD mkHierarchyBDD(FeatureGraph<T> g) {
		BDD result = _builder.mkHierarchy(g).andWith(_builder.mkFeatureNodeAnd(g.root()));

		BDD existVar = _builder.one ();
		Set<FeatureNode<T>> g1Mand = g.selectMandatoryNodes();
		for (FeatureNode<T> v : g1Mand)
			existVar.andWith (_builder.mkSet(v.features()));

		result.exist(existVar);
		existVar.free ();

		return result;
	}

}
