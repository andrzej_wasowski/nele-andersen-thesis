package ca.uwaterloo.gsd.merge.distance;

import net.sf.javabdd.BDD;
import ca.uwaterloo.gsd.fds.BDDBuilder;
import ca.uwaterloo.gsd.fm.FeatureGraph;

public class HierarchyDistance<T> implements DistanceMeasure<T> {

	BDDBuilder<T> mBuilder;

	public HierarchyDistance(BDDBuilder<T> builder) {
		mBuilder = builder;

	}


	/**
	 * TODO compare this with distancePlus
	 */
	@Override
	public long distance(FeatureGraph<T> fd1, FeatureGraph<T> fd2) {
		BDD hf1 = mBuilder.mkHierarchy(fd1);
		BDD hf2 = mBuilder.mkHierarchy(fd2);

		BDD features = mBuilder.mkSet(fd1.features())
		.andWith(mBuilder.mkSet(fd2.features()));

		int hdiff = cardinalityXor (hf1, hf2, features);

		hf1.free();
		hf2.free();
		features.free();

//		BDD hf1 = mkHierarchyBDD(fd1);
//		BDD hf2 = mkHierarchyBDD(fd2);


		return hdiff;
	}

	/**
	 * Computes | a XOR b | and frees a and b
	 * @param a
	 * @param b
	 * @return
	 */
	private int cardinalityXor(BDD a, BDD b, BDD support) {

		BDD hdiff = a.xor (b);
		int hDiffCount = (int) hdiff.satCount(support);
		hdiff.free ();
		return hDiffCount;
	}



}
