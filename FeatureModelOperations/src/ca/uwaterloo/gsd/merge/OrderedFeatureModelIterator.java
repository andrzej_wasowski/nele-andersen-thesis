/**
* Copyright (c) 2009 Steven She
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
*
* Contributors:
*   Steven She - initial API and implementation
*/
package ca.uwaterloo.gsd.merge;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import net.sf.javabdd.BDD;

import org.apache.commons.collections15.CollectionUtils;

import ca.uwaterloo.gsd.fds.BDDBuilder;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.merge.distance.DistanceMeasure;
import ca.uwaterloo.gsd.merge.distance.DistancePlus;

public class OrderedFeatureModelIterator<T> extends BasicFeatureModelIterator<T>{

	public static class ConfigurationCountComparator<T> implements Comparator<FeatureModel<T>> {

		BDDBuilder<T> mBuilder;

		public ConfigurationCountComparator(BDDBuilder<T> builder) {
			mBuilder = builder;
		}

		@Override
		public int compare(FeatureModel<T> o1, FeatureModel<T> o2) {
			Collection<T> universe = CollectionUtils.union(o1.features(), o2.features());
			BDD support = mBuilder.mkSet(universe);
			BDD f1 = mBuilder.mkConfiguration(o1);
			BDD f2 = mBuilder.mkConfiguration(o2);
			int c1 = (int) f1.satCount(support);
			int c2 = (int) f2.satCount(support);
			f1.free();
			f2.free();
			support.free();
			return c1 == c2 ? 0 : (c1 < c2 ? -1 : 1);
		}
	}

	public static class SimilarityComparator<T> implements Comparator<FeatureModel<T>> {
		DistanceMeasure<T> measure;
		FeatureModel<T> mOrig;

		public SimilarityComparator(FeatureModel<T> original, BDDBuilder<T> builder) {
			this.mOrig = original;
			measure = new DistancePlus<T>(builder);
		}

		@Override
		public int compare(FeatureModel<T> o1, FeatureModel<T> o2) {
			long dist1 = measure.distance(mOrig.getDiagram(), o1.getDiagram());
			long dist2 = measure.distance(mOrig.getDiagram(), o2.getDiagram());
			return dist1 == dist2 ? 0 : (dist1 < dist2 ? -1 : 1);
		}

	}


	/**
	 * Compare using Configuration Count then Similarity.
	 */
	public static class ConfSimComparator<T> extends ConfigurationCountComparator<T> {
		FeatureModel<T> mOrig;
		SimilarityComparator<T> mSimComp;
		public ConfSimComparator(FeatureModel<T> original, BDDBuilder<T> builder) {
			super(builder);
			mOrig = original;
			mSimComp = new SimilarityComparator<T>(original, builder);
		}
		@Override
		public int compare(FeatureModel<T> o1, FeatureModel<T> o2) {
			int comp = super.compare(o1, o2);
			return comp == 0 ? mSimComp.compare(o1, o2) : comp;
		}
	}

	/**
	 * Compare using Similarity then Configuration Count.
	 */
	public static class SimConfComparator<T> extends SimilarityComparator<T> {

		ConfigurationCountComparator<T> mConfComp;

		public SimConfComparator(FeatureModel<T> original, BDDBuilder<T> builder) {
			super(original, builder);
			mConfComp = new ConfigurationCountComparator<T>(builder);
		}

		@Override
		public int compare(FeatureModel<T> o1, FeatureModel<T> o2) {
			int comp = super.compare(o1, o2);
			return comp == 0 ? mConfComp.compare(o1,o2) : comp;
		}
	}

	Comparator<FeatureModel<T>> _comp;
	List<FeatureModel<T>> _models;
	int i=0;

	public OrderedFeatureModelIterator(FeatureGraph<T> graph, Comparator<FeatureModel<T>> comp) {
		super(graph);
		_comp = comp;

		_models = new ArrayList<FeatureModel<T>>(super.total());

		while (super.hasNext()) {
			FeatureModel<T> fm = super.next();
			_models.add(fm);
		}

		assert _models.size() > 0;

		Collections.sort(_models, _comp);
	}

	@Override
	public FeatureModel<T> next() {
		return _models.get(i++);
	}

	@Override
	public boolean hasNext() {
		return i < _models.size();
	}

}
