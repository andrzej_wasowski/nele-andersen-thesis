package ca.uwaterloo.gsd.merge.experiments;


public class BenchmarkEntry {
	private final Benchmark _benchmark;
	private final long[] _values;

	protected BenchmarkEntry(Benchmark mark) {
		_benchmark = mark;
		_values = new long[_benchmark.getFields().length];
	}

	public void put(String field, long val) {
		_values[_benchmark.getFieldPos(field)] = val;
	}
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Simple CSV output, spaces not supported.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (long val : _values) {
			sb.append(val).append(',');
		}
		sb.deleteCharAt(sb.length()-1);
		
		return sb.toString();
	}
}
