package ca.uwaterloo.gsd.merge.experiments;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import junit.framework.TestCase;
import net.sf.javabdd.BDD;
import ca.uwaterloo.gsd.fds.BDDBuilder;
import ca.uwaterloo.gsd.fds.BDDBuilderJava;
import ca.uwaterloo.gsd.fds.FDSFactory;
import ca.uwaterloo.gsd.fds.FGBuilder;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureModelParser;
import ca.uwaterloo.gsd.fm.FeatureModelSerializer;
import ca.uwaterloo.gsd.merge.FeatureModelGenerator;
import ca.uwaterloo.gsd.merge.SemanticOperations;

public class FDSBenchmarkTests extends TestCase {

	BDDBuilder<String> _builder = FDSFactory.makeStringBDDBuilder();

	
	@Override
	protected void setUp() throws Exception {
		Logger.getLogger(BDDBuilderJava.class.getName()).setLevel(Level.INFO);
	}
	
	public void test50() {
		FeatureModelGenerator gen = new FeatureModelGenerator(50);
		BDD F = _builder.one();
		do {
			F.free();
			FeatureModel<String> generated = gen.generate(0);
			F = _builder.mkStructure(generated);
		} while (F.isZero());

		FDSFactory.makeFeatureGraph(F, _builder);
		F.free();
	}
	
	public void test80() {
		FeatureModelGenerator gen = new FeatureModelGenerator(80);
		BDD F = _builder.one();
		do {
			F.free();
			FeatureModel<String> generated = gen.generate(0);
			F = _builder.mkStructure(generated);
		} while (F.isZero());

		FDSFactory.makeFeatureGraph(F, _builder);
		F.free();
	}
	
	public void test100() throws Exception {
		doTest(100, 1);
	}
	
	public void test150() throws Exception {
		doTest(150, 1);
	}
	
	public void test200() throws Exception {
		doTest(200, 1);
	}
	
	public void test250() throws Exception {
		doTest(250, 1);
	}
	
	public void test500() throws Exception {
		doTest(500, 1);
	}
	
	public void testBenchmark50() throws Exception {
		doTest(50, 20);
	}
	
	public void testBenchmark100() throws Exception {
		doTest(100, 10);
	}
	
	public void testBenchmark200() throws Exception{
		doTest(200, 10);
	}
	
	public void testBenchmarkUntil200() throws Exception {
		for (int i=20; i<=200; i+=20) {
			doTest(i, 20);
		}
	}
	
	
	public void testGenerate500() throws Exception {
		FeatureModelGenerator gen = new FeatureModelGenerator(500);
		FeatureModelSerializer ser = new FeatureModelSerializer(true);
		FeatureModel<String> fm = gen.generate(0);
		BDD F = null;
		do {
			if (F != null)
				F.free();
			_builder.getFactory().reset();
			_builder = FDSFactory.makeStringBDDBuilder();
			fm = gen.generate(0);
			F = _builder.mkStructure(fm);
		}
		while (F.isZero());
		
		FileWriter writer = new FileWriter(new File("output/gen.fm"));
		String output = ser.toString(fm);
		assertEquals(fm, FeatureModelParser.parseString(output));
		writer.write(output);
		writer.close();
	}
	
	
	public void doTest(int features, int reps) throws IOException {
		FeatureModelGenerator gen = new FeatureModelGenerator(features);

		FGBuilder<String> fg = new FGBuilder<String>(_builder);
		for (int i=0; i<reps; i++) {
			BDD F = null;
			FeatureModel<String> generated;
			do {
				if (F != null) {
					F.free();
					_builder.reset();
				}
				
				generated = gen.generate(0);
				F = _builder.mkStructure(generated);
			} while (F.isZero());

			FeatureModelSerializer ser = new FeatureModelSerializer(true);
			FileWriter writer = new FileWriter(new File("output/last.fm"));
			String output = ser.toString(generated);
			assertEquals(generated, FeatureModelParser.parseString(output));
			writer.write(output);
			writer.close();
			
			BDD alive = SemanticOperations.removeDeadFeatures(F);
			fg.build(alive);
			F.free();
			alive.free();
			_builder.reset();
		}
		FileWriter writer = new FileWriter(new File("output/" + features + ".txt"));
		System.out.println(fg.getBenchmark());
		writer.write(fg.getBenchmark().toString());
		writer.close();
	}
}
