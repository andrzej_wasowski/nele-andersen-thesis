package ca.uwaterloo.gsd.merge.experiments;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import junit.framework.TestCase;
import net.sf.javabdd.BDD;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Factory;

import ca.uwaterloo.gsd.fds.BDDBuilder;
import ca.uwaterloo.gsd.fds.FDSFactory;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureModelParser;
import ca.uwaterloo.gsd.fm.FeatureModelSerializer;
import ca.uwaterloo.gsd.merge.BasicFeatureModelIterator;
import ca.uwaterloo.gsd.merge.FeatureModelGenerator;
import ca.uwaterloo.gsd.merge.FeatureModelMutator;
import ca.uwaterloo.gsd.merge.SemanticOperations;
import ca.uwaterloo.gsd.merge.distance.DistanceMeasure;
import ca.uwaterloo.gsd.merge.distance.DistancePlus;
import dk.itu.fds.ImplicationGraph;

public class Experiments extends TestCase {

	private final File DATASET = new File("results/ds-15/");
	long seed = 2009;
	int features = 15;
	int models = 20;

	FeatureModelGenerator _gen;
	FeatureModelSerializer _serializer;
	SemanticOperations<String> _ops;

	public static Logger logger = Logger.getLogger("fmm.Experiments");

	@Override
	protected void setUp() throws Exception {
		_gen = new FeatureModelGenerator(features);
		_gen.setSeed(seed);
		_serializer = new FeatureModelSerializer(true);
	}


	public void testMakeDataset() throws Exception {
		File dir = new File("results/" + getName());
		dir.mkdir();

		for (int i=1; i<=models; i++) {
			FeatureModel<String> model = _gen.generate(0);
			FileWriter writer = new FileWriter(new File(dir, i + ".fm"));
			String serial = _serializer.toString(model);
			writer.write(serial);
			//Check output file for later reproduction of experiment results
			assertEquals(model, FeatureModelParser.parseString(serial));
			writer.close();
		}

	}

	public void testProject20() throws Exception {
		double feature_perc = 0.2;
		projection(feature_perc, new File(DATASET,"dataset"), new File(DATASET + "/20"));
	}

	public void testProject50() throws Exception {
		double feature_perc = 0.5;
		projection(feature_perc, new File(DATASET, "dataset"), new File(DATASET + "/50"));
	}

	public void testProject80() throws Exception {
		double feature_perc = 0.8;
		projection(feature_perc, new File(DATASET,"dataset"), new File(DATASET + "/80"));
	}


	private void projection(double feature_perc, File dataDir, File dir) throws IOException {


		dir.mkdir();
		File renderDir = new File(dir, "render/");
		File gfdDir = new File(dir, "gfd/");
		File exDir = new File(dir, "ideal/");
		FileWriter results = new FileWriter(new File(dir, "results.txt"));

		renderDir.delete();
		renderDir.mkdir();
		exDir.delete();
		exDir.mkdir();
		gfdDir.delete();
		gfdDir.mkdir();


		for (File f : dataDir.listFiles(new FmFilter())) {
			if (f.isDirectory()) continue;

			BDDBuilder<String> builder = FDSFactory.makeStringBDDBuilder();
			SemanticOperations<String> ops = new SemanticOperations<String>(builder);
			DistanceMeasure<String> dist = new DistancePlus<String>(builder);

			/*
			 * Load the feature model
			 */
			FeatureModel<String> fm = FeatureModelParser.parseFile(f.getAbsolutePath());


			for (int i=1; i<=10; i++) {
				String currFileName = removeExtension(f.getName()) + "_" + i + ".fm";

				/*
				 * Determine set of features to project
				 */
				BDD struct = builder.mkStructure(fm);
				assertFalse(f + " is a contradiction / tautalogy!", struct.isZero() || struct.isOne());
				Set<String> features;
				BDD proj;
				do {
					features = _gen.featureSet(fm, feature_perc);
					proj = ops.project(struct, features);
				} while (proj.isZero() || proj.isOne());

				BDD features_f = builder.mkSet(features);

				/*
				 * Save GFD for reference
				 */
				FeatureGraph<String> gfd = FDSFactory.makeFeatureGraph(proj, builder);
				FileWriter gfdWriter = new FileWriter(new File(gfdDir, currFileName));
				String gfd_string = _serializer.toString(new FeatureModel<String>(gfd));
				System.out.println(gfd_string);
				assertEquals(gfd, FeatureModelParser.parseString(gfd_string).getDiagram());
				gfdWriter.write(gfd_string);
				gfdWriter.close();


				/*
				 * Render
				 */
				logger.info("Features: " + features + "Features_f: " + features_f);
				FeatureModel<String> render = ops.render(proj, features_f, builder
						.one(), fm, fm);


				assertTrue(render.features().size() > 1);


				/*
				 * Save resulting render diagram
				 */
				FileWriter rWriter = new FileWriter(new File(renderDir, currFileName));
				String render_string = _serializer.toString(render);
				System.out.println(gfd);
				System.out.println(render);
				System.out.println(render_string);
				assertEquals(render, FeatureModelParser.parseString(render_string));
				rWriter.write(render_string);
				rWriter.close();




				/*
				 * Calculate semantic distance of rendered with the original
				 */
				long renderDist = dist.distance(render.getDiagram(), fm.getDiagram());

				/*
				 * Find the ideal render diagram using exhaustive search
				 */
				BasicFeatureModelIterator<String> iter = new BasicFeatureModelIterator<String>(gfd);
				long minExDist = Long.MAX_VALUE;
				FeatureModel<String> minEx = null;
				boolean seenRender = false;
				do {
					FeatureModel<String> exfm = iter.next();
					long exDist = dist.distance(exfm.getDiagram(), fm.getDiagram());
					if (exDist < minExDist) {
						minExDist = exDist;
						minEx = exfm;
					}

					if (exfm.equals(render)) {
						seenRender = true;
					}
				}
				while (iter.hasNext() && minExDist > 0);

				FileWriter exWriter = new FileWriter(new File(exDir, currFileName));
				exWriter.write(_serializer.toString(minEx));
				exWriter.close();

//				assertTrue(currFileName, seenRender);


				/*
				 * Save the resulting data to results.txt
				 */
				results.write(String.format("%1$-10s%2$-70s%3$-10d%4$-10d\n",
						currFileName, toFeaturesString(features), renderDist, minExDist ));

				features_f.free();
				struct.free();
				proj.free();
			}
			results.flush();
		}
		results.close();
	}


	public void testUnion5() throws Exception {
		executeOp(opUnion, 5, new File(DATASET, "dataset/"), new File(DATASET, "u5/"));
	}

	public void testUnion10() throws Exception {
		executeOp(opUnion, 10, new File(DATASET, "dataset/"), new File(DATASET, "u10/"));
	}

	public void testIntersection5() throws Exception {
		executeOp(opIntersect, 50, new File(DATASET, "dataset/"), new File(DATASET, "i5/"));
	}

	public void testIntersection10() throws Exception {
		executeOp(opIntersect, 10, new File(DATASET, "dataset/"), new File(DATASET, "i10/"));
	}

	public void testDifference5() throws Exception {
		executeOp(opDiff, 5, new File(DATASET, "dataset/"), new File(DATASET, "d5/"));
	}

	public void testDifference10() throws Exception {
		executeOp(opDiff, 10, new File(DATASET, "dataset/"), new File(DATASET, "d10/"));
	}


	public void testSatisfiability() throws Exception {
		File dataDir = new File(DATASET,"dataset/");
		BDDBuilder<String> builder = FDSFactory.makeStringBDDBuilder();
		for (File f : dataDir.listFiles(new FmFilter())) {
			FeatureModel<String> fm = FeatureModelParser.parseFile(f.getAbsolutePath());
			BDD struct = builder.mkStructure(fm);
			BDD noDead = ImplicationGraph.removeDeadFeatures(struct);

			assertFalse("Model not satisfiable: " + f.getName(),noDead.isZero() || noDead.isOne());

			struct.free();
			noDead.free();
		}
	}


	public void testGenerateFeatureModel() throws Exception {
		BDDBuilder<String> builder = FDSFactory.makeStringBDDBuilder();

		FeatureModelGenerator gen = new FeatureModelGenerator(20);
		FeatureModel<String> fm = gen.generate(0);
		String fm_string = _serializer.toString(fm);
		assertEquals(fm, FeatureModelParser.parseString(fm_string));

		BDD struct = builder.mkStructure(fm);
		BDD noDead = ImplicationGraph.removeDeadFeatures(struct);

		assertFalse(noDead.isZero() || noDead.isOne());

		System.out.println(fm_string);
	}


	private void executeOp(ExperimentOp op, int mutations, File dataDir, File dir) throws IOException {
		dir.mkdir();
		File renderDir = new File(dir, "/render/");
		File gfdDir = new File(dir, "/gfd/");
		File exDir = new File(dir, "/ideal/");
		FileWriter results = new FileWriter(new File(dir, "/results.txt"));

		renderDir.delete();
		renderDir.mkdir();
		exDir.delete();
		exDir.mkdir();
		gfdDir.delete();
		gfdDir.mkdir();


		for (File f : dataDir.listFiles(new FmFilter())) {
			if (f.isDirectory()) continue;

			BDDBuilder<String> builder = FDSFactory.makeStringBDDBuilder();
			SemanticOperations<String> semOps = new SemanticOperations<String>(builder);
			DistanceMeasure<String> dist = new DistancePlus<String>(builder);

			/*
			 * Load the feature model
			 */
			FeatureModel<String> fm = FeatureModelParser.parseFile(f.getAbsolutePath());
			BDD orig_struct = builder.mkStructure(fm);


			for (int i=1; i<=10; i++) {


				String currFileName = removeExtension(f.getName()) + "_" + i + ".fm";

				/*
				 * Mutate diagram
				 */
				BDD mutantStruct;
				FeatureModel<String> mutant;
				BDD result = null;
				BDD removeDead = null;
				int mutationCount=0;
				do {
					if (result != null) {
						removeDead.free();
						result.free();
					}

					if (mutationCount == 1000) {
						fail("Tried 1000 mutations on " + currFileName + " not satisfiable!");
					}

					FeatureModelMutator mutator = new FeatureModelMutator(new Factory<String>() {
						int i=100;
						public String create() {
							return "n" + i++;
						}
					});
					mutant = mutator.mutate(fm, mutations);
					mutantStruct = builder.mkStructure(mutant);
					result = op.execute(orig_struct, mutantStruct, semOps);
					removeDead = ImplicationGraph.removeDeadFeatures(result);
					mutationCount++;
				} while (removeDead.isZero() || removeDead.isOne());

				removeDead.free();

				Set<String> feat = new HashSet<String>(CollectionUtils.union(mutant.features(), fm.features()));
				BDD features_bdd = builder.mkSet(feat);

				/*
				 * Save GFD for reference
				 */
				FeatureGraph<String> gfd = FDSFactory.makeFeatureGraph(result, builder);
				FileWriter gfdWriter = new FileWriter(new File(gfdDir, currFileName));
				String gfd_string = _serializer.toString(new FeatureModel<String>(gfd));
				System.out.println("Union BDD: " + result);
				assertEquals(gfd, FeatureModelParser.parseString(gfd_string).getDiagram());
				gfdWriter.write(gfd_string);
				gfdWriter.close();



				/*
				 * Render
				 */
				FeatureModel<String> render = semOps.render(result, features_bdd , builder
						.one(), fm, fm);


				assertTrue(render.features().size() > 1);


				/*
				 * Save resulting render diagram
				 */
				FileWriter rWriter = new FileWriter(new File(renderDir, currFileName));
				String render_string = _serializer.toString(render);
				System.out.println(gfd);
				System.out.println(render);
				System.out.println(render_string);
				assertEquals(render, FeatureModelParser.parseString(render_string));
				rWriter.write(render_string);
				rWriter.close();


				/*
				 * Calculate semantic distance of rendered with the original
				 */
				long renderDist = dist.distance(render.getDiagram(), fm.getDiagram());

				/*
				 * Find the ideal render diagram using exhaustive search
				 */
				BasicFeatureModelIterator<String> iter = new BasicFeatureModelIterator<String>(gfd);
				long minExDist = Long.MAX_VALUE;
				FeatureModel<String> minEx = null;
				boolean seenRender = false;
				do {
					FeatureModel<String> exfm = iter.next();
					long exDist = dist.distance(exfm.getDiagram(), fm.getDiagram());
					if (exDist < minExDist) {
						minExDist = exDist;
						minEx = exfm;
					}

					if (exfm.equals(render)) {
						seenRender = true;
					}
				}
				while (iter.hasNext() && minExDist > 0);

				FileWriter exWriter = new FileWriter(new File(exDir, currFileName));
				exWriter.write(_serializer.toString(minEx));
				exWriter.close();




				/*
				 * Save the resulting data to results.txt
				 */
				results.write(String.format("%1$-10s%2$-10d%3$-10d\n",
						currFileName, renderDist, minExDist ));

				mutantStruct.free();
				features_bdd.free();
			}
			results.flush();
		}
		results.close();
	}





	public interface ExperimentOp {
		public BDD execute(BDD b1, BDD b2, SemanticOperations<String> op);
	}


	public ExperimentOp opUnion = new ExperimentOp() {

		@Override
		public BDD execute(BDD b1, BDD b2, SemanticOperations<String> ops) {
			return ops.union(b1, b2);
		}

	};

	public ExperimentOp opIntersect = new ExperimentOp() {

		@Override
		public BDD execute(BDD b1, BDD b2, SemanticOperations<String> ops) {
			return ops.intersection(b1, b2);
		}

	};


	public ExperimentOp opDiff = new ExperimentOp() {

		@Override
		public BDD execute(BDD b1, BDD b2, SemanticOperations<String> ops) {
			return ops.difference(b1, b2);
		}

	};






	public String toFeaturesString(Set<String> features) {
		StringBuilder sb = new StringBuilder();
		for (String s : features) {
			sb.append(s + ",");
		}
		sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}

	public class FmFilter implements FilenameFilter {

		@Override
		public boolean accept(File dir, String name) {
			return name.endsWith(".fm");
		}

	}

	private String removeExtension(String name) {
		return name.substring(0, name.lastIndexOf(".fm"));
	}

}
