package ca.uwaterloo.gsd.merge.experiments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Does not support spaces in the header names!
 * @author shshe
 *
 */
public class Benchmark extends ArrayList<BenchmarkEntry> {

	private static final long serialVersionUID = 1L;
	private final String[] _headers;
	private final Map<String, Integer> _fieldToPos; 
	
	public Benchmark(String... headers) {
		assert headers.length > 0;
		
		_headers = headers;
		_fieldToPos = new HashMap<String, Integer>(headers.length * 2);
		for (int i=0; i<headers.length; i++) {
			_fieldToPos.put(headers[i], i);
		}
	}
	
	public String[] getFields() {
		return _headers;
	}
	
	public int getFieldPos(String field) {
		return _fieldToPos.get(field);
	}
	
	public BenchmarkEntry mkEntry() {
		return new BenchmarkEntry(this);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		//Do Headers
		for (String head : _headers) {
			sb.append(head).append(',');
		}
		sb.deleteCharAt(sb.length()-1);
		sb.append("\n");
		
		for (BenchmarkEntry en : this) {
			sb.append(en).append("\n");
		}
		
		return sb.toString();
	}
}