package ca.uwaterloo.gsd.merge;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SetPermutationsMap<T>  extends SetPermutations<T> {
	private T[] origin;

	@SuppressWarnings("unchecked")
	public SetPermutationsMap(List<Collection<? extends T>> sets, List<? extends T> origin) {
		super(sets);
		this.origin = (T[]) origin.toArray();
	}

	/**
	 * Returns a ?bipartite? map from the origin to its choice.
	 * @return
	 */
    public Map<T, T> nextElementMap() {
    	Map<T, T> result = new HashMap<T, T>();
    	List<T> next = next();
    	assert next.size() == origin.length;
    	for (int i=0; i<origin.length;i++) {
    		result.put(origin[i], next.get(i));
    	}
    	return result;
    }


}
