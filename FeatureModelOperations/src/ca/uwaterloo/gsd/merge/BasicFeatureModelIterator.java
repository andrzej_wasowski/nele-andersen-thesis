/**
* Copyright (c) 2009 Steven She
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
*
* Contributors:
*   Steven She - initial API and implementation
*/
package ca.uwaterloo.gsd.merge;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.collections15.MultiMap;

import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureNode;

public class BasicFeatureModelIterator<T> {

	FeatureGraph<T> _g;

	private SetPermutations<FeatureEdge> _permHierarchy;
	private SetPermutations<FeatureEdge> _permGroups;
	private FeatureGraph<T> _next;
	private FeatureGraph<T> _hi;

	private static Logger logger = Logger.getLogger("fmm.BasicFeatureModelIterator");

	private MultiMap<FeatureNode<T>, FeatureEdge> _ambigHi;
	private MultiMap<FeatureNode<T>, FeatureEdge> _ambigGrouped;


	static {
//		logger.setLevel(Level.OFF);
	}

	public BasicFeatureModelIterator(FeatureGraph<T> g) {
		_g = g;
		_ambigHi = Disambiguator.findAmbiguousHierarchy(g);
		_permHierarchy = makePermutations(_ambigHi.map().values());

		logger.info("Ambiguous hierarchy: " + _ambigHi);
	}


	public boolean hasNext() {
		return  _permHierarchy.hasNext() || _permGroups.hasNext();
	}


	public FeatureModel<T> next() {
		_next = null;
		if (_permGroups != null && _permGroups.hasNext()) {
			computeNextGroups();
		}
		else if (_permHierarchy.hasNext()) {
			computeNextHierarchy();
			computeNextGroups();
		}
		assert _next != null;
		return new FeatureModel<T>(_next);
	}


	private void computeNextHierarchy() {

		_hi = _g.clone();

		if (_permHierarchy.hasNext()) {
			Set<FeatureEdge> ambigEdges = new HashSet<FeatureEdge>(_ambigHi.values());
			List<FeatureEdge> elements = _permHierarchy.next();
			ambigEdges.removeAll(elements);

			for (FeatureEdge e : ambigEdges) {
				_hi.removeEdge(_hi.findEdge(e, _g));
			}
		}

		_ambigGrouped = Disambiguator.findAmbiguousGrouped(_hi);
		_permGroups = makePermutations(_ambigGrouped.map().values());
	}

	private void computeNextGroups() {

		if (!_permGroups.hasNext()) {
			_next = _hi;
			return;
		}

		FeatureGraph<T> result = _hi.clone();

		if (_permGroups.hasNext()) {
			Set<FeatureEdge> ambigEdges = new HashSet<FeatureEdge>(_ambigGrouped.values());
			List<FeatureEdge> elements = _permGroups.next();
			ambigEdges.removeAll(elements);
			for (FeatureEdge e : ambigEdges) {
				result.removeEdge(result.findEdge(e, _hi));
			}
		}

		_next = result;
	}


	public int total() {
		return _permHierarchy.total();
	}




	private SetPermutations<FeatureEdge> makePermutations(Collection<Collection<FeatureEdge>> sets) {
		List<Collection<? extends FeatureEdge>> choices
				= new ArrayList<Collection<? extends FeatureEdge>>();
		choices.addAll(sets);
		return new SetPermutations<FeatureEdge>(choices);
	}

}
