package ca.uwaterloo.gsd.merge;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Map.Entry;
import java.util.logging.Logger;

import net.sf.javabdd.BDD;

import org.apache.commons.collections15.MapIterator;
import org.apache.commons.collections15.MultiMap;
import org.apache.commons.collections15.iterators.EntrySetMapIterator;
import org.apache.commons.collections15.multimap.MultiHashMap;

import ca.uwaterloo.gsd.algo.TransitiveReduction;
import ca.uwaterloo.gsd.fds.BDDBuilder;
import ca.uwaterloo.gsd.fds.FDSFactory;
import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureNode;
import ca.uwaterloo.gsd.fm.ImplicationGraph;
import ca.uwaterloo.gsd.fm.SimpleEdge;
import ca.uwaterloo.gsd.merge.distance.DistanceMeasure;
import ca.uwaterloo.gsd.merge.distance.DistancePlus;
import ca.uwaterloo.gsd.merge.distance.HierarchyDistance;

public class Disambiguator<T> {


	OperationListener opList = new ConsoleOperationListener(Disambiguator.class);
	Logger logger = Logger.getLogger("fmm.Disambiguate");
	private final BDDBuilder<T> _builder;
	private final FeatureGraph<T> _fd1;
	private final FeatureGraph<T> _fd2;

	public Disambiguator(FeatureGraph<T> fd1, BDDBuilder<T> builder) {
		_fd1 = fd1;
		_builder = builder;
		_fd2 = new FeatureGraph<T>();
	}

	public Disambiguator(FeatureGraph<T> fd1, FeatureGraph<T> fd2, BDDBuilder<T> builder) {
		this._fd1 = fd1;
		this._fd2 = fd2;
		_builder = builder;
	}


	public FeatureGraph<T> doHierarchy(FeatureGraph<T> gfd) {
		return disambiguate(gfd, new HierarchyDistance<T>(_builder), findAmbiguousHierarchy(gfd));
	}


	public FeatureGraph<T> doHierarchyAlternate(FeatureGraph<T> gfd) {
		MultiMap<FeatureNode<T>, FeatureEdge> ambig = findAmbiguousHierarchy(gfd);
		FeatureGraph<T> result = gfd.clone();

		BDD hf1 = _builder.mkHierarchy(_fd1);
		BDD hf2 = _builder.mkHierarchy(_fd2);
		ImplicationGraph<T> hi1 = FDSFactory.makeImplicationGraph(hf1, _builder);
		ImplicationGraph<T> hi2 = FDSFactory.makeImplicationGraph(hf2, _builder);

		for (Entry<FeatureNode<T>, Collection<FeatureEdge>> en : ambig.entrySet()) {

			FeatureEdge chosen = null;
			outer: for (FeatureEdge e : en.getValue()) {
				FeatureNode<T> source = gfd.getSource(e);
				FeatureNode<T> target = gfd.getTarget(e);

				for (T f1 : source.features()) {
					for (T f2 : target.features()) {
						if (hi1.findEdge(f1, f2) != null) {
							chosen = e;
							break outer;
						}
					}
				}
			}

			if (chosen == null) {
				outer: for (FeatureEdge e : en.getValue()) {
					FeatureNode<T> source = gfd.getSource(e);
					FeatureNode<T> target = gfd.getTarget(e);

					for (T f1 : source.features()) {
						for (T f2 : target.features()) {
							if (hi2.findEdge(f1, f2) != null) {
								chosen = e;
								break outer;
							}
						}
					}
				}
			}

			assert chosen != null;

			//Remove edges
			for (FeatureEdge e : en.getValue()) {
				if (e != chosen)
					result.removeEdge(result.findEdge(e, gfd));
			}
		}

		return result;
	}




	public FeatureGraph<T> doGroupsHeuristic(FeatureGraph<T> gfd) {
		return disambiguate(gfd, new DistancePlus<T>(_builder), findAmbiguousGrouped(gfd));

	}

	public FeatureGraph<T> doAndGroups(FeatureGraph<T> gfd) {
		FeatureGraph<T> result = gfd.clone();

		BDD hf1 = _builder.mkHierarchy(_fd1);
		BDD hf2 = _builder.mkHierarchy(_fd2);
		ImplicationGraph<T> hi1 = FDSFactory.makeImplicationGraph(hf1, _builder);
		ImplicationGraph<T> hi2 = FDSFactory.makeImplicationGraph(hf2, _builder);

		Set<FeatureNode<T>> andNodes = gfd.selectAndNodes();
		for (FeatureNode<T> v : andNodes) {
			//Hierarchy Graph
			ImplicationGraph<T> hiG = new ImplicationGraph<T>();

			for (T f : v.features()) {
				for (T g : v.features()) {
					if (hi1.findEdge(f, g) != null || hi2.findEdge(f, g) != null)
						hiG.addEdge(f, g);
				}
			}
			
			TransitiveReduction.INSTANCE.reduce(hiG);
			logger.info("HI G!\n" + hiG);



			for (SimpleEdge e : hiG.topologicalEdges()) {
				T source = hiG.getSource(e);
				T target = hiG.getDest(e);

				FeatureNode<T> group = result.findNode(source);
				group = result.replaceVertex(group, group.remove(source));

				logger.info("Processing: " + source + " -> " + target + " new group: " + group);

				FeatureNode<T> extract = new FeatureNode<T>(source);
				result.addVertex(extract);

				FeatureNode<T> destNode = result.findNode(target);
				result.addEdge(extract, destNode, FeatureEdge.OPTIONAL);
				result.addEdge(extract, destNode, FeatureEdge.MANDATORY);
			}

			logger.info("RESULT!\n" + result);
		}

		return result; //FIXME
	}

	public FeatureGraph<T> removeHierarchyViolatingGroups(FeatureGraph<T> gfd) {
		FeatureGraph<T> result = gfd.clone();
		for (FeatureEdge e : findHierarchyViolatingGroups(gfd)) {
			result.removeEdge(result.findEdge(e, gfd));
		}
		return result;
	}


	private FeatureGraph<T> disambiguate(FeatureGraph<T> gfd, DistanceMeasure<T> measure,
			MultiMap<FeatureNode<T>, FeatureEdge> ambigMap) {

		Collection<FeatureEdge> ambigEdges = ambigMap.values();

		for (Entry<FeatureNode<T>, Collection<FeatureEdge>> en : ambigMap.entrySet()) {
			Collection<FeatureEdge> choices = en.getValue();
			FeatureEdge strongest = null;

			long min1 = Long.MAX_VALUE;
			long min2 = Long.MAX_VALUE;

			for (FeatureEdge retain : choices) {
				FeatureGraph<T> gp = gfd.clone();
				for (FeatureEdge e : choices) {
					if (e == retain) continue;
					gp.removeEdge(gp.findEdge(e, gfd));
				}

				long d1 = measure.distance(gp, _fd1);
				long d2 = measure.distance(gp, _fd2);

				if (d1 < min1) {
					strongest = retain;
					min1 = d1;
					min2 = d2;
				}
				else if (d1 == min1 && d2 < min2) {
					strongest = retain;
					min2 = d2;
				}
				else if (d1 == min1 && d2 == min2) {
					logger.severe("Distance is the same for "
							+ gfd.edgeString(retain) + ", ignoring!");
				}
			}

			assert strongest != null;
			ambigEdges.remove(strongest);
		}

		FeatureGraph<T> result = gfd.clone();
		for (FeatureEdge e : ambigEdges) {
			result.removeEdge(result.findEdge(e, gfd));
		}

		return result;
	}


	public static <T>
		MultiMap<FeatureNode<T>, FeatureEdge> findAmbiguousGrouped(FeatureGraph<T> g) {

		MultiMap<FeatureNode<T>, FeatureEdge> result
			= new MultiHashMap<FeatureNode<T>, FeatureEdge>();

		for (FeatureEdge e : g.selectGroupEdges()) {
			for (FeatureNode<T> s : g.getSources(e)) {
				result.put(s, e);
			}
		}

		//Remove unambiguous grouped nodes
		MapIterator<FeatureNode<T>, Collection<FeatureEdge>>
			iter = new EntrySetMapIterator<FeatureNode<T>, Collection<FeatureEdge>>(result.map());

		while (iter.hasNext()) {
			iter.next();
			if (iter.getValue().size() == 1)
				iter.remove();
		}

		return result;
	}


	public static <T>
		MultiMap<FeatureNode<T>, FeatureEdge> findAmbiguousHierarchy(FeatureGraph<T> g) {

		MultiMap<FeatureNode<T>, FeatureEdge> result
			= new MultiHashMap<FeatureNode<T>, FeatureEdge>();

		for (FeatureEdge e : g.selectHierarchyEdges()) {
			FeatureNode<T> source = g.getSource(e);
			result.put(source, e);
		}

		//Remove hierarchy nodes with only one hierarchy edge
		MapIterator<FeatureNode<T>, Collection<FeatureEdge>>
			iter = new EntrySetMapIterator<FeatureNode<T>, Collection<FeatureEdge>>(result.map());

		while (iter.hasNext()) {
			iter.next();
			if (iter.getValue().size() == 1)
				iter.remove();
		}

		return result;
	}


	public static <T>
			Set<FeatureEdge> findHierarchyViolatingGroups(FeatureGraph<T> g) {
		Set<FeatureEdge> result = new HashSet<FeatureEdge>();
		for (FeatureEdge e : g.selectGroupEdges()) {

			FeatureNode<T> target = g.getTarget(e);
			for (FeatureNode<T> v : g.getSources(e)) {
				if (g.findHierarchyEdge(v, target) == null) {
					result.add(e);
					break;
				}
			}
		}
		return result;
	}

	public static <T>  boolean isAmbiguous(FeatureGraph<T> g) {
		return findAmbiguousGrouped(g).size() > 0
				|| findAmbiguousHierarchy(g).size() > 0;
	}


}
