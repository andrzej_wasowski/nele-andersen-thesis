/**
* Copyright (c) 2009 Steven She
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
*
* Contributors:
*   Steven She - initial API and implementation
*/
package ca.uwaterloo.gsd.merge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.collections15.Closure;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Factory;
import org.apache.commons.collections15.Predicate;

import ca.uwaterloo.gsd.fm.Expression;
import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureNode;
import ca.uwaterloo.gsd.merge.ExpressionGenerator.CannotSatisfyException;

/**
 * Only works on Basic Feature Models.
 * @author Steven She (shshe@uwaterloo.ca)
 *
 */
public class FeatureModelMutator {

	/*
	 * Adding / deleting leaf feature
	 * TODO moving a feature + subtree to a new parent
	 * making mandatory -> optional
	 * making optional -> mandatory
	 * changing type of a group
	 * adding extra constraint
	 * removing extra constraint
	 */

	Random rand = new Random();
	Factory<String> featureFactory;
	public static Logger logger = Logger.getLogger("fmm.Mutator");

	public FeatureModelMutator(Factory<String> factory) {
		featureFactory = factory;
	}

	public void setSeed(long seed) {
		rand.setSeed(seed);
	}

	public FeatureModel<String> mutate(FeatureModel<String> fm, int numOps) {
		FeatureModel<String> result = fm.clone();
		for (int i=0; i<numOps; i++) {
			Operation op = wheelOfMutation(result);
			op.execute(result);
		}
		return result;
	}


	private Operation wheelOfMutation(FeatureModel<String> fm) {
		int choice;
		Operation op;
		do {
			choice = rand.nextInt(7);
			switch (choice) {
			case 0:
				op = opAddNode;
				break;
			case 1:
				op = opRemoveNode;
				break;
			case 2:
				op = opToggle;
				break;
			case 3:
				op = opChangeGroup;
				break;
			case 4:
				op = opAddConstraint;
				break;
			case 5:
				op = opRemoveConstraint;
				break;
			case 6:
				op = opMoveSubtree;
				break;
			default:
				assert false;
			return null;
			}
		}
		while (!op.canExecute(fm));
		return op;
	}

	Operation opAddNode = new Operation() {
		@Override
		public boolean canExecute(FeatureModel<String> fm) {
			return true;
		}

		@SuppressWarnings("unchecked")
		public void execute(FeatureModel<String> fm) {
			FeatureGraph<String> g = fm.getDiagram();
			FeatureNode<String>[] features = g.vertices().toArray(new FeatureNode[0]);
			FeatureNode<String> p = features[rand.nextInt(features.length)];

			FeatureNode<String> v = new FeatureNode<String>(featureFactory.create());


			g.addVertex(v);
			g.addEdge(v, p, FeatureEdge.OPTIONAL);

			boolean isMandatory = rand.nextBoolean();
			if (isMandatory)
				g.addEdge(v, p, FeatureEdge.MANDATORY);
		}

	};


	Operation opRemoveNode = new Operation() {

		public boolean canExecute(FeatureModel<String> fm) {
			return fm.getDiagram().vertices().size() > 1;
		}


		@SuppressWarnings("unchecked")
		public void execute(FeatureModel<String> fm) {
			logger.info("Operation: Remove Node");
			FeatureGraph<String> g = fm.getDiagram();
			FeatureNode<String>[] leaves = g.leaves().toArray(new FeatureNode[0]);
			FeatureNode<String> del = leaves[rand.nextInt(leaves.length)];
			assert del != g.root();

			shrinkGroups(del, g);
			g.removeVertex(del);
		}


	};

	Operation opMoveSubtree = new Operation() {

		public boolean canExecute(FeatureModel<String> fm) {
			return fm.getDiagram().vertices().size() > 3;
		}

		public void execute(FeatureModel<String> fm) {

			FeatureGraph<String> g = fm.getDiagram();
			ArrayList<FeatureNode<String>> vertices = new ArrayList<FeatureNode<String>>(g.vertices());

			FeatureNode<String> root = g.root();
			FeatureNode<String> subtree;
			Set<FeatureNode<String>> invalid;
			do {
				subtree = vertices.get(rand.nextInt(vertices.size()));
				invalid = g.descendants(subtree);
				invalid.addAll(g.parents(subtree));
				invalid.add(subtree);
			} while (subtree == root || invalid.containsAll(vertices));


			vertices.removeAll(invalid);
			FeatureNode<String> newParent = vertices.get(rand.nextInt(vertices.size()));
			FeatureNode<String> oldParent = g.parents(subtree).iterator().next();

			logger.info("Operation: Move Subtree: " + subtree + " -> " + newParent + " old parent: " + oldParent);

			assert g.parents(subtree).size() == 1;
			assert vertices.size() > 0;

			shrinkGroups(subtree, g);
			g.removeEdge(g.findHierarchyEdge(subtree, oldParent));
			g.addEdge(subtree, newParent, FeatureEdge.OPTIONAL);
		}

	};

	Operation opToggle = new Operation() {

		Collection<FeatureNode<String>> solitary;

		public boolean canExecute(FeatureModel<String> fm) {
			final FeatureGraph<String> g = fm.getDiagram();

			final Set<FeatureEdge> groupEdges = g.selectGroupEdges();
			solitary = CollectionUtils.select(g.vertices(),
					new Predicate<FeatureNode<String>>() {

				public boolean evaluate(FeatureNode<String> v) {
					return !CollectionUtils.containsAny(groupEdges, g.outgoingEdges(v));
				}

			});
			return solitary.size() > 0;
		}

		@SuppressWarnings("unchecked")
		public void execute(FeatureModel<String> fm) {
			assert solitary.size() > 0;
			FeatureGraph<String> g = fm.getDiagram();

			FeatureNode<String>[] soliArray = solitary.toArray(new FeatureNode[0]);
			FeatureNode<String> chosen;

			chosen = soliArray[rand.nextInt(soliArray.length)];
			for (FeatureNode<String> p : g.parents(chosen)) {
				FeatureEdge mandEdge = g.findEdge(Arrays.asList( chosen ), p, FeatureEdge.MANDATORY);

				//Toggle
				if (mandEdge == null)
					g.addEdge(Arrays.asList (chosen ), p , FeatureEdge.MANDATORY);
				else
					g.removeEdge(mandEdge);
			}
		}

	};


	Operation opChangeGroup = new Operation() {

		public boolean canExecute(FeatureModel<String> fm) {
			return fm.getDiagram().selectGroupEdges().size() > 0;
		}

		public void execute(FeatureModel<String> fm) {
			FeatureGraph<String> g = fm.getDiagram();
			Set<FeatureEdge> groupEdges = g.selectGroupEdges();
			int next = rand.nextInt(groupEdges.size());
			Iterator<FeatureEdge> iter = groupEdges.iterator();
			FeatureEdge chosen = iter.next();
			while (next-- > 0) {
				chosen = iter.next();
			}

			int type = -1;
			do {
				switch(rand.nextInt(3)) {
				case 0:
					type = FeatureEdge.OR;
					break;
				case 1:
					type = FeatureEdge.XOR;
					break;
				case 2:
					type = FeatureEdge.MUTEX;
					break;
				}
			} while (type != chosen.getType());

			Set<FeatureNode<String>> sources = g.getSources(chosen);
			FeatureNode<String> target = g.getTarget(chosen);

			g.removeEdge(chosen);
			g.addEdge(sources, target, type);
		}

	};

	//FIXME check if contradiction, or if identical
	Operation opAddConstraint = new Operation() {

		@Override
		public boolean canExecute(FeatureModel<String> fm) {
			return fm.features().size() > 2;
		}

		public void execute(FeatureModel<String> fm) {
			String[] featArray = new String[fm.features().size()];
			fm.features().toArray(featArray);
			ExpressionGenerator gen = new ExpressionGenerator(fm);
			Expression<String> clause = null;
			while (clause == null) {
				try {
					clause = gen.makeClause(featArray);
				}
				catch (CannotSatisfyException e) {}
			}
			fm.getConstraints().add(clause);

		}

	};

	Operation opRemoveConstraint = new Operation() {

		public boolean canExecute(FeatureModel<String> fm) {
			return fm.getConstraints().size() > 0;
		}

		public void execute(FeatureModel<String> fm) {
			String[] featArray = new String[fm.features().size()];
			fm.features().toArray(featArray);
			int index = rand.nextInt(fm.getConstraints().size());
			fm.getConstraints().remove(index);
		}

	};


	private void shrinkGroups(FeatureNode<String> del, FeatureGraph<String> g) {
		for (FeatureEdge e : g.outgoingEdges(del)) {
			if (e.getType() == FeatureEdge.OPTIONAL) continue;

			Set<FeatureNode<String>> sources = g.getSources(e);
			FeatureNode<String> target = g.getTarget(e);

			assert sources.contains(del);
			sources.remove(del);

			g.removeEdge(e);

			if (sources.size() == 1) {
				switch (e.getType()) {
				case FeatureEdge.OR:
				case FeatureEdge.XOR:
					g.addEdge(sources, target, FeatureEdge.MANDATORY);
					break;
				case FeatureEdge.MUTEX:
					//Do nothing
					break;
				}
			}
			else if (sources.size() > 1) {
				g.addEdge(sources, target, e.getType());
			}
		}
	}

	public interface Operation extends Closure<FeatureModel<String>>{
		public boolean canExecute(FeatureModel<String> fm);
	}


}
