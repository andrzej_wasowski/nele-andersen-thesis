package ca.uwaterloo.gsd.fds;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.Map.Entry;

public class BDDBuilderSerializer {

	public static String asString(BDDBuilder<?> builder) {
		StringBuilder sb = new StringBuilder();
		List<Entry<?,Integer>> list 
		= new ArrayList<Entry<?,Integer>>(builder.getFeatureMap().entrySet());

		Collections.sort(list, new Comparator<Entry<?, Integer>>() {
			public int compare(Entry<?, Integer> e1, Entry<?, Integer> e2) {
				return e1.getValue() == e2.getValue() ? 0 : e1.getValue() < e2.getValue() ? -1 : 1;
			}
		});

		for (Entry<?, Integer> en : list)
			sb.append(en.getValue()).append(" ").append(en.getKey().toString()).append("\n");
		
		return sb.toString();
	}
	
	
	public static BDDBuilder<String> fromString(String s) {
		BDDBuilder<String> result = FDSFactory.makeStringBDDBuilder();
		Scanner scanner = new Scanner(s);
		while (scanner.hasNext()) {
			int val = scanner.nextInt();
			String key = scanner.nextLine().substring(1); //Ignore space, read until EOL
			result.getFeatureMap().put(key, val);
		}
		scanner.close();
		return result;
	}
	
	
	public static void save(BDDBuilder<?> builder, File f) throws IOException {
		FileWriter writer = new FileWriter(f);
		writer.write(asString(builder));
		writer.close();
	}
	
	public static BDDBuilder<String> load(File f) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(f));
		StringBuilder sb = new StringBuilder();
		while (reader.ready())
			sb.append(reader.readLine()).append("\n");
		reader.close();
		return fromString(sb.toString());
	}
}
