package ca.uwaterloo.gsd.fds;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import ca.uwaterloo.gsd.fm.Expression;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureNode;

public interface BDDBuilder<T> {
	public int add(T f);
	public int remove(T f);
	public BDD get(T f);
	public BDD nget(T f);
	public boolean contains(T f);

	public int variable(T f);
	public T feature(int var);
	public T syntheticRoot();
	public Set<T> domain();
	public Set<T> support(BDD formula);
	public BDD mkExpression(Expression<T> expr);
	public BDD mkCardinality(FeatureGraph<T> g);
	public BDD mkHierarchy(FeatureGraph<T> g);
	public BDD mkFeatureNodeAnd(FeatureNode<T> v);
	public BDD mkFeatureNodeOr(FeatureNode<T> v);
	public BDD mkConfiguration(FeatureModel<T> fm);
	public BDD mkStructure(FeatureModel<T> fm);
	public BDD mkSet(Collection<T> features);
	public BDDFactory getFactory();
	public Map<T, Integer> getFeatureMap();
	public BDD one();
	public BDD zero();
	public void reset();
}
