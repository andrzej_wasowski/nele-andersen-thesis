package ca.uwaterloo.gsd.fds;

import java.util.logging.Logger;

import net.sf.javabdd.BDD;
import ca.uwaterloo.gsd.algo.TransitiveReduction;
import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureNode;
import ca.uwaterloo.gsd.fm.FeatureType;
import ca.uwaterloo.gsd.fm.ImplicationGraph;
import ca.uwaterloo.gsd.fm.SimpleEdge;
import dk.itu.fds.ValidDomains;

public class FrozenCoreBuilder<T> {

	private final BDDBuilder<T> _builder;
	Logger logger = Logger.getLogger("fmm.FrozenCore");

	public FrozenCoreBuilder(BDDBuilder<T> builder) {
		this._builder = builder;
	}

	public FeatureGraph<T> build(FeatureGraph<T> gfd, BDD frozen) {
		ImplicationGraph<T> result = FDSFactory.makeImplicationGraph(frozen, _builder);
		TransitiveReduction.INSTANCE.reduce(result);
		logger.info("Frozen Implication Graph: " + result);
		return applyFrozenCore(gfd, result);
	}

	/**
	 * Creates frozen structure after the GFD has been synthesized.
	 */
	protected FeatureGraph<T> applyFrozenCore(FeatureGraph<T> g, ImplicationGraph<T> impl) {
		if (impl.getVertexCount() == 0)
			return g;

		FeatureGraph<T> result = g.clone();
		BDD gfd_struct = _builder.mkStructure(new FeatureModel<T>(g));
		ImplicationGraph<T> hiG = FDSFactory.makeImplicationGraph(gfd_struct, _builder);

		for (SimpleEdge e : impl.getEdges()) {
			T fSource = impl.getSource(e);
			T fDest = impl.getDest(e);

			if (!hiG.containsEdge(hiG.findEdge(fSource, fDest))) {
				logger.severe("Frozen edge " + fSource + " -> "
						+ fDest + " not present in GFD!");
				continue;
			}


			FeatureNode<T> source = result.findNode(fSource);

			if (source.getType() == FeatureType.AND_GROUP) {
				result.replaceVertex(source, source.remove(fSource));
				source = new FeatureNode<T>(fSource);
				result.addVertex(source);
			}

			//Freezing a mandatory feature from an AND-Group
			//FIXME NOW
			if (remainsMandatory(fSource, fDest, gfd_struct)) {
//				source.setType(FeatureType.MANDATORY);
			}

			FeatureNode<T> target = result.findNode(fDest);
			result.addEdge(source, target, FeatureEdge.OPTIONAL);

		}
		gfd_struct.free();

		return result;
	}

	private boolean remainsMandatory(T f1, T f2, BDD gfd) {
		int var1 = _builder.variable(f1);
		int var2 = _builder.variable(f2);

		BDD v1 = _builder.get(f1);
		BDD f1_restrict = gfd.id().restrictWith(v1.not());
		v1.free();

		ValidDomains vd1 = new ValidDomains(f1_restrict);
		if (vd1.canBeZero(var2) && !vd1.canBeOne(var2)) {
			BDD v2 = _builder.get(f2);
			BDD f2_restrict = gfd.id().restrictWith(v2.not());
			v2.free();

			ValidDomains vd2 = new ValidDomains(f2_restrict);
			f2_restrict.free();
			if (vd2.canBeZero(var1) && !vd2.canBeOne(var1)) {
				return true;
			}
		}

		return false;
	}

}
