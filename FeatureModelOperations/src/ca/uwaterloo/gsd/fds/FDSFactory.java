package ca.uwaterloo.gsd.fds;

import net.sf.javabdd.BDD;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.ImplicationGraph;
import ca.uwaterloo.gsd.fm.sat.SATBuilder;
import ca.uwaterloo.gsd.merge.SemanticOperations;


public class FDSFactory {
	
	public static <T> BDDBuilder<T> makeBDDBuilder(T synth) {
		return new BDDBuilderJava<T>(synth);
	}
	
	public static BDDBuilder<String> makeStringBDDBuilder() {
		return new BDDBuilderJava<String>("Synth");
	}
	
	public static <T> SATBuilder<T> makeSATBuilder() {
		return new SATBuilder<T>();
	}
	
	public static <T> ImplicationGraph<T> makeImplicationGraph(BDD formula, BDDBuilder<T> builder) {
		return IGBuilder.build(formula, builder);
	}
	
	public static <T> FeatureGraph<T> makeFeatureGraph(BDD formula, BDDBuilder<T> builder ) {
		BDD alive = SemanticOperations.removeDeadFeatures(formula);
		FeatureGraph<T> g = new FGBuilder<T>(builder).build(alive);
		alive.free();
		return g;
	}
}
