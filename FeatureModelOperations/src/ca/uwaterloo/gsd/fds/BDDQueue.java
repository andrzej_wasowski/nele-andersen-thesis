/**
* Copyright (c) 2009 Steven She
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
*
* Contributors:
*   Steven She - initial API and implementation
*/
package ca.uwaterloo.gsd.fds;

import java.util.Comparator;
import java.util.PriorityQueue;

import net.sf.javabdd.BDD;

public class BDDQueue extends PriorityQueue<BDD>{
	private static final long serialVersionUID = 2149156386061285730L;
	private final BDDBuilder<?> mBuilder;

	public BDDQueue(BDDBuilder<?> builder) {
		super(10, new Comparator<BDD>() {
			public int compare(BDD o1, BDD o2) {
				return o1.nodeCount() == o2.nodeCount() ? 0
						: o1.nodeCount() < o2.nodeCount() ? -1 : 1;
			}
		});
		this.mBuilder = builder;
	}


	public BDD getConjoinedBDD() {
		BDD result = mBuilder.one();
		while (size() > 0) {
			BDD polled = poll();
			result.andWith(polled);
		}
		return result;
	}
}
