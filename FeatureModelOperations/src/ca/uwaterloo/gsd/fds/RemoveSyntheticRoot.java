package ca.uwaterloo.gsd.fds;

import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureNode;

public class RemoveSyntheticRoot {
	private RemoveSyntheticRoot() {}
	
	public static <T> void remove(FeatureGraph<T> g) {
		FeatureNode<T> v = g.root();
		if ((v.getProperties() & FeatureNode.SYNTHETIC) != FeatureNode.SYNTHETIC)
			throw new IllegalArgumentException("Root is not synthetic!");
		
		//Locate the mandatory sub-feature (ie. the new root) and remove all edges to it
		for (FeatureEdge e : g.incomingEdges(v)) {
			if (e.getType() == FeatureEdge.MANDATORY)
				g.removeEdge(g.findHierarchyEdge(g.getSource(e), v));
			
		}
		
		//Remove all free variables
		for (FeatureEdge e : g.incomingEdges(v)) {
			assert (g.getSource(e).getProperties() & FeatureNode.FREE) == FeatureNode.FREE;
			g.removeVertex(g.getSource(e));
		}
		
		g.removeVertex(v);
	}
	
}
