package ca.uwaterloo.gsd.fds;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;

import org.apache.commons.collections15.CollectionUtils;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.BronKerboschCliqueFinder;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import ca.uwaterloo.gsd.algo.DirectedCliqueFinder;
import ca.uwaterloo.gsd.algo.TransitiveReduction;
import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureNode;
import ca.uwaterloo.gsd.fm.ImplicationGraph;
import ca.uwaterloo.gsd.fm.SimpleEdge;
import ca.uwaterloo.gsd.merge.experiments.Benchmark;
import ca.uwaterloo.gsd.merge.experiments.BenchmarkEntry;
import dk.itu.fds.Implicant;
import dk.itu.fds.PrimeImplicants;
import dk.itu.fds.PrimeImplicantsOptimized;
import dk.itu.fds.ValidDomains;

public class FGBuilder<T> {

	// removed private from fields to allow extending
	protected Benchmark _benchmark = new Benchmark("implication", "hierarchy", "mutex-graph","mutex-cliques", "or", "total");
	protected BenchmarkEntry bmEntry = _benchmark.mkEntry();
	protected Logger logger = Logger.getLogger("fmm.FGBuilder");
	
	protected BDD _formula;
	protected BDDFactory _factory;
	protected final BDDBuilder<T> _builder;
	
	/**
	 * Assigned in mkMutexGroups (should refactor this out)
	 */
	protected Collection<Set<FeatureNode<T>>> _mutexCliques;
	
	public FGBuilder(BDDBuilder<T> builder) {
		_builder = builder;
	}

	public FeatureGraph<T> build(BDD formula) {
		long start = System.currentTimeMillis();
		
		_formula = formula;
		_factory = formula.getFactory();
		
	
		FeatureGraph<T> g = new FeatureGraph<T>();
		
		logger.info("Implication graph");
		long impStart = System.currentTimeMillis();
		ImplicationGraph<T> impl = FDSFactory.makeImplicationGraph(_formula, _builder);
		long impEnd = System.currentTimeMillis();
		bmEntry.put("implication", impEnd - impStart);
		
		logger.info("And-groups");
		long andStart = System.currentTimeMillis();
		mkHierarchyAndGroups(g, impl);
		long andEnd = System.currentTimeMillis();
		bmEntry.put("hierarchy", andEnd - andStart);
		
		logger.info("Transitive reduction");
		TransitiveReduction.INSTANCE.reduce(g);
		
		logger.info("Mutex graph and groups");
		mkMutexGroups(g);
			
		logger.info("Prime implicants and Or groups");
		long orStart = System.currentTimeMillis();
		mkOrGroups(g);
		long orEnd = System.currentTimeMillis();
		bmEntry.put("or", orEnd - orStart);
		
		mkSyntheticRoot(g);
		
		long end = System.currentTimeMillis();
		bmEntry.put("total", end - start);
		
		_benchmark.add(bmEntry);
		
		return g;
	}
	
	public void mkHierarchyAndGroups(FeatureGraph<T> g, ImplicationGraph<T> impl) {
		List<Set<T>> cliques = DirectedCliqueFinder.INSTANCE.findAll(impl);
		
		//Make the hierarchy
		for (Set<T> clique : cliques)
			g.addVertex(new FeatureNode<T>(clique));
		
		for (T v : impl.vertices())
			if (!g.features().contains(v))
				g.addVertex(new FeatureNode<T>(v));
		
		for (Set<T> clique : cliques) {

			FeatureNode<T> group = g.findNode(clique.iterator().next());
			
			for (T u : clique) {
				for (SimpleEdge e : impl.incomingEdges(u).toArray(new SimpleEdge[0])) {
					T source = impl.getSource(e);
					if (!clique.contains(source))
						g.addEdge(g.findNode(source), group, FeatureEdge.OPTIONAL);
					impl.removeEdge(e);
				}
				
				for (SimpleEdge e : impl.outgoingEdges(u).toArray(new SimpleEdge[0])) {
					T target = impl.getTarget(e);
					if (!clique.contains(target))
						g.addEdge(group, g.findNode(target), FeatureEdge.OPTIONAL);
					impl.removeEdge(e);
				}
			}
		}
		
		//Add the remaining hierarchy edges
		for (SimpleEdge e : impl.edges()) 
			g.addEdge(g.findNode(impl.getSource(e)), 
					g.findNode(impl.getTarget(e)), 
					FeatureEdge.OPTIONAL);

	}
	
	@SuppressWarnings("unchecked")
	public void mkMutexGroups(FeatureGraph<T> g) {
		long mutexStart = System.currentTimeMillis();
		//Make Mutex Graph
		UndirectedGraph<FeatureNode<T>, DefaultEdge> mutex 
			= new SimpleGraph<FeatureNode<T>, DefaultEdge>(DefaultEdge.class);
		
		for (FeatureNode<T> v : g.vertices())
			mutex.addVertex(v);
		
		List<Integer> support = support();
		
		for (FeatureNode<T> v : g.vertices()) {
			FeatureNode<T>[] siblings = g.children(v).toArray(new FeatureNode[0]);
			
			for (int i=0; i < siblings.length; i++) {
				FeatureNode<T> s1 = siblings[i];
				BDD mx = _formula.id ().andWith (_builder.mkFeatureNodeAnd(s1));

				if (mx.isZero ())
					throw new UnsupportedOperationException ("Dead features should have been removed!");
				
				ValidDomains vd = new ValidDomains(mx, Collections.min(support), Collections.max(support));
				for (int j=i+1;j<siblings.length; j++) {
					FeatureNode<T> s2 = siblings[j];
					
					//Only need to check one of the features (if it's an and-group)
					int v2 = _builder.variable(s2.features().iterator().next());
					if (!vd.canBeOne(v2) && vd.canBeZero(v2))
						mutex.addEdge(s1, s2);
				}
				mx.free();
			}
		}
		long mutexEnd = System.currentTimeMillis();
		bmEntry.put("mutex-graph", mutexEnd - mutexStart);
		mutexStart = System.currentTimeMillis();
		//Create Mutex Groups
		_mutexCliques = findMutexCliques(mutex);
		for (Set<FeatureNode<T>> clique : _mutexCliques) {
			FeatureNode<T> parent = commonParent(g, clique);
			
			//Mutex group doesn't have a common parent ?
			if (parent == null) {
				logger.warning("Mutex group without a common parent: " + clique);
				continue;
			}
			
			g.addEdge(clique, parent, FeatureEdge.MUTEX);
		}
		mutexEnd = System.currentTimeMillis();
		bmEntry.put("mutex-cliques", mutexEnd - mutexStart);
	}
	
	
	public void mkOrGroups(FeatureGraph<T> g) {
		for (FeatureNode<T> v : g.vertices()) {
			BDD b = _factory.one();
			for (T f: v.features())
				b.andWith(_factory.nithVar(_builder.variable(f)));
			
			// project formula on v and its children
			Set<T> keep = new HashSet<T>();
			keep.addAll(v.features());
			
			for (FeatureNode<T> child : g.children(v))
				keep.addAll(child.features());
			
			Collection<T> remove = CollectionUtils.subtract(_builder.domain(), keep);
			BDD exist = _builder.mkSet(remove);
			BDD frag = _formula.id().exist(exist);
			
			PrimeImplicants pi = new PrimeImplicantsOptimized (b, frag);
			
			frag.free();
			exist.free();
			b.free();
			
			for (Implicant imp : pi) {
				Implicant imp1 = imp.removeNegations();
				
				Set<FeatureNode<T>> grouped = new HashSet<FeatureNode<T>>();
				for (int i : imp1)
					grouped.add(g.findNode(_builder.feature(i)));
				
				if (grouped.size() == 0 || grouped.contains(v))
					continue;
				assert g.children(v).containsAll(grouped);
				
				if (g.findEdge(grouped, v, FeatureEdge.XOR) != null)
					continue;
				//TODO maybe better to do a restrict on frag and calculate ValidDomains
				//else if (_mutexCliques.contains(grouped)) {
				else if (isMutex(grouped)) {
					//Need to remove mutex since xor edge subsumes
					if(g.findEdge(grouped, v, FeatureEdge.MUTEX) != null)
						g.removeEdge(g.findEdge(grouped, v, FeatureEdge.MUTEX));
					g.addEdge(grouped, v, FeatureEdge.XOR);
				}
				else
					g.addEdge(grouped, v, FeatureEdge.OR);
			}
		}
	}

	/*
	 * added by nele
	 */
	protected boolean isMutex(Set<FeatureNode<T>> grouped){
		for(Set<FeatureNode<T>> mutex : _mutexCliques){
			if(mutex.containsAll(grouped))
				return true;
		}
		return false;
	}
	
	public void mkSyntheticRoot(FeatureGraph<T> g) {
		
		Set<FeatureNode<T>> roots = findRoots(g);
		
		FeatureNode<T> synth = new FeatureNode<T>(_builder.syntheticRoot());
		synth.setProperties(FeatureNode.SYNTHETIC);
		g.addVertex(synth);
		for (FeatureNode<T> v : roots) {
			g.addEdge(v, synth, FeatureEdge.OPTIONAL);
			g.addEdge(v, synth, FeatureEdge.MANDATORY);
		}
		
		//Add free variables as optional features
		Collection<T> freeVars = CollectionUtils.subtract(_builder.domain(), supportAsFeatures());
		for (T free : freeVars) {
			FeatureNode<T> v = new FeatureNode<T>(free);
			v.setProperties(FeatureNode.FREE);
			g.addVertex(v);
			g.addEdge(v, synth, FeatureEdge.OPTIONAL);
		}
		
	}
	
	
	private Set<FeatureNode<T>> findRoots(FeatureGraph<T> g) {
		Set<FeatureNode<T>> cand = new HashSet<FeatureNode<T>>(g.vertices());
		for (FeatureEdge e : g.edges()) {
			cand.removeAll(g.getSources(e));
		}
		return cand;
	}
	
	
	
	/**
	 * @return null if the group doesn't have a common parent
	 */
	protected FeatureNode<T> commonParent(FeatureGraph<T> g, Set<FeatureNode<T>> group) {
		Set<FeatureNode<T>> parents = new HashSet<FeatureNode<T>>(g.vertices());
		for (FeatureNode<T> v : group) {
			parents.retainAll(g.parents(v));
			if (parents.size() == 0) return null;
		}
		
		if (parents.size () > 1)
			logger.warning("Multiple parents for mutex: " + group + "=" + parents);
		
		return parents.iterator().next ();
	}
	
	protected FeatureNode<T> leastCommonAncestor(FeatureGraph<T> g, Set<FeatureNode<T>> group) {
		Set<FeatureNode<T>> common = new HashSet<FeatureNode<T>>(g.vertices());
		
		for (FeatureNode<T> v : group) {
			Set<FeatureNode<T>> ancestors = g.ancestors(v);
			common.retainAll (ancestors);
		}

		//Remove transitive parents //FIXME ?
		Iterator<FeatureNode<T>> iter = common.iterator();
		while (iter.hasNext()) {
			FeatureNode<T> v = iter.next();
			if (CollectionUtils.intersection(common, g.descendants(v)).size() > 0)
				iter.remove();
		}

		if (common.size () > 1)
			logger .warning("Multiple parents for mutex: " + group + "=" + common);
		
		return common.iterator().next ();
	}

	
	protected List<Integer> support() {
		BDD supp = _formula.support();
		int[] arr = supp.scanSet();
		supp.free();
		List<Integer> result = new ArrayList<Integer>();
		if (arr != null) {
			for (int i : arr) {
				result.add(i);
			}
		}
		return result;
	}
	
	private List<T> supportAsFeatures() {
		List<Integer> intSupp = support();
		List<T> result = new ArrayList<T>(intSupp.size());
		for (int i : intSupp) {
			result.add(_builder.feature(i));
		}
		return result;
	}
	
	protected Collection<Set<FeatureNode<T>>> findMutexCliques(
			UndirectedGraph<FeatureNode<T>, DefaultEdge> g) {
		
		BronKerboschCliqueFinder<FeatureNode<T>, DefaultEdge> finder
			= new BronKerboschCliqueFinder<FeatureNode<T>, DefaultEdge>(g);

		Collection<Set<FeatureNode<T>>> cliques = finder.getAllMaximalCliques();
		Iterator<Set<FeatureNode<T>>> iter = cliques.iterator();
		while (iter.hasNext()) {
			if (iter.next().size() < 2)
				iter.remove();
		}
		return cliques;
	}

	
	public Benchmark getBenchmark() {
		return _benchmark;
	}
}
