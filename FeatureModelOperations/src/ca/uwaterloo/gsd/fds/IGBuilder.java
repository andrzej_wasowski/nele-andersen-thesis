package ca.uwaterloo.gsd.fds;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import ca.uwaterloo.gsd.fm.ImplicationGraph;
import dk.itu.fds.Util;
import dk.itu.fds.ValidDomains;

public class IGBuilder<T> {


	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(IGBuilder.class.getName());

	public static <T> ImplicationGraph<T> build(BDD problem, BDDBuilder<T> builder) {
		BDD sup = problem.support ();
		int[] isup = sup.scanSet ();
		sup.free ();
		return build (problem, builder, isup == null ? new int[] {} : isup);
	}


	protected static <T> ImplicationGraph<T> build(BDD problem, BDDBuilder<T> builder, int[] support) {
		ImplicationGraph<T> g = new ImplicationGraph<T>();
		
		int max_var = Util.max(support);
		int min_var = Util.min(support);

		HashSet<Integer> set_support = new HashSet<Integer>();

		for (int i : support)
			set_support.add (i);

		BDDFactory B = problem.getFactory ();

		for (int i : support) {
			
			BDD temp = problem.id ().andWith (B.nithVar (i));


			Set<Integer> falsified;
			if (temp.isZero()) { // if B is true in all assignments then it is implied by all
				falsified = new HashSet<Integer>();
				falsified.addAll(set_support);
			} else falsified = findFalsified (temp, min_var, max_var);
			temp.free ();
			falsified.retainAll (set_support);
			falsified.remove(i);
			
			for (int fal : falsified) {
				g.addEdge(builder.feature(fal), builder.feature(i));
			}
		}
		
		return g;
	}


	/**
	 * Find all variables in the problem that are 0 in all satisfiable assignments
	 * @param problem constraint representing the problem
	 * @param min_var only consider variables with identifiers min_var or larger
	 * @param max_var only consider variables with
	 * @return
	 */
	static public Set<Integer> findFalsified(BDD problem, int min_var, int max_var) {

		Set<Integer> value = new HashSet<Integer> ();
		ValidDomains vd = new ValidDomains (problem, min_var, max_var);
		for (int var = min_var; var <= max_var; ++var)
			if (vd.canBeZero (var) && !vd.canBeOne (var))
				value.add (var);
		return value;
	}

}