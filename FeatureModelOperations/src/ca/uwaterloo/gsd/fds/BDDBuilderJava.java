/**
* Copyright (c) 2009 Steven She
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
*
* Contributors:
*  Steven She - initial API and implementation
*/
package ca.uwaterloo.gsd.fds;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;

import org.apache.commons.collections15.BidiMap;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;
import org.apache.commons.collections15.iterators.LoopingIterator;

import ca.uwaterloo.gsd.fm.DepthFirstIterator;
import ca.uwaterloo.gsd.fm.Expression;
import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureNode;

/**
 * Treats all features as a single universe even if they belong to different
 * feature models.
 *
 * @author Steven She <shshe@uwaterloo.ca>
 *
 * @param <T>
 */
public class BDDBuilderJava<T> implements BDDBuilder<T>{
	
	private static BDDFactory _factory;
	// Can't start at 0 since machinery treats negatives as
	// negations
	private int i = 1;

	private BidiMap<T, Integer> _featToVar = new DualHashBidiMap<T, Integer>();
	private final T _synth;

	private Logger logger = Logger.getLogger(BDDBuilderJava.class.getName());

	public BDDBuilderJava(T syntheticRoot) {
		initFactory();
		_synth = syntheticRoot; // don't want to add this to domain
	}

	private void initFactory() {
		if (_factory == null) {
			_factory = BDDFactory.init("buddy", 10000000, 1000000);
			// TODO WARNING: Setting this to a high number causes satCount to
			// fail (even when specifying a support BDD!)
			_factory.setVarNum(500);
			_factory.setMaxIncrease(500000);
			_factory.setIncreaseFactor(1.25);			
		}
	}

	public BDDBuilderJava(BDDBuilder<T> builder) {
		_featToVar = new DualHashBidiMap<T, Integer>(builder.getFeatureMap());
		_factory = builder.getFactory();
		_synth = builder.syntheticRoot();
	}

	public boolean contains(T o) {
		return _featToVar.containsKey(o);
	}

	public BDD get(T o) {
		assert o != null;
		int var = _featToVar.containsKey(o) ? _featToVar.get(o) : add(o);
		return _factory.ithVar(var);
	}
	
	public BDD nget(T o) {
		assert o != null;
		int var = _featToVar.containsKey(o) ? _featToVar.get(o) : add(o);
		return _factory.nithVar(var);
	}

	public BDD mkSet(Collection<T> set) {
		return getFactory().makeSet(vars(set));
	}

	private BDD mkOrNodes(Collection<FeatureNode<T>> set) {
		BDD result = zero();
		for (FeatureNode<T> v : set) {
			for (T f : v.features()) {
				result.orWith(get(f));
			}
		}
		return result;
	}

	public BDD mkFeatureNodeAnd(FeatureNode<T> v) {
		return mkSet(v.features());
	}
	
	public BDD mkFeatureNodeOr(FeatureNode<T> v) {
		BDD result = _factory.zero();
		for (T f : v.features()) {
			result.orWith(get(f));
		}
		return result;
	}

	private BDD mkFeatureNodeNot(FeatureNode<T> v) {
		BDD and = mkSet(v.features());
		BDD result = and.not();
		and.free();
		return result;
	}


	public int variable(T o) {
		return _featToVar.containsKey(o) ? _featToVar.get(o) : add(o);
	}

	public T feature(int i) {
		return _featToVar.inverseBidiMap().get(i);
	}

	public BDDFactory getFactory() {
		return _factory;
	}

	@Override
	public Map<T, Integer> getFeatureMap() {
		return _featToVar;
	}

	/**
	 * Extend the number of variables in the BDDFactory, but only when necessary.
	 * @return the old varNum
	 */
	private int extVarNum(int num) {
		if (_factory.varNum() - i + 1 >= num) {
			return _factory.varNum();
		}
		return _factory.setVarNum(_factory.varNum() + 1000);
	}

	/**
	 * Returns a BDD representing true.
	 */
	public BDD one() {
		return _factory.one();
	}

	/**
	 * Returns a BDD representing false.
	 * @return
	 */
	public BDD zero() {
		return _factory.zero();
	}

	public int add(T o) {
		assert o != null;
		assert !_featToVar.containsKey(o);
		if (o.equals(_synth))
			throw new IllegalArgumentException("feature " + o + " is the same as the synthetic root feature!");
		
		int var = i++;
		extVarNum(1);

		_featToVar.put(o, var);
		return var;
	}

	public int remove(T o) {
		return _featToVar.remove(o);
	}

	/**
	 * Resets the feature to var mapping and the BDDFactory.
	 * FIXME there seems to be a bug with supportSize in BuDDY and JavaBDD not resetting.
	 */
	public void reset() {
		i=1;
		_featToVar = new DualHashBidiMap<T, Integer>();
		//FIXME Workaround for bug with supportSize variable in JFactory
		int newVarNum = _factory.varNum() + 1;
		_factory.reset();
		_factory.setVarNum(newVarNum);
	}
	
	protected int[] vars(Collection<T> features) {
		int[] result = new int[features.size()];
		Iterator<T> iter = features.iterator();
		for (int i=0; i<result.length; i++) {
			result[i] = variable(iter.next());
		}
		return result;
	}


	public Set<T> support(BDD formula) {
		Set<T> result = new HashSet<T>();
		BDD supp = formula.support();

		if (supp.isOne()) {
			supp.free();
			return result;
		}

		int[] set = supp.scanSet();
		for (int i=0; i<set.length; i++) {
			result.add(feature(set[i]));
		}
		supp.free();
		return result;
	}

	public BDD mkExpression(Expression<T> expr) {
		if (expr == null) return null;

		BDD left = mkExpression(expr.getLeft());
		BDD right = mkExpression(expr.getRight());
		switch (expr.getType()) {
		case TRUE:
			return one();
		case FALSE:
			return zero();
		case AND:
			return left.andWith(right);
		case OR:
			return left.orWith(right);
		case IMPLIES:
			return left.impWith(right);
		case IFF:
			return left.biimpWith(right);
		case NOT:
			BDD result = left.not();
			left.free();
			return result;
		case FEATURE:
			return get(expr.getFeature());
		default:
			throw new UnsupportedOperationException();
		}
	}

	/**
	 * @see {@link #mkConfiguration(FeatureGraph)}
	 */
	public BDD mkConfiguration(FeatureModel<T> model) {
		BDD diagram = mkConfiguration(model.getDiagram());
		for (Expression<T> expr : model.getConstraints()) {
			diagram.andWith(mkExpression(expr));
		}
		return diagram;
	}

	/**
	 * Creates a BDD representing the configuration space of the FeatureGraph.
	 * The root feature is set such that any configuration that does not contain
	 * the root is disallowed.
	 */
	protected BDD mkConfiguration(FeatureGraph<T> g) {
		if (g.vertices().size() == 0) return one();
		FeatureNode<T> root = g.root();
		return mkStructure(g).andWith(mkSet(root.features()));
	}

	/**
	 * @see {@link #mkStructure(FeatureGraph)}
	 */
	public BDD mkStructure(FeatureModel<T> model) {
		BDD diagram = mkStructure(model.getDiagram());
		for (Expression<T> expr : model.getConstraints()) {
			diagram.andWith(mkExpression(expr));
		}
		return diagram;
	}

	/**
	 * Creates a BDD representing the structural semantics of the FeatureGraph.
	 * The root feature is not set, since it reduces the BDD. We want to
	 * maintain all structure in the FeatureGraph.
	 *
	 */
	protected BDD mkStructure(FeatureGraph<T> g) {
		if (g.vertices().size() == 0) return one();
		BDD result = mkHierarchy(g);
		result.andWith(mkAndGroups(g));
		//Use the hierarchy BDD to further restrict the cardinality edges
		//since they are mostly disjunctive
		mkCardinality(g, result);
		return result;
	}


	@SuppressWarnings("unchecked")
	private BDD mkMutex(Set<FeatureNode<T>> sources) {
		if (sources.size() == 1)
			return one();

		BDDQueue queue = new BDDQueue(this);

		FeatureNode<T>[] arr = sources.toArray(new FeatureNode[0]);
		for (int i=0; i<arr.length-1; i++) {
			for (int j=i+1; j<arr.length; j++) {
				FeatureNode<T> v1 = arr[i];
				FeatureNode<T> v2 = arr[j];
				queue.add(mkFeatureNodeOr(v1).impWith(mkFeatureNodeNot(v2)));
			}
		}

		return queue.getConjoinedBDD();
	}


	public BDD mkRoot(FeatureGraph<T> g) {
		return mkFeatureNodeAnd(g.root());
	}

	/**
	 * <p>
	 * Creates a BDD describing the implications "upwards" in the feature tree.
	 * These upwards implications describe child-parent implications. Mandatory
	 * and AND-Grouped features are considered optional.
	 * </p>
	 *
	 * @see {@link #mkCardinality(FeatureGraph)}
	 */
	public BDD mkHierarchy(FeatureGraph<T> g) {
		logger.info("Making BDD hierarchy edges");
		BDDQueue queue = new BDDQueue(this);
		
		//Marcilio says DFS is good for BDD compilation of FMs!
		DepthFirstIterator<T> iter = new DepthFirstIterator<T>(g);
		while (iter.hasNext()) {
			FeatureEdge e = iter.next();
			if (e.getType() == FeatureEdge.OPTIONAL) {
				BDD sources = mkOrNodes(g.getSources(e));
				BDD target = mkFeatureNodeAnd(g.getTarget(e));
				queue.add(sources.impWith(target));
			}
		}
		return queue.getConjoinedBDD();
	}
	
	public BDD mkCardinality(FeatureGraph<T> g) {
		return mkCardinality(g, one());
	}
	
	private BDD mkAndGroups(FeatureGraph<T> g) {
		BDD result = one();
		//AND-Groups
		for (FeatureNode<T> v : g.vertices()) {
			if (v.features().size() > 1) {
				LoopingIterator<T> iter = new LoopingIterator<T>(v.features());
				T start = iter.next();
				T prev = start;
				do {
					T next = iter.next();
					result.andWith(get(prev).impWith(get(next)));
					prev = next;
				} while (prev != start);
			}
		}
		return result;
	}

	private BDD mkCardinality(FeatureGraph<T> g, BDD result) {
		logger.info("Making BDD cardinality edges");

		for (FeatureEdge e : g.edges()) {

			Set<FeatureNode<T>> sources = g.getSources(e);
			FeatureNode<T> target = g.getTarget(e);
			switch (e.getType()) {
			case FeatureEdge.MUTEX:
				result.andWith(mkMutex(sources));
				break;
			case FeatureEdge.XOR:
			case FeatureEdge.MANDATORY:
				result.andWith(mkFeatureNodeAnd(target)
						.impWith(mkOrNodes(sources)));
				//Mutual Exclusions
				result.andWith(mkMutex(sources));
				break;
			case FeatureEdge.OR:
				result.andWith(mkFeatureNodeAnd(target)
						.impWith(mkOrNodes(sources)));
				break;
			}
		}


		return result;
	}

	public Set<T> domain() {
		return Collections.unmodifiableSet(_featToVar.keySet());
	}

	public T syntheticRoot() {
		return _synth;
	}


}
