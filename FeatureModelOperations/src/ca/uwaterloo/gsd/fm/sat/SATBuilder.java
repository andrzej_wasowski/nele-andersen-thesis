package ca.uwaterloo.gsd.fm.sat;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.iterators.LoopingIterator;
import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;
import org.sat4j.specs.TimeoutException;

import ca.uwaterloo.gsd.fds.BDDBuilder;
import ca.uwaterloo.gsd.fm.Expression;
import ca.uwaterloo.gsd.fm.ExpressionType;
import ca.uwaterloo.gsd.fm.FeatureEdge;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureModel;
import ca.uwaterloo.gsd.fm.FeatureNode;

public class SATBuilder<T> {
	private Map<T, Integer> _featToVar; 
	private ISolver _solver = SolverFactory.newDefault();
	private int i=1;

	public SATBuilder() {
		_featToVar = new HashMap<T, Integer>();
	}
	
	public SATBuilder(Collection<T> vars) {
		for (T v : vars)
			add(v);
	}
	
	public SATBuilder(BDDBuilder<T> builder) {
		_featToVar = builder.getFeatureMap();
	}
	

	public int add(T o) {
		assert o != null;
		assert !_featToVar.containsKey(o);
		int var = i++;
		_featToVar.put(o, var);
		return var;
	}


	public int variable(T feat) {
		if (!_featToVar.containsKey(feat)) 
			throw new IllegalArgumentException(feat + " does not exist in map!");
		return _featToVar.get(feat);
	}
	
	public IProblem mkModel(FeatureModel<T> fm) throws TimeoutException, ContradictionException {
			_solver = SolverFactory.newDefault();
			FeatureGraph<T> g = fm.getDiagram();
			

			_solver.addClause( mkRoot(g) );
			_solver.addAllClauses( mkHierarchy(g) );
			_solver.addAllClauses( mkAndGroups(g) );
			_solver.addAllClauses( mkCardinality(g) );
			
			for (Expression<T> expr : fm.getConstraints()) {
				IVec<IVecInt> clauses = mkExpression(expr);
				_solver.addAllClauses(clauses);
			}

			return _solver;
	}
	
//	public boolean isSatisfiable(FeatureModel<T> fm, Expression<T> assump) throws TimeoutException {
//		
//	}
	
	public boolean isSatisfiable(FeatureModel<T> fm) throws TimeoutException {
		try {
			return mkModel(fm).isSatisfiable();
		}
		catch (ContradictionException e) {
			return false;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Expression<T>> splitConjunction(Expression<T> e) {
		if (e == null) return Collections.emptyList();
		else if (e.getType() == ExpressionType.AND) {
			return CollectionUtils.union(
					splitConjunction(e.getLeft()),
					splitConjunction(e.getRight()));
		}
		else
			return Arrays.asList( e );
	}
	
	public IVec<IVecInt> mkExpression(Expression<T> expr) throws ContradictionException {
		IVec<IVecInt> vec = new Vec<IVecInt>();		
		for (Expression<T> split : splitConjunction(expr)) {
		
			LinkedList<Integer> result = new LinkedList<Integer>();
			mkClause_Rec(split, result);
			
			int[] arr = new int[result.size()];
			int i=0;
			for (int next : result) {
				arr[i++] = next;
			}
			
			vec.push(new VecInt(arr));
		}
		return vec;
	}
	
	private void mkClause_Rec(Expression<T> expr, List<Integer> result) {
		if (expr == null) return;
		assert expr.getType() != ExpressionType.AND;
		
		switch (expr.getType()) {
		case OR:
			assert expr.getLeft().getType() == ExpressionType.OR 
				|| expr.getLeft().getType() == ExpressionType.FEATURE
			    || expr.getLeft().getType() == ExpressionType.NOT;
			
			assert expr.getRight().getType() == ExpressionType.OR 
		       || expr.getRight().getType() == ExpressionType.FEATURE
		       || expr.getRight().getType() == ExpressionType.NOT;
			
			mkClause_Rec(expr.getLeft(), result);
			mkClause_Rec(expr.getRight(), result);
			break;
		case NOT:
			assert expr.getLeft().getType() == ExpressionType.FEATURE;
			result.add(variable(expr.getLeft().getFeature()) * -1);
			break;
		case FEATURE:
			result.add(variable(expr.getFeature()));
			break;
		default:
			throw new UnsupportedOperationException("Only clauses in CNF are supported!");
		}
	}
	
	public IVec<IVecInt> mkAndGroups(FeatureGraph<T> g) throws ContradictionException {
		IVec<IVecInt> result = new Vec<IVecInt>();
		for (FeatureNode<T> v : g.selectAndNodes()) {
			LoopingIterator<T> iter = new LoopingIterator<T>(v.features());
			T start = iter.next();
			T prev = start;
			do {
				T next = iter.next();
				int ante = variable(prev) * -1;
				int cons = variable(next);
				result.push(new VecInt(new int[] { ante, cons} ));
				prev = next;
			} while (prev != start);
		}
		return result;
	}
	
	public IVecInt mkRoot(FeatureGraph<T> g) throws ContradictionException {
		T f = g.root().features().iterator().next();
		return new VecInt(new int[] { variable(f) } );
	}

	public IVec<IVecInt> mkHierarchy(FeatureGraph<T> g) throws ContradictionException {
		IVec<IVecInt> result = new Vec<IVecInt>();
		for (FeatureEdge e : g.selectHierarchyEdges()) {
			FeatureNode<T> source = g.getSource(e);
			FeatureNode<T> target = g.getTarget(e);

			for (T sf : source.features()) {
				for (T tf : target.features()) {
					int ante = variable(sf) * -1;
					int cons = variable(tf);
					result.push(new VecInt(new int[] {ante, cons} ));
				}
			}
		}
		return result;
	}

	public IVec<IVecInt> mkCardinality(FeatureGraph<T> g) throws ContradictionException {
		//FIXME
		IVec<IVecInt> result = new Vec<IVecInt>();
		for (FeatureEdge e : g.selectCardinalityEdges()) {
			switch (e.getType()) {
			case FeatureEdge.OR:
				combine( result, mkOrGroup(g.getSources(e), g.getTarget(e)) );
				break;
			case FeatureEdge.MUTEX:
				combine ( result, mkMutex(g.getSources(e)) );
				break;
			case FeatureEdge.XOR:
				combine ( result, mkOrGroup(g.getSources(e), g.getTarget(e)) );
				combine ( result, mkMutex(g.getSources(e)) );
				break;
			case FeatureEdge.MANDATORY:
				combine ( result, mkMandatory(g.getSource(e), g.getTarget(e)) );
				break;
			}
		}
		return result;
	}
	
	private IVecInt mkMandatory(FeatureNode<T> source, FeatureNode<T> target) throws ContradictionException {
		int ante = variable(target.features().iterator().next()) * -1;
		int cons = variable(source.features().iterator().next());
		return new VecInt(new int[] { ante, cons} );
	}

	private IVecInt mkOrGroup(Set<FeatureNode<T>> sources, FeatureNode<T> target) throws ContradictionException {
		int[] clause = new int[sources.size() + 1];
		clause[0] = variable(target.features().iterator().next()) * -1;
		Iterator<FeatureNode<T>> iter = sources.iterator();
		for (int i=1; i<clause.length; i++)
			clause[i] = variable(iter.next().features().iterator().next());
		
		return new VecInt(clause);
	}
	
	private IVec<IVecInt> mkMutex(Set<FeatureNode<T>> sources) throws ContradictionException {
		IVec<IVecInt> result = new Vec<IVecInt>();
		
		LoopingIterator<FeatureNode<T>> iter
			= new LoopingIterator<FeatureNode<T>>(sources);

		FeatureNode<T> start = iter.next();
		FeatureNode<T> prev = start;
		do {
			FeatureNode<T> next = iter.next();
			int ante = variable(next.features().iterator().next()) * -1;
			int cons = variable(prev.features().iterator().next()) * -1;
			result.push(new VecInt(new int[] { ante, cons} ));
		} while (prev != start);
		
		return result;
	}
	
	private IVec<IVecInt> combine(IVec<IVecInt> base, IVec<IVecInt> merge) {
		Iterator<IVecInt> iter = merge.iterator();
		while (iter.hasNext())
			base.push(iter.next());
		return base;
	}
	
	private IVec<IVecInt> combine(IVec<IVecInt> base, IVecInt merge) {
		base.push(merge);
		return base;
	}
}
