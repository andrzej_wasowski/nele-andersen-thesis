 package ca.uwaterloo.gsd.fds;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.javabdd.BDD;
import ca.uwaterloo.gsd.fm.FeatureGraph;
import ca.uwaterloo.gsd.fm.FeatureNode;
import ca.uwaterloo.gsd.fm.RootFeature.RootType;
import dk.itu.fds.Graph;
import dk.itu.fds.ImplicationGraph;

/**
 *
 * Hack to convert FeatureGraph data structures
 * I really want to rewrite this.
 * @author shshe
 *
 * @param <T>
 */
public class FeatureGraphBuilder<T> {

	BDDBuilder<T> mBuilder;
	dk.itu.fds.FeatureGraph mBddGraph;
	FeatureGraph<T> result;
	Map<Integer, FeatureNode<T>> varToNode;


	public FeatureGraphBuilder(BDDBuilder<T> builder) {
		mBuilder = builder;
	}

	/**
	 * Synthesizes a Generalized feature model.
	 * @return Generalized feature model.
	 */
	public FeatureGraph<T> build(BDD formula, T rootFeature) {
		BDD temp = mBuilder.one();
		FeatureGraph<T> built = build(formula, temp, rootFeature, RootType.STANDARD);
		temp.free();
		return built;
	}

	public FeatureGraph<T> build(BDD formula, BDD featuresBDD, T rootFeature) {
		return build(formula, featuresBDD, rootFeature, RootType.STANDARD);
	}

	/**
	 * Creates an unnamed root feature in FeatureGraph. This is merely a derived
	 * feature with no semantics and serves only as a single container for the
	 * rest of the model.
	 *
	 * @return
	 */
	public FeatureGraph<T> build(BDD formula, BDD featuresBDD, T rootFeature, RootType type) {
		result = new FeatureGraph<T>();
		varToNode = new HashMap<Integer, FeatureNode<T>>();
		mBuilder.extVarNum(20);

		BDD removed = ImplicationGraph.removeDeadFeatures(formula);
		Graph implGraph = ImplicationGraph.build(removed);

		System.out.println("----- IMPLICATION GRAPH -----");
		System.out.println(implGraph);

		mBddGraph = dk.itu.fds.FeatureGraph.build(implGraph, removed);
		System.out.println("----- BDD GRAPH -----");
		System.out.println(mBddGraph);
		System.out.println("-----    END    -----");

		//FIXME better logic here
		if (!mBuilder.containsFeature(rootFeature)) {
			mBuilder.addVar(rootFeature);
		}
		if (result.featureNode(rootFeature) == null)
			result.addRoot(rootFeature, type);

		FeatureNode<T> rootNode = result.featureNode(rootFeature);
		Set<Integer> roots = findRootVertices();
		for (int i : roots) {
			FeatureNode<T> child = build(i);
			result.addEdge(child, rootNode);
		}

		//Add free variables as optional features of Root
		int[] vars = featuresBDD.scanSet();
		if (vars != null) {
			for (int i : vars) {
				T feature = mBuilder.ithFeature(i);
				FeatureNode<T> node = result.featureNode(feature);
				if (!result.containsVertex(node)) {
					FeatureNode<T> free = result.addOptional(feature);
					result.addEdge(free, rootNode);
				}
			}
		}

		removed.free();

		return result;
	}

	protected FeatureNode<T> build(int v) {
		List<FeatureNode<T>> children = new ArrayList<FeatureNode<T>>();

		//Process children
		if (mBddGraph.edges().get(v) != null) {
			for (int childV : mBddGraph.edges().get(v)) {
				FeatureNode<T> child = build(childV);
				assert !children.contains(child);
				children.add(child);
			}
		}

		FeatureNode<T> node;
		if (isAndGroup(v)) {
			Set<Integer> grpVars = mBddGraph.getAndGroups().get(v);
			Collection<FeatureNode<T>> grouped = varsToFeatureNodes(grpVars);
			node = result.addAndGroupNodes(grouped);
			System.out.println("AND GROUP: " + node);
			children.removeAll(grouped);
		}
		else if (isOrGroup(v)) {
			Set<Integer> grpVars = mBddGraph.getOrGroups().get(v);
			Collection<FeatureNode<T>> grouped = varsToFeatureNodes(grpVars);
			node = result.addOrGroupNodes(grouped);
			children.removeAll(grouped);
		}
		else if (isXorGroup(v)) {
			Set<Integer> grpVars = mBddGraph.getXorGroups().get(v);
			Collection<FeatureNode<T>> grouped = varsToFeatureNodes(grpVars);
			node = result.addXorGroupNodes(grouped);
			children.removeAll(grouped);
		}
		else if (isMutexGroup(v)) {
			Set<Integer> grpVars = mBddGraph.getMutexGroups().get(v);
			Collection<FeatureNode<T>> grouped = varsToFeatureNodes(grpVars);
			node = result.addMutexGroupNodes(grouped);
			children.removeAll(grouped);
		}
		else {
			System.out.println("looking for: " + v + " " + mBuilder.ithFeature(v));
			node = result.addOptional(mBuilder.ithFeature(v));
		}
		addVar(v, node);
		addChildrenNodes(node, children);

		return node;
	}

	private void addVar(int v, FeatureNode<T> node) {
		varToNode.put(v, node);
		result.addVertex(node);
	}


	/**
	 * This method could return multiple root vertices if there is a disjoint feature graph.
	 *
	 * @category important
	 * @return
	 */
	protected Set<Integer> findRootVertices() {
		Set<Integer> vertices = new HashSet<Integer>(mBddGraph.vertices());
		for (Set<Integer> edgeTargets : mBddGraph.edges().values()) {
			vertices.removeAll(edgeTargets);
		}
		return vertices;
	}

	private Collection<FeatureNode<T>> varsToFeatureNodes(Collection<Integer> items) {
		Set<FeatureNode<T>> grpFeats = new HashSet<FeatureNode<T>>();
		for (int i : items) {
			assert varToNode.containsKey(i);
			grpFeats.add(varToNode.get(i));
		}
		return grpFeats;
	}

	private void addChildrenNodes(FeatureNode<T> parent, Collection<FeatureNode<T>> children) {
		for (FeatureNode<T> child : children) {
			if (!result.containsEdge(child, parent)) {
				result.addEdge(child, parent);
			}
		}
	}

	private boolean isAndGroup(int var) {
		return mBddGraph.getAndGroups().keySet().contains(var);
	}

	private boolean isOrGroup(int var) {
		return mBddGraph.getOrGroups().keySet().contains(var);
	}

	private boolean isXorGroup(int var) {
		return mBddGraph.getXorGroups().keySet().contains(var);
	}

	private boolean isMutexGroup(int var) {
		return mBddGraph.getMutexGroups().keySet().contains(var);
	}

}
