package dk.itu.fds;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;

public class BDDBuilder {

static private BDDFactory _factory = null;



public BDDFactory factory() {

	return _factory;
}



public BDDBuilder() {

	if (_factory == null) {
		_factory = BDDFactory.init ("java", 200000, 50000);
		_factory.setVarNum (1000);
	}
}



public BDDBuilder(BDDFactory factory) {

	assert BDDBuilder._factory == null;
	BDDBuilder._factory = factory;
	_factory.setVarNum (Math.max (2000, _factory.varNum()));
}



public BDD mkAnd(int root, int[] children) {

	BDD temp = _factory.one ();

	for (int i : children)
		temp.andWith (ith (i));

	return mkGroup (root, children).andWith (ith (root).impWith (temp));
}



/**
 * The name is slightly misleading. This ting generates implications from
 * children to parent, but nothing more.
 */
public BDD mkGroup(int root, int[] children) {

	BDD result = _factory.one ();

	for (int i : children)
		result.andWith (ith (i).impWith (ith (root)));

	return result;
}



public BDD mkMandatory(int root, int child) {

	return ith (child).biimpWith (ith (root));
}



public BDD mkOptional(int root, int child) {

	return ith (child).impWith (ith (root));
}



public BDD mkOr(int root, int[] children) {

	BDD temp = _factory.zero ();

	for (int i : children)
		temp.orWith (ith (i));

	return mkGroup (root, children).andWith (ith (root).impWith (temp));
}



// NOTE: we cannot possibly enforce the root variable to be true in our models,
// as this
// makes it impossible to detect the implications of root easily.
// NOTE 2008-07-08: this has now been fixed in ImplicationGraph.java and it
// should work
public BDD simpleFixture() {

	BDD[] temps = new BDD[] { mkOr (1, new int[] { 3, 4, 5 }),
		mkMandatory (1, 2), mkOptional (2, 13), mkMandatory (3, 12),
		mkAnd (4, new int[] { 6, 7, 8 }), mkOptional (5, 11),
		mkMandatory (7, 10), mkOptional (8, 9) };

	BDD result = _factory.one ();

	for (BDD b : temps)
		result.andWith (b);

	return result;
}



// do not change this fixture. It is used in regression tests
public BDD simpleFixtureNoCliques() {

	BDD needless = _factory.makeSet (new int[] { 1, 2, 3, 4, 6, 7, 8, 10,
		12 });
	BDD b16 = ith (16).biimpWith (ith (1));
	BDD b15 = ith (15).biimpWith (ith (3));
	BDD b14 = ith (14).biimpWith (ith (4));
	BDD sf = simpleFixture ().andWith (b16).andWith (b15).andWith (b14);
	BDD result = sf.exist (needless);

	needless.free ();
	sf.free ();

	return result;
}



public BDD oneOrGroup() {

	return mkOr (1, new int[] { 2, 3, 4 });
}



public BDD fig1() {

	BDD nand78 = ith (6).impWith (ith (7).biimpWith (nith (8)));
	BDD result = mkMandatory (1, 2).andWith (mkMandatory (1, 3)).andWith (
		mkOr (3, new int[] { 4, 5 })).andWith (mkMandatory (1, 6)).andWith (
		mkOr (6, new int[] { 7, 8 })).andWith (nand78).andWith (
		mkOptional (1, 9)).andWith (mkOptional (1, 10));
	return result;
}



public BDD fig1ea() {

	return fig1 ().andWith (ith (4).impWith (ith (8))).andWith (
		ith (9).impWith (ith (10)));

}



private BDD nith(int v) {

	return _factory.nithVar (v);
}



private BDD ith(int v) {

	return _factory.ithVar (v);
}



/**
 * Represent A*B + B. Used in tests and experiments. The problem with this
 * models is that A is not present in the support. The map is that A is 1, B is
 * 2.
 *
 * @return
 */
public BDD reducedVariable() {

	return (ith (1).andWith (ith (2))).orWith (ith (2));
}



/**
 * Represent A*B + B. Used in tests and experiments. The problem with this
 * models is that A is not present in the support. The map is that A is 1, B is
 * 2.
 *
 * @return
 */
public BDD notX() {

	return nith (1);
}

}
