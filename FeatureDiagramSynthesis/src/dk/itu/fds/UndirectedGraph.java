package dk.itu.fds;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

import org.jgrapht.alg.BronKerboschCliqueFinder;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

public class UndirectedGraph extends Graph {

	public UndirectedGraph(int[] vertices) {

		super(vertices);
	}

	@Override
	public void connect(Integer source, Collection<Integer> targets) {
		super.connect(source, targets);
		for (int v : targets) {
			super.connect(v, source);
		}
	}

	@Override
	public void connect(Integer source, Integer target) {
		super.connect(source, target);
		super.connect(target, source);
	}


	/**
	 * SS I cheated. Using JGraphT's CliqueFinder for now...
	 * Cliques of size 1 are not returned.
	 * @return
	 */
	public Collection<Set<Integer>> findAllUndirectedCliques() {
		org.jgrapht.UndirectedGraph<Integer, DefaultEdge> g
			= new SimpleGraph<Integer, DefaultEdge>(DefaultEdge.class);

		for (int v : vertices()) {
			g.addVertex(v);
		}

		Iterator<Integer> it1 = vertices().iterator();

		while (it1.hasNext()) {
			int i = it1.next();
			Iterator<Integer> it2 = vertices().iterator();
			while (it2.hasNext()) {
				int j = it2.next();
				if (connected(i,j))
					g.addEdge(i,j);
			}
		}

		BronKerboschCliqueFinder<Integer, DefaultEdge> finder
			= new BronKerboschCliqueFinder<Integer, DefaultEdge>(g);

		Collection<Set<Integer>> cliques = finder.getAllMaximalCliques();

		//Filter out cliques of size 1
		Collection<Set<Integer>> results = new LinkedList<Set<Integer>>();
		for (Set<Integer> clique : cliques) {
			if (clique.size() > 1) {
				results.add(clique);
			}
		}
		return results;
	}

}
