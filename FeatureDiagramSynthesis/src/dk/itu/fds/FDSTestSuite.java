package dk.itu.fds;

import junit.framework.Test;
import junit.framework.TestSuite;

public class FDSTestSuite {
	public static Test suite() {
		TestSuite suite = new TestSuite("FDS Tests");

		suite.addTestSuite(FeatureGraphTest.class);
		suite.addTestSuite(ImplicationGraphTest.class);
		suite.addTestSuite(PrimeImplicantsTest.class);
		suite.addTestSuite(ValidDomainsTest.class);

		return suite;
	}

}
