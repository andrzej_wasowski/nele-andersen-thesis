package dk.itu.fds;

import net.sf.javabdd.BDDFactory;
import junit.framework.TestCase;

public class ValidDomainsTest extends TestCase {

private BDDBuilder _builder = new BDDBuilder ();
private BDDFactory _factory = _builder.factory ();



protected void setUp() throws Exception {

	super.setUp ();
}



public void testOnOneEmpty() {

	ValidDomains vd = new ValidDomains (_factory.one ());

	assertTrue (vd.isEmpty ());
}



public void testOnOne1toN() {

	final int N = 10;
	for (int i = 0; i < N; ++i) {

		ValidDomains vd = new ValidDomains (_factory.one (), 0, i);

		assertEquals (i + 1, vd.size ());

		for (int j = 0; j <= i; ++j) {
			assertTrue (vd.canBeOne (j));
			assertTrue (vd.canBeZero (j));
		}

	}
}



public void testOnZero() {

	try {
		new ValidDomains (_factory.zero ());
	} catch (java.lang.AssertionError e) {
		return;
	}

	fail ("Valid domains (intentionally) does not work for inconsistent formulae.");
}



public void testOnOneOrGroup() {

	ValidDomains vd = new ValidDomains (_builder.oneOrGroup ());

	assertEquals (4, vd.size ());

	for (int i = 1; i <= 4; ++i) {

		assertTrue (vd.canBeOne (i));
		assertTrue (vd.canBeZero (i));

	}

}


public void testOnSimpleFixture() {

	ValidDomains vd = new ValidDomains (_builder.simpleFixture ());

	assertEquals (13, vd.size ());

	for (int i = 1; i <= 13; ++i) {

		assertTrue (vd.canBeOne (i));
		assertTrue (vd.canBeZero (i));

	}

}
}
