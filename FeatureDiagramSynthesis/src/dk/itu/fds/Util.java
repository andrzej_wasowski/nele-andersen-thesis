package dk.itu.fds;

public class Util {

/**
 * returns Integer.MIN_VALUE for an empty or null array
 * 
 * @param elements
 */
public static int max(int[] elements) {

	int result = Integer.MIN_VALUE;

	if (elements != null)
		for (int i : elements)
			result = (result >= i ? result : i);
	return result;
}



/**
 * returns Integer.MAX_VALUE for an empty or null array
 * 
 * @param elements
 */
public static int min(int[] elements) {

	int result = Integer.MAX_VALUE;

	if (elements != null)
		for (int i : elements)
			result = (result <= i ? result : i);
	return result;
}



/**
 * returns Integer.MIN_VALUE for an empty or null array
 * 
 * @param elements
 */
public static int max(Iterable<Integer> elements) {

	int result = Integer.MIN_VALUE;

	if (elements != null)
		for (int i : elements)
			result = (result >= i ? result : i);
	return result;
}



/**
 * returns Integer.MAX_VALUE for an empty or null array
 * 
 * @param elements
 */
public static int min(Iterable<Integer> elements) {

	int result = Integer.MAX_VALUE;

	if (elements != null)
		for (int i : elements)
			result = (result <= i ? result : i);
	return result;
}



public static int[] asIntArray(Integer[] a) {

	int[] result = new int[a.length];
	for (int i = 0; i < a.length; i++)
		result[i] = a[i];
	return result;
}

public static Integer[] asIntegerArray(int[] a) {

	Integer[] result = new Integer[a.length];
	for (int i = 0; i < a.length; i++)
		result[i] = a[i];
	return result;
}

}
