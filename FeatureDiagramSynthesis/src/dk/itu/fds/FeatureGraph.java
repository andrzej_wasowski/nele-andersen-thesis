package dk.itu.fds;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;

/**
 * Edges in this graph are directed downwards in the hierarchy, so from parents
 * to children. This is somewhat inconsistent as they do not follow the
 * direction of implications at all, as far as I can see (so for group nodes
 * they agree with implications, but for regular hierarchy relation they are
 * reverse to implications) [2208-07-08]
 *
 * @author wasowski-fevo
 *
 */
public class FeatureGraph extends Graph {

private BDD _formula = null;
private BDDFactory _factory = null;

/**
 * Hack
 */
static Logger logger = Logger.getLogger("fds.FeatureGraph");
Map<Integer, Set<Integer>> mAndGroups = new HashMap<Integer, Set<Integer>>();
Map<Integer, Set<Integer>> mOrGroups  = new HashMap<Integer, Set<Integer>>();
Map<Integer, Set<Integer>> mXorGroups = new HashMap<Integer, Set<Integer>>();
Map<Integer, Set<Integer>> mMutexGroups = new HashMap<Integer, Set<Integer>>();


public BDD formula() {

	return _formula;
}



public Map<Integer, Set<Integer>> getAndGroups() {

	return mAndGroups;
}



public Map<Integer, Set<Integer>> getOrGroups() {

	return mOrGroups;
}



public Map<Integer, Set<Integer>> getXorGroups() {

	return mXorGroups;
}



public Map<Integer, Set<Integer>> getMutexGroups() {

	return mMutexGroups;
}



private FeatureGraph() {

}



public FeatureGraph(Graph G, BDD formula) {

	super (G);
	this._formula = formula.id ();
	this._factory = formula.getFactory ();

}



public static FeatureGraph build(Graph G, BDD formula) {

	FeatureGraph result = new FeatureGraph (G, formula);
	logger.info ("Creating and nodes...");
	result.createAndNodes ();
	result.transitiveReduction ();
	
	logger.info ("Creating or nodes...");
	result.createOrNodes ();
	
	logger.info ("Creating mutex nodes...");
	result.createMutexNodes ();

	return result;
}



public boolean isXor(List<Integer> vars, BDD formula) {

	int min_var = Util.min (vars);
	int max_var = Util.max (vars);
	for (int v : vars) {

		BDD restricted = formula.id ().restrictWith (ithVar (v));
		ValidDomains vd = new ValidDomains (restricted, Math.min (
			min_var, restricted.var ()), max_var);

		for (Integer u : vars) {

			if (u == v)
				continue;
			if (vd.canBeOne (u))
				return false;
		}
	}

	return true;
}



private int orGroupUpperBound(List<Integer> vars, BDD formula) {

	if (isXor (vars, formula))
		return 1;
	else
		return vars.size ();
}



public void createOrNodes() {

	List<Integer> oldVertices = new LinkedList<Integer> (vertices ());
	List<int[]> newEdges = new LinkedList<int[]> ();

	for (int v : oldVertices) {

		if (isSink (v))
			continue;

		BDD b = _factory.nithVar (v);

		// project formula on v and its children
		Set<Integer> vchildren = this.children (v);
		Integer[] children = vchildren.toArray(new Integer[0]);
		int[] N = new int[children.length+1];
		N[0] = v;
		for (int i = 0; i < children.length; ++i)
			N[i+1] = children[i];
		BDD frag = BDDUtil.project (_formula,N);

		PrimeImplicants pi = new PrimeImplicantsOptimized (b, frag);
		b.free ();
		frag.free ();

		for (Implicant imp : pi) {
			Implicant imp1 = imp.removeNegations ();

			if (imp1.size () < 2)
				continue;
			if (imp1.contains (v))
				continue;
			if (!vchildren.containsAll (imp1))
				continue;

			int gv = newVertex ();
			newEdges.add (new int[] { v, gv });

			StringBuffer lab = new StringBuffer (gv + " [1.."
				+ orGroupUpperBound (imp1, _formula) + "]: ");

			for (int u : imp1) {
				lab.append (" " + u);
				newEdges.add (new int[] { gv, u });
			}

			//Hack
			Set<Integer> groupSet = new HashSet<Integer>(imp1);
			if (isXor(imp1, _formula)) {
				mXorGroups.put(gv, groupSet);
			}
			else {
				mOrGroups.put(gv, groupSet);
			}

			vstyles ().put (gv, new NodeStyle (lab.toString ()));

		}
	}

	for (int[] e : newEdges)
		connect (e[0], e[1]);
}



public void createAndNodes() {

	Collection<Set<Integer>> cliques = findAllDirectedCliques ();

	for (Set<Integer> clique : cliques) {

		int v = newVertex ();

		// Hack
		Set<Integer> groupSet = new HashSet<Integer> ();

		String lab = v + " [" + clique.size () + ".." + clique.size ()
			+ "]:";
		for (int u : clique)
			lab += " " + u;
		vstyles ().put (v, new NodeStyle (lab));
		Set<Integer> vtargets = new HashSet<Integer> (clique);

		while (_factory.varNum () <= v)
			_factory.setVarNum (_factory.varNum () * 2);

		_formula.andWith (ithVar (v).biimpWith (
			ithVar (clique.iterator ().next ())));

		BDD bclique = _factory.one ();
		for (int u : clique) {
			// Hack
			groupSet.add (u);

			Set<Integer> vs = edges ().get (u);
			vs.removeAll (clique);
			vtargets.addAll (vs);
			vs.clear ();
			edges ().remove (u);
			vstyles ().get (u).put ("shape", "box");
			vstyles ().get (u).put ("style", "filled");

			bclique.andWith (ithVar (u));
		}

		BDD temp = _formula.exist (bclique);
		_formula.free ();
		bclique.free ();
		_formula = temp;

		for (Set<Integer> vs : edges ().values ()) {

			if (vs.removeAll (clique))
				vs.add (v);
		}

		// Hack
		mAndGroups.put (v, groupSet);

		edges ().put (v, vtargets);

	}

}



public void createMutexNodes() {

	if (vertices ().size () == 0 || _formula.isOne ())
		return;

	BDD support = _formula.support ();
	int[] sup = support.scanSet ();
	support.free ();

	int min_var = Util.min (sup);
	int max_var = Util.max (sup);

	// Need to buil implGraph since _formula has changed in createAndNodes()
	Graph implG = ImplicationGraph.build (_formula);
	UndirectedGraph mutex = new UndirectedGraph (sup);

	// Not needed if we want to keep transitive groups?
	// implG.transitiveReduction();

	Collection<Set<Integer>> siblings = implG.edges ().values ();

	// Construct Mutex Graph
	for (Set<Integer> sibSet : siblings) {
		Integer[] sib = sibSet.toArray (new Integer[sibSet.size ()]);
		for (int i = 0; i < sib.length; i++) {
			int s1 = sib[i];

			if (Arrays.binarySearch (sup, s1) < 0)
				continue;

			BDD mx = _formula.id ().andWith (ithVar (s1));

			if (mx.isZero ()) {
				throw new UnsupportedOperationException (
					"Dead features should be removed!");
			} else {
				ValidDomains vd = new ValidDomains (mx,
					min_var, max_var);

				for (int j = i + 1; j < sib.length; j++) {
					int s2 = sib[j];

					if (Arrays.binarySearch (sup, s2) < 0)
						continue;

					if (!vd.canBeOne (s2)
						&& vd.canBeZero (s2)) {
						mutex.connect (s1, s2);
					}
				}
			}
			mx.free ();
		}
	}

	// Find cliques
	Collection<Set<Integer>> cliques = mutex.findAllUndirectedCliques ();
	List<int[]> newEdges = new LinkedList<int[]> ();
	for (Set<Integer> clique : cliques) {
		//Find Common Ancestor
		int parent = closestAncestor(implG, clique);

		//Steven
		if (existsXorGroup(parent, clique)) {
			continue;
		}

		int gv = newVertex();
		newEdges.add(new int[] {parent, gv});
		StringBuffer lab = new StringBuffer(gv + " [0..1]:");

		for (int u : clique) {
			lab.append (" " + u);
			newEdges.add (new int[] { gv, u });
		}

		vstyles ().put (gv, new NodeStyle (lab.toString ()));

		//Hack
		mMutexGroups.put(gv, clique);

		for (int[] e : newEdges)
			connect (e[0], e[1]);
	}

}



/**
 * @param implG
 *                transitive implication graph
 */
private int closestAncestor(Graph implG, Set<Integer> mutex) {

	Graph reversed = implG.reverseEdges ();
	Set<Integer> common = new HashSet<Integer> (vertices ());
	for (int v : mutex) {
		Set<Integer> ancestors = reversed.edges ().get (v);
		common.retainAll (ancestors);
	}

	Set<Integer> parents = new HashSet<Integer> ();
	for (int v : common) {
		for (int u : children (v)) {
			if (common.contains (u)) {
				parents.add (v);
				break;
			}
		}
	}

	common.removeAll (parents);
	if (common.size () > 1) {
		System.err.println ("WARNING: multiple parents "
			+ " for mutex: " + mutex + "=" + common);
	}
	return common.iterator ().next ();
}

private boolean existsXorGroup(int parent, Set<Integer> members) {
	Set<Integer> groups = new HashSet<Integer>(edges().get(parent));
	groups.retainAll(mXorGroups.keySet());
	for (int g : groups) {
		if (mXorGroups.containsKey(g) && mXorGroups.get(g).equals(members))
			return true;
	}
	return false;
}

/**
 * This should never find anything in graphs from which cliques had been
 * removed.
 *
 */
public void findMandatorySubfeatures() {

	for (Map.Entry<Integer, Set<Integer>> e : edges ().entrySet ()) {

		int v = e.getKey ();
		for (int u : e.getValue ()) {
			BDD implication = _formula.id ().andWith (ithVar (v)).impWith (
				ithVar (u));
			if (implication.isOne ())
				appendLabel (u, "[mandatory]");
		}
	}
}



private BDD ithVar(int v) {

	return _factory.ithVar (v);
}
}
