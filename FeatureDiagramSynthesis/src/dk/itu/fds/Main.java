package dk.itu.fds;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;

public class Main {

protected static BDDFactory B;



/**
 * @param args
 */
public static void main(String[] args) {

	B = BDDFactory.init ("java", 150000, 30000);
	B.setVarNum (40);

	BDDBuilder BB = new BDDBuilder (B);
	// BDD formula = BB.simpleFixture ();
	// BDD formula = BB.reducedVariable ();
	BDD formula = BB.notX ();
	
	assert !formula.isZero();
	assert !formula.isOne ();

	Graph G = ImplicationGraph.build (formula);
	Graph FG = new FeatureGraph (G, formula);
	formula.free ();
	// // just a hack to remove loops, without touching cliques:
	// G.removeLoops ();
	// System.out.print (G.toString ());
	// FG.removeLoops();
	System.out.print (FG.toString ());
}
}
