package dk.itu.fds;


import net.sf.javabdd.BDD;

public class PrimeImplicantsVanilla extends PrimeImplicants implements Iterable<Implicant> {

public PrimeImplicantsVanilla(BDD formula, BDD theory) {

	super (formula, theory);
}



public PrimeImplicantsVanilla(BDD formula) {

	super (formula);
}



public PrimeImplicantsVanilla(BDD formula, BDD theory, BDD support) {

	super (formula, theory, support);
}



/**
 * FIXME: !!! this algorithm may be potentially exponential. One should use
 * caching to avoid computing the same primes all over again. We will not see it
 * when reconstructing the feature models probably, but one can give formulae
 * with extremely many primes.
 * 
 * Also we should probably be use simplify instead of restrict. Using restrict
 * we actually record the path that we come from in the bdd, eliminating a lot
 * caching possibilities.
 * 
 * @param f
 * @param last_var
 * @return
 */
protected BDD negativePrimesWith(BDD f, int last_var) {

	// Logger.getAnonymousLogger().info("last_var: " + last_var +
	// " negate: " + f);

	if (f.isOne ())
		return negateFromTo (last_var + 1, max_var + 1);

	if (f.isZero ())
		return f;

	int v = f.var ();
	assert last_var < v;
	
	BDD negs = negateFromTo (last_var + 1, v);
	BDD result = B.one ();

	BDD fv = f.restrict (B.ithVar (v)); // freed
	BDD fnv = f.restrict (B.nithVar (v));// freed
	BDD v2 = B.ithVar (v * 2); // freed
	BDD nv2 = v2.not ();
	BDD nv2succ = B.nithVar (v * 2 + 1); // freed
	f.free ();
	f = null;

	BDD without_v = negativePrimesWith (fnv.and (fv), v);
	result.andWith (without_v.and (nv2)).andWith (negs.id ());
	nv2.free ();
	nv2 = null;

	result.orWith (
		negativePrimesWith (fnv, v).andWith (without_v.not ()).andWith (
			v2).andWith (nv2succ)).andWith (negs);

	fv.free ();
	fv = null;

	// The part below is not needed as we are only interested in implicants
	// with positive occurrences.
	// result.orWith(negativePrimesWith(fv).andWith(tempnot).andWith(B.ithVar(2
	// * v))
	// .andWith(B.ithVar(2 * v + 1)));

	return result;
}


}
