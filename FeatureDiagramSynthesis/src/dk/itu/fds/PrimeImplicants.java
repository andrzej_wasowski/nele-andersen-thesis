package dk.itu.fds;

import java.util.Iterator;
import java.util.ListIterator;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;

abstract public class PrimeImplicants implements Iterable<Implicant> {

protected BDD primes = null;
protected BDD support = null;
protected int max_var = -1;
protected BDDFactory B = null;



public boolean equals(PrimeImplicants pi) {

	return this.primes.equals (pi.primes)
		&& this.support.equals (pi.support)
		&& this.max_var == pi.max_var;
}

public boolean equals (Object pi) {
	return this.equals ((PrimeImplicants) pi);
}

protected static class ImplicantsIterator implements Iterator<Implicant> {

ListIterator<byte[]> curr;
int[] support = null;



/**
 * 
 * @param support
 *                Support of the BDD representing the formula (and theory, so
 *                the problem). Not the support of the BDD representing the
 *                metaproduct
 */
@SuppressWarnings("unchecked")
private ImplicantsIterator(BDD primes, BDD support) {

	assert support != null;
	// absolutely stupid interface of javabdd
	this.curr = primes.allsat ().listIterator();
	assert curr != null;
	this.support = support.scanSet ();
	if (this.support == null)
		this.support = new int[0];
}



public boolean hasNext() {
	if (!curr.hasNext())
		return false;
	
	boolean isNextNull =  curr.next() == null;
	curr.previous();
	return !isNextNull;
}



public Implicant next() {

	return new Implicant ((byte[]) curr.next (), support);
}



public void remove() {

	assert false; // not implemented
}
}



public PrimeImplicants(BDD formula, BDD theory) {
	BDD f = formula.support ();
	BDD g = theory.support ();
	mkPrimeImplicantsWith (formula.id (), theory.id (),
		formula.support ().and (theory.support ()));

}



public PrimeImplicants(BDD formula) {

	this.B = formula.getFactory ();

	mkPrimeImplicantsWith (formula.id (), B.one (), formula.support ());
}



public PrimeImplicants(BDD formula, BDD theory, BDD support) {

	mkPrimeImplicantsWith (formula.id (), theory.id (), support.id ());
}



public void free() {

	primes.free ();
	support.free ();

}



public BDD getBDD() {

	return primes;
}



public PrimeImplicants() {

	super ();
}



public Iterator<Implicant> iterator() {

	assert primes != null;
	return new ImplicantsIterator (this.primes, this.support);
}



protected BDD negateFromTo(int from, int to) {

	BDD result = B.one ();
	for (int i = from; i < to; ++i)
		result.andWith (B.nithVar (i * 2));
	return result;
}



abstract protected BDD negativePrimesWith(BDD f, int last_var);



protected void mkPrimeImplicantsWith(BDD formula, BDD theory, BDD support) {

	this.support = support.id ();

	this.B = formula.getFactory ();

	int[] isup = support.scanSet ();
	max_var = Util.max (isup);
	
	if (B.varNum() < 3*max_var)
		B.setVarNum (3*max_var);

	this.primes = this.negativePrimesWith (theory.imp (formula), -1);

	// why not? this.primes = negativePrimesWith (formula.and (theory), -1);
	// 2009-03-11 This is actually correct, or at least exactly the same as
	// in the SPLC09 paper. Well, essentially theory.imp(! formula) and
	// and theory.and (formula) have the same implicants. We do not do the
	// negation here, becasue the formula is already given in a negated
	// form.

	// FIXME!: we should actually compute the *implicants* of phi as a
	// metaprduct
	// and conjoin them in, to avoid this stupid enumeration and checkin
	// implication of phi. This should! be done for camera ready.

	// It seems that we do not have to remove any of the primes, because
	// in our specific application (where formula is a negated literal)
	// there will be precisely one vacous implicant, which will not be
	// generated as it is positive.

}

}
