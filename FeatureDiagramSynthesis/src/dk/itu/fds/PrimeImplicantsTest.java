package dk.itu.fds;

import java.util.Iterator;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import junit.framework.TestCase;

public class PrimeImplicantsTest extends TestCase {

private final BDDBuilder _builder = new BDDBuilder ();
private final BDDFactory _factory = _builder.factory ();
private final BDD _simpleFixture = _builder.simpleFixture ();
private final BDD _simpleFixtureNoCliques = _builder.simpleFixtureNoCliques ();
private final BDD _oneOrGroup = _builder.oneOrGroup ();
private BDD _tuxOnIce;



protected void setUp() throws Exception {

	super.setUp ();
	this._tuxOnIce = this._factory.load ("tests/tuxonice.bdd");

	System.out.println ("tuxonice node count: " + _tuxOnIce.nodeCount ());
	int[] s = _tuxOnIce.support ().scanSet ();
	System.out.println ("tuxOnIce min var: " + Util.min (s));
	System.out.println ("tuxOnIce max var: " + Util.max (s));
	
	_tuxOnIce.printDot ();

}



protected void tearDown() throws Exception {

}



public void testOnZero() {

	BDD support = _factory.one ();

	// should just have one empty prime implicant.
	for (int i = 1; i <= 4; ++i) {

		PrimeImplicants piV = new PrimeImplicantsVanilla (
			_factory.zero (), _factory.one (), support);
		PrimeImplicants piO = new PrimeImplicantsOptimized (
			_factory.zero (), _factory.one (), support);

		support.orWith (_factory.ithVar (i));

		Iterator<Implicant> j = piV.iterator ();
		assertTrue ("There should not be any implicants", !j.hasNext ());
		piV.free ();

		j = piO.iterator ();
		assertTrue ("There should not be any implicants", !j.hasNext ());
		piO.free ();
	}
}



public void testJavaBDDInvariants() {

	BDD u1 = _factory.ithVar (1).andWith (
		_factory.ithVar (3).orWith (_factory.nithVar (3)));
	BDD u2 = _factory.ithVar (1).andWith (_factory.ithVar (3)).orWith (
		_factory.ithVar (1).andWith (_factory.nithVar (3)));

	assertEquals (u1, u2);
	assertTrue (u1.equals (u2));
	assertNotSame (u1, u2);
	assertTrue (u1 != u2);
	assertTrue (u1.hashCode () == u2.hashCode ());
}



public void testOnOne() {

	BDD support = _factory.one ();

	// should just have one empty prime implicant.
	for (int i = 1; i <= 4; ++i) {

		PrimeImplicants pi = new PrimeImplicantsVanilla (
			_factory.one (), _factory.one (), support);

		support.orWith (_factory.ithVar (i));

		Iterator<Implicant> j = pi.iterator ();
		assertTrue ("There should be at least an empty implicant",
			j.hasNext ());
		Implicant imp = j.next ();

		assertTrue ("There should only be an empty implicant",
			imp.isEmpty ());
		assertTrue ("There should only be an empty implicant",
			!j.hasNext ());
		pi.free ();
	}

}



public void testFormulaWithASinglePrime() {

	PrimeImplicants pi = new PrimeImplicantsVanilla (_simpleFixture,
		_factory.one ());
	Iterator<Implicant> it = pi.iterator ();
	assertTrue ("There should be one implicant", it.hasNext ());
	Implicant imp = it.next ();
	int j = 1;
	for (int k : imp)
		assertEquals ("Should contain all negatives from -1", -(j++), k);
	assertEquals ("All negatives up to -13 included", j, 14);
	assertTrue ("There should only be one implicant", !it.hasNext ());
}



private PrimeImplicants testFormula(BDD formula, BDD theory) {

	PrimeImplicants[] pis = {
		new PrimeImplicantsVanilla (formula.id (), theory.id (),
			theory.support ().andWith (formula.support ())),

		new PrimeImplicantsOptimized (formula.id (), theory.id (),
			theory.support ().andWith (formula.support ())) };

	assertEquals (pis[0], (pis[1]));

	for (PrimeImplicants pi : pis)
		for (Implicant imp : pi) {

			assert !imp.isEmpty () || formula.isOne ();
			BDD bddImp = _factory.one ();
			for (int v : imp) {

				assertTrue ("Only negative implicants", v < 0);
				bddImp = bddImp.andWith (_factory.nithVar (-v));
			}

			BDD proposition = bddImp.impWith (theory.id ().impWith (
				formula.id ()));
			assertTrue ("Truly an implicant?", proposition.isOne ());
			bddImp.free ();

		}

	return pis[0];

}



public void testOnOneOrGroup() {

	PrimeImplicants pi = testFormula (
		_factory.nithVar (1).and (_oneOrGroup).exist (
			_factory.ithVar (1)), _factory.one ());

	int i = 0;
	for (Implicant imp : pi) {
		++i;
		assertEquals (3, imp.size ());
		assertTrue (imp.contains (-2));
		assertTrue (imp.contains (-3));
		assertTrue (imp.contains (-4));
	}
	assertEquals ("Should only be one implicant", 1, i);

}



public void testOptimizedPerformanceWithTuxOnIce() {

	BDD v = _tuxOnIce.support ();
	BDD u = v;

	while (!v.isOne ()) {

		new PrimeImplicantsOptimized (
			_factory.nithVar (v.var ()), _tuxOnIce,
			_tuxOnIce.support ());

		v = v.high ();
	}

	v = null;
	u.free ();
	u = null;
}



public void testVanillaPerformanceWithTuxOnIce() {

	BDD v = _tuxOnIce.support ();
	BDD u = v;

	while (!v.isOne ()) {

		new PrimeImplicantsVanilla (_factory.nithVar (v.var ()),
			_tuxOnIce, _tuxOnIce.support ());
		v = v.high ();
	}

	v = null;
	u.free ();
	u = null;
}



public void testOnSimpleFixtureNoCliques() {

	testFormula (_factory.nithVar (15), _simpleFixtureNoCliques);
}



public void testOnSimpleFixture() {

	testFormula (_simpleFixture, _factory.one ());

}

}
