package dk.itu.fds;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

public static Test suite() {

	TestSuite suite = new TestSuite ("Test for dk.itu.fds");
	//$JUnit-BEGIN$
	suite.addTestSuite (PrimeImplicantsTest.class);
	suite.addTestSuite (FeatureGraphTest.class);
	suite.addTestSuite (ValidDomainsTest.class);
	suite.addTestSuite (ImplicationGraphTest.class);
	//$JUnit-END$
	return suite;
}

}
