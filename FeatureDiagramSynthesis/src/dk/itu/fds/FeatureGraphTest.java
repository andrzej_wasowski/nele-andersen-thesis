package dk.itu.fds;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;
import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;

public class FeatureGraphTest extends TestCase {

private BDDBuilder _builder = new BDDBuilder ();
private BDDFactory _factory = _builder.factory ();
private BDD _simpleFixture = _builder.simpleFixture ();
private Graph _simpleGraphFixture = ImplicationGraph.build (_simpleFixture);

private BDD _simpleFixtureNoCliques = _builder.simpleFixtureNoCliques ();
private Graph _simpleGraphFixtureNoCliques = ImplicationGraph.build (_simpleFixtureNoCliques);

private BDD _oneOrGroup = _builder.oneOrGroup ();
private Graph _oneOrGroupGraph = ImplicationGraph.build (_oneOrGroup);

private BDD _fig1 = _builder.fig1 ();
private Graph _fig1Graph = ImplicationGraph.build (_fig1);

private BDD _fig1ea = _builder.fig1ea ();
private Graph _fig1eaGraph = ImplicationGraph.build (_fig1ea);

private BDD _powerVanilla;
private BDD _powerVanillaFinal;
private BDD _cpuFreq;



protected void setUp() throws Exception {

	super.setUp ();

	_powerVanilla = _factory.load ("tests/power-vanilla.bdd");
	_powerVanillaFinal = _factory.load ("tests/power-vanilla-final.bdd");
	_cpuFreq= _factory.load ("tests/cpufreq.bdd");

}



public void testCreateAndNodesOnSimpleFixture() {

	FeatureGraph FG = new FeatureGraph (_simpleGraphFixture, _simpleFixture);

	FG.createAndNodes ();
	FG.transitiveReduction ();
	assertEquals (16, FG.vertices ().size ());

	int[] oldvars = new int[] { 1, 2, 3, 12, 4, 6, 7, 8, 10 };
	int[] newvars = new int[] { 14, 15, 16 };

	BDD temp1 = _simpleFixture.exist (_factory.makeSet (oldvars));
	BDD temp2 = FG.formula ().exist (_factory.makeSet (newvars));
	assertTrue (temp1.biimpWith (temp2).isOne ());

}



public void testThree() {

	BDD test = _factory.ithVar (1).impWith (_factory.ithVar (2));
	test.andWith (_factory.ithVar (2).impWith (_factory.ithVar (3)));
	test.andWith (_factory.ithVar (3).impWith (_factory.ithVar (1)));
	test.andWith (_factory.ithVar (1).impWith (_factory.ithVar (3)));

	FeatureGraph featGraph = FeatureGraph.build (
		ImplicationGraph.build (test), test);

	System.out.println (featGraph);
}



public void testPowerVanilla() {

	FeatureGraph featGraph = FeatureGraph.build (
		ImplicationGraph.build (_powerVanilla), _powerVanilla);
	System.out.println (featGraph);
}



public void testPowerVanillaFinal() {

	BDD alive = ImplicationGraph.removeDeadFeatures (_powerVanillaFinal);
	FeatureGraph featGraph = FeatureGraph.build (
		ImplicationGraph.build (alive), alive);
	System.out.println (featGraph);
}

public void testCpuFreq() {

	BDD alive = ImplicationGraph.removeDeadFeatures (_cpuFreq);
	FeatureGraph featGraph = FeatureGraph.build (
		ImplicationGraph.build (alive), alive);
	System.out.println (featGraph);
}

public void testCreateOrNodesOnOneOrGroup() {

	FeatureGraph FG = new FeatureGraph (_oneOrGroupGraph, _oneOrGroup);

	assertEquals (4, FG.vertices ().size ());

	FG.createOrNodes ();

	assertEquals (5, FG.vertices ().size ());
	assertEquals (2, FG.edges ().size ());

	for (Map.Entry<Integer, Set<Integer>> e : FG.edges ().entrySet ()) {

		if (e.getKey () == 1) {
			assertEquals (4, e.getValue ().size ());
			assertTrue (e.getValue ().contains (5));
		} else {
			assertEquals (5, (int) e.getKey ());
			assertEquals (3, e.getValue ().size ());
			assertTrue (e.getValue ().contains (2));
			assertTrue (e.getValue ().contains (3));
			assertTrue (e.getValue ().contains (4));
		}
	}
}



public void testCreateOrNodesOnSimpleFixture() {

	FeatureGraph FG = new FeatureGraph (_simpleGraphFixture, _simpleFixture);
	assertEquals (13, FG.vertices ().size ());

	FG.createAndNodes ();
	assertEquals (16, FG.vertices ().size ());

	FG.transitiveReduction ();
	FG.createOrNodes ();

	assertEquals (17, FG.vertices ().size ());
	assertEquals (5, FG.edges ().size ());

	int n = 0;
	for (Set<Integer> s : FG.edges ().values ())
		n += s.size ();
	assertEquals (19, n);
}



public void testCreateOrNodesOnSimpleFixtureWithoutCliques() {

	FeatureGraph FG = new FeatureGraph (_simpleGraphFixtureNoCliques,
		_simpleFixtureNoCliques);
	FG.createAndNodes ();
	FG.transitiveReduction ();
	FG.createOrNodes ();
}



public void testCreateOrNodesOnFig1() {

	FeatureGraph FG = new FeatureGraph (_fig1Graph, _fig1);
	FG.createAndNodes ();
	FG.transitiveReduction ();
	FG.createOrNodes ();

	assertEquals (13, FG.vertices ().size ());
	assertEquals (3, FG.edges ().size ());

	int n = 0;
	for (Set<Integer> s : FG.edges ().values ())
		n += s.size ();
	assertEquals (16, n);
}



public void testCreateOrNodesOnFig1ea() {

	FeatureGraph FG = new FeatureGraph (_fig1eaGraph, _fig1ea);
	System.out.print (FG);
	FG.createAndNodes ();
	System.out.print (FG);
	FG.createOrNodes ();
	System.out.print (FG);
	FG.transitiveReduction ();
	System.out.print (FG);

	// assertEquals(13, FG.vertices().size());
	// assertEquals(3, FG.edges().size());

	// int n = 0;
	// for(Set<Integer> s : FG.edges().values())
	// n+=s.size();
	// assertEquals(12, n);
}



public void testMutexGroups() {

	BDD formula = ithVar (2).impWith (ithVar (1)).andWith (
		ithVar (3).impWith (ithVar (1)));
	formula.andWith (ithVar (4).impWith (ithVar (1)));
	formula.andWith (ithVar (5).impWith (ithVar (4)));
	formula.andWith (ithVar (1).impWith (ithVar (6)));
	// Mutex
	formula.andWith (ithVar (2).impWith (nithVar (3)));
	formula.andWith (ithVar (3).impWith (nithVar (4)));
	formula.andWith (ithVar (4).impWith (nithVar (2)));

	Graph ig = ImplicationGraph.build (formula);
	FeatureGraph fg = FeatureGraph.build (ig, formula);
	System.out.println (fg);

	// TODO assertions
}



public void testDeadFeatures() {

	BDD formula = ithVar (2).impWith (ithVar (1)).andWith (
		ithVar (3).impWith (ithVar (1)));
	formula.andWith (ithVar (2).impWith (ithVar (3)));
	formula.andWith (ithVar (3).impWith (nithVar (2)));

	BDD removed = ImplicationGraph.removeDeadFeatures (formula);
	formula.free ();
	Graph ig = ImplicationGraph.build (removed);
	FeatureGraph fg = FeatureGraph.build (ig, removed);

	HashMap<Integer, Set<Integer>> expectedEdges = new HashMap<Integer, Set<Integer>> ();
	expectedEdges.put (1, new HashSet<Integer> (Arrays.asList (3)));
	assertEquals (expectedEdges, fg.edges ());
	assertEquals (new HashSet<Integer> (Arrays.asList (1, 3)),
		fg.vertices ());
}



private BDD ithVar(int i) {

	return _factory.ithVar (i);
}



private BDD nithVar(int i) {

	return _factory.nithVar (i);
}



public void testCrash() {

	// System.out.print(FeatureGraph.build(_oneOrGroupGraph, _oneOrGroup));
	// System.out.print(FeatureGraph.build(_simpleGraphFixture,
	// _simpleFixture));
	// System.out.print(FeatureGraph.build(_fig1Graph, _fig1));
	// System.out.print(FeatureGraph.build(_fig1eaGraph, _fig1ea));

	try {
		_factory.save ("simpleFixture.bdd", _simpleFixture);
		_factory.save ("fig1ea.bdd", _fig1ea);
	} catch (IOException e) {
	}
}
}
