package dk.itu.fds;

import java.util.HashMap;
import java.util.Map;

public class NodeStyle extends HashMap<String,String> {


	private static final long serialVersionUID = 1L;

	public NodeStyle (String label) {
		
		super();
		put("label", "\"" + label + "\"");
	}
	
	public NodeStyle (NodeStyle that) {
		
		super ();
		putAll(that);
	}
	
	public String toString() {
		
		StringBuffer s = new StringBuffer();
		s.append ("[");
		for (Map.Entry<String, String> e : this.entrySet ())
			s.append (e.getKey () +"=" + e.getValue () + ",");
		s.append ("]");
			
		return s.toString();
	}
}
