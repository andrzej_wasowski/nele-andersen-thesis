package dk.itu.fds;

import java.util.ArrayList;

import net.sf.javabdd.BDD;

/**
 * An implicant is just a list of literals. The convention is that a positive
 * integer n represents a positive literal with variable having index n. A
 * negative integer n represents a negative literal with variable having index
 * -n.
 * 
 * @author wasowski
 * 
 */
public class Implicant extends ArrayList<Integer> {

/**
 * 
 */
private static final long serialVersionUID = 1L;



public boolean add(Integer v) {

	assert v != null;
	assert v != 0;
	return super.add (v);
}



public Implicant() {

	super ();
}



public String toString() {

	StringBuffer result = new StringBuffer ("{");

	for (Integer i : this) {

		result.append (i).append (".");
	}

	return result.append("}").toString ();
}



/**
 * This constructor from min term is not tested
 * 
 * @param u
 */
public Implicant(BDD u) {

	super ();

	u.printDot ();
	assert u.satCount () == 1;

	BDD v = u;
	while (!(v.isOne () || v.isZero ())) {

		assert v.var () % 2 == 0;

		if (v.high ().isZero ()) {

			assert v.low ().high ().isZero ();
			assert !v.low ().isOne ();
			v = v.low ().low ();

		} else {

			assert v.low ().isZero ();
			assert v.high ().high ().isZero (); // negative
			// literals
			// only;

			this.add (-v.var () / 2);
			v = v.high ().low ();
		}
	}
}


/**
 * TODO: explain the encoding of implicants in the BDD
 * @param u metaproduct encoding of the implicant
 * @param support the support of the PRIMES bdd
 */
public Implicant(byte[] u, int[] support) {

	super ();
	
	assert u != null;
	
	if (support == null)
		return;

	for (int i = 0; i < support.length; ++i) {
		
		if (u[2 * support[i]] == 0)
			continue;

		assert u[2*support[i] + 1] == 0 : "expected negative literals only";
		
		this.add (-support[i]);
	}
}


public Implicant removeNegations() {
	
	Implicant result = new Implicant();
	
	for (Integer i : this)
		result.add(-i);
	return result;
}
}
