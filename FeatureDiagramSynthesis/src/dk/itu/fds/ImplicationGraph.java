package dk.itu.fds;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;

public class ImplicationGraph {

private ImplicationGraph() {

}

static public Graph build(BDD problem) {

	BDD sup = problem.support ();
	int[] isup = sup.scanSet ();
	sup.free ();

	return build (problem, isup == null ? new int[] {} : isup);
}

static public Graph build(BDD problem, int min_var, int max_var) {

	int size = Math.max(max_var-Math.max (min_var,0)+1,0);
	int[] support = new int[size];
	for (int i = 0; i < size; ++i, ++min_var)
		support[i] = min_var;
	return build (problem, support);
}

static public Graph build(BDD problem, int[] support) {

	int max_var = Util.max(support);
	int min_var = Util.min(support);

	HashSet<Integer> set_support = new HashSet<Integer>();

	for (int i : support)
		set_support.add (i);

	Graph graph = new Graph (support);

	BDDFactory B = problem.getFactory ();

	for (int i : support) {

		BDD temp = problem.id ().andWith (B.nithVar (i));


		Set<Integer> falsified;
		if (temp.isZero()) { // if B is true in all assignments then it is implied by all
			falsified = new HashSet<Integer>();
			falsified.addAll(set_support);
		} else falsified = findFalsified (temp, min_var, max_var);
		temp.free ();
		falsified.retainAll (set_support);
		falsified.remove(i);
		graph.connect (i, falsified);
	}

	return graph;
}


/**
 * Find all variables in the problem that are 0 in all satisfiable assignments
 * @param problem constraint representing the problem
 * @param min_var only consider variables with identifiers min_var or larger
 * @param max_var only consider variables with
 * @return
 */
static public Set<Integer> findFalsified(BDD problem, int min_var, int max_var) {

	Set<Integer> value = new HashSet<Integer> ();
	ValidDomains vd = new ValidDomains (problem, min_var, max_var);
	for (int var = min_var; var <= max_var; ++var)
		if (vd.canBeZero (var) && !vd.canBeOne (var))
			value.add (var);
	return value;
}

/**
 * Collapses equivalent (coincident) variables into one in the BDD.
 *
 * @param problem
 * @param sup
 */
public static BDD removeDeadFeatures(BDD problem) {
	if (problem.isOne() || problem.isZero())
		return problem.id();

	BDD sup = problem.support ();
	int[] support = sup.scanSet();
	sup.free();

	ArrayList<Integer> dead = new ArrayList<Integer>();

	for (int v : support) {
		BDD check =	problem.id().restrictWith(problem.getFactory().ithVar(v));
		if (check.isZero()) {
			dead.add(v);
		}
		check.free();
	}

	int[] deadArr = new int[dead.size()];
	for (int i=0; i<dead.size(); i++) {
		deadArr[i] = dead.get(i);
	}
//	int[] deadArr = Util.asIntArray (dead.toArray(new Integer[0]));

	Logger.getAnonymousLogger().info("Dead Features:" + dead);
	BDD exist = problem.getFactory().makeSet(deadArr);
	BDD result = problem.exist(exist);
	exist.free();

	return result;
}
}
