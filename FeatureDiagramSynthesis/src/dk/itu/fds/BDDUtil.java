package dk.itu.fds;

import java.util.HashSet;
import java.util.Set;

import net.sf.javabdd.BDD;

public class BDDUtil {

public static BDD project(BDD u, int[] domain) {

	assert u != null;
	assert domain != null;
	assert u.support () != null;
	assert u.support ().scanSet () != null || u.isOne () || u.isZero ();

	if (u.isOne () || u.isZero ())
		return u.id();

	int[] supp = u.support ().scanSet ();
	Set<Integer> S = new HashSet<Integer> (supp.length);

	for (int i : supp)
		S.add (i);
	for (int i : domain)
		S.remove (i);

	BDD removedVars = u.getFactory ().makeSet (
		Util.asIntArray (S.toArray (new Integer[0])));
	return u.exist (removedVars);
}
}
