package dk.itu.fds;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import net.sf.javabdd.BDD;

public class PrimeImplicantsOptimized extends PrimeImplicants implements
	Iterable<Implicant> {

public PrimeImplicantsOptimized(BDD formula, BDD theory) {

	super (formula, theory);
}



public PrimeImplicantsOptimized(BDD formula) {

	super (formula);
}



public PrimeImplicantsOptimized(BDD formula, BDD theory, BDD support) {

	super (formula, theory, support);

}



/**
 * FIXME: !!! this algorithm may be potentially exponential. One should use
 * caching to avoid computing the same primes all over again. We will not see it
 * when reconstructing the feature models probably, but one can give formulae
 * with extremely many primes.
 * 
 * Added 2009-03-14: A NOTE FOR WHOMEVER WOULD CONSIDER OPTIMIZING THIS CODE
 * 
 * We seem to have such an example in the tuxOnInce test case. I experimented
 * with caching but only about half calls exhibit cache hits (the rest is
 * misses). This is not ideal, as then the cache really has to be very large
 * (half of all exponentially many primes) and using and consulting it makes the
 * application slower! No dynamic programming story here: there is genuinly
 * exponentially many new bdds created, with very little dependency on the
 * existing ones. Coudert and Madre write about this in their paper actually.
 * They mention another method which avoids the problem although (for them) is
 * slower in practice. This should be tried probably - but I could not get hold
 * of the technical report describing that method.
 * 
 * Then one should also try developments by Antoine Rauzy and others. Papers
 * hint at use of Zero-suppressed BDDs (ZBDDs) instead of metaproducts, which
 * supposedly are a better, more natural representation for primes. However
 * JavaBDD does not support ZBDDs. I believe CUDD and JDD do support them. We
 * would need to port to these packages, or, extend JAvaBDD to support ZBDDs.
 * 
 * Finally our SPLC07 paper cites a SAT based method for finding prime
 * implicants. It could make sense to try it as well (or look at papers that
 * follow up).
 * 
 * @param f
 * @param last_var
 * @return
 */
protected BDD negativePrimesWith(BDD f, int last_var) {

	if (f.isOne ())
		return negateFromTo (last_var + 1, max_var + 1);

	if (f.isZero ())
		return f;

	int v = f.var ();

	BDD negs = negateFromTo (last_var + 1, v);

	BDD np_fnvfvv = negativePrimesWith (f.low ().and (f.high ()), v);
	BDD np_fnvv = negativePrimesWith (f.low ().id (), v);

	f.free ();

	assert (v * 2 + 1) < B.varNum ();

	BDD tmp1 = B.nithVar (v * 2).andWith (negs.id ());
	
	BDD tmp2 = B.ithVar (v * 2).andWith (B.nithVar (v * 2 + 1)).andWith (
		negs);
	BDD tmp3 = np_fnvfvv.not ();
	return np_fnvfvv.id ().andWith (tmp1).orWith (
		np_fnvv.andWith (tmp3).andWith (tmp2));

}
}
