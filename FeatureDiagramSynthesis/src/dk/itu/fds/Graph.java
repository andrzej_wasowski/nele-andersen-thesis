package dk.itu.fds;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class Graph {

private Map<Integer, Set<Integer>> _edges = new HashMap<Integer, Set<Integer>> ();
private Set<Integer> _vertices = new HashSet<Integer> ();
private Map<Integer, NodeStyle> _vstyles = new HashMap<Integer, NodeStyle> ();
int max_vertex = -1;



public Graph() {

}



/**
 * A copy constructor (makes a shallow copy of G).
 * 
 * @param G
 */
public Graph(Graph G) {

	assert G != null;

	for (Map.Entry<Integer, Set<Integer>> e : G._edges.entrySet ())
		_edges.put (e.getKey (), new HashSet<Integer> (e.getValue ()));

	_vertices.addAll (G.vertices ());

	for (Map.Entry<Integer, NodeStyle> e : G._vstyles.entrySet ())
		_vstyles.put (e.getKey (), new NodeStyle (e.getValue ()));

	max_vertex = G.max_vertex;
}



public Graph(int[] vertices) {

	_edges = new HashMap<Integer, Set<Integer>> ();
	this._vertices = new HashSet<Integer> ();
	this._vstyles = new HashMap<Integer, NodeStyle> ();
	for (Integer i : vertices) {
		this._vertices.add (i);
		this._vstyles.put (i, new NodeStyle (i.toString ()));
	}
	max_vertex = Util.max (vertices);
}



public int newVertex() {

	_vertices.add (++max_vertex);
	return max_vertex;
}



public boolean connected(int source, int target) {

	assert _vertices.contains (source);
	assert _vertices.contains (target);
	if (_edges.containsKey (source)) {

		assert _edges.get (source) != null;
		return _edges.get (source).contains (target);

	} else
		return false;
}



public void connect(Integer source, Integer target) {

	assert source != target;
	assert _vertices.contains (source);
	assert _vertices.contains (target);

	Set<Integer> neighbours;
	if (_edges.containsKey (source)) {
		assert _edges.get (source) != null;
		neighbours = _edges.get (source);
	} else
		neighbours = new HashSet<Integer> ();

	neighbours.add (target);
	_edges.put (source, neighbours);
}



public void connect(Integer source, Collection<Integer> targets) {

	assert (!targets.contains (source));
	if (targets.isEmpty ())
		return;

	assert _vertices.contains (source);
	assert _vertices.containsAll (targets);

	Set<Integer> neighbours;

	if (_edges.containsKey (source)) {

		assert _edges.get (source) != null;
		neighbours = _edges.get (source);
	} else
		neighbours = new HashSet<Integer> ();

	neighbours.addAll (targets);
	neighbours.remove (source);
	_edges.put (source, neighbours);
}



public String toString() {

	StringBuffer buf = new StringBuffer ();

	buf.append ("digraph {\n");

	for (int v : _vertices)
		buf.append (v + _vstyles.get (v).toString () + ";\n");

	for (Map.Entry<Integer, Set<Integer>> me : _edges.entrySet ()) {
		for (int i : me.getValue ()) {
			buf.append (me.getKey ());
			buf.append ("->");
			buf.append (i);
			buf.append ("; ");
		}
		buf.append ("\n");
	}
	buf.append ("}\n");
	return buf.toString ();
}



// only used before visualization. So you can assume loops everywhere in the
// implication graph
void removeLoops() {

	for (int k : _edges.keySet ()) {
		_edges.get (k).remove (k);
	}
}



protected Set<Integer> sources() {

	Set<Integer> result = new HashSet<Integer> (_vertices);

	for (Set<Integer> adj : _edges.values ())
		result.removeAll (adj);
	return result;
}



protected boolean isSink(int v) {

	assert !(_edges.containsKey (v) && _edges.get (v).isEmpty ());
	return !_edges.containsKey (v);
}



Set<Integer> sinks() {

	Set<Integer> result = new HashSet<Integer> ();

	for (Map.Entry<Integer, Set<Integer>> e : _edges.entrySet ())
		if (e.getValue ().isEmpty ())
			result.add (e.getKey ());
	return result;
}



/**
 * Finds a clique: a simple algorithm that only works in a directed graph with
 * transitive adjacency relation. Returns null if no clique is found.
 * 
 * Only nontrivial (non singleton) cliques are returned.
 */
public List<Set<Integer>> findAllDirectedCliques() {

	List<Set<Integer>> result = new LinkedList<Set<Integer>> ();
	Set<Integer> seeds = new HashSet<Integer> ();
	seeds.addAll (_vertices);

	while (!seeds.isEmpty ()) {

		Set<Integer> clique = growClique (seeds.iterator ().next ());

		if (clique.size () > 1)
			result.add (clique);
		seeds.removeAll (clique);
	}

	return result;

}



/**
 * An algorithm for finding cliques in TRANSITIVE DAGS by running BFS on
 * bidirectional edges.
 * 
 * @param seed
 * @return
 */
private Set<Integer> growClique(int seed) {

	Set<Integer> clique = new HashSet<Integer> ();
	LinkedList<Integer> fifo = new LinkedList<Integer> ();
	fifo.add (seed);

	// inefficient. Could be improved.
	while (!fifo.isEmpty ()) {

		int v = fifo.removeFirst ();
		clique.add (v);
		Set<Integer> vs = biNeighbours (v);
		vs.removeAll (clique);
		vs.removeAll (fifo);
		fifo.addAll (vs);
	}

	return clique;
}



/**
 * Returns a set of neighbours of v that are connected with v in both
 * directions.
 * 
 * @param v
 * @return
 */
public Set<Integer> biNeighbours(int v) {

	assert _vertices.contains (v);

	Set<Integer> result = new HashSet<Integer> ();
	if (_edges.containsKey (v))
		for (int u : _edges.get (v))
			if (connected (u, v))
				result.add (u);

	return result;
}



// FIXME: a version with accumulator would be more efficient.
public Set<Integer> descendants(int v) {

	HashSet<Integer> result = new HashSet<Integer> ();

	if (!_edges.containsKey (v))
		return result;

	for (int u : _edges.get (v)) {
		result.add (u);
		result.addAll (descendants (u));
	}

	return result;
}



public Set<Integer> descendants(Set<Integer> V) {

	HashSet<Integer> result = new HashSet<Integer> ();

	for (Integer v : V)
		result.addAll (descendants (v));

	return result;
}



public Set<Integer> children(int v) {

	if (_edges.containsKey (v))
		return _edges.get (v);

	return new HashSet<Integer> ();
}



public int[] childrenArray(int v) {

	return Util.asIntArray ((Integer[]) children (v).toArray ());
}



public Map<Integer, Set<Integer>> edges() {

	return _edges;
}



public Set<Integer> vertices() {

	return _vertices;
}



protected Map<Integer, NodeStyle> vstyles() {

	return _vstyles;
}



public void appendLabel(int vertex, String label) {

	String l = "" + vertex;

	NodeStyle styles;

	if (_vstyles.containsKey (vertex)) {

		styles = _vstyles.get (vertex);

		if (styles.containsKey ("label"))

			l = styles.get ("label");

		styles.put ("label", l + label);
	} else
		styles = new NodeStyle (l + label);

	_vstyles.put (vertex, styles);
}



/**
 * DAGS only. quick&dirty = slow EMBARRASSING!
 * TODO optimize by using a topological sort
 * 
 */
public void transitiveReduction() {

	for (Integer v : _vertices) {

		_transitiveReduction (v);
	}
}



private void _transitiveReduction(int v) {

	if (_edges.containsKey (v)) {

		Set<Integer> U = _edges.get (v);
		Set<Integer> needless = descendants (v);
		needless.retainAll (descendants (children (v)));
		U.removeAll (needless);
		if (U.isEmpty ())
			_edges.remove (v);
	}

	return;
}



protected int[] verticesArray() {

	int[] vz = new int[vertices ().size ()];
	int i = 0;
	for (int v : vertices ()) {
		vz[i++] = v;
	}
	return vz;
}



public Graph reverseEdges() {

	Graph G = new Graph (verticesArray ());
	for (Entry<Integer, Set<Integer>> e : _edges.entrySet ()) {
		int v = e.getKey ();
		for (int u : e.getValue ()) {
			G.connect (u, v);
		}
	}
	return G;
}

}
