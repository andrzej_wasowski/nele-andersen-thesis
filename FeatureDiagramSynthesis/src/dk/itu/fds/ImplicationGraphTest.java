package dk.itu.fds;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import junit.framework.TestCase;

public class ImplicationGraphTest extends TestCase {

private BDDBuilder _builder = new BDDBuilder ();
private BDDFactory _factory = _builder.factory ();
private BDD _simpleFixture = _builder.simpleFixture ();
private BDD _simpleFixtureNoCliques = _builder.simpleFixtureNoCliques ();
private BDD _fig1 = _builder.fig1 ();
private BDD _oneOrGroup = _builder.oneOrGroup ();



protected void setUp() throws Exception {

	super.setUp ();
}



public void testOnSimpleFixture() {

	Graph G = ImplicationGraph.build (_simpleFixture);
	
	assertEquals (13, G.vertices().size());
	assertEquals (10, G.edges().size ());
}

public void testOnOneOrGroup() {
	
	Graph G = ImplicationGraph.build (_oneOrGroup);
	
	assertEquals (4, G.vertices().size());
	assertEquals (1, G.edges().size());
	assertTrue   (G.edges ().get (1).contains (2));
	assertTrue   (G.edges ().get (1).contains (3));
	assertTrue   (G.edges ().get (1).contains (4));
}


public void testForCrashes() {

	ImplicationGraph.build (_fig1);
	ImplicationGraph.build (_simpleFixtureNoCliques);
}


public void testOnEmpty() {

	Graph G = ImplicationGraph.build (_factory.one (), -1, -1);

	assertTrue ("Empty graph should have no vertices",
		G.vertices().isEmpty ());
	assertTrue ("Empty graph should have no edges", G.edges().isEmpty ());
}



public void testTautologiesOfSize1To10() {

	for (int i = 0; i <= 9; ++i) {

		Graph G = ImplicationGraph.build (_factory.one (), 0, i);

		assertTrue ("size " + i, G.vertices().size () == i + 1);
		assertTrue ("There should be no egdes, not even loops.",
			G.edges().isEmpty ());
	}
}
}
