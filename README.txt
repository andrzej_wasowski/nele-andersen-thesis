This CD contains four Java projects:

FeatureModel, FeatureModelOperations, FeatureDiagramSynthesis, and Thesis.

* FeatureModel - This project contains a framework for creating feature models and
  generalized feature model. (due to Steven She)

* FeatureModelOperations - This project contains routines for constructing and 
  manipulating BDDs. The class FGBuilder implements the BDD based optimized version of 
  the FGE algorithm compromising completeness. (due to Steven She)

* FeatureDiagramSynthesis - This project contains an earlier implements of the FGE algorithm
  parts of the implementation is reused in the FeatureModelOperations project, and is thus 
  referenced by this project. (due to Andrzej Waskowski)

* Thesis - This project contains the code produced for this Thesis project. It is references 
  all of the above projects. Below is a list explaining where to find the different implementations
	of the FGE algorithm:  

	fds.FGBuilder - An implementation of the complete FGE algorithm based on SAT solving 
	
	fds.FGBuilderBDDBasedExtention - An implementation of the complete FGE algorithm based on BDDs (a modification of the FGBuilder in the FeatureModelOperations project)
	
	fds.FGBuilderOptimized - An implementation of the optimized FGE algorithm compromising completeness as described in Chapter 8
	
	extensions.AlternativeFGBuilder - An implementation of the alternative or-group computation described in Chapter 10
